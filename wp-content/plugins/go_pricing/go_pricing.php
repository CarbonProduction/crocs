<?php
/**
 * Plugin Name: Go Pricing - WordPress Responsive Pricing Tables
 * Plugin URI:  http://www.go-pricing.com
 * Description: The New Generation Pricing Tables. If you like traditional Pricing Tables, but you would like get much more out of it, then this rodded product is a useful tool for you.
 * Version:     3.3.5.p
 * Author:      Granth
 * Author URI:  http://granthweb.com
 * Text Domain: go_pricing_textdomain
 * Domain Path: /lang
 */

// Prevent direct call
if ( !defined( 'WPINC' ) ) die;

// Prevent redeclaring class
if ( class_exists( 'GW_GoPricing' ) ) wp_die ( __( 'GW_GoPricing class has already been declared!', 'go_pricing_textdomain' ) );	

// Include & init main class
include_once( plugin_dir_path( __FILE__ ) . 'includes/class_go_pricing.php' );
GW_GoPricing::instance( __FILE__ );

// Register activation / deactivation / uninstall hooks
register_activation_hook( __FILE__, array( 'GW_GoPricing', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'GW_GoPricing', 'deactivate' ) );
register_uninstall_hook( __FILE__, array( 'GW_GoPricing', 'uninstall' ) );

/**
 * Check for update. Pixedelic based
 */
add_filter( 'pre_set_site_transient_update_plugins', 'pixedelic_go_pricing_check_for_plugin_update');
function pixedelic_go_pricing_check_for_plugin_update($checked_data) {
	global $wp_version;

	$plugin_name = plugin_basename( __FILE__ );

	$api_url = 'http://www.pixedelic.com/api/api.php';

	if (empty($checked_data->checked))
		return $checked_data;

	$args = array(
		'dir' => 'go_pricing',
		'slug' => 'go_pricing',
		'version' => $checked_data->checked[ $plugin_name ],
		'id' => apply_filters('pixedelic_itemID',''),
		'user' => apply_filters('pixedelic_username',''),
		'license' => apply_filters('pixedelic_licensekey','')
	);

	$request_string = array(
			'body' => array(
				'action' => 'basic_check', 
				'request' => serialize($args),
				'api-key' => md5(get_bloginfo('url'))
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
		);
	
	$raw_response = wp_remote_post($api_url, $request_string);

	if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
		$response = unserialize($raw_response['body']);

	if (is_object($response) && !empty($response)) // Feed the update data into WP updater
		$checked_data->response[ $plugin_name ] = $response;
	
	return $checked_data;
}

/**
 * Call for update. Pixedelic based
 */
add_filter( 'plugins_api', 'pixedelic_go_pricing_plugin_api_call', 10, 3);
function pixedelic_go_pricing_plugin_api_call($def, $action, $args) {
	global $wp_version;
	
	$plugin_slug = 'go_pricing';
	$api_url = 'http://www.pixedelic.com/api/api.php';
	
	if (!isset($args->slug) || ($args->slug != $plugin_slug))
		return false;
		
	$request_string = array(
			'body' => array(
				'action' => $action, 
				'request' => serialize($args),
				'api-key' => md5(get_bloginfo('url'))
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
		);
	
	$request = wp_remote_post($api_url, $request_string);

	if (is_wp_error($request)) {
		$res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
	} else {
		$res = unserialize($request['body']);

		if ($res === false)
			$res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
	}
	
	return $res;
}
?>