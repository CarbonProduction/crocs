<?php

add_action( 'init', 'pixedelic_create_member_post_type' );
function pixedelic_create_member_post_type() {
	$labels = array(
		'name'               => esc_html__( 'Team members', 'logan' ),
		'singular_name'      => esc_html__( 'Member', 'logan' ),
		'menu_name'          => esc_html__( 'Team members', 'logan' ),
		'name_admin_bar'     => esc_html__( 'Member', 'logan' ),
		'add_new'            => esc_html__( 'Add New', 'logan' ),
		'add_new_item'       => esc_html__( 'Add New Member', 'logan' ),
		'new_item'           => esc_html__( 'New Member', 'logan' ),
		'edit_item'          => esc_html__( 'Edit Member', 'logan' ),
		'view_item'          => esc_html__( 'View Member', 'logan' ),
		'all_items'          => esc_html__( 'All Members', 'logan' ),
		'search_items'       => esc_html__( 'Search Members', 'logan' ),
		'parent_item_colon'  => esc_html__( 'Parent Members:', 'logan' ),
		'not_found'          => esc_html__( 'No members found.', 'logan' ),
		'not_found_in_trash' => esc_html__( 'No members found in Trash.', 'logan' )
	);

	$args = array(
		'labels'             => $labels,
        //'description'        => __( 'Description.', 'logan' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'    	     => array(
			'slug' => 'member',
		),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'          => 'dashicons-groups',
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'member', $args );
}


//add_action( 'init', 'pixedelic_create_work_post_type' );
function pixedelic_create_work_post_type() {
	$labels = array(
		'name'               => esc_html__( 'Works', 'logan' ),
		'singular_name'      => esc_html__( 'Work', 'logan' ),
		'menu_name'          => esc_html__( 'Works', 'logan' ),
		'name_admin_bar'     => esc_html__( 'Work', 'logan' ),
		'add_new'            => esc_html__( 'Add New', 'logan' ),
		'add_new_item'       => esc_html__( 'Add New Work', 'logan' ),
		'new_item'           => esc_html__( 'New Work', 'logan' ),
		'edit_item'          => esc_html__( 'Edit Work', 'logan' ),
		'view_item'          => esc_html__( 'View Work', 'logan' ),
		'all_items'          => esc_html__( 'All Works', 'logan' ),
		'search_items'       => esc_html__( 'Search Works', 'logan' ),
		'parent_item_colon'  => esc_html__( 'Parent Works:', 'logan' ),
		'not_found'          => esc_html__( 'No works found.', 'logan' ),
		'not_found_in_trash' => esc_html__( 'No works found in Trash.', 'logan' )
	);

	$args = array(
		'labels'             => $labels,
        //'description'        => __( 'Description.', 'logan' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'    	     => array(
			'slug' => 'work',
		),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'          => 'dashicons-portfolio',
	    'supports' => array(
	      'title',
	      'thumbnail',
	      'editor',
	      'excerpt',
	      'trackbacks',
	      'custom-fields',
	      'comments',
	      'revisions',
	      'post-formats')
	);

	register_post_type( 'pix-work', $args );

	//*******************************************
	//
	//	Add new taxonomy, make it hierarchical (like categories)
	//
	//*******************************************
	 
	$labels_pr = array(
		'name'              => esc_html__( 'Projects', 'logan' ),
		'singular_name'     => esc_html__( 'Project', 'logan' ),
		'search_items'      => esc_html__( 'Search Projects', 'logan' ),
		'all_items'         => esc_html__( 'All Projects', 'logan' ),
		'parent_item'       => esc_html__( 'Parent Project', 'logan' ),
		'parent_item_colon' => esc_html__( 'Parent Project:', 'logan' ),
		'edit_item'         => esc_html__( 'Edit Project', 'logan' ),
		'update_item'       => esc_html__( 'Update Project', 'logan' ),
		'add_new_item'      => esc_html__( 'Add New Project', 'logan' ),
		'new_item_name'     => esc_html__( 'New Project Name', 'logan' ),
		'menu_name'         => esc_html__( 'Project', 'logan' ),
	);

	$args_pr = array(
		'hierarchical'      => true,
		'labels'            => $labels_pr,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'project' ),
	);

	register_taxonomy( 'pix-projects', array( 'pix-work' ), $args_pr );

	$labels_tg = array(
		'name'                       => esc_html__( 'Work tags', 'logan' ),
		'singular_name'              => esc_html__( 'Work tag', 'taxonomy singular name', 'logan' ),
		'search_items'               => esc_html__( 'Search tags', 'logan' ),
		'popular_items'              => esc_html__( 'Popular tags', 'logan' ),
		'all_items'                  => esc_html__( 'All tags', 'logan' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => esc_html__( 'Edit tag', 'logan' ),
		'update_item'                => esc_html__( 'Update tag', 'logan' ),
		'add_new_item'               => esc_html__( 'Add New tag', 'logan' ),
		'new_item_name'              => esc_html__( 'New Tag Name', 'logan' ),
		'separate_items_with_commas' => esc_html__( 'Separate tags with commas', 'logan' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove tags', 'logan' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used tags', 'logan' ),
		'not_found'                  => esc_html__( 'No tags found.', 'logan' ),
		'menu_name'                  => esc_html__( 'Work tags', 'logan' ),
	);

	$args_tg = array(
		'hierarchical'          => false,
		'labels'                => $labels_tg,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'work-tag' ),
	);

	register_taxonomy( 'pix-tags', array( 'pix-work' ), $args_tg );
}
