<?php

if ( ! class_exists( 'WP_Customize_Setting' ) ) {
	$class_wp_customizer_Setting = ABSPATH . 'wp-includes/class-wp-customize-setting.php';
	if ( file_exists( $class_wp_customizer_Setting ) )
		require $class_wp_customizer_Setting;
}

final class Pix_Customizer_Option extends WP_Customize_Setting {
	
	public function import( $value ) 
	{
		$this->update( $value );	
	}
}

final class Pix_Customizer_Importer {

	static public function _import( $file ) 
	{
		// Make sure we have a valid nonce.
		/*if ( ! wp_verify_nonce( $_REQUEST['pix-cei-import'], 'pix-cei-importing' ) ) {
			return;
		}*/
		
		// Make sure WordPress upload support is loaded.
		if ( ! function_exists( 'wp_handle_upload' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
				
		// Setup global vars.
		global $wp_customize;
		
		// Setup internal vars.
		$template	 = get_template();
		$overrides   = array( 'test_form' => FALSE, 'mimes' => array('dat' => 'text/dat') );
		$file        = wp_handle_sideload( $file, $overrides );

		/*if ( ! function_exists('media_sideload_image') ) {
			$media_sideload_image = ABSPATH . 'wp-admin/includes/media.php';
			if ( file_exists( $media_sideload_image ) )
				require $media_sideload_image;
		}*/

		// Make sure we have an uploaded file.
		if ( isset( $file['error'] ) ) {
			esc_html_e($file['error']);
			return;
		}
		if ( ! file_exists( $file['file'] ) ) {
			esc_html_e( 'Error importing settings! Please try again.', 'logan' );
			return;
		}
		
		// Get the upload data.
		$raw  = file_get_contents( $file['file'] );
		$data = @unserialize( $raw );
		
		// Remove the uploaded file.
		//unlink( $file['file'] );
		
		// Data checks.
		if ( 'array' != gettype( $data ) ) {
			esc_html_e( 'Error importing settings! Please check that you uploaded a customizer export file.', 'logan' );
			return;
		}
		if ( ! isset( $data['template'] ) || ! isset( $data['mods'] ) ) {
			esc_html_e( 'Error importing settings! Please check that you uploaded a customizer export file.', 'logan' );
			return;
		}
		if ( $data['template'] != $template ) {
			esc_html_e( 'Error importing settings! The settings you uploaded are not for the current theme.', 'logan' );
			return;
		}
		
		// Import images.
		//if ( isset( $_REQUEST['pix-cei-import-images'] ) ) {
			$data['mods'] = self::_import_images( $data['mods'] );
		//}
		
		// Import custom options.
		if ( isset( $data['options'] ) ) {
			
			foreach ( $data['options'] as $option_key => $option_value ) {
				
				$option = new Pix_Customizer_Option( $wp_customize, $option_key, array(
					'default'		=> '',
					'type'			=> 'option',
					'capability'	=> 'edit_theme_options'
				) );
				
				$option->import( $option_value );
			}
		}
		
		// Call the customize_save action.
		do_action( 'customize_save', $wp_customize );
		
		// Loop through the mods.
		foreach ( $data['mods'] as $key => $val ) {
			
			// Call the customize_save_ dynamic action.
			do_action( 'customize_save_' . $key, $wp_customize );
			
			// Save the mod.
			set_theme_mod( $key, $val );
		}
		
		// Call the customize_save_after action.
		do_action( 'customize_save_after', $wp_customize );

		esc_html_e( 'Success' );
	}
	
	/**
	 * Check if attachment already exists.
	 *
	 * @author Pixedelic.
	 */
	static private function _check_attachment( $post_name ) 
	{
        $args = array(
            'post_per_page' => 1,
            'post_type'     => 'attachment',
            'name'          => trim ( $post_name ),
        );
        $get_posts = new Wp_Query( $args );

        if ( $get_posts && isset($get_posts->posts[0]) )
            return $get_posts->posts[0];
        else
          return false;
	}

	/**
	 * Imports images for settings saved as mods.
	 *
	 * @since 0.1
	 * @access private
	 * @param array $mods An array of customizer mods.
	 * @return array The mods array with any new import data.
	 */
	static private function _import_images( $mods ) 
	{
		foreach ( $mods as $key => $val ) {

			if ( self::_is_image_url( $val ) ) {
				
				$info = pathinfo($val);
				$file_name =  basename($val,'.'.$info['extension']);
				$file_exists = self::_check_attachment( $file_name );
				
				if ( $file_exists ) {

					$id = $file_exists->ID;
					$mods[ $key ] = $file_exists->guid;

					$data = new stdClass();
					$meta 					= wp_get_attachment_metadata( $id );
					$data->attachment_id	= $id;
					$data->url				= wp_get_attachment_url( $id );
					$data->thumbnail_url	= wp_get_attachment_thumb_url( $id );
					$data->height			= $meta['height'];
					$data->width			= $meta['width'];

					if ( isset( $mods[ $key . '_data' ] ) ) {

						$mods[ $key . '_data' ] = $data;
						update_post_meta( $data->attachment_id, '_wp_attachment_is_custom_header', get_stylesheet() );
					}

				} else {

					$data = self::_sideload_image( $val );
					
					if ( ! is_wp_error( $data ) ) {
						
						$mods[ $key ] = $data->url;
						
						if ( isset( $mods[ $key . '_data' ] ) ) {
							$mods[ $key . '_data' ] = $data;
							update_post_meta( $data->attachment_id, '_wp_attachment_is_custom_header', get_stylesheet() );
						}
					}
				}
			}
		}
		
		return $mods;
	}
	
	/**
	 * Taken from the core media_sideload_image function and
	 * modified to return an array of data instead of html.
	 *
	 * @since 0.1
	 * @access private
	 * @param string $file The image file path.
	 * @return array An array of image data.
	 */
	static private function _sideload_image( $file ) 
	{
		$data = new stdClass();
		
		if ( ! function_exists( 'media_handle_sideload' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
		}
		if ( ! empty( $file ) ) {
			
			// Set variables for storage, fix file filename for query strings.
			preg_match( '/[^\?]+\.(jpe?g|jpe|gif|png|svg)\b/i', $file, $matches );
			$file_array = array();
			$file_array['name'] = basename( $matches[0] );
	
			// Download file to temp location.
			$file_array['tmp_name'] = download_url( $file );
	
			// If error storing temporarily, return the error.
			if ( is_wp_error( $file_array['tmp_name'] ) ) {
				return $file_array['tmp_name'];
			}
	
			// Do the validation and storage stuff.
			$id = media_handle_sideload( $file_array, 0 );
	
			// If error storing permanently, unlink.
			if ( is_wp_error( $id ) ) {
				@unlink( $file_array['tmp_name'] );
				return $id;
			}
			
			// Build the object to return.
			$meta					= wp_get_attachment_metadata( $id );
			$data->attachment_id	= $id;
			$data->url				= wp_get_attachment_url( $id );
			$data->thumbnail_url	= wp_get_attachment_thumb_url( $id );
			$data->height			= $meta['height'];
			$data->width			= $meta['width'];
		}
	
		return $data;
	}
	
	/**
	 * Checks to see whether a string is an image url or not.
	 *
	 * @since 0.1
	 * @access private
	 * @param string $string The string to check.
	 * @return bool Whether the string is an image url or not.
	 */
	static private function _is_image_url( $string = '' ) 
	{
		if ( is_string( $string ) ) {
			
			if ( preg_match( '/\.(jpg|jpeg|png|gif)/i', $string ) ) {
				return true;
			}
		}
		
		return false;
	}
}
