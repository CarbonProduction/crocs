<?php

class LoganAddons{

	/**
	 * @since   0.1.1
	 *
	 * @var     string
	 */
	protected $version = '0.1.1';

	/**
	 * @since   0.0.1
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'loganaddons';

	/**
	 * @since   0.0.1
	 *
	 * @var      string
	 */
	protected $plugin_name = 'LoganAddons';

	/**
	 * @since   0.0.1
	 *
	 * @var      object
	 */
	protected static $instance = null;

	public function __construct() {
		add_action( 'init', array( &$this, 'load_plugin_textdomain' ) );

		add_action( 'customize_controls_init', array( &$this, 'js_vars' ) );
		add_action( 'admin_head', array( &$this, 'js_vars' ) );
		add_action( 'admin_footer', array( &$this, 'font_box' ) );

		add_action( 'tiny_mce_before_init', array( &$this, 'add_tinymce_tags' ) );
		add_action( 'init', array( &$this, 'custom_tinymce_css' ) );

		add_filter( 'mce_external_plugins', array( &$this, 'add_loganaddons_js' ) );
		add_filter( 'tiny_mce_version', array( &$this, 'shortcodelic_refresh_mce' ) );
		add_action( 'init', array( &$this, 'add_logan_buttons' ), 100 );
		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_scripts' ) );

		add_shortcode( 'logan-icon', array( &$this, 'loganIcons') );
		add_action('wp_ajax_pix_search_posts', array( &$this, 'post_search_action') );

		add_filter( 'pre_set_site_transient_update_plugins', array( &$this, 'check_for_plugin_update' ));
		add_filter( 'plugins_api', array( &$this, 'plugin_api_call' ), 10, 3);
    }

	/**
	 * Return an instance of this class.
	 *
	 * @since   0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since   0.0.1
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public static function activate( $network_wide ) {
	}

	/**
	 * Load text domain for translation.
	 *
	 * @since   0.0.1
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, WP_LANG_DIR . '/' . $domain . '/' . $locale . '.mo' );
		load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
	}

/*=========================================================================================*/

	/**
	 * Add a stylesheet to tinyMCE editor.
	 *
	 * @since   0.0.1
	 */
	public static function custom_tinymce_css() {
		add_editor_style( LOGANADDONS_URL.'css/tinymce.css' );
		if ( function_exists('pixedelic_google_font_editor') && pixedelic_google_font_editor() != '' )
			add_editor_style( pixedelic_google_font_editor() );
   	}


	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since    0.0.1
	 */
	public function admin_styles() {
		global $pagenow;

		if ( 'post.php' == $pagenow || 'post-new.php' == $pagenow || 'widgets.php' == $pagenow ) {
			if ( file_exists(get_template_directory().'/font/font-awesome.css') )
				wp_enqueue_style( 'fontawesome-font', get_template_directory_uri() . '/font/font-awesome.css', array(), '4.3.0' );

			if ( file_exists(get_template_directory().'/font/budicon-font.css') )
			    wp_enqueue_style( 'budicon-font', get_template_directory_uri() . '/font/budicon-font.css', array(), '1.2' );

			if ( file_exists(get_template_directory().'/css/jquery-ui.css') )
			    wp_enqueue_style( 'jquery-style', get_template_directory_uri().'/css/jquery-ui.css', array() );

		    wp_enqueue_style( 'loganaddons', LOGANADDONS_URL.'css/loganaddons.css', array() );
		}

	}

	/**
	 * Register and enqueue admin-specific scripts.
	 *
	 * @since    0.0.1
	 */
	public function admin_scripts() {
		global $pagenow;
		if ( 'post.php' == $pagenow || 'post-new.php' == $pagenow || 'widgets.php' == $pagenow ) {
			wp_enqueue_script("pix-post-select", LOGANADDONS_URL."scripts/pix_post_select.js", array('jquery','jquery-ui-dialog','jquery-ui-autocomplete'));
			wp_enqueue_script("loganaddons", LOGANADDONS_URL."scripts/loganaddons.js", array('jquery','jquery-ui-dialog'));
		}
	}

	/**
	 * Set the style formats on tinyMCE editor.
	 *
	 * @since   0.0.1
	 */
	public static function add_tinymce_tags($settings) {

		$settings['fontsize_formats'] = "0.725em 0.9em 1.175em 1.5em 1.725em 2em 2.175em 2.5em 2.725em 3em 3.175em 3.5em 3.725em 4em 4.175em 4.5em 4.725em 5em 5.175em 5.5em 5.725em 6em 6.175em 6.5em 6.725em 7em";

		$settings['fontsize_formats'] = "0.725em 0.9em 1.175em 1.5em 1.725em 2em 2.175em 2.5em 2.725em 3em 3.175em 3.5em 3.725em 4em 4.175em 4.5em 4.725em 5em 5.175em 5.5em 5.725em 6em 6.175em 6.5em 6.725em 7em";

		$settings['style_formats'] = "[
			{title: 'Styles', items: [
				{title: 'Subtitle', selector: 'p,h1,h2,h3,h4,h5,h6', classes: 'subtitle'},
				{title: 'Alternative font', selector: 'a,p,h1,h2,h3,h4,h5,h6', inline: 'span', classes: 'alternative_font'},
				{title: 'Alternative color', selector: 'a,p,h1,h2,h3,h4,h5,h6', inline: 'span', classes: 'alternative_color'},
				{title: 'Accent color', selector: 'a,p,h1,h2,h3,h4,h5,h6', inline: 'span', classes: 'accent_color'},
				{title: 'Pull left', selector: 'p,div,h1,h2,h3,h4,h5,h6,span,img,a', classes: 'pull-left'},
				{title: 'Pull right', selector: 'p,div,h1,h2,h3,h4,h5,h6,span,img,a', classes: 'pull-right'},
				{title: 'First letter', selector: 'p', classes: 'first-letter'},
				{title: 'First line', selector: 'p', classes: 'first-line'},
				{title: 'Horizontal lined', selector: 'p,h1,h2,h3,h4,h5,h6', classes: 'hor-lined'},
				{title: 'No margin', selector: 'p,div,h1,h2,h3,h4,h5,h6,span,img,a', classes: 'no_margin'},
				{title: 'Align-left', selector: 'p,div,h1,h2,h3,h4,h5,h6,span,img,a', classes: 'alignleft'},
				{title: 'Align-right', selector: 'p,div,h1,h2,h3,h4,h5,h6,span,img,a', classes: 'alignright'},
				{title: 'Quote left', selector: 'blockquote', classes: 'quote-left'},
				{title: 'Quote right', selector: 'blockquote', classes: 'quote-right'},
				{title: 'Uppercase', selector: 'a,p,h1,h2,h3,h4,h5,h6', inline: 'span', classes: 'uppercase'},
			]},
			{title: 'Effects', items: [
				{title: 'Top to bottom', selector: '*', classes: 'wpb_animate_when_almost_visible wpb_top-to-bottom'},
				{title: 'Bottom to top', selector: '*', classes: 'wpb_animate_when_almost_visible wpb_bottom-to-top'},
				{title: 'Left to right', selector: '*', classes: 'wpb_animate_when_almost_visible wpb_left-to-right'},
				{title: 'Right to left', selector: '*', classes: 'wpb_animate_when_almost_visible wpb_right-to-left'},
				{title: 'Appear from center', selector: '*', classes: 'wpb_animate_when_almost_visible wpb_appear'}
			]},
			{title: 'Line height', items: [
				{title: 'Line height 1', inline: 'span', styles: {'line-height': '1'}},
				{title: 'Line height 1.25', inline: 'span', styles: {'line-height': '1.25'}},
				{title: 'Line height 1.5', inline: 'span', styles: {'line-height': '1.5'}},
				{title: 'Line height 1.75', inline: 'span', styles: {'line-height': '1.75'}},
				{title: 'Line height 2', inline: 'span', styles: {'line-height': '2'}},
			]}
		]";

	    return $settings;
   	}

	/**
	 * Set the button on tinyMCE editor.
	 *
	 * @since   0.0.1
	 */
	function add_logan_buttons() {

		if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
			return;
		if ( get_user_option('rich_editing') == 'true' ) {
			add_filter('mce_external_plugins', array( &$this, 'add_loganaddons_js' ) );
			add_filter('mce_buttons_2', array( &$this, 'register_loganaddons_buttons_page' ) );
		}

	}

	function add_loganaddons_js($plugin_array) {
		$plugin_array['pix'] = LOGANADDONS_URL.'scripts/tinymce.js';
		$plugin_array['sc_buttons'] = LOGANADDONS_URL.'scripts/sc_buttons.js';
		return $plugin_array;
	}

	function register_loganaddons_buttons_page($buttons) {
		array_push(
			$buttons,
			"loganaddons_icons"
		);
		array_unshift( $buttons, 'fontsizeselect', 'styleselect' );
		return $buttons;
	}

	function loganaddons_refresh_mce($ver) {
		$ver += 3;
		return $ver;
	}

	/**
	 * Set the content width as JS var.
	 *
	 * @since    0.0.1
	 */
	public static function js_vars() {
		global $pagenow;

		?>

		<script type="text/javascript">
		//<![CDATA[
			var loganaddon_tinymce_text_icons = "<?php _e('Icons', 'loganaddons'); ?>";
		//]]>
		</script>

		<?php
	}

	/**
	 * Print font icon generator on footer for dialog box.
	 *
	 * @since    0.0.1
	 */
	public static function font_box() {
		global $pagenow;

		if ( 'post.php' == $pagenow || 'post-new.php' == $pagenow || 'widgets.php' == $pagenow ) { ?>

			<div id="pix-post-select" class="hidden" style="background-image:url(<?php echo esc_url( admin_url() . '/images/spinner-2x.gif' ); ?>);">
		        <?php require_once get_template_directory() . '/inc/post-select.php'; ?>
		    </div><!-- #lpix-post-select -->

			<div id="pix-list-icon" class="hidden" style="background-image:url(<?php echo esc_url( admin_url() . '/images/spinner-2x.gif' ); ?>);">
		        <?php require_once get_template_directory() . '/inc/icon-list.php'; ?>
		    </div><!-- #pix-list-icon -->

		<?php }
	}

	/**
	 * @since    1.0.0
	 */
	public function loganIcons($atts, $content = null) {

		extract(shortcode_atts(array(
	        'icon' => '',
	        'size' => '',
	        'color' => ''
	    ), $atts));

		$out = '<i class="' . esc_attr($icon) . '"';

		if ( $size!='' || $color!='' ) {

			$out .= ' style="';

			if ( $size!='')
				$out .= 'font-size:' . esc_attr($size) . ';';

			if ( $color!='')
				$out .= 'color:' . esc_attr($color) . ';';

			$out .= '"';

		}

		$out .= '></i>';

		return $out;


	}	

	/**
	 * @since    1.0.0
	 */
	public function post_search_action() {
		if ( empty( $_REQUEST['_widgets_nonce'] ) || !wp_verify_nonce( $_REQUEST['_widgets_nonce'], 'widgets_action' ) ) return;
		$term = !empty($_GET['term']) ? stripslashes($_GET['term']) : '';
		$type = !empty($_GET['type']) ? stripslashes($_GET['type']) : '_all';
		if($type == '_all') $type = explode(',', siteorigin_widget_post_selector_all_post_types());
		$results = array();
		$r = new WP_Query( array('s' => $term, 'post_status' => 'publish', 'posts_per_page' => 20, 'post_type' => $type) );
		foreach($r->posts as $post) {
	//			$thumbnail = wp_get_attachment_image_src($post->ID);
			$results[] = array(
				'label' => $post->post_title,
				'value' => $post->ID,
			);
		}
		header('content-type:application/json');
		echo json_encode($results);
		exit();
	}
	
	/**
	 * Check for update.
	 *
	 * @since    0.1.1
	 */
	public function check_for_plugin_update($checked_data) {
		global $wp_version;

		$api_url = 'http://www.pixedelic.com/api/api.php';

		if (empty($checked_data->checked) || !isset($checked_data->checked[ LOGANADDONS_NAME ]))
			return $checked_data;
		
		$args = array(
			'dir' => 'logan-addons',
			'slug' => 'logan-addons',
			'version' => $checked_data->checked[ LOGANADDONS_NAME ],
			'id' => apply_filters('pixedelic_itemID',''),
			'user' => apply_filters('pixedelic_username',''),
			'license' => apply_filters('pixedelic_licensekey','')
		);

		$request_string = array(
				'body' => array(
					'action' => 'basic_check', 
					'request' => serialize($args),
					'api-key' => md5(get_bloginfo('url'))
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
			);
		
		$raw_response = wp_remote_post($api_url, $request_string);

		if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
			$response = unserialize($raw_response['body']);

		if (is_object($response) && !empty($response)) // Feed the update data into WP updater
			$checked_data->response[ LOGANADDONS_NAME ] = $response;
		
		return $checked_data;
	}

	/**
	 * Call for update.
	 *
	 * @since    0.0.1
	 */
	public function plugin_api_call($def, $action, $args) {
		global $wp_version;
		
		$plugin_slug = 'logan-addons';
		$api_url = 'http://www.pixedelic.com/api/api.php';
		
		if (!isset($args->slug) || ($args->slug != $plugin_slug))
			return false;
			
		$plugin_info = get_site_transient('update_plugins');
		$current_version = $plugin_info->checked[ LOGANADDONS_NAME ];
		$args->version = $current_version;
		
		$request_string = array(
				'body' => array(
					'action' => $action, 
					'request' => serialize($args),
					'api-key' => md5(get_bloginfo('url'))
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
			);
		
		$request = wp_remote_post($api_url, $request_string);

		if (is_wp_error($request)) {
			$res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
		} else {
			$res = unserialize($request['body']);

			if ($res === false)
				$res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
		}
		
		return $res;
	}

}