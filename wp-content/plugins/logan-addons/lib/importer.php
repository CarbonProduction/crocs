<?php
add_action( 'customize_controls_enqueue_scripts', 'addons_customize_controls_scripts' );
function addons_customize_controls_scripts() {
    global $logan_theme_version;

    $array_dep = array( 'jquery', 'jquery-ui-sortable', 'jquery-ui-dialog', 'jquery-ui-accordion' );
    wp_enqueue_script( 'logan_addons_customizer_controls', LOGANADDONS_URL.'scripts/importer.js', $array_dep, true );

}

add_action( 'wp_ajax_pix_demo_importer', 'pixedelic_one_click_demo_installer' );
if ( ! function_exists('pixedelic_one_click_demo_installer')): 
function pixedelic_one_click_demo_installer(){

    $demo = $_POST['demo'];
    $step = $_POST['step'];
    $go = false;

    switch($demo) {
        case 'default':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-home-default.xml';
            $revslider_name = 'Home default';
            $revslider_path = get_template_directory() . '/inc/demo/home-default-slider.zip';
            $dat_file = get_template_directory_uri() . '/inc/demo/home-default.dat';
            $home_post = 'Default home';
            $footer_post = 'Footer';
        break;
        case 'agency':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-agency-home.xml';
            $revslider_name = 'Agency home';
            $revslider_path = get_template_directory() . '/inc/demo/agency-home-slider.zip';
            $dat_file = get_template_directory_uri() . '/inc/demo/agency-home.dat';
            $home_post = 'Agency home';
            $footer_post = 'Footer';
        break;
        case 'creative':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-creative-home.xml';
            $revslider_name = '';
            $revslider_path = '';
            $dat_file = get_template_directory_uri() . '/inc/demo/creative-home.dat';
            $home_post = 'Creative home';
            $footer_post = 'Footer creative';
        break;
        case 'corporate':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-corporate-home.xml';
            $revslider_name = 'Corporate home';
            $revslider_path = get_template_directory() . '/inc/demo/corporate-home-slider.zip';
            $dat_file = get_template_directory_uri() . '/inc/demo/corporate-home.dat';
            $home_post = 'Corporate home';
            $footer_post = 'Footer';
        break;
        case 'shop':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-home-shop.xml';
            $revslider_name = 'Home shop';
            $revslider_path = get_template_directory() . '/inc/demo/home-shop-slider.zip';
            $dat_file = get_template_directory_uri() . '/inc/demo/home-shop.dat';
            $home_post = 'Home shop';
            $footer_post = 'Footer';
        break;
        case 'hotel':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-hotel-home.xml';
            $revslider_name = 'Hotel home';
            $revslider_path = get_template_directory() . '/inc/demo/hotel-home-slider.zip';
            $dat_file = get_template_directory_uri() . '/inc/demo/hotel-home.dat';
            $home_post = 'Hotel';
            $footer_post = 'Footer';
        break;
        case 'magazine':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-magazine-home.xml';
            $revslider_name = 'Magazine home';
            $revslider_path = get_template_directory() . '/inc/demo/magazine-home-slider.zip';
            $dat_file = get_template_directory_uri() . '/inc/demo/magazine-home.dat';
            $home_post = 'Magazine';
            $footer_post = 'Footer';
        break;
        case 'personal':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-personal-blog.xml';
            $revslider_name = 'Personal blog';
            $revslider_path = get_template_directory() . '/inc/demo/personal-blog-slider.zip';
            $dat_file = get_template_directory_uri() . '/inc/demo/personal-blog.dat';
            $home_post = 'Personal blog home';
            $footer_post = 'Footer';
        break;
        case 'parallax':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-parallax-shop.xml';
            $revslider_name = '';
            $revslider_path = '';
            $dat_file = get_template_directory_uri() . '/inc/demo/parallax-shop.dat';
            $home_post = 'Parallax shop';
            $footer_post = 'Footer';
        break;
        case 'restaurant':
            $go = true;
            $theme_xml_file_content = get_template_directory() . '/inc/demo/demo-restaurant-home.xml';
            $revslider_name = 'Restaurant home';
            $revslider_path = get_template_directory() . '/inc/demo/restaurant-home-slider.zip';
            $dat_file = get_template_directory_uri() . '/inc/demo/restaurant-home.dat';
            $home_post = 'Restaurant home';
            $footer_post = 'Footer';
        break;
    }

    if ( !$go )
        return;

//********** FIRST STEP **********//

    if ( $step == '1' ) {

        if ( ! function_exists('pix_wie_process_import_file') ) {
            $pix_import_widgets = LOGANADDONS_PATH . 'lib/widget.importer.php';
            if ( file_exists( $pix_import_widgets ) )
                require $pix_import_widgets;
        }

        if ( function_exists('pix_wie_process_import_file') ) {
            $wie_file = get_template_directory() . '/inc/demo/widgets.wie';
            ob_start();
            pix_wie_process_import_file($wie_file);
            ob_end_clean();

            flush_rewrite_rules();
        }

    } elseif ( $step == '2' ) {

        if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

        if ( ! class_exists( 'WP_Importer' ) ) {
            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            if ( file_exists( $class_wp_importer ) )
                require $class_wp_importer;
        }

        if ( ! class_exists('Pix_Import') ) {
            $pix_import = LOGANADDONS_PATH . 'lib/class.importer.php';
            if ( file_exists( $pix_import ) )
                require $pix_import;
        }

        if ( class_exists('Pix_Import') ) {
            $theme_xml_file = get_template_directory() . '/inc/demo/demo-media-1.xml';
            $importer = new Pix_Import();
            $importer->fetch_attachments = true;
            ob_start();
            $importer->import($theme_xml_file);
            ob_end_clean();

            flush_rewrite_rules();
        }

    } elseif ( $step == '3' ) {

        if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

        if ( ! class_exists( 'WP_Importer' ) ) {
            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            if ( file_exists( $class_wp_importer ) )
                require $class_wp_importer;
        }

        if ( ! class_exists('Pix_Import') ) {
            $pix_import = LOGANADDONS_PATH . 'lib/class.importer.php';
            if ( file_exists( $pix_import ) )
                require $pix_import;
        }

        if ( class_exists('Pix_Import') ) {
            $theme_xml_file = get_template_directory() . '/inc/demo/demo-media-2.xml';
            $importer = new Pix_Import();
            $importer->fetch_attachments = true;
            ob_start();
            $importer->import($theme_xml_file);
            ob_end_clean();

            flush_rewrite_rules();
        }

    } elseif ( $step == '4' ) {

        if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

        if ( ! class_exists( 'WP_Importer' ) ) {
            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            if ( file_exists( $class_wp_importer ) )
                require $class_wp_importer;
        }

        if ( ! class_exists('Pix_Import') ) {
            $pix_import = LOGANADDONS_PATH . 'lib/class.importer.php';
            if ( file_exists( $pix_import ) )
                require $pix_import;
        }

        if ( class_exists('Pix_Import') ) {
            $theme_xml_file = get_template_directory() . '/inc/demo/demo-media-3.xml';
            $importer = new Pix_Import();
            $importer->fetch_attachments = true;
            ob_start();
            $importer->import($theme_xml_file);
            ob_end_clean();

            flush_rewrite_rules();
        }

    } elseif ( $step == '5' ) {

        if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

        if ( ! class_exists( 'WP_Importer' ) ) {
            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            if ( file_exists( $class_wp_importer ) )
                require $class_wp_importer;
        }

        if ( ! class_exists('Pix_Import') ) {
            $pix_import = LOGANADDONS_PATH . 'lib/class.importer.php';
            if ( file_exists( $pix_import ) )
                require $pix_import;
        }

        if ( class_exists('Pix_Import') ) {
            $theme_xml_file = get_template_directory() . '/inc/demo/demo-media-4.xml';
            $importer = new Pix_Import();
            $importer->fetch_attachments = true;
            ob_start();
            $importer->import($theme_xml_file);
            ob_end_clean();

            flush_rewrite_rules();
        }

    } elseif ( $step == '6' ) {

        if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

        if ( ! class_exists( 'WP_Importer' ) ) {
            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            if ( file_exists( $class_wp_importer ) )
                require $class_wp_importer;
        }

        if ( ! class_exists('Pix_Import') ) {
            $pix_import = LOGANADDONS_PATH . 'lib/class.importer.php';
            if ( file_exists( $pix_import ) )
                require $pix_import;
        }

        if ( class_exists('Pix_Import') ) {
            $theme_xml_file = get_template_directory() . '/inc/demo/demo-media-5.xml';
            $importer = new Pix_Import();
            $importer->fetch_attachments = true;
            ob_start();
            $importer->import($theme_xml_file);
            ob_end_clean();

            flush_rewrite_rules();
        }

    } elseif ( $step == '7' ) {

        if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

        if ( ! class_exists( 'WP_Importer' ) ) {
            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            if ( file_exists( $class_wp_importer ) )
                require $class_wp_importer;
        }

        if ( ! class_exists('Pix_Import') ) {
            $pix_import = LOGANADDONS_PATH . 'lib/class.importer.php';
            if ( file_exists( $pix_import ) )
                require $pix_import;
        }

        if ( class_exists('Pix_Import') ) {
            $theme_xml_file = get_template_directory() . '/inc/demo/demo-media-6.xml';
            $importer = new Pix_Import();
            $importer->fetch_attachments = true;
            ob_start();
            $importer->import($theme_xml_file);
            ob_end_clean();

            flush_rewrite_rules();
        }

    } elseif ( $step == '8' ) {

        if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

        if ( ! class_exists( 'WP_Importer' ) ) {
            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            if ( file_exists( $class_wp_importer ) )
                require $class_wp_importer;
        }

        if ( ! class_exists('Pix_Import') ) {
            $pix_import = LOGANADDONS_PATH . 'lib/class.importer.php';
            if ( file_exists( $pix_import ) )
                require $pix_import;
        }

        if ( class_exists('Pix_Import') ) {
            wp_delete_nav_menu( 'logan-main-menu' );

            $importer = new Pix_Import();
            $importer->fetch_attachments = true;
            ob_start();
            $importer->import($theme_xml_file_content);
            ob_end_clean();

            flush_rewrite_rules();
        }

    } elseif ( $step == '9' ) {

        if ( class_exists('RevSlider') && $revslider_name!='' ) {
            
            $slider = new RevSlider();

            $arrSliders = $slider->getArrSliders();

            $revsliders = array();
            if ( $arrSliders ) {
                foreach ( $arrSliders as $slider ) {
                    $revsliders[ $slider->getTitle() ] = $slider->getAlias();
                }
            }

            ob_start();
            if ( !isset($revsliders[$revslider_name]) ) {
                
                $slider->importSliderFromPost(true, false, $revslider_path);
                
            }
            if ( !isset($revsliders['Magazine home']) ) {
                
                $slider->importSliderFromPost(true, false, get_template_directory() . '/inc/demo/magazine-home-slider.zip');
                
            }
            if ( !isset($revsliders['Personal blog']) ) {
                
                $slider->importSliderFromPost(true, false, get_template_directory() . '/inc/demo/personal-blog-slider.zip');
                
            }
            ob_clean();
            
            ob_end_clean();
        }

    } elseif ( $step == '10' ) {

        if ( class_exists('GW_GoPricing_AdminPage_Impex') ) {
            
            $tables = get_template_directory() . '/inc/demo/pricing-tables.txt';

            if ( file_exists( $tables )) {

                $importer = new GW_GoPricing_AdminPage_Impex();
                ob_start();
                $importer->import($tables, true, array('all'));
                ob_end_clean();

                flush_rewrite_rules();
            }

        }

    } elseif ( $step == '11' ) {

        require_once(ABSPATH . 'wp-admin/includes/file.php');

        if ( ! class_exists('Pix_Customizer_Importer') ) {
            $pix_customizer_importer = LOGANADDONS_PATH . 'lib/customizer.importer.php';
            if ( file_exists( $pix_customizer_importer ) )
                require $pix_customizer_importer;
        }

        if ( class_exists('Pix_Customizer_Importer') ) {
            
            $timeout_seconds = 5;
            $temp_file = download_url( $dat_file, $timeout_seconds );

            if (!is_wp_error( $temp_file )) {
                $file = array(
                    'name' => basename($dat_file), // ex: wp-header-logo.png
                    'type' => 'text/dat',
                    'tmp_name' => $temp_file,
                    'error' => 0,
                    'size' => filesize($temp_file),
                );
                    
                $importer = new Pix_Customizer_Importer();
                ob_start();
                $importer->_import($file);
                ob_end_clean();

                flush_rewrite_rules();
            }
        }

    } elseif ( $step == '12' ) {

        $locations = get_theme_mod( 'nav_menu_locations' );
        $locations['primary'] = 'logan-main-menu';

        set_theme_mod( 'nav_menu_locations', $locations );

        $post_exists = post_exists($home_post);

        if ( $post_exists ) {
            $post = get_page_by_title( $home_post );

            update_option('show_on_front', 'page');
            update_option('page_on_front', $post->ID);

            $blog = get_page_by_title( 'Personal blog' );
            update_option('page_for_posts', $blog->ID);              

        }

        if ( post_exists( 'Default shop' ) ) {
            $shop = get_page_by_title( 'Default shop' );
            update_option('woocommerce_shop_page_id', $shop->ID);
        }

        if ( post_exists( $footer_post ) ) {
            $footer = get_page_by_title( $footer_post );
            set_theme_mod('pix_footer_page', $footer->ID);
        }

    } elseif ( $step == '13' ) {
        if ( ! function_exists('pix_ts_upload_custom_icons') ) {
            $pix_icon_importer = LOGANADDONS_PATH . 'lib/ts-icon.importer.php';
            if ( file_exists( $pix_icon_importer ) )
                require_once $pix_icon_importer;
        }

        if ( function_exists('pix_ts_upload_custom_icons') ) {
            $icons = get_template_directory_uri() . '/inc/demo/budicon-icomoon.zip';

            $timeout_seconds = 5;
            $temp_file = download_url( $icons, $timeout_seconds );

            if (!is_wp_error( $temp_file )) {
                $file = array(
                    'url' => $icons,
                    'name' => basename($icons), // ex: wp-header-logo.png
                    'type' => 'application/zip',
                    'tmp_name' => $temp_file,
                    'error' => 0,
                    'size' => filesize($temp_file),
                );
                    

                ob_start();
                add_filter('upload_dir', 'pix_filter_upload_dir');
                pix_ts_upload_custom_icons($file);
                remove_filter('upload_dir', 'pix_filter_upload_dir');
                ob_end_clean();

                flush_rewrite_rules();
            }

        }
    }

}
endif; //pix_one_click_demo_installer

add_action( 'customize_controls_print_footer_scripts', 'addons_demo_messages' );
function addons_demo_messages(){ ?>

    <div id="pix-demo-installer-dialog" class="hidden">
    </div><!-- #list-icon -->

    <?php
        $missing_plugin_attention = '';
        $demo_plugin_attention = '';
        if ( !class_exists('RevSlider')) {
            $missing_plugin_attention .= "&nbsp;&nbsp;&nbsp;&#8226; " . esc_html__('Revolution Slider', 'logan');
        }
        if ( !class_exists('GW_GoPricing_AdminPage_Impex')) {
            $br = $missing_plugin_attention == '' ? '' : '<br>';
            $missing_plugin_attention .= $br . "&nbsp;&nbsp;&nbsp;&#8226; " . esc_html__('Go Pricing', 'logan');
        }
        if ( !post_type_exists('jetpack-portfolio')) {
            $br = $missing_plugin_attention == '' ? '' : '<br>';
            $missing_plugin_attention .= $br . "&nbsp;&nbsp;&nbsp;&#8226; " . esc_html__('Jetpack', 'logan');
        }
        if ( !pixedelic_is_woocommerce_active()) {
            $br = $missing_plugin_attention == '' ? '' : '<br>';
            $missing_plugin_attention .= $br . "&nbsp;&nbsp;&nbsp;&#8226; " . esc_html__('WooCommerce', 'logan');
        }

        if ( $missing_plugin_attention!='' ) {
            $start_demo_plugin_attention = '<p>' . esc_html__('It is highly recommended to activate the following plugins in order to install the demo content properly:', 'logan') . '</p>';
            $demo_plugin_attention = $start_demo_plugin_attention . '<p>' . $missing_plugin_attention . '</p>';
        }
    ?>

    <script>
        var demo_modal_panel_title = "<?php esc_html_e('One click demo installer', 'logan'); ?>";
        var demo_r_u_sure = "<?php echo $demo_plugin_attention; ?><p><?php wp_kses( _e( '<strong>N.B.:</strong> you are importing a complete demo. This includes posts, pages, comments, widgets, media. All the theme options available in the Customizer will be overridden and lost so it is highly recommended a backup if you don\'t want to lose your previous data.', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p><p><?php wp_kses( _e( 'The entire process can take about 5 minutes, but it can be also different because of internet conntection, traffic on the server etc.', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        var demo_continue = "<?php wp_kses( _e( 'Yes I\'m sure. Go on!', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?>";
        var demo_step = [];
        demo_step[1] = "<p><strong><small><?php esc_html_e('CONTENT', 'logan'); ?></small></strong></p><p class=\"demo_step_loading\"><?php wp_kses( _e( '1/11. Importing sidebars and widgets (few seconds)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[2] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '2/11. Importing the 1st of 6 blocks of media (about 15 sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[3] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '3/11. Importing the 2nd of 6 blocks of media (about 25 sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[4] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '4/11. Importing the 3rd of 6 blocks of media (about 25 sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[5] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '5/11. Importing the 4th of 6 blocks of media (about 40 sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[6] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '6/11. Importing the 5th of 6 blocks of media (about 40 sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[7] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '7/11. Importing the last block of media (about 20 sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[8] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '8/11. Importing posts, pages, comments, menus (about 12 sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[9] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '9/11. Importing Revolutions Sliders (about 8 sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[10] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '10/11. Importing pricing tables (few sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[11] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '11/11. Importing theme options (about 12 sec.)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[12] = "<p><strong><small><?php esc_html_e('SETTINGS', 'logan'); ?></small></strong></p><p class=\"demo_step_loading\"><?php wp_kses( _e( '1/2. Setting home page, menu, footer and shop page', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[13] = "<p class=\"demo_step_loading\"><?php wp_kses( _e( '2/2. Install icons (few seconds)', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
        demo_step[14] = "<p><?php wp_kses( _e( '<strong>GOTCHA! Your demo is ready, enjoy Logan</strong>', 'logan' ), array( 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?><?php wp_kses( _e( '<br><small><strong>P.S.:</strong> live customizer is refreshing and this message will self-destruct in 8 seconds</small>', 'logan' ), array( 'small' => array(), 'strong' => array(), 'em' => array(), 'br' => array() ) ); ?></p>";
    </script>
<?php }

if ( ! function_exists( 'pix_filter_upload_dir' )) :
function pix_filter_upload_dir($upload) {
    $upload['subdir']   = '/ts-vcsc-icons/custom-pack';
    $upload['path']     = $upload['basedir'] . $upload['subdir'];
    $upload['url']      = $upload['baseurl'] . $upload['subdir'];
    return $upload;
}
endif;