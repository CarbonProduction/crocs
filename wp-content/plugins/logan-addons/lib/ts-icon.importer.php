<?php
	global $VISUAL_COMPOSER_EXTENSIONS;
	global $TS_VCSC_tinymceCustomCount;
	global $TS_VCSC_Icons_Custom;
	
	if ( ! function_exists( 'pix_TS_VCSC_RecursiveRMDIR' )) :
	function pix_TS_VCSC_RecursiveRMDIR($dir) {
		foreach (scandir($dir) as $file) {
		   if ('.' === $file || '..' === $file) continue;
		   if (is_dir("$dir/$file")) pix_TS_VCSC_RecursiveRMDIR("$dir/$file");
		   else unlink("$dir/$file");
		}
		rmdir($dir);
	}
	endif;
	
	//print_r (get_option('ts_vcsc_extend_settings_tinymceCustomArray', ''));
	//echo get_option('ts_vcsc_extend_settings_tinymceCustomName', '');
	//echo get_option('ts_vcsc_extend_settings_tinymceCustomAuthor', '');
	//echo get_option('ts_vcsc_extend_settings_tinymceCustomCount', '');
	//echo get_option('ts_vcsc_extend_settings_tinymceCustomDate', '');
	
	/* get uploaded file, unzip .zip, store files in appropriate locations, populate page with custom icons
	wp_handle_upload ( http://codex.wordpress.org/Function_Reference/wp_handle_upload )
	** TO DO RENAME UPLOADED FILE TO ts-vcsc-custom-pack.zip ** */
	if ( ! function_exists( 'pix_ts_upload_custom_icons' )) :
	function pix_ts_upload_custom_icons( $file ) {
		$uploadedfile 				= $file;
		$upload_replace   			= 'on';
		$upload_relative   			= 'on';
		$upload_overrides 			= array('test_form' => false);
		$upload_directory 			= wp_upload_dir();
		$font_directory				= $upload_directory['basedir'] . '/ts-vcsc-icons/custom-pack';
		// TO DO
		// get filename dynamically so user doesn't need to customize zip name
		// ERROR CHECKING SO ONLY .ZIP's ARE UPLOADED
		// hide ajax loader if no pack is uploaded
		// export json file for importing back to icomoon - spit back out json file 
		// create a 'Download Pack' button and 'Download .json' button  
		/*
		$filename = $uploadedFile
		*/
		$filename 					= $file["name"];
		$source 					= $file["tmp_name"];
		$type 						= $file["type"]; 
		$name 						= explode(".", $filename);
		$accepted_types 			= array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
		foreach ($accepted_types as $mime_type) {
			if ($mime_type == $type) {
				$okay = true;
				break;
			} 
		} 
		$continue 					= strtolower($name[1]) == 'zip' ? true : false;
		if (!$continue) {
			TS_VCSC_CustomFontImportMessages('warning', 'The file you are trying to upload is not a .zip file. Please try again.');
		}
		/* PHP current path */
		$filepath 					= $upload_directory['basedir'] . '/ts-vcsc-icons/custom-pack/';  	// absolute path to the directory where zipper.php is in
		$filenoext 					= basename ($filename, '.zip');  		// absolute path to the directory where zipper.php is in (lowercase)
		$filenoext 					= basename ($filenoext, '.ZIP');  		// absolute path to the directory where zipper.php is in (when uppercase)
		$targetdir 					= $filepath; 							// target directory
		$filenameClear				= trim(str_replace(' ', '-', $filename));
		//$targetzip 				= $filepath . $filename; 	
		$targetzip 					= $filepath . $filenameClear; 			// target zip file
		/* create directory if not exists', otherwise overwrite */
		/* target directory is same as filename without extension */
		if (is_dir($targetdir)) pix_TS_VCSC_RecursiveRMDIR ($targetdir);
		mkdir($targetdir, 0777);
		/* here it is really happening */
		//if (move_uploaded_file($source, $targetzip)) {
		if ( copy( $file['url'], $targetzip ) ) {
			if (class_exists('ZipArchive')) {
				$zip 				= new ZipArchive();
				$x 					= $zip->open($targetzip);  				// open the zip file to extract
				if ($x === true) {
					$zip->extractTo($targetdir); 							// place in the directory with same name  
					$zip->close();
					$unzipfile		= true;
				} else {
					$unzipfile		= false;
				}
			} else {
				$dest_path 			= $upload_directory['path'];
				$dest_url			= $upload_directory['url'];				
				//$unzipfile 		= unzip_file($dest_path . '/' . $filename, $dest_path);
				$unzipfile 			= unzip_file($dest_path . '/' . $filenameClear, $dest_path);
			}
			$movefile 				= true;
		} else {	
			$movefile 				= false;
		}		
		// if upload was successful
		if ($movefile) {	
			echo '<script>
				jQuery(document).ready(function() {
					jQuery(".ts-vcsc-custom-pack-preloader").hide();
					jQuery("#uninstall-pack-button").removeAttr("disabled");
					jQuery("#ts_vcsc_custom_pack_field").attr("disabled", "disabled");
					jQuery("input[value=Import]").attr("disabled", "disabled");
					jQuery(".ts-vcsc-custom-pack-buttons").after("<div class=updated><p class=fontPackUploadedSuccess>Custom Font Pack successfully uploaded!</p></div>");
				});
			</script>';
			// unzip the file contents to the same directory
			WP_Filesystem();
			$dest 					= wp_upload_dir();
			$dest_path 				= $dest['path'];
			$dest_url				= $dest['url'];
			$fileNameNoSpaces 		= trim(str_replace(' ', '-', $uploadedfile['name']));
			$basicCheck				= true;
			$filesFound				= true;
			if ($unzipfile) {
				if (file_exists($dest_path . '/' . $fileNameNoSpaces)) {
					rename($dest_path . '/' . $fileNameNoSpaces, $dest_path . '/ts-vcsc-custom-pack.zip');
				} else {
					$filesFound 	= false;
				}
				if (file_exists($dest_path . '/selection.json')) {
					rename($dest_path . '/selection.json', $dest_path . '/ts-vcsc-custom-pack.json');
				} else {
					$basicCheck		= false;
				}
				// Change Path of linked Font Files in Style.css
				if ((file_exists($dest_path . '/style.css')) && ($upload_replace == 'on')) {
					$styleCSS 		= $dest_path . '/style.css';
					if (ini_get('allow_url_fopen') == '1') {
						$currentStyles 						= file_get_contents($styleCSS);
						// for css and js files that are not needed any more
						if (strpos($dest_url, '/ts-vcsc-icons/custom-pack') !== false) {
							$newStyles 						= str_replace("url('fonts/", "url('" . $dest_url . "/fonts/", $currentStyles);
						} else {
							$newStyles 						= str_replace("url('fonts/", "url('" . $dest_url . "/ts-vcsc-icons/custom-pack/fonts/", $currentStyles);
						}
						// Write the contents back to the file
						$file_put_contents 					= file_put_contents($styleCSS, $newStyles);
					}
				} else if ($upload_replace == 'on') {
					$basicCheck = false;
				}
				// Delete unecessary files / add error checking
				if (file_exists($dest_path . '/demo-files')) {
					TS_VCSC_RemoveDirectory($dest_path . '/demo-files');
				};
				if (file_exists($dest_path . '/demo.html')) {
					unlink($dest_path . '/demo.html'); 
				};
				if (file_exists($dest_path . '/Read Me.txt')) {
					unlink($dest_path . '/Read Me.txt'); 
				};
				if (file_exists($dest_path . '/variables.scss')) {
					unlink($dest_path . '/variables.scss'); 
				};
				if (($basicCheck == true) && ($filesFound == true)) {
					// Process JSON File to create and store Font Array
					$Custom_JSON_URL 							= $dest_url . '/ts-vcsc-custom-pack.json';
					
					//echo 'JSON #1: ' . $dest_path . '/ts-vcsc-custom-pack.json' . '<br/>';
					//echo 'JSON #2: ' . $Custom_JSON_URL . '<br/>';
					
					if (function_exists('curl_init')) {
						$ch 									= curl_init();
						$timeout 								= 5;
						curl_setopt($ch, CURLOPT_URL, 			$Custom_JSON_URL);
						curl_setopt($ch, CURLOPT_HEADER,		0);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
						//curl_setopt($ch, CURLOPT_PROTOCOLS,	CURLPROTO_ALL);
						curl_setopt($ch, CURLOPT_USERAGENT, 	'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0');
						$Custom_JSON 							= curl_exec($ch);
						curl_close($ch);
					} else if (ini_get('allow_url_fopen') == '1') {
						$Custom_JSON							= file_get_contents($Custom_JSON_URL);
					}
					if (!is_wp_error($Custom_JSON) && !empty($Custom_JSON)) {
						
						//echo $Custom_JSON;
						
						$Custom_Code                        	= json_decode($Custom_JSON, true);						
						
						//echo $dest_url . '/style.css' . '<br/>';
						//echo str_replace(home_url(), '', $dest_url . '/style.css') . '<br/>';
						//var_dump($Custom_Code);
						
						if ((isset($Custom_Code['IcoMoonType'])) && (isset($Custom_Code['icons']))){
							$TS_VCSC_Icons_Custom               = array();
							$TS_Custom_User_Font				= array();
							if (isset($Custom_Code['preferences']['fontPref']['prefix'])) {
								$Custom_Class_Prefix			= $Custom_Code['preferences']['fontPref']['prefix'];
							} else {
								$Custom_Class_Prefix			= "";
							}
							if (isset($Custom_Code['preferences']['fontPref']['postfix'])) {
								$Custom_Class_Postfix			= $Custom_Code['preferences']['fontPref']['postfix'];
							} else {
								$Custom_Class_Postfix			= "";
							}
							if (isset($Custom_Code['metadata']['name'])) {
								$Custom_Font_Name				= $Custom_Code['metadata']['name'];
							} else {
								$Custom_Font_Name				= "Unknown Font";
							}
							if (isset($Custom_Code['metadata']['designer'])) {
								$Custom_Font_Author             = $Custom_Code['metadata']['designer'];
							} else {
								$Custom_Font_Author				= "Unknown Author";
							}
							if (isset($Custom_Code['icons'])) {
								foreach ($Custom_Code['icons'] as $item) {
									if (isset($item['properties']['name']) && isset($item['properties']['code'])) {
										$Custom_Class_Full = $Custom_Class_Prefix . $item['properties']['name'] . $Custom_Class_Postfix;
										$Custom_Class_Code = $item['properties']['code'];
										$TS_Custom_User_Font[$Custom_Class_Full] = $Custom_Class_Code;
									}
								}
							}
							$TS_VCSC_Icons_Custom				= $TS_Custom_User_Font;
							if (count($TS_VCSC_Icons_Custom) > 1) {
								if (is_array($TS_VCSC_Icons_Custom)) {
									$TS_VCSC_tinymceCustomCount	= count(array_unique($TS_VCSC_Icons_Custom));
								} else {
									$TS_VCSC_tinymceCustomCount	= count($TS_VCSC_Icons_Custom);
								}
							} else {
								$TS_VCSC_tinymceCustomCount		= count($TS_VCSC_Icons_Custom);
							}
							// Export Font Array to PHP file
							/*if (ini_get('allow_url_fopen') == '1') {
								$phpArray = $dest_path . '/ts-vcsc-custom-pack.php';
								$file_put_contents = file_put_contents($phpArray, '<?php $VISUAL_COMPOSER_EXTENSIONS->TS_VCSC_Icons_Custom = ' . var_export($TS_VCSC_Icons_Custom, true) . '; ?>');
							}*/
							// Store Custom Font Data in WordPress Settings
							update_option('ts_vcsc_extend_settings_tinymceCustom', 			1);							
							if ($upload_relative == 'on') {
								update_option('ts_vcsc_extend_settings_tinymceCustomJSON', 	$dest_url . '/ts-vcsc-custom-pack.json');
								update_option('ts_vcsc_extend_settings_tinymceCustomPath', 	$dest_url . '/style.css');
								update_option('ts_vcsc_extend_settings_tinymceCustomPHP', 	$dest_url . '/ts-vcsc-custom-pack.php');
							} else {
								update_option('ts_vcsc_extend_settings_tinymceCustomJSON', 	str_replace(home_url(), '', $dest_url . '/ts-vcsc-custom-pack.json'));
								//update_option('ts_vcsc_extend_settings_tinymceCustomPath', wp_make_link_relative($dest_url . '/style.css'));
								update_option('ts_vcsc_extend_settings_tinymceCustomPath', 	str_replace(home_url(), '', $dest_url . '/style.css'));
								update_option('ts_vcsc_extend_settings_tinymceCustomPHP', 	str_replace(home_url(), '', $dest_url . '/ts-vcsc-custom-pack.php'));
							}							
							update_option('ts_vcsc_extend_settings_tinymceCustomArray', 	$TS_VCSC_Icons_Custom);
							update_option('ts_vcsc_extend_settings_tinymceCustomName', 		ucwords($Custom_Font_Name));
							update_option('ts_vcsc_extend_settings_tinymceCustomAuthor', 	ucwords($Custom_Font_Author));
							update_option('ts_vcsc_extend_settings_tinymceCustomCount', 	$TS_VCSC_tinymceCustomCount);
							update_option('ts_vcsc_extend_settings_tinymceCustomDate',		date('Y/m/d h:i:s A'));
							// Display Success Message / Disable File Upload Field
							echo '<script>
								jQuery(document).ready(function() {
									jQuery(".dropDownDownload").removeAttr("disabled");
									jQuery(".fontPackUploadedSuccess").parent("div").after("<div class=updated><p class=fontPackSuccessUnzip>Custom Font Pack successfully unzipped!</p></div>");
								});
							</script>';	 
							echo '<script>
								jQuery(document).ready(function() {
								jQuery(".fontPackSuccessUnzip").parent("div").after("<div class=updated><p>A Custom Font named &quot;' . ucwords($Custom_Font_Name) . '&quot; with ' . $TS_VCSC_tinymceCustomCount . ' icon(s) could be found and installed!</p></div>");
									setTimeout(function() {
										jQuery(".updated").fadeOut();
									}, 5000);
								});
							</script>';
							$output = "";
							$output .= "<div id='ts-vcsc-extend-preview' class=''>";
								$output .="<div id='ts-vcsc-extend-preview-name'>Font Name: " . ucwords($Custom_Font_Name) . "</div>";
								$output .="<div id='ts-vcsc-extend-preview-author'>Font Author: " . 	get_option('ts_vcsc_extend_settings_tinymceCustomAuthor', 'Custom User') . "</div>";
								$output .="<div id='ts-vcsc-extend-preview-count'>Icon Count: " . 		get_option('ts_vcsc_extend_settings_tinymceCustomCount', 0) . "</div>";
								$output .="<div id='ts-vcsc-extend-preview-date'>Uploaded: " . 			get_option('ts_vcsc_extend_settings_tinymceCustomDate', '') . "</div>";
								$output .= "<div id='ts-vcsc-extend-preview-list' class=''>";
								$icon_counter = 0;
								foreach ($VISUAL_COMPOSER_EXTENSIONS->TS_VCSC_Icons_Custom as $key => $option ) {
									$font = explode('-', $key);
									$output .= "<div class='ts-vcsc-icon-preview ts-freewall-active' data-name='" . $key . "' data-code='" . $option . "' data-font='" . strtolower($iconfont) . "' data-count='" . $icon_counter . "' rel='" . $key . "'><span class='ts-vcsc-icon-preview-icon'><i class='" . $key . "'></i></span><span class='ts-vcsc-icon-preview-name'>" . $key . "</span></div>";
									$icon_counter = $icon_counter + 1;
								}
								$output .= "</div>";
							$output .= "</div>";
							TS_VCSC_CustomFontImportMessages('success', 'Your Custom Font Pack has been successfully installed.');
						} else {
							TS_VCSC_ResetCustomFont();
							echo '<script>
								jQuery(document).ready(function() {
									jQuery(".fontPackUploadedSuccess").parent("div").after("<div class=error><p>This font was not created with the IcoMoon App and/or is missing a valid JSON data file. Please upload only font packages created via IcoMoon.</p></div>");
								});
							</script>';
							TS_VCSC_CustomFontImportMessages('warning', 'This font was not created with the IcoMoon App and/or is missing a valid JSON data file. Please upload only font packages created via IcoMoon.');
						}
					} else {
						TS_VCSC_ResetCustomFont();
						echo '<script>
							jQuery(document).ready(function() {
								jQuery(".fontPackUploadedSuccess").parent("div").after("<div class=error><p>There was a problem while importing the custom font package file.</p></div>");
							});
						</script>';
						TS_VCSC_CustomFontImportMessages('warning', 'There was a problem while importing the custom font package file.');
					}
				} else {
					TS_VCSC_ResetCustomFont();
					if ($filesFound == false) {
						echo '<script>
							jQuery(document).ready(function() {
								jQuery(".fontPackUploadedSuccess").parent("div").after("<div class=error><p>There seems to be an issue with the read/write permissions of the upload folder; please check your server settings.</p></div>");
							});
						</script>';
						TS_VCSC_CustomFontImportMessages('warning', 'There seems to be an issue with the read/write permissions of the upload folder; please check your server settings.');
					} else {
						echo '<script>
							jQuery(document).ready(function() {
								jQuery(".fontPackUploadedSuccess").parent("div").after("<div class=error><p>This font package is missing a valid JSON data file and/or style.css file. In that case, please upload only complete font packages created via IcoMoon.</p></div>");
							});
						</script>';
						TS_VCSC_CustomFontImportMessages('warning', 'This font package is missing a valid JSON data file and/or style.css file. Please upload only complete font packages created via IcoMoon.');
					}
				}
			} else {
				TS_VCSC_ResetCustomFont();
				echo '<script>
					jQuery(document).ready(function() {
						jQuery(".fontPackUploadedSuccess").parent("div").after("<div class=error><p>There was a problem while unzipping the custom font package file.</p></div>");
					});
				</script>';
				TS_VCSC_CustomFontImportMessages('warning', 'There was a problem while unzipping the custom font package file.');
			}
		} else {
			TS_VCSC_ResetCustomFont();
			echo '<script>
				jQuery(document).ready(function() {
					jQuery(".ts-vcsc-custom-pack-buttons").after("<div class=error><p class=fontPackUploadedError>There was a problem while uploading the custom font package file.</p></div>");
				});
			</script>';
			TS_VCSC_CustomFontImportMessages('warning', 'There was a problem while uploading the custom font package.');
		}
	}
	endif;