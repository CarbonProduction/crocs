<?php 
/*
Plugin Name: Logan addons
Plugin URI: http://www.pixedelic.com/
Description: Additional features for Logan theme
Version: 0.1.1
Author: Manuel Masia | Pixedelic.com
Author URI: http://www.pixedelic.com
License: GPL2
*/

define( 'LOGANADDONS_PATH', plugin_dir_path( __FILE__ ) );
define( 'LOGANADDONS_URL', plugin_dir_url( __FILE__ ) );
define( 'LOGANADDONS_NAME', plugin_basename( __FILE__ ) );

require_once( LOGANADDONS_PATH . 'lib/functions.php' );
require_once( LOGANADDONS_PATH . 'lib/cpt.php' );

register_activation_hook( __FILE__, array( 'LoganAddons', 'activate' ) );
register_uninstall_hook( __FILE__, array( 'LoganAddons', 'uninstall' ) );

LoganAddons::get_instance();

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
/**
 * Visual Composer.
 */
if (is_plugin_active('js_composer/js_composer.php')) {
	require_once(LOGANADDONS_PATH . 'visual-composer/vc-assets.php');
	require_once(LOGANADDONS_PATH . 'visual-composer/vc-map.php');
	require_once(LOGANADDONS_PATH . 'visual-composer/vc-functions.php');
	require_once(LOGANADDONS_PATH . 'visual-composer/vc-params.php');
	require_once(LOGANADDONS_PATH . 'visual-composer/vc-shortcodes.php');
}

/**
 * WooCommerce.
 */
add_action('after_setup_theme', 'logan_addons_custom_wc_shortcodes');
function logan_addons_custom_wc_shortcodes(){
	require_once(LOGANADDONS_PATH . 'lib/importer.php');
	require_once( LOGANADDONS_PATH . 'lib/class.importer.php' );
	
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if (is_plugin_active('woocommerce/woocommerce.php')) {
		require_once(LOGANADDONS_PATH . 'lib/wc-shortcodes.php');
	}
}
