( function( $ ) {
	$(window).on('load', function(){
		wp.pixDemoImporter = {

			init: function() {

				var $wrap = $('#input_pix_demo_installer');

				$('label', $wrap).on('click', function(e){

					$('label', $wrap).removeClass('active');

					var $label = $(this).addClass('active'),
						name = $label.attr('for'),
						value = $('input#' + name, $wrap).val();

					e.preventDefault();

					var $button = $('<a href="#" class="button-primary button" />');
					$button.text(demo_continue);
					var $modal = $('#pix-demo-installer-dialog').html(demo_r_u_sure).append($button);

					$modal.dialog({
						resizable: true,
						draggable: true,
						modal: true,
						dialogClass: 'pix-modal demo-installer',
						width: 'auto',
						position: { my: "center", at: "center", of: window },
						title: demo_modal_panel_title,
						open: function( event, ui ) {
							$button.on('click', function(){
								var step = 1,
									repeated = false;

								var load_demo_next_step = function(step){

									var html = $modal.html();

									if ( !repeated || step > 1 ) { //to run widgets twice
										$modal.html(html + demo_step[step]);
									}

									$modal.dialog({
										position: { my: "center", at: "center", of: window }
									});

									var data = {
										action: 'pix_demo_importer',
										demo: value,
										step: step
									};

									jQuery.post(ajaxurl, data, function(response) {

										//console.log(response);

										html = $modal.html();

										if ( repeated ) { //to run widgets twice
											step = step + 1;
											$('p.demo_step_loading', $modal).removeClass('demo_step_loading').addClass('demo_step_loaded');
										} else {
											repeated = true;
										}

										$modal.dialog({
											position: { my: "center", at: "center", of: window }
										});

										if ( step <= 13 ) {

											load_demo_next_step(step);

										} else {

											$modal.html(html + demo_step[14]);

											$('p.demo_step_loading', $modal).removeClass('demo_step_loading').addClass('demo_step_loaded');

											wp.customize.previewer.refresh();

											setTimeout(function(){
												$modal.dialog('close');
												//window.location.reload();
											}, 5000);

										}
									});

								};

								$modal.html('');
								load_demo_next_step(step);

							});

						}

					});
				});

			}

		};
		wp.pixDemoImporter.init();

	});

} )( jQuery );
