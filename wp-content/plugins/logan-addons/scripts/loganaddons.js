var pix_filterIcon = function(){
	$ = jQuery;
	$(document).off('keyup', '#pix-icon-finder input[type="text"]');
	$(document).on('keyup', '#pix-icon-finder input[type="text"]', function(){
		var $input = $(this),
			$iconList = $('#pix-icon-render'),
			val,
			setInput;

		var showFilteredIcons = function(){
			val = $input.val();

			if ( val === '' ) {
				$('> div', $iconList).show();
			} else {
				$('> div', $iconList).not('[data-keys*="'+val+'"]').hide();
				$('> div[data-keys*="'+val+'"]', $iconList).show();
			}
		};
		clearTimeout(setInput);
		setInput = setTimeout(showFilteredIcons, 50);
	});
};

var pix_selectIcon = function(){
	$ = jQuery;
	$('#pix-list-icon .the-list > div').off('click')
		.on('click', function(){
		var icon;
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
		} else {
			icon = $(this).attr('class');
			$('#pix-list-icon .the-list > div.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	});
};

var pix_loadIconList = function(){
	$ = jQuery;
	$(document).off('change', '#pix-icon-finder select');
	$(document).on('change', '#pix-icon-finder select', function(){
		//ajaxLoaders(true);
		var t = $(this),
			url = $('option:selected', t).data('url');

		var ajaxReqIcon = function(){
			$.ajax({
				url: url,
				type: "POST",
				cache: false,
				success: function(loadeddata) {
					var html = $("<div/>").append(loadeddata.replace(/<script(.|\s)*?\/script>/g, "")).html();
					$('#pix-icon-render')
						.html(html)
						.animate({opacity:1},250);
					pix_selectIcon();
					pix_filterIcon();
				}
			});
		};

		$('#pix-icon-render').animate({opacity:0},250, function(){
			ajaxReqIcon();
		});
	});
};

function pix_openFontSelector($id){
	var div = jQuery('#pix-list-icon'),
		triggerChange;

	div.dialog({
		resizable: false,
		draggable: false,
		modal: true,
		dialogClass: 'pix-modal font-modal wp-dialog',
		width: 'auto',
		position: { my: "center", at: "center", of: window },
		title: "Select an icon",
		buttons: {
			"Select": function() {
				var icon = jQuery('#pix-list-icon .the-list > div.selected').data('iconname');
				if ( typeof icon != 'undefined' && icon !== '' ) {

					/*jQuery('input.pix-icon-item', $item).val(icon);
					jQuery('.icon-preview i', $item).attr('class', icon);*/
					var sc = '[logan-icon icon="' + icon + '"]';
					tinyMCE.get($id).selection.setContent(sc);
				}
				jQuery('#pix-list-icon .the-list > div.selected').removeClass('selected');
				jQuery( this ).dialog( "close" );
			}
		},
		open: function( event, ui ) {
			pix_loadIconList();
			if ( $('#pix-icon-render').html() === '' )
				$('#pix-icon-finder select').trigger('change');
		}
	});
}