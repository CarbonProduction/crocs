(function($){

"use strict";

var PIX = window.PIX || {};

PIX.smoothScroll = function() {
	var $body = $('body'),
		$html = $('html');
    $('a.pixscroll[href^="#"], .woocommerce-review-link[href^="#"]').on('click',function(e) {
		e.preventDefault();
		var $el = $(this),
		runFunc = function(){
			$el.each(function(){
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var target = $(this.hash),
						start = parseFloat($(window).scrollTop());
					target = target.length && target || $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						var targetOffset = target.offset().top - 150,
							time = Math.abs(targetOffset-start) < 100 ? 100 : Math.abs(targetOffset-start);
						time = time > 1000 ? 1000 : time;
						$body.animate({scrollTop: targetOffset}, time, 'easeOutQuad');
						$html.animate({scrollTop: targetOffset}, time, 'easeOutQuad');
					}
				}
			});
		};
		if ( $el.attr('href').slice(-1)!='#' ) {
			if ( $el.hasClass('woocommerce-review-link') ) {
				$('a[href="#tab-reviews"]').click();
			}
			var set = setTimeout(runFunc, 10);
		}
    });

    $('a[href="#"]').on('click',function(e) {
		e.preventDefault();
    });
};

PIX.init = function(){

	PIX.smoothScroll();

};

$(function(){
	PIX.init();
});

})(jQuery);