(function() {
    tinymce.create('tinymce.plugins.logan', {
		init : function(ed, url) {
			var t = this,
				activeID = tinyMCE.activeEditor.id;

			ed.addButton('loganaddons_icons', {
				//title : 'Font icons',
				text: loganaddon_tinymce_text_icons,
				//icon: 'logan_fonticon',
				onclick : function() {
					pix_openFontSelector(activeID);
				}
			});

		}

	});

    tinymce.PluginManager.add('sc_buttons', tinymce.plugins.logan);
})();