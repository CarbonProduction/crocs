(function ( $ ) {

	/* soundcloud */
    $('.vc_ui-panel-content .pix-vc-double_input').each(function(){
    	var $this = $(this),
			$field = $('input[type="hidden"]', $this),
			$textarea = $('textarea', $this),
			val = $field.val(),
			act_val = val.replace(/pix_quot;/g, '"').replace(/pix_op_brack;/g,'[').replace(/pix_cl_brack;/g,']');

		$textarea.val(act_val);

		$textarea.on('input', function(){
			val = $textarea.val();
			var esc_shortcode = val.replace(/\"/g, 'pix_quot;').replace(/\[/g,'pix_op_brack;').replace(/\]/g,'pix_cl_brack;');
			$field.val(esc_shortcode);
		});
    });

	/* media */
    $('.vc_ui-panel-content .pix-vc-media').each(function(){
		var $this = $(this),
			$field = $('input[type="hidden"]', $this),
			$textarea = $('textarea', $this),
			val = $field.val(),
			pix_media_frame,
			formlabel = 0,
			act_val = val.replace(/pix_quot;/g, '"').replace(/pix_op_brack;/g,'[').replace(/pix_cl_brack;/g,']');

		$textarea.val(act_val);

		$this.off('click', 'input[type="button"].remove_media');
		$this.on('click', 'input[type="button"].remove_media', function( e ){
			$field.add($textarea).val('');
		});

		$this.off('click', 'input[type="button"].add_media');
		$this.on('click', 'input[type="button"].add_media', function( e ){
			e.preventDefault();

			pix_media_frame = wp.media.frames.pix_media_frame = wp.media({
				className: 'media-frame pix-media-frame',
				frame: 'post',
				state: 'insert',
				multiple: false,
				library: { type: ['audio','video'] },
			});

			pix_media_frame.state('playlist-edit').on( 'update', function( selection ) {
				var shortcode = wp.media.playlist.shortcode( selection ).string();
				$textarea.val(shortcode);
				var esc_shortcode = shortcode.replace(/\"/g, 'pix_quot;').replace(/\[/g,'pix_op_brack;').replace(/\]/g,'pix_cl_brack;');
				$field.val(esc_shortcode);
			});

			pix_media_frame.state('video-playlist-edit').on( 'update', function( selection ) {
				var shortcode = wp.media.playlist.shortcode( selection ).string();
				$textarea.val(shortcode);
				var esc_shortcode = shortcode.replace(/\"/g, 'pix_quot;').replace(/\[/g,'pix_op_brack;').replace(/\]/g,'pix_cl_brack;');
				$field.val(esc_shortcode);
			});

			pix_media_frame.on('insert', function(){
				var media_attachments = pix_media_frame.state().get('selection').toJSON();

				$.each(media_attachments, function(index, el) {
					var type = this.type,
						props = wp.media.string.props('', this);

					props.link = 'embed';

					var shortcode = wp.media.string._audioVideo( type, props, this );
					$textarea.val(shortcode);
					var esc_shortcode = shortcode.replace(/\"/g, 'pix_quot;').replace(/\[/g,'pix_op_brack;').replace(/\]/g,'pix_cl_brack;');
					$field.val(esc_shortcode);
				});
			});

			pix_media_frame.open();
		});

		$this.off('click', 'input[type="button"].edit_media');
		$this.on('click', 'input[type="button"].edit_media', function( e ){
			e.preventDefault();

			var type;

			val = $textarea.val();

			if ( val==='' )
				return;

			if ( val.indexOf('[audio') == 0 ) {
				var shortcode = wp.shortcode.next( 'audio', val ).shortcode,
					defaults = wp.media.audio.defaults;

				console.log(defaults);

				pix_media_frame = wp.media({
					className: 'media-frame pix-media-frame',
					frame: 'audio',
					state: 'audio-details',
					metadata: _.defaults( shortcode.attrs.named, defaults )
				});

				pix_media_frame.state( 'audio-details' ).on( 'update', function( selection ) {
					var shortcode = wp.media.audio.shortcode( selection ).string();
					$textarea.val(shortcode);
					var esc_shortcode = shortcode.replace(/\"/g, 'pix_quot;').replace(/\[/g,'pix_op_brack;').replace(/\]/g,'pix_cl_brack;');
					$field.val(esc_shortcode);
				});
			} else if ( val.indexOf('[video') == 0 ) {
				var shortcode = wp.shortcode.next( 'video', val ).shortcode,
					defaults = wp.media.video.defaults;

				console.log(shortcode);

				pix_media_frame = wp.media({
					className: 'media-frame pix-media-frame',
					frame: 'video',
					state: 'video-details',
					metadata: _.defaults( shortcode.attrs.named, defaults )
				});

				pix_media_frame.state( 'video-details' ).on( 'update', function( selection ) {
					var shortcode = wp.media.video.shortcode( selection ).string();
					$textarea.val(shortcode);
					var esc_shortcode = shortcode.replace(/\"/g, 'pix_quot;').replace(/\[/g,'pix_op_brack;').replace(/\]/g,'pix_cl_brack;');
					$field.val(esc_shortcode);
				});
			} else if ( val.indexOf('[playlist') === 0 ) {

				var list = wp.media.playlist,
					type = '';

				var shortcode = wp.shortcode.next( list.tag, val ),
					defaultPostId = list.defaults.id,
					attachments, selection, state;

				// Bail if we didn't match the shortcode or all of the content.
				if ( ! shortcode || shortcode.content !== val ) {
					return;
				}

				// Ignore the rest of the match object.
				shortcode = shortcode.shortcode;

				if ( _.isUndefined( shortcode.get('id') ) && ! _.isUndefined( defaultPostId ) ) {
					shortcode.set( 'id', defaultPostId );
				}

				attachments = list.attachments( shortcode );

				selection = new wp.media.model.Selection( attachments.models, {
					props:    attachments.props.toJSON(),
					multiple: true
				});

				selection[ list.tag ] = attachments[ list.tag ];

				// Fetch the query's attachments, and then break ties from the
				// query to allow for sorting.
				selection.more().done( function() {
					// Break ties with the query.
					selection.props.set({ query: false });
					selection.unmirror();
					selection.props.unset('orderby');
				});

				if ( val.indexOf('video') != -1 ) {
					type = 'video-'
				}

				pix_media_frame = wp.media({
					frame:     'post',
					state:     type + 'playlist-edit',
					title:     list.editTitle,
					editing:   true,
					multiple:  true,
					selection: selection
				});

				pix_media_frame.state('playlist-edit').on( 'update', function( selection ) {
					var shortcode = wp.media.playlist.shortcode( selection ).string();
					$textarea.val(shortcode);
					var esc_shortcode = shortcode.replace(/\"/g, 'pix_quot;').replace(/\[/g,'pix_op_brack;').replace(/\]/g,'pix_cl_brack;');
					$field.val(esc_shortcode);
				});

				pix_media_frame.state('video-playlist-edit').on( 'update', function( selection ) {
					var shortcode = wp.media.playlist.shortcode( selection ).string();
					$textarea.val(shortcode);
					var esc_shortcode = shortcode.replace(/\"/g, 'pix_quot;').replace(/\[/g,'pix_op_brack;').replace(/\]/g,'pix_cl_brack;');
					$field.val(esc_shortcode);
				});


			} else {
				return;
			}

			pix_media_frame.open();
		});

    });

	/* slider */
    $('.vc_ui-panel-content .pix-vc-slider').each(function(){
		var $this = $(this),
			min = parseFloat($this.attr('data-min')),
			max = parseFloat($this.attr('data-max')),
			step = parseFloat($this.attr('data-step')),
			range = $this.attr('data-range')=='1' ? true : 'min',
			values = $this.find('input').val(),
			$amount = $this.find('.pix-vc-slider-value');
		values = values.split(',');
		$this.find('.pix-actual-slider').slider({
			range: range,
			min: min,
			max: max,
			step: step,
			values: values,
			create: function( event, ui ) {
				$this.find('.pix-actual-slider').removeClass('ui-slider');
			},
			slide: function( event, ui ) {
				if ( range == true ) {
					$amount.text( ui.values[ 0 ] + "," + ui.values[ 1 ] );
					$this.find('input').val( ui.values[ 0 ] + "," + ui.values[ 1 ] );
				} else {
					$amount.text( ui.value );
					$this.find('input').val( ui.value );
					var move = $(ui.handle).css('left')
				}
			}
		});
    });

	/* shortcodes */
	var Shortcodes = vc.shortcodes;

	window.PixVcButtonsView = vc.shortcode_view.extend( {
		changeShortcodeParams: function ( model ) {
			var params, $wrapper, $custom;

			//window.VcMessageView.__super__.changeShortcodeParams.call( this, model );
			params = model.get( 'params' );
			$wrapper = this.$el.find('> .wpb_element_wrapper');
			$custom = $('> .pix-vc-shortcode-wrap', $wrapper);

			var icon_single = params.icon_single!=='' ? '<i class="' + params.icon_single + '"></i>&nbsp;&nbsp;&nbsp;' : '';
			var icon_double = params.icon_double!=='' ? '&nbsp;&nbsp;&nbsp;<i class="' + params.icon_double + '"></i>' : '';
			var separator = '';

			if ( (icon_single + params.single_content)!='' && (params.double_content + icon_double)!='' )
				separator = '&nbsp;&nbsp;&nbsp;<span class="pix-vc-button-wrap-separator"></span>&nbsp;&nbsp;&nbsp;';

			$custom.html( icon_single + params.single_content + separator + params.double_content + icon_double );
		}
	} );

	window.PixVcIconboxesView = vc.shortcode_view.extend( {
		changeShortcodeParams: function ( model ) {
			var params, $wrapper, $custom;

			//window.VcMessageView.__super__.changeShortcodeParams.call( this, model );
			params = model.get( 'params' );
			$wrapper = this.$el.find('> .wpb_element_wrapper');
			$custom = $('> .pix-vc-shortcode-wrap', $wrapper);

			var icon = params.icon!=='' ? '<br><i class="' + params.icon + '" style="font-size:' + params.size + 'px"></i><br>' : '';

			$custom.html( icon + params.content );
		}
	} );

	window.PixVcImageboxView = vc.shortcode_view.extend( {
		changeShortcodeParams: function ( model ) {
			var params, $wrapper, $custom;

			//window.VcMessageView.__super__.changeShortcodeParams.call( this, model );

			params = model.get( 'params' );
			$wrapper = this.$el.find('> .wpb_element_wrapper');
			$custom = $('> .pix-vc-shortcode-wrap', $wrapper);

			if ( params.image != '' ) {
				var data = {
					action: 'pixedelic_get_thumb',
					content: params.image
				};
				$.post(ajaxurl, data)
					.success(function(html){
						var $image = $('<img src="' + html + '" class="attachment-thumbnail vc_general vc_element-icon">');
						$('.wpb_element_title > i', $wrapper).hide();
						$('.wpb_element_title img', $wrapper).remove();
						$('.wpb_element_title', $wrapper).append($image);
					});
				}

			$custom.html( params.content );

		}
	} );

	window.PixVcSeparatorView = vc.shortcode_view.extend( {
		changeShortcodeParams: function ( model ) {
			var params, $wrapper, $custom;

			//window.VcMessageView.__super__.changeShortcodeParams.call( this, model );

			params = model.get( 'params' );
			$wrapper = this.$el.find('> .wpb_element_wrapper');
			$custom = $('> .pix-vc-shortcode-wrap', $wrapper);

			var color = typeof params.color != 'undefined' && params.color != '' ? params.color : '#222324',
				alignment = typeof params.alignment != 'undefined' && params.alignment != '' ? params.alignment : 'center',
				floating = alignment == 'center' ? 'none' : alignment,
				height = typeof params.height != 'undefined' && params.height != '' ? params.height : '2',
				max_width = typeof params.max_width != 'undefined' && params.max_width != '' ? params.max_width : '',
				width = typeof params.width != 'undefined' && params.width != '' ? params.width : '100',

			$separator = $('<div>').css({
				backgroundColor: color,
				'float': floating,
				height: height + 'px',
				margin: '20px auto',
				maxWidth: max_width + 'px',
				width: width + '%'
			});

			$custom.html( $separator );

		}
	} );

})( window.jQuery );