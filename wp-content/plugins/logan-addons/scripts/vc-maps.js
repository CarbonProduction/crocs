(function($){

"use strict";

var PIX_MAPS = window.PIX_MAPS || {};

PIX_MAPS.init = function(){

    $('.pix_map').each(function(){

        var id = 'id' + Math.round(new Date().getTime() + (Math.random() * 100)),
            $map = $(this).attr('id', id),
            opts = $map.data('opts');

        var w = parseFloat(opts.width),
            h = parseFloat(opts.height),
            lat = opts.lat,
            lng = opts.long,
            maptype = opts.maptypectrl != "hide" ? true : false,
            typepos = opts.maptypectrl.toUpperCase(),
            pancontrol = opts.pancontrol != "hide" ? true : false,
            panpos = opts.pancontrol.toUpperCase(),
            zoomcontrol = opts.zoomcontrol != "hide" ? true : false,
            zoomcontrolsize = typeof opts.zoomcontrolsize !== "undefined" ? opts.zoomcontrolsize : "DEFAULT",
            zoompos = opts.zoomcontrol.toUpperCase(),
            scalecontrol = opts.scalecontrol != "hide" ? true : false,
            swcontrol = opts.swcontrol != "hide" ? true : false,
            swpos = opts.swcontrol.toUpperCase();

        var mapSize = function() {
            $map.css({
                width: w+'%'
            });
            var mapW = $map.width(),
                mapH = mapW * h * 0.01;
            $map.css({
                height: mapH
            });
        };
        mapSize();
        $(window).on("resize map-refresh", mapSize);

        var map;

        if (opts.type == "streetview") {
            map = GMaps.createPanorama({
                el: '#' + id,
                lat: lat,
                lng: lng,
                scrollwheel: false,
                panControlOptions: {
                    position: google.maps.ControlPosition[panpos]
                },
                zoomControlOptions: {
                    position: google.maps.ControlPosition[zoompos],
                    style: google.maps.ZoomControlStyle[zoomcontrolsize]
                },
                pov: {
                    heading: parseFloat(opts.heading),
                    pitch: parseFloat(opts.pitch),
                    zoom: Math.floor(parseFloat(opts.zoom) / 3) - 1
                }
            });
        } else {
            map = new GMaps({
                el: $map.get(0),
                lat: lat,
                lng: lng,
                scrollwheel: false,
                zoom: parseFloat(opts.zoom),
                mapTypeId: opts.type,
                mapTypeControl: maptype,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition[typepos]
                },
                panControl: pancontrol,
                panControlOptions: {
                    position: google.maps.ControlPosition[panpos]
                },
                zoomControl: zoomcontrol,
                zoomControlOptions: {
                    position: google.maps.ControlPosition[zoompos],
                    style: google.maps.ZoomControlStyle[zoomcontrolsize]
                },
                scaleControl: scalecontrol,
                streetViewControl: swcontrol,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition[swpos]
                },
                styles: $map.data('styles')
            });

            map.setCenter(lat, lng);

            $(window).on('resize map-refresh', function(){
                map.setCenter(lat, lng);
                map.refresh();
            });

            if ( opts.marker === '' ) {

                map.addMarker({
                    lat: lat,
                    lng: lng
                });

            } else if ( opts.marker == 'custom' && opts.custom_marker !== '' ) {
                var u = new Image;
                u.onload = function() {
                    var e = u.height / 2,
                        r = u.width / 2,
                        n = new google.maps.MarkerImage(opts.custom_marker, null, null, null, new google.maps.Size(r, e));
                    map.addMarker({
                        lat: lat,
                        lng: lng,
                        icon: n
                    })
                };
                u.src = opts.custom_marker;
            }
        }

   });

};

$(function(){
    PIX_MAPS.init();
});

})(jQuery);