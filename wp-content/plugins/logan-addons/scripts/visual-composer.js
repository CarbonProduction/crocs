function pix_vc_google_map_custom_json_sanitize(){
	var $ = jQuery,
		$txtarea = $('.custom_style'),
		set;

	var val = $txtarea.val();

	val = val.replace(/BRACK_/g, '[');
	val = val.replace(/_KCARB/g, ']');
	val = val.replace(/_QUOTES_/g, '"');
	val = val.replace(/_QUOTE_/g, '\'');

	$txtarea.val(val);

	$('[data-vc-ui-element="button-close"], [data-vc-ui-element="button-save"]').on('click', function(e){
		e.preventDefault();

		var val = $txtarea.val();

		val = val.replace(/\[/g, 'BRACK_');
		val = val.replace(/\]/g, '_KCARB');
		val = val.replace(/"/g, '_QUOTES_');
		val = val.replace(/\'/g, '_QUOTE_');
		val = val.replace(/(\r\n|\n|\r)/gm,"");

		$txtarea.val(val);

	});
}