<?php
add_filter('init', 'pix_vc_support_post_types');
if ( ! function_exists('pix_vc_support_post_types') ) :
/**
 */
function pix_vc_support_post_types(){

	$list = array(
	    'page',
	    'jetpack-portfolio'
	);
	vc_set_default_editor_post_types( $list );

}
endif;//pix_vc_support_post_types

if ( !function_exists('str_lreplace')) :
/**
* A function to replace the last occurence
*/
function str_lreplace($search, $replace, $subject) {
    $pos = strrpos($subject, $search);

    if($pos !== false)
    {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
}
endif;

add_action( 'save_post', 'logan_vc_button_custom_css' );
if ( !function_exists('logan_vc_button_custom_css')) :
/**
*/
function logan_vc_button_custom_css( $post_id ) {

    if ( wp_is_post_revision( $post_id ) )
		return;

	$post = get_post($post_id);
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return;

	$postarr = get_post($post_id,'ARRAY_A');
	$content = $postarr['post_content'];

	$prev_attr = array();
	$again = false;

	/* buttons */

	preg_match_all( '/\[pix_button(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		if ( in_array( $attr, $prev_attr ) ) {
			$again = true;
		}
		$prev_attr[] = $attr;
	}

	if ( $again ) {

		$content = preg_replace_callback(
		    '/\[pix_button(.*?)custom_css_uniq_id="(.*?)"/',
		    create_function('$m','
		    	global $check_id;
		        $uniq_id = uniqid("pix-custom-css-");
		        return "[pix_button".$m[1]."custom_css_uniq_id=\"".$uniq_id."\"";
		    '),
		    $content);

	}
	
	$content = preg_replace( '/\[pix_button(.*?)custom_css_uniq_id="(.*?)"(.*?)\]/', '[pix_button$1$3custom_css_uniq_id="$2"]' , $content );
	$content = preg_replace( '/\"custom_css_uniq_id/', '" custom_css_uniq_id' , $content );

	$meta_css = '';
	preg_match_all( '/\[pix_button(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		$selector = '.logan .' . esc_attr($attr) . ' > a.logan-button';
		$selector_1 = '.logan .' . esc_attr($attr) . ' > a.logan-button:first-child';
		$selector_2 = '.logan .' . esc_attr($attr) . ' > a.logan-button:last-child';

		$a_style = '';
		$a_hover_style = '';

		$a_1_style = '';
		$a_1_hover_style = '';
		$i_1_style = '';

		$a_2_style = '';
		$a_2_hover_style = '';
		$i_2_style = '';

		preg_match( '/radius="(.*?)"/i', $shortcodes[1][$index], $radius );
		if ( !empty($radius) )
			$a_style .= '-moz-border-radius:' . intval($radius[1]) . 'px!important; border-radius:' . intval($radius[1]) . 'px!important;';

		preg_match( '/border_width="(.*?)"/i', $shortcodes[1][$index], $border_width );
		if ( !empty($border_width) )
			$a_style .= 'border-width:' . intval($border_width[1]) . 'px!important;';

		preg_match( '/single_bg_color="(.*?)"/i', $shortcodes[1][$index], $single_bg_color );
		if ( !empty($single_bg_color) )
			$a_style .= 'background-color:' . esc_attr($single_bg_color[1]) . '!important;';

		preg_match( '/single_text_color="(.*?)"/i', $shortcodes[1][$index], $single_text_color );
		if ( !empty($single_text_color) )
			$a_style .= 'color:' . esc_attr($single_text_color[1]) . '!important;';

		preg_match( '/text_size="(.*?)"/i', $shortcodes[1][$index], $text_size );
		if ( !empty($text_size) )
			$a_style .= 'font-size:' . esc_attr($text_size[1]) . 'px!important;';

		preg_match( '/single_border_color="(.*?)"/i', $shortcodes[1][$index], $single_border_color );
		if ( !empty($single_border_color) )
			$a_style .= 'border-color:' . esc_attr($single_border_color[1]) . '!important;';

		if ( $a_style!='' )
			$meta_css .= $selector . ' { ' . $a_style . ' } ';

		preg_match( '/icon_size="(.*?)"/i', $shortcodes[1][$index], $icon_size );
		if ( !empty($icon_size) )
			$i_1_style .= 'font-size:' . intval($icon_size[1]) . 'px!important;';

		preg_match( '/single_icon_color="(.*?)"/i', $shortcodes[1][$index], $single_icon_color );
		if ( !empty($single_icon_color) )
			$i_1_style .= 'color:' . esc_attr($single_icon_color[1]) . '!important;';

		if ( $i_1_style!='' )
			$meta_css .= $selector . ' > i { ' . $i_1_style . ' } ';

		preg_match( '/single_text_color_hover="(.*?)"/i', $shortcodes[1][$index], $single_text_color_hover );
		if ( !empty($single_text_color_hover) )
			$a_hover_style .= 'color:' . esc_attr($single_text_color_hover[1]) . '!important;';

		preg_match( '/single_bg_color_hover="(.*?)"/i', $shortcodes[1][$index], $single_bg_color_hover );
		if ( !empty($single_bg_color_hover) )
			$a_hover_style .= 'background-color:' . esc_attr($single_bg_color_hover[1]) . '!important;';

		preg_match( '/single_border_color_hover="(.*?)"/i', $shortcodes[1][$index], $single_border_color_hover );
		if ( !empty($single_border_color_hover) )
			$a_hover_style .= 'border-color:' . esc_attr($single_border_color_hover[1]) . '!important;';

		if ( !empty($border_width) )
			$a_hover_style .= 'border-width:' . intval($border_width[1]) . 'px!important;';

		if ( $a_hover_style!='' )
			$meta_css .= $selector . ':hover { ' . $a_hover_style . ' } ';

		preg_match( '/single_icon_color_hover="(.*?)"/i', $shortcodes[1][$index], $single_icon_color_hover );
		if ( !empty($single_icon_color_hover) )
			$meta_css .= $selector . ':hover > i { color:' . esc_attr($single_icon_color_hover[1]) . '!important; }';

		/* DOUBLE */

		preg_match( '/double_bg_color="(.*?)"/i', $shortcodes[1][$index], $double_bg_color );
		if ( !empty($double_bg_color) )
			$a_2_style .= 'background-color:' . esc_attr($double_bg_color[1]) . '!important;';

		preg_match( '/double_text_color="(.*?)"/i', $shortcodes[1][$index], $double_text_color );
		if ( !empty($double_text_color) )
			$a_2_style .= 'color:' . esc_attr($double_text_color[1]) . '!important;';

		preg_match( '/double_border_color="(.*?)"/i', $shortcodes[1][$index], $double_border_color );
		if ( !empty($double_border_color) )
			$a_2_style .= 'border-color:' . esc_attr($double_border_color[1]) . '!important;';

		if ( $a_2_style!='' )
			$meta_css .= $selector_2 . ' { ' . $a_2_style . ' } ';

		preg_match( '/double_icon_color="(.*?)"/i', $shortcodes[1][$index], $double_icon_color );
		if ( !empty($double_icon_color) )
			$i_2_style .= 'color:' . esc_attr($double_icon_color[1]) . '!important;';

		if ( $i_2_style!='' )
			$meta_css .= $selector_2 . ' > i { ' . $i_2_style . ' } ';

		preg_match( '/double_text_color_hover="(.*?)"/i', $shortcodes[1][$index], $double_text_color_hover );
		if ( !empty($double_text_color_hover) )
			$a_2_hover_style .= 'color:' . esc_attr($double_text_color_hover[1]) . '!important;';

		preg_match( '/double_bg_color_hover="(.*?)"/i', $shortcodes[1][$index], $double_bg_color_hover );
		if ( !empty($double_bg_color_hover) )
			$a_2_hover_style .= 'background-color:' . esc_attr($double_bg_color_hover[1]) . '!important;';

		preg_match( '/double_border_color_hover="(.*?)"/i', $shortcodes[1][$index], $double_border_color_hover );
		if ( !empty($double_border_color_hover) )
			$a_2_hover_style .= 'border-color:' . esc_attr($double_border_color_hover[1]) . '!important;';

		if ( $a_2_hover_style!='' )
			$meta_css .= $selector_2 . ':hover { ' . $a_hover_style . ' } ';

		preg_match( '/double_icon_color_hover="(.*?)"/i', $shortcodes[1][$index], $double_icon_color_hover );
		if ( !empty($double_icon_color_hover) )
			$meta_css .= $selector_2 . ':hover > i { color:' . esc_attr($double_icon_color_hover[1]) . '!important; }';

		/* SEPARATOR */

		preg_match( '/icon_single="(.*?)"/i', $shortcodes[1][$index], $icon_single );
		preg_match( '/single_content="(.*?)"/i', $shortcodes[1][$index], $single_content );
		preg_match( '/icon_double="(.*?)"/i', $shortcodes[1][$index], $icon_double );
		preg_match( '/double_content="(.*?)"/i', $shortcodes[1][$index], $double_content );

		if ( ( !empty($icon_single) || !empty($single_content) ) &&  ( !empty($icon_double) || !empty($double_content) ) ) {

			if ( is_rtl() ) {
				$meta_css .= $selector_2 . ' { border-right-width: 0!important; -moz-border-radius-topright: 0!important; border-top-right-radius: 0!important; -moz-border-radius-bottomright: 0!important; border-bottom-right-radius: 0!important; }';
				$meta_css .= $selector_1 . ' { border-left-width: 0!important; -moz-border-radius-topleft: 0!important; border-top-left-radius: 0!important; -moz-border-radius-bottomleft: 0!important; border-bottom-left-radius: 0!important; }';
			} else {
				$meta_css .= $selector_1 . ' { border-right-width: 0!important; -moz-border-radius-topright: 0!important; border-top-right-radius: 0!important; -moz-border-radius-bottomright: 0!important; border-bottom-right-radius: 0!important; }';
				$meta_css .= $selector_2 . ' { border-left-width: 0!important; -moz-border-radius-topleft: 0!important; border-top-left-radius: 0!important; -moz-border-radius-bottomleft: 0!important; border-bottom-left-radius: 0!important; }';
			}

			preg_match( '/separator="(.*?)"/i', $shortcodes[1][$index], $separator );

			if ( !empty($separator) ) {

				$meta_css .= '.logan .' . esc_attr($attr) . ' > .logan-button-separator { ';

				preg_match( '/separator_text_color="(.*?)"/i', $shortcodes[1][$index], $separator_text_color );
				if ( !empty($separator_text_color) )
					$meta_css .= 'color:' . esc_attr($separator_text_color[1]) . '!important;';

				preg_match( '/separator_border_color="(.*?)"/i', $shortcodes[1][$index], $separator_border_color );
				if ( !empty($separator_border_color) )
					$meta_css .= 'border-color:' . esc_attr($separator_border_color[1]) . '!important;';

				preg_match( '/separator_bg_color="(.*?)"/i', $shortcodes[1][$index], $separator_bg_color );
				if ( !empty($separator_bg_color) )
					$meta_css .= 'background-color:' . esc_attr($separator_bg_color[1]) . '!important;';

				$meta_css .= ' }';

			}

			if ( !empty($separator) ) {

				$meta_css .= '.logan .' . esc_attr($attr) . ' > .logan-button-separator-border { ';

				preg_match( '/separator_border_width="(.*?)"/i', $shortcodes[1][$index], $separator_border_width );
				if ( !empty($separator_border_width) && $separator == 'border' ) {
					$meta_css .= 'width:' . esc_attr($separator_border_width[1]) . 'px!important;';
					$meta_css .= 'top:' . esc_attr($separator_border_width[1]) . 'px!important;';
					$meta_css .= 'bottom:' . esc_attr($separator_border_width[1]) . 'px!important;';
				} elseif ( !empty($border_width) ) {
					$meta_css .= 'width:' . esc_attr($border_width[1]) . 'px!important;';
					$meta_css .= 'top:' . esc_attr($border_width[1]) . 'px!important;';
					$meta_css .= 'bottom:' . esc_attr($border_width[1]) . 'px!important;';
				}
				if ( !empty($separator_border_color) && $separator[1] == 'border' ) //line 193
					$meta_css .= 'background-color:' . esc_attr($separator_border_color[1]) . '!important;';
				elseif ( !empty($single_border_color) && empty($separator_border_color) )
					$meta_css .= 'background-color:' . esc_attr($single_border_color[1]) . '!important;';

				$meta_css .= ' }';

			}
		}
	}

	if ( $meta_css=='' ) {
		delete_post_meta( $post_id, '_logan_shortcodes_custom_css' );
	} else {
		update_post_meta( $post_id, '_logan_shortcodes_custom_css', $meta_css );
	}

	if ( $postarr['post_content']==$content )
		return;

	$postarr['post_content'] = $content;
	$post_id = wp_update_post($postarr);

}
endif;

add_action( 'save_post', 'logan_vc_woo_custom_css' );
if ( !function_exists('logan_vc_woo_custom_css')) :
/**
* @since Logan 1.0
*/
function logan_vc_woo_custom_css( $post_id ) {

    if ( wp_is_post_revision( $post_id ) )
		return;

	$post = get_post($post_id);
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return;

	$postarr = get_post($post_id,'ARRAY_A');
	$content = $postarr['post_content'];

	$meta_css = '';
	$prev_attr = array();

	/* product_category */

	$again_product_category = false;
	preg_match_all( '/\[product_category(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		if ( in_array( $attr, $prev_attr ) ) {
			$again_product_category = true;
		}
		$prev_attr[] = $attr;
	}

	if ( $again_product_category ) {

		$content = preg_replace_callback(
		    '/\[product_category(.*?)custom_css_uniq_id="(.*?)"/',
		    create_function('$m','
		    	global $check_id;
		        $uniq_id = uniqid("pix-custom-css-");
		        return "[product_category".$m[1]."custom_css_uniq_id=\"".$uniq_id."\"";
		    '),
		    $content);

	}
	
	$content = preg_replace( '/\[product_category(.*?)custom_css_uniq_id="(.*?)"(.*?)\]/', '[product_category$1$3custom_css_uniq_id="$2"]' , $content );
	$content = preg_replace( '/\"custom_css_uniq_id/', '" custom_css_uniq_id' , $content );

	preg_match_all( '/\[product_category(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		$selector = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products {';
		$selector_1 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li {';
		$selector_2 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li > a {';

		preg_match( '/gutter="(.*?)"/i', $shortcodes[1][$index], $gutter );
		if ( !empty($gutter) ) {
	        $padding_a = intval($gutter[1]) >= 20 ? 0 : 20-intval($gutter[1]);
			$selector .= 'margin-left: -' . intval($gutter[1]) . 'px!important; padding-top: ' . intval($gutter[1]) . 'px!important;';
			$selector_1 .= 'padding: 0 0 ' . intval($gutter[1]) . 'px ' . intval($gutter[1]) . 'px!important;';
			$selector_2 .= 'padding: 20px ' . $padding_a . 'px ' . $padding_a . 'px!important;';
		}

		$selector .= '}';
		$selector_1 .= '}';
		$selector_2 .= '}';

		$meta_css .= $selector . $selector_1 . $selector_2;

	}

	/* featured_products */

	$again_featured_products = false;
	preg_match_all( '/\[featured_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		if ( in_array( $attr, $prev_attr ) ) {
			$again_featured_products = true;
		}
		$prev_attr[] = $attr;
	}

	if ( $again_featured_products ) {

		$content = preg_replace_callback(
		    '/\[featured_products(.*?)custom_css_uniq_id="(.*?)"/',
		    create_function('$m','
		    	global $check_id;
		        $uniq_id = uniqid("pix-custom-css-");
		        return "[featured_products".$m[1]."custom_css_uniq_id=\"".$uniq_id."\"";
		    '),
		    $content);

	}
	
	$content = preg_replace( '/\[featured_products(.*?)custom_css_uniq_id="(.*?)"(.*?)\]/', '[featured_products$1$3custom_css_uniq_id="$2"]' , $content );
	$content = preg_replace( '/\"custom_css_uniq_id/', '" custom_css_uniq_id' , $content );

	preg_match_all( '/\[featured_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		$selector = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products {';
		$selector_1 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li {';
		$selector_2 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li > a {';

		preg_match( '/gutter="(.*?)"/i', $shortcodes[1][$index], $gutter );
		if ( !empty($gutter) ) {
	        $padding_a = intval($gutter[1]) >= 20 ? 0 : 20-intval($gutter[1]);
			$selector .= 'margin-left: -' . intval($gutter[1]) . 'px!important; padding-top: ' . intval($gutter[1]) . 'px!important;';
			$selector_1 .= 'padding: 0 0 ' . intval($gutter[1]) . 'px ' . intval($gutter[1]) . 'px!important;';
			$selector_2 .= 'padding: 20px ' . $padding_a . 'px ' . $padding_a . 'px!important;';
		}

		$selector .= '}';
		$selector_1 .= '}';
		$selector_2 .= '}';

		$meta_css .= $selector . $selector_1 . $selector_2;

	}

	/* recent_products */

	$again_recent_products = false;
	preg_match_all( '/\[recent_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		if ( in_array( $attr, $prev_attr ) ) {
			$again_recent_products = true;
		}
		$prev_attr[] = $attr;
	}

	if ( $again_recent_products ) {

		$content = preg_replace_callback(
		    '/\[recent_products(.*?)custom_css_uniq_id="(.*?)"/',
		    create_function('$m','
		    	global $check_id;
		        $uniq_id = uniqid("pix-custom-css-");
		        return "[recent_products".$m[1]."custom_css_uniq_id=\"".$uniq_id."\"";
		    '),
		    $content);

	}
	
	$content = preg_replace( '/\[recent_products(.*?)custom_css_uniq_id="(.*?)"(.*?)\]/', '[recent_products$1$3custom_css_uniq_id="$2"]' , $content );
	$content = preg_replace( '/\"custom_css_uniq_id/', '" custom_css_uniq_id' , $content );

	preg_match_all( '/\[recent_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		$selector = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products {';
		$selector_1 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li {';
		$selector_2 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li > a {';

		preg_match( '/gutter="(.*?)"/i', $shortcodes[1][$index], $gutter );
		if ( !empty($gutter) ) {
	        $padding_a = intval($gutter[1]) >= 20 ? 0 : 20-intval($gutter[1]);
			$selector .= 'margin-left: -' . intval($gutter[1]) . 'px!important; padding-top: ' . intval($gutter[1]) . 'px!important;';
			$selector_1 .= 'padding: 0 0 ' . intval($gutter[1]) . 'px ' . intval($gutter[1]) . 'px!important;';
			$selector_2 .= 'padding: 20px ' . $padding_a . 'px ' . $padding_a . 'px!important;';
		}

		$selector .= '}';
		$selector_1 .= '}';
		$selector_2 .= '}';

		$meta_css .= $selector . $selector_1 . $selector_2;

	}

	/* products */

	$again_products = false;
	preg_match_all( '/\[products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		if ( in_array( $attr, $prev_attr ) ) {
			$again_products = true;
		}
		$prev_attr[] = $attr;
	}

	if ( $again_products ) {

		$content = preg_replace_callback(
		    '/\[products(.*?)custom_css_uniq_id="(.*?)"/',
		    create_function('$m','
		    	global $check_id;
		        $uniq_id = uniqid("pix-custom-css-");
		        return "[products".$m[1]."custom_css_uniq_id=\"".$uniq_id."\"";
		    '),
		    $content);

	}
	
	$content = preg_replace( '/\[products(.*?)custom_css_uniq_id="(.*?)"(.*?)\]/', '[products$1$3custom_css_uniq_id="$2"]' , $content );
	$content = preg_replace( '/\"custom_css_uniq_id/', '" custom_css_uniq_id' , $content );

	preg_match_all( '/\[products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		$selector = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products {';
		$selector_1 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li {';
		$selector_2 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li > a {';

		preg_match( '/gutter="(.*?)"/i', $shortcodes[1][$index], $gutter );
		if ( !empty($gutter) ) {
	        $padding_a = intval($gutter[1]) >= 20 ? 0 : 20-intval($gutter[1]);
			$selector .= 'margin-left: -' . intval($gutter[1]) . 'px!important; padding-top: ' . intval($gutter[1]) . 'px!important;';
			$selector_1 .= 'padding: 0 0 ' . intval($gutter[1]) . 'px ' . intval($gutter[1]) . 'px!important;';
			$selector_2 .= 'padding: 20px ' . $padding_a . 'px ' . $padding_a . 'px!important;';
		}

		$selector .= '}';
		$selector_1 .= '}';
		$selector_2 .= '}';

		$meta_css .= $selector . $selector_1 . $selector_2;

	}

	/* sale_products */

	$again_sale_products = false;
	preg_match_all( '/\[sale_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		if ( in_array( $attr, $prev_attr ) ) {
			$again_sale_products = true;
		}
		$prev_attr[] = $attr;
	}

	if ( $again_sale_products ) {

		$content = preg_replace_callback(
		    '/\[sale_products(.*?)custom_css_uniq_id="(.*?)"/',
		    create_function('$m','
		    	global $check_id;
		        $uniq_id = uniqid("pix-custom-css-");
		        return "[sale_products".$m[1]."custom_css_uniq_id=\"".$uniq_id."\"";
		    '),
		    $content);

	}
	
	$content = preg_replace( '/\[sale_products(.*?)custom_css_uniq_id="(.*?)"(.*?)\]/', '[sale_products$1$3custom_css_uniq_id="$2"]' , $content );
	$content = preg_replace( '/\"custom_css_uniq_id/', '" custom_css_uniq_id' , $content );

	preg_match_all( '/\[sale_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		$selector = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products {';
		$selector_1 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li {';
		$selector_2 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li > a {';

		preg_match( '/gutter="(.*?)"/i', $shortcodes[1][$index], $gutter );
		if ( !empty($gutter) ) {
	        $padding_a = intval($gutter[1]) >= 20 ? 0 : 20-intval($gutter[1]);
			$selector .= 'margin-left: -' . intval($gutter[1]) . 'px!important; padding-top: ' . intval($gutter[1]) . 'px!important;';
			$selector_1 .= 'padding: 0 0 ' . intval($gutter[1]) . 'px ' . intval($gutter[1]) . 'px!important;';
			$selector_2 .= 'padding: 20px ' . $padding_a . 'px ' . $padding_a . 'px!important;';
		}

		$selector .= '}';
		$selector_1 .= '}';
		$selector_2 .= '}';

		$meta_css .= $selector . $selector_1 . $selector_2;

	}

	/* best_selling_products */

	$again_best_selling_products = false;
	preg_match_all( '/\[best_selling_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		if ( in_array( $attr, $prev_attr ) ) {
			$again_best_selling_products = true;
		}
		$prev_attr[] = $attr;
	}

	if ( $again_best_selling_products ) {

		$content = preg_replace_callback(
		    '/\[best_selling_products(.*?)custom_css_uniq_id="(.*?)"/',
		    create_function('$m','
		    	global $check_id;
		        $uniq_id = uniqid("pix-custom-css-");
		        return "[best_selling_products".$m[1]."custom_css_uniq_id=\"".$uniq_id."\"";
		    '),
		    $content);

	}
	
	$content = preg_replace( '/\[best_selling_products(.*?)custom_css_uniq_id="(.*?)"(.*?)\]/', '[best_selling_products$1$3custom_css_uniq_id="$2"]' , $content );
	$content = preg_replace( '/\"custom_css_uniq_id/', '" custom_css_uniq_id' , $content );

	preg_match_all( '/\[best_selling_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		$selector = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products {';
		$selector_1 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li {';
		$selector_2 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li > a {';

		preg_match( '/gutter="(.*?)"/i', $shortcodes[1][$index], $gutter );
		if ( !empty($gutter) ) {
	        $padding_a = intval($gutter[1]) >= 20 ? 0 : 20-intval($gutter[1]);
			$selector .= 'margin-left: -' . intval($gutter[1]) . 'px!important; padding-top: ' . intval($gutter[1]) . 'px!important;';
			$selector_1 .= 'padding: 0 0 ' . intval($gutter[1]) . 'px ' . intval($gutter[1]) . 'px!important;';
			$selector_2 .= 'padding: 20px ' . $padding_a . 'px ' . $padding_a . 'px!important;';
		}

		$selector .= '}';
		$selector_1 .= '}';
		$selector_2 .= '}';

		$meta_css .= $selector . $selector_1 . $selector_2;

	}

	/* top_rated_products */

	$again_top_rated_products = false;
	preg_match_all( '/\[top_rated_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		if ( in_array( $attr, $prev_attr ) ) {
			$again_top_rated_products = true;
		}
		$prev_attr[] = $attr;
	}

	if ( $again_top_rated_products ) {

		$content = preg_replace_callback(
		    '/\[top_rated_products(.*?)custom_css_uniq_id="(.*?)"/',
		    create_function('$m','
		    	global $check_id;
		        $uniq_id = uniqid("pix-custom-css-");
		        return "[top_rated_products".$m[1]."custom_css_uniq_id=\"".$uniq_id."\"";
		    '),
		    $content);

	}
	
	$content = preg_replace( '/\[top_rated_products(.*?)custom_css_uniq_id="(.*?)"(.*?)\]/', '[top_rated_products$1$3custom_css_uniq_id="$2"]' , $content );
	$content = preg_replace( '/\"custom_css_uniq_id/', '" custom_css_uniq_id' , $content );

	preg_match_all( '/\[top_rated_products(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		$selector = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products {';
		$selector_1 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li {';
		$selector_2 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li > a {';

		preg_match( '/gutter="(.*?)"/i', $shortcodes[1][$index], $gutter );
		if ( !empty($gutter) ) {
	        $padding_a = intval($gutter[1]) >= 20 ? 0 : 20-intval($gutter[1]);
			$selector .= 'margin-left: -' . intval($gutter[1]) . 'px!important; padding-top: ' . intval($gutter[1]) . 'px!important;';
			$selector_1 .= 'padding: 0 0 ' . intval($gutter[1]) . 'px ' . intval($gutter[1]) . 'px!important;';
			$selector_2 .= 'padding: 20px ' . $padding_a . 'px ' . $padding_a . 'px!important;';
		}

		$selector .= '}';
		$selector_1 .= '}';
		$selector_2 .= '}';

		$meta_css .= $selector . $selector_1 . $selector_2;

	}

	/* product_attribute */

	$again_product_attribute = false;
	preg_match_all( '/\[product_attribute(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		if ( in_array( $attr, $prev_attr ) ) {
			$again_product_attribute = true;
		}
		$prev_attr[] = $attr;
	}

	if ( $again_product_attribute ) {

		$content = preg_replace_callback(
		    '/\[product_attribute(.*?)custom_css_uniq_id="(.*?)"/',
		    create_function('$m','
		    	global $check_id;
		        $uniq_id = uniqid("pix-custom-css-");
		        return "[product_attribute".$m[1]."custom_css_uniq_id=\"".$uniq_id."\"";
		    '),
		    $content);

	}
	
	$content = preg_replace( '/\[product_attribute(.*?)custom_css_uniq_id="(.*?)"(.*?)\]/', '[product_attribute$1$3custom_css_uniq_id="$2"]' , $content );
	$content = preg_replace( '/\"custom_css_uniq_id/', '" custom_css_uniq_id' , $content );

	preg_match_all( '/\[product_attribute(.*?)custom_css_uniq_id="(.*?)"/', $content, $shortcodes );
	foreach ( $shortcodes[2] as $index => $attr ) {
		$selector = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products {';
		$selector_1 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li {';
		$selector_2 = '.logan .woocommerce.' . esc_attr($attr) . ' ul.products li > a {';

		preg_match( '/gutter="(.*?)"/i', $shortcodes[1][$index], $gutter );
		if ( !empty($gutter) ) {
	        $padding_a = intval($gutter[1]) >= 20 ? 0 : 20-intval($gutter[1]);
			$selector .= 'margin-left: -' . intval($gutter[1]) . 'px!important; padding-top: ' . intval($gutter[1]) . 'px!important;';
			$selector_1 .= 'padding: 0 0 ' . intval($gutter[1]) . 'px ' . intval($gutter[1]) . 'px!important;';
			$selector_2 .= 'padding: 20px ' . $padding_a . 'px ' . $padding_a . 'px!important;';
		}

		$selector .= '}';
		$selector_1 .= '}';
		$selector_2 .= '}';

		$meta_css .= $selector . $selector_1 . $selector_2;

	}

	if ( $meta_css=='' ) {
		delete_post_meta( $post_id, '_logan_shortcodes_woo_css' );
	} else {
		update_post_meta( $post_id, '_logan_shortcodes_woo_css', $meta_css );
	}

	if ( $postarr['post_content']==$content )
		return;

	$postarr['post_content'] = $content;
	$post_id = wp_update_post($postarr);

}
endif;

add_action('wp_head','logan_print_custom_vc_css_shortcodes', 100000);
if ( !function_exists('logan_print_custom_vc_css_shortcodes')) :
/**
* @since Logan 1.0
*/
function logan_print_custom_vc_css_shortcodes() {
	global $post;

	if ( !$post )
		return;

	$css = get_post_meta( $post->ID, '_logan_shortcodes_custom_css', true );

	if ( $css != '' ) {
		echo '<style type="text/css" data-type="pix_vc_shortcodes-custom-css">' . $css . '</style>';
	}

	$woo = get_post_meta( $post->ID, '_logan_shortcodes_woo_css', true );

	if ( $woo != '' ) {
		echo '<style type="text/css" data-type="pix_vc_shortcodes-woo-css">' . $woo . '</style>';
	}

}
endif;


add_filter('vc_shortcode_output', 'logan_vc_shortcode_output', 10, 3);
if ( !function_exists('logan_vc_shortcode_output')) :
/**
* Filters the output to add an inner shadow
* @since Logan 1.0
*/
function logan_vc_shortcode_output($output, $obj, $atts) {
	if($obj->settings('base')=='vc_row' || $obj->settings('base')=='vc_row_inner' ) {
		$shadow = logan_vc_shadow_shortcode($atts, '');
		$full = '<div class="vc_row-full-width"></div>';
		if (strpos( $output, $full) !== false) {
			$output = str_lreplace( '</div><div class="vc_row-full-width">', $shadow . '</div><div class="vc_row-full-width">', $output );
		} else {
			$output = str_lreplace( '</div>', $shadow . '</div>', $output );
		}

		$overlay = logan_vc_overlay_color($atts, '');
		if ( $overlay!='' )
			$output = preg_replace( '/ class="vc_row ([^\>]*)>/s', ' class="vc_row $1>' . $overlay, $output, 1 );

		/*$pos = logan_vc_cell_pos($atts, '');
		if ( $pos != '' )
			$output = preg_replace( '/ class="vc_row /', ' class="vc_row vc_vertical_pos vc_vertical_pos_' . $pos . ' ' , $output, 1 );*/

		$height = logan_vc_row_max_h($atts, '');
		if ( $height != '' ) {
			if ( preg_match('/class="vc_row ([^\>]*)style="(.*?)"/i', $output) ) {
				$output = preg_replace( '/class="vc_row ([^\>]*) style="(.*?)"/i', 'class="vc_row $1 style="$2; max-height:' . intval($height) . 'px;"', $output, 1 );
				$output = preg_replace( '/style="(.*?);;(.*?)"/i', 'style="$1;$2"', $output, 1 );
			} elseif ( preg_match('/<div ([^\>]*) style="(.*?)" ([^\>]*) class="vc_row /i', $output) ) {
				$output = preg_replace( '/<div ([^\>]*) style="(.*?)" ([^\>]*) class="vc_row /i', '<div $1 style="$2; max-height:' . intval($height) . 'px;" $3 class="vc_row ', $output, 1 );
				$output = preg_replace( '/style="(.*?);;(.*?)"/i', 'style="$1;$2"', $output, 1 );
			} else {
				$output = preg_replace( '/ class="vc_row ([^\"]*)"/i', ' class="vc_row $1" style="max-height:' . intval($height) . 'px;"', $output, 1 );
			} 
		}

		$parallax = logan_vc_row_parallax($atts, '');
		if ( $parallax != '' )
			$output = preg_replace( '/ class="vc_row /', $parallax . 'class="vc_row ' , $output, 1 );

		$overflow = logan_vc_overflow_elem($atts, '');
		if ( $overflow != '' )
			$output = preg_replace( '/ class="vc_row /', ' class="vc_row ' . $overflow .' ', $output, 1 );
	}

	if($obj->settings('base')=='rev_slider_vc') {
		$data = logan_vc_parallax_rev_shortcode($atts, '');
		$output = preg_replace( '/<div class="wpb_revslider_element wpb_content_element">/', '<div class="wpb_revslider_element wpb_content_element"' . $data .'>', $output, 1 );
	}

	if($obj->settings('base')=='vc_column' || $obj->settings('base')=='vc_column_inner' ) {
		$stick = logan_vc_sticky_column($atts, '');
		if ( $stick != '' )
			$output = preg_replace( '/ class="wpb_column /', ' class="wpb_column ' . $stick .' ', $output, 1 );

		$overflow = logan_vc_overflow_elem($atts, '');
		if ( $overflow != '' )
			$output = preg_replace( '/ class="vc_column-inner /', ' class="vc_column-inner ' . $overflow .' ', $output, 1 );
	}

	if($obj->settings('base')=='vc_column_text') {
		$data = logan_vc_text_column_shadow($atts, '');
		$output = preg_replace( '/ class="wpb_text_column /', ' class="wpb_text_column ' . $data .' ', $output, 1 );
	}

	if($obj->settings('base')=='vc_tta_tabs') {
		$custom = logan_vc_tabs_custom($atts, '');
		$output = preg_replace( '/ class="vc_tta-container"(.+?)>/i', " class=\"vc_tta-container\"$1 $custom>", $output, 1 );
		$output = preg_replace( '/ vc_tta-style-([^\"]*) /i', " ", $output, 1 );
		$output = preg_replace( '/ class="vc_general ([^\"]*)"/i', " class=\"vc_general $1 vc_tta-style-classic\"", $output, 1 );
	}

	if($obj->settings('base')=='vc_tta_accordion') {
		$output = preg_replace( '/ vc_tta-style-([^\"]*) /i', " ", $output, 1 );
		$output = preg_replace( '/ class="vc_general ([^\"]*)"/i', " class=\"vc_general $1 vc_tta-style-logan\"", $output, 1 );
	}

	if($obj->settings('base')=='vc_tta_tour') {
		$output = preg_replace( '/ vc_tta-style-([^\"]*) /i', " ", $output, 1 );
		$output = preg_replace( '/ class="vc_general ([^\"]*)"/i', " class=\"vc_general $1 vc_tta-style-classic\"", $output, 1 );
	}

	if($obj->settings('base')=='vc_message') {
		$icon = logan_vc_custom_icon($atts, '');
		if ( $icon!='' )
			$output = preg_replace( '/<div class="vc_message_box-icon">(.*?)<\/div>/s', '<div class="vc_message_box-icon"><i class="' . esc_attr($icon) . '"></i></div>', $output, 1 );
	}

	if($obj->settings('base')=='vc_tta_section') {
		$icon = logan_vc_tabs_shortcode($atts, '');
		if ( $icon!='' )
			$output = preg_replace( '/<span class="vc_tta-title-text">(.+?)<\/span>/i', $icon, $output, 1 );
			$output = preg_replace( '/<span class="vc_tta-title-text"><\/span>/i', $icon, $output, 1 );
	}

	if($obj->settings('base')=='vc_text_separator') {
		$icon = logan_vc_custom_icon($atts, '');
		if ( $icon!='' )
			$output = preg_replace( '/<span class="vc_icon_element-icon ([^\"]*)"/s', '<span class="vc_icon_element-icon ' . $icon . '"', $output, 1 );
	}

	if($obj->settings('base')=='vc_gallery') {
		$data = logan_vc_gallery($atts, '');
		if ( $data!='' )
			$output = preg_replace( '/ class="wpb_gallery_slidesslick([^\"]*)"/s', ' class="wpb_gallery_slidesslick$1" ' . $data . ' ', $output, 1 );

		$output = preg_replace( '/ class="([^\"]*)prettyphoto([^\"]*)"/s', ' class="$1cbox$2"', $output );
		$output = preg_replace( '/ rel="prettyPhoto\[(.*?)\]"/s', ' data-rel="cbox[$1]"', $output );
	}

	if($obj->settings('base')=='vc_images_carousel') {
		$output = preg_replace( '/ class="([^\"]*)prettyphoto([^\"]*)"/s', ' class="$1cbox$2"', $output );
		$output = preg_replace( '/ rel="prettyPhoto\[(.*?)\]"/s', ' data-rel="cbox[$1]"', $output );
		$output = preg_replace( '/ data-ride="(.*?)"/s', '', $output, 1 );
		$output = preg_replace( '/ style="(.*?)"/s', '', $output, 1 );
	}

	if($obj->settings('base')=='vc_single_image') {
		$output = preg_replace( '/ class="([^\"]*)prettyphoto([^\"]*)"/s', ' class="$1cbox$2"', $output );
		$output = preg_replace( '/ rel="prettyPhoto\[(.*?)\]"/s', ' data-rel="cbox[$1]"', $output );
	}

	return $output;
}
endif;

if ( !function_exists('logan_vc_gallery')) :
/**
* @since Logan 1.0
*/
function logan_vc_gallery($atts, $content) {

	extract( shortcode_atts( array(
	    "slick_autoplay" => "",
	    "slick_speed" => "",
	    "slick_dots" => "",
	    "slick_arrows" => "",
	), $atts ) );

	$output = ' data-slick-autoplay="' . esc_attr($slick_autoplay) . '" data-slick-speed="' . esc_attr($slick_speed) . '" data-slick-dots="' . esc_attr($slick_dots) . '" data-slick-arrows="' . esc_attr($slick_arrows) . '"';

	return $output;
}
endif;

if ( !function_exists('logan_vc_overlay_color')) :
/**
* @since Logan 1.0
*/
function logan_vc_overlay_color($atts, $content) {

	extract( shortcode_atts( array(
	    "overlay" => ""
	), $atts ) );

	if ( $overlay=='' )
		return;

	$output = '<div class="vc-logan-overlay" style="background-color:' . esc_attr($overlay) . '"></div>';

	return $output;
}
endif;

if ( !function_exists('logan_vc_shadow_shortcode')) :
/**
* @since Logan 1.0
*/
function logan_vc_shadow_shortcode($atts, $content) {

	extract( shortcode_atts( array(
	    "inner_shadow" => ""
	), $atts ) );

	if ( $inner_shadow=='' )
		return;

	$output = '<div class="vc-row-logan-shadow dir-' . esc_attr($inner_shadow). '"></div>';

	return $output;
}
endif;

if ( !function_exists('logan_vc_sticky_column')) :
/**
* @since Logan 1.0
*/
function logan_vc_sticky_column($atts, $content) {

	extract( shortcode_atts( array(
	    "sticky_col" => ""
	), $atts ) );

	if ( $sticky_col=='' )
		return;

	$output = 'stickit';

	return $output;
}
endif;

if ( !function_exists('logan_vc_overflow_elem')) :
/**
* @since Logan 1.0
*/
function logan_vc_overflow_elem($atts, $content) {

	extract( shortcode_atts( array(
	    "overflow" => ""
	), $atts ) );

	if ( $overflow=='' )
		return;

	$output = esc_attr($overflow);

	return $output;
}
endif;

if ( !function_exists('logan_vc_text_column_shadow')) :
/**
* @since Logan 1.0
*/
function logan_vc_text_column_shadow($atts, $content) {

	extract( shortcode_atts( array(
	    "pix_box_shadow" => ""
	), $atts ) );

	if ( $pix_box_shadow=='' )
		return;

	$output = esc_attr($pix_box_shadow);

	return $output;
}
endif;

if ( !function_exists('logan_vc_parallax_rev_shortcode')) :
/**
* @since Logan 1.0
*/
function logan_vc_parallax_rev_shortcode($atts, $content) {

	extract( shortcode_atts( array(
	    "rev_slider_parax" => ""
	), $atts ) );

	if ( $rev_slider_parax=='' )
		return;

	$output = ' data-parallax="on" data-offset="0" data-offset-param="' . esc_attr($rev_slider_parax) . '"';

	return $output;
}
endif;

if ( !function_exists('logan_vc_tabs_custom')) :
/**
* @since Logan 1.0
*/
function logan_vc_tabs_custom($atts) {

	extract( shortcode_atts( array(
	    "fullwidth_tabs" => "",
	    "no_style" => "",
	    "button_look" => ""
	), $atts ) );

	if ( $fullwidth_tabs=='' && $no_style=='' && $button_look=='' )
		return;
	else
		$output = "data-tabs-fullwidth=\"". esc_attr($fullwidth_tabs) ."\" data-tabs-nostyle=\"". esc_attr($no_style) ."\" data-tabs-button=\"". esc_attr($button_look) ."\"";

	return $output;
}
endif;

if ( !function_exists('logan_vc_custom_icon')) :
/**
* @since Logan 1.0
*/
function logan_vc_custom_icon($atts) {

	extract( shortcode_atts( array(
	    "logan_icon_enable" => "",
	    "logan_icon" => "",
	), $atts ) );

	if ( $logan_icon_enable=='' || $logan_icon=='' )
		return;

	return $logan_icon;
}
endif;

if ( !function_exists('logan_vc_tabs_shortcode')) :
/**
* @since Logan 1.0
*/
function logan_vc_tabs_shortcode($atts) {

	extract( shortcode_atts( array(
	    "fx" => "",
	    "logan_icon_enable" => "",
	    "logan_icon" => "",
	    "icon_position" => "before",
	    "icon_size" => "1",
	    "title" => ""
	), $atts ) );

	if ( ( $logan_icon_enable!='yes' || $logan_icon=='' ) && $fx == '' )
		return;

	$fx = $fx != '' ? 'wpb_animate_when_almost_visible wpb_' . $fx : '';

	$output = "<span class=\" $fx\"";

	if ( $logan_icon_enable=='yes' ) {
		if ( $icon_position == 'after' ) {

			$output .= " data-icon-position=\"" . esc_attr($icon_position) ."\">" . esc_attr($title) ."<i class=\"" . esc_attr($logan_icon) ."\" style=\"font-size:" . esc_attr($icon_size) ."em\"></i></span>";

		} else {

			$output .= " data-icon-position=\"" . esc_attr($icon_position) ."\"><i class=\"" . esc_attr($logan_icon) ."\" style=\"font-size:" . esc_attr($icon_size) ."em\"></i>" . esc_attr($title) ."</span>";

		}		
	} else {

		$output .= ">$title</span>";

	}

	return $output;
}
endif;

if ( !function_exists('logan_vc_icons_shortcode')) :
/**
* @since Logan 1.0
*/
function logan_vc_icons_shortcode($atts) {

	extract( shortcode_atts( array(
	    "logan_icon_enable" => "",
	    "logan_icon" => ""
	), $atts ) );

	if ( $logan_icon_enable!='yes' || $logan_icon=='' )
		return;

	return $logan_icon;
}
endif;

if ( !function_exists('logan_vc_row_max_h')) :
/**
* @since Logan 1.0
*/
function logan_vc_row_max_h($atts) {

	extract( shortcode_atts( array(
	    "max_height" => ""
	), $atts ) );

	if ( $max_height=='' )
		return;

	$output = $max_height;

	return $output;
}
endif;

if ( !function_exists('logan_vc_media_grid')) :
/**
* @since Logan 1.0
*/
function logan_vc_media_grid($atts) {

	extract( shortcode_atts( array(
	    "btn_i_type" => "",
	    "btn_i_icon_budicons" => ""
	), $atts ) );

	if ( $btn_i_type!='budicons' || $btn_i_icon_budicons=='' )
		return;

	$output = $btn_i_icon_budicons;

	return $output;
}
endif;

if ( !function_exists('logan_vc_row_parallax')) :
/**
* @since Logan 1.0
*/
function logan_vc_row_parallax($atts) {

	extract( shortcode_atts( array(
	    "parallax_value" => "",
	    "parallax_position" => ""
	), $atts ) );

	if ( $parallax_value=='' && $parallax_position=='' )
		return;

	$output = '';

	if ( $parallax_value!='' )
		$output .= ' data-offset-param="' . esc_attr($parallax_value) . '" ';

	if ( $parallax_position!='' ) {
		$parallax_position = explode(",", $parallax_position);
		$right = 100 - $parallax_position[1];
		$output .= ' data-parax-left="' . esc_attr($parallax_position[0]) . '" data-parax-right="' . esc_attr($right) . '" ';
	}

	return $output;
}
endif;

add_filter( 'vc_autocomplete_logan_team_members_include_callback', 'logan_team_members_include_field_search', 10, 1 );
add_filter( 'vc_autocomplete_logan_team_members_include_render', 'logan_team_members_include_field_render', 10, 1 );
add_filter( 'vc_autocomplete_logan_team_members_exclude_callback', 'logan_team_members_include_field_search', 10, 1 );
add_filter( 'vc_autocomplete_logan_team_members_exclude_render', 'logan_team_members_include_field_render', 10, 1 );

function logan_team_members_include_field_search( $search_string ) {
	$query = $search_string;
	$data = array();
	$args = array( 's' => $query, 'post_type' => 'member' );
	$args['vc_search_by_title_only'] = true;
	$args['numberposts'] = - 1;
	if ( strlen( $args['s'] ) == 0 ) {
		unset( $args['s'] );
	}
	add_filter( 'posts_search', 'vc_search_by_title_only', 500, 2 );
	$posts = get_posts( $args );
	if ( is_array( $posts ) && ! empty( $posts ) ) {
		foreach ( $posts as $post ) {
			$data[] = array(
				'value' => $post->ID,
				'label' => $post->post_title,
				'group' => $post->post_type,
			);
		}
	}

	return $data;
}

function logan_team_members_include_field_render( $value ) {
	$post = get_post( $value['value'] );

	return is_null( $post ) ? false : array(
		'label' => $post->post_title,
		'value' => $post->ID,
		'group' => $post->post_type
	);
}

add_filter( 'vc_autocomplete_logan_project_include_callback', 'logan_project_include_field_search', 10, 1 );
add_filter( 'vc_autocomplete_logan_project_include_render', 'logan_project_include_field_render', 10, 1 );
add_filter( 'vc_autocomplete_logan_project_exclude_callback', 'logan_project_include_field_search', 10, 1 );
add_filter( 'vc_autocomplete_logan_project_esclude_render', 'logan_project_include_field_render', 10, 1 );

function logan_project_include_field_search( $search_string ) {
	$query = $search_string;
	$data = array();
	$args = array( 's' => $query, 'post_type' => 'jetpack-portfolio' );
	$args['vc_search_by_title_only'] = true;
	$args['numberposts'] = - 1;
	if ( strlen( $args['s'] ) == 0 ) {
		unset( $args['s'] );
	}
	add_filter( 'posts_search', 'vc_search_by_title_only', 500, 2 );
	$posts = get_posts( $args );
	if ( is_array( $posts ) && ! empty( $posts ) ) {
		foreach ( $posts as $post ) {
			$data[] = array(
				'value' => $post->ID,
				'label' => $post->post_title,
				'group' => $post->post_type,
			);
		}
	}

	return $data;
}

function logan_project_include_field_render( $value ) {
	$post = get_post( $value['value'] );

	return is_null( $post ) ? false : array(
		'label' => $post->post_title,
		'value' => $post->ID,
		'group' => $post->post_type
	);
}

add_filter( 'vc_autocomplete_logan_project_taxonomies_callback', 'logan_autocomplete_project_taxonomies_field_search', 10, 1 );
add_filter( 'vc_autocomplete_logan_project_taxonomies_render', 'logan_autocomplete_project_taxonomies_field_render', 10, 1 );

function logan_autocomplete_project_taxonomies_field_search( $search_string ) {
	$data = array();
	$taxonomies_types = array('jetpack-portfolio-type');
	$taxonomies = get_terms( $taxonomies_types, array(
		'hide_empty' => false,
		'search' => $search_string
	) );
	if ( is_array( $taxonomies ) && ! empty( $taxonomies ) ) {
		foreach ( $taxonomies as $t ) {
			if ( is_object( $t ) ) {
				$data[] = vc_get_term_object( $t );
			}
		}
	}

	return $data;
}

function logan_autocomplete_project_taxonomies_field_render( $term ) {
	$taxonomies_types = array('jetpack-portfolio-type');
	$terms = get_terms(  $taxonomies_types, array(
		'include' => array( $term['value'] ),
		'hide_empty' => false,
	) );
	$data = false;
	if ( is_array( $terms ) && 1 === count( $terms ) ) {
		$term = $terms[0];
		$data = vc_get_term_object( $term );
	}

	return $data;
}

add_filter( 'vc_autocomplete_logan_post_include_callback', 'logan_include_field_search', 10, 1 );
add_filter( 'vc_autocomplete_logan_post_include_render', 'logan_include_field_render', 10, 1 );
add_filter( 'vc_autocomplete_logan_post_exclude_callback', 'logan_include_field_search', 10, 1 );
add_filter( 'vc_autocomplete_logan_post_exclude_render', 'logan_include_field_render', 10, 1 );

function logan_include_field_search( $search_string ) {
	$query = $search_string;
	$data = array();
	$args = array( 's' => $query, 'post_type' => 'post' );
	$args['vc_search_by_title_only'] = true;
	$args['numberposts'] = - 1;
	if ( strlen( $args['s'] ) == 0 ) {
		unset( $args['s'] );
	}
	add_filter( 'posts_search', 'vc_search_by_title_only', 500, 2 );
	$posts = get_posts( $args );
	if ( is_array( $posts ) && ! empty( $posts ) ) {
		foreach ( $posts as $post ) {
			$data[] = array(
				'value' => $post->ID,
				'label' => $post->post_title,
				'group' => $post->post_type,
			);
		}
	}

	return $data;
}

function logan_include_field_render( $value ) {
	$post = get_post( $value['value'] );

	return is_null( $post ) ? false : array(
		'label' => $post->post_title,
		'value' => $post->ID,
		'group' => $post->post_type
	);
}

add_filter( 'vc_autocomplete_logan_post_taxonomies_callback', 'logan_autocomplete_taxonomies_field_search', 10, 1 );
add_filter( 'vc_autocomplete_logan_post_taxonomies_render', 'logan_autocomplete_taxonomies_field_render', 10, 1 );

function logan_autocomplete_taxonomies_field_search( $search_string ) {
	$data = array();
	$taxonomies_types = array('category', 'post_tag');
	$taxonomies = get_terms( $taxonomies_types, array(
		'hide_empty' => false,
		'search' => $search_string
	) );
	if ( is_array( $taxonomies ) && ! empty( $taxonomies ) ) {
		foreach ( $taxonomies as $t ) {
			if ( is_object( $t ) ) {
				$data[] = vc_get_term_object( $t );
			}
		}
	}

	return $data;
}

function logan_autocomplete_taxonomies_field_render( $term ) {
	$taxonomies_types = array('category', 'post_tag');
	$terms = get_terms(  $taxonomies_types, array(
		'include' => array( $term['value'] ),
		'hide_empty' => false,
	) );
	$data = false;
	if ( is_array( $terms ) && 1 === count( $terms ) ) {
		$term = $terms[0];
		$data = vc_get_term_object( $term );
	}

	return $data;
}

if ( ! function_exists( 'pix_sanitize_hex_color' ) ) :
/**
 * Copy of sanitize_color()
 * @since Logan 1.0
 */
function pix_sanitize_hex_color( $color ) {
    if ( '' === $color )
        return '';

    // 3 or 6 hex digits, or the empty string.
    if ( preg_match('|^#([A-Fa-f0-9]{3}){1,2}$|', $color ) )
        return $color;

    if ( preg_match('|^([A-Fa-f0-9]{3}){1,2}$|', $color ) )
        return '#'.$color;

    return null;
}
endif;

add_filter('vc_google_fonts_get_fonts_filter', 'pix_vc_google_font_list');
if ( ! function_exists( 'pix_vc_google_font_list' ) ) :
/**
 * @since Logan 1.0
 */
function pix_vc_google_font_list() {
    $font_list = '[
{"font_family":"ABeeZee","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Abel","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Abril Fatface","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Aclonica","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Acme","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Actor","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Adamina","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Advent Pro","font_styles":"100,200,300,regular,500,600,700","font_types":"100 light regular:100:normal,200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Aguafina Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Akronim","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Aladin","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Aldrich","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Alef","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Alegreya","font_styles":"regular,italic,700,700italic,900,900italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Alegreya SC","font_styles":"regular,italic,700,700italic,900,900italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Alegreya Sans","font_styles":"100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,800,800italic,900,900italic","font_types":"100 light regular:100:normal,100 light italic:100:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,700 bold regular:700:normal,700 bold italic:700:italic,800 bold regular:800:normal,800 bold italic:800:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Alegreya Sans SC","font_styles":"100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,800,800italic,900,900italic","font_types":"100 light regular:100:normal,100 light italic:100:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,700 bold regular:700:normal,700 bold italic:700:italic,800 bold regular:800:normal,800 bold italic:800:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Alex Brush","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Alfa Slab One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Alice","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Alike","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Alike Angular","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Allan","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Allerta","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Allerta Stencil","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Allura","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Almendra","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Almendra Display","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Almendra SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Amarante","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Amaranth","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Amatic SC","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Amethysta","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Amiri","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Amita","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Anaheim","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Andada","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Andika","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Angkor","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Annie Use Your Telescope","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Anonymous Pro","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Antic","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Antic Didone","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Antic Slab","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Anton","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Arapey","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Arbutus","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Arbutus Slab","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Architects Daughter","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Archivo Black","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Archivo Narrow","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Arimo","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Arizonia","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Armata","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Artifika","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Arvo","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Arya","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Asap","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Asar","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Asset","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Astloch","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Asul","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Atomic Age","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Aubrey","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Audiowide","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Autour One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Average","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Average Sans","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Averia Gruesa Libre","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Averia Libre","font_styles":"300,300italic,regular,italic,700,700italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Averia Sans Libre","font_styles":"300,300italic,regular,italic,700,700italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Averia Serif Libre","font_styles":"300,300italic,regular,italic,700,700italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Bad Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Balthazar","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bangers","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Basic","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Battambang","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Baumans","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bayon","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Belgrano","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Belleza","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"BenchNine","font_styles":"300,regular,700","font_types":"300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Bentham","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Berkshire Swash","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bevan","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bigelow Rules","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bigshot One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bilbo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bilbo Swash Caps","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Biryani","font_styles":"200,300,regular,600,700,800,900","font_types":"200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal,900 bold regular:900:normal"},{"font_family":"Bitter","font_styles":"regular,italic,700","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal"},{"font_family":"Black Ops One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bokor","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bonbon","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Boogaloo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bowlby One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bowlby One SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Brawler","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bree Serif","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bubblegum Sans","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Bubbler One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Buda","font_styles":"300","font_types":"300 light regular:300:normal"},{"font_family":"Buenard","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Butcherman","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Butterfly Kids","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cabin","font_styles":"regular,italic,500,500italic,600,600italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Cabin Condensed","font_styles":"regular,500,600,700","font_types":"400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Cabin Sketch","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Caesar Dressing","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cagliostro","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Calligraffitti","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cambay","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Cambo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Candal","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cantarell","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Cantata One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cantora One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Capriola","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cardo","font_styles":"regular,italic,700","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal"},{"font_family":"Carme","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Carrois Gothic","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Carrois Gothic SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Carter One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Catamaran","font_styles":"100,200,300,regular,500,600,700,800,900","font_types":"100 light regular:100:normal,200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal,900 bold regular:900:normal"},{"font_family":"Caudex","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Caveat","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Caveat Brush","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cedarville Cursive","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ceviche One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Changa One","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Chango","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Chau Philomene One","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Chela One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Chelsea Market","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Chenla","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cherry Cream Soda","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cherry Swash","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Chewy","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Chicle","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Chivo","font_styles":"regular,italic,900,900italic","font_types":"400 regular:400:normal,400 italic:400:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Chonburi","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cinzel","font_styles":"regular,700,900","font_types":"400 regular:400:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"Cinzel Decorative","font_styles":"regular,700,900","font_types":"400 regular:400:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"Clicker Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Coda","font_styles":"regular,800","font_types":"400 regular:400:normal,800 bold regular:800:normal"},{"font_family":"Coda Caption","font_styles":"800","font_types":"800 bold regular:800:normal"},{"font_family":"Codystar","font_styles":"300,regular","font_types":"300 light regular:300:normal,400 regular:400:normal"},{"font_family":"Combo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Comfortaa","font_styles":"300,regular,700","font_types":"300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Coming Soon","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Concert One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Condiment","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Content","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Contrail One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Convergence","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cookie","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Copse","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Corben","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Courgette","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cousine","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Coustard","font_styles":"regular,900","font_types":"400 regular:400:normal,900 bold regular:900:normal"},{"font_family":"Covered By Your Grace","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Crafty Girls","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Creepster","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Crete Round","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Crimson Text","font_styles":"regular,italic,600,600italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Croissant One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Crushed","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cuprum","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Cutive","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Cutive Mono","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Damion","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Dancing Script","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Dangrek","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Dawning of a New Day","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Days One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Dekko","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Delius","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Delius Swash Caps","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Delius Unicase","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Della Respira","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Denk One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Devonshire","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Dhurjati","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Didact Gothic","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Diplomata","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Diplomata SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Domine","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Donegal One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Doppio One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Dorsa","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Dosis","font_styles":"200,300,regular,500,600,700,800","font_types":"200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal"},{"font_family":"Dr Sugiyama","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Droid Sans","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Droid Sans Mono","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Droid Serif","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Duru Sans","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Dynalight","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"EB Garamond","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Eagle Lake","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Eater","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Economica","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Eczar","font_styles":"regular,500,600,700,800","font_types":"400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal"},{"font_family":"Ek Mukta","font_styles":"200,300,regular,500,600,700,800","font_types":"200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal"},{"font_family":"Electrolize","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Elsie","font_styles":"regular,900","font_types":"400 regular:400:normal,900 bold regular:900:normal"},{"font_family":"Elsie Swash Caps","font_styles":"regular,900","font_types":"400 regular:400:normal,900 bold regular:900:normal"},{"font_family":"Emblema One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Emilys Candy","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Engagement","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Englebert","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Enriqueta","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Erica One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Esteban","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Euphoria Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ewert","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Exo","font_styles":"100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","font_types":"100 light regular:100:normal,100 light italic:100:italic,200 light regular:200:normal,200 light italic:200:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic,800 bold regular:800:normal,800 bold italic:800:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Exo 2","font_styles":"100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","font_types":"100 light regular:100:normal,100 light italic:100:italic,200 light regular:200:normal,200 light italic:200:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic,800 bold regular:800:normal,800 bold italic:800:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Expletus Sans","font_styles":"regular,italic,500,500italic,600,600italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Fanwood Text","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Fascinate","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fascinate Inline","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Faster One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fasthand","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fauna One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Federant","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Federo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Felipa","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fenix","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Finger Paint","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fira Mono","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Fira Sans","font_styles":"300,300italic,regular,italic,500,500italic,700,700italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Fjalla One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fjord One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Flamenco","font_styles":"300,regular","font_types":"300 light regular:300:normal,400 regular:400:normal"},{"font_family":"Flavors","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fondamento","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Fontdiner Swanky","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Forum","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Francois One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Freckle Face","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fredericka the Great","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fredoka One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Freehand","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fresca","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Frijole","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fruktur","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Fugaz One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"GFS Didot","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"GFS Neohellenic","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Gabriela","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Gafata","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Galdeano","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Galindo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Gentium Basic","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Gentium Book Basic","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Geo","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Geostar","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Geostar Fill","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Germania One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Gidugu","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Gilda Display","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Give You Glory","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Glass Antiqua","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Glegoo","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Gloria Hallelujah","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Goblin One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Gochi Hand","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Gorditas","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Goudy Bookletter 1911","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Graduate","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Grand Hotel","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Gravitas One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Great Vibes","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Griffy","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Gruppo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Gudea","font_styles":"regular,italic,700","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal"},{"font_family":"Gurajada","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Habibi","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Halant","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Hammersmith One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Hanalei","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Hanalei Fill","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Handlee","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Hanuman","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Happy Monkey","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Headland One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Henny Penny","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Herr Von Muellerhoff","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Hind","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Hind Siliguri","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Hind Vadodara","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Holtwood One SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Homemade Apple","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Homenaje","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"IM Fell DW Pica","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"IM Fell DW Pica SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"IM Fell Double Pica","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"IM Fell Double Pica SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"IM Fell English","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"IM Fell English SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"IM Fell French Canon","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"IM Fell French Canon SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"IM Fell Great Primer","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"IM Fell Great Primer SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Iceberg","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Iceland","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Imprima","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Inconsolata","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Inder","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Indie Flower","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Inika","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Inknut Antiqua","font_styles":"300,regular,500,600,700,800,900","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal,900 bold regular:900:normal"},{"font_family":"Irish Grover","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Istok Web","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Italiana","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Italianno","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Itim","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Jacques Francois","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Jacques Francois Shadow","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Jaldi","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Jim Nightshade","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Jockey One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Jolly Lodger","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Josefin Sans","font_styles":"100,100italic,300,300italic,regular,italic,600,600italic,700,700italic","font_types":"100 light regular:100:normal,100 light italic:100:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Josefin Slab","font_styles":"100,100italic,300,300italic,regular,italic,600,600italic,700,700italic","font_types":"100 light regular:100:normal,100 light italic:100:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Joti One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Judson","font_styles":"regular,italic,700","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal"},{"font_family":"Julee","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Julius Sans One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Junge","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Jura","font_styles":"300,regular,500,600","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal"},{"font_family":"Just Another Hand","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Just Me Again Down Here","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Kadwa","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Kalam","font_styles":"300,regular,700","font_types":"300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Kameron","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Kantumruy","font_styles":"300,regular,700","font_types":"300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Karla","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Karma","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Kaushan Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Kavoon","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Kdam Thmor","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Keania One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Kelly Slab","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Kenia","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Khand","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Khmer","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Khula","font_styles":"300,regular,600,700,800","font_types":"300 light regular:300:normal,400 regular:400:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal"},{"font_family":"Kite One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Knewave","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Kotta One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Koulen","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Kranky","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Kreon","font_styles":"300,regular,700","font_types":"300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Kristi","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Krona One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Kurale","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"La Belle Aurore","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Laila","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Lakki Reddy","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Lancelot","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Lateef","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Lato","font_styles":"100,100italic,300,300italic,regular,italic,700,700italic,900,900italic","font_types":"100 light regular:100:normal,100 light italic:100:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"League Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Leckerli One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ledger","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Lekton","font_styles":"regular,italic,700","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal"},{"font_family":"Lemon","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Libre Baskerville","font_styles":"regular,italic,700","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal"},{"font_family":"Life Savers","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Lilita One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Lily Script One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Limelight","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Linden Hill","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Lobster","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Lobster Two","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Londrina Outline","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Londrina Shadow","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Londrina Sketch","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Londrina Solid","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Lora","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Love Ya Like A Sister","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Loved by the King","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Lovers Quarrel","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Luckiest Guy","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Lusitana","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Lustria","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Macondo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Macondo Swash Caps","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Magra","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Maiden Orange","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Mako","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Mallanna","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Mandali","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Marcellus","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Marcellus SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Marck Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Margarine","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Marko One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Marmelad","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Martel","font_styles":"200,300,regular,600,700,800,900","font_types":"200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal,900 bold regular:900:normal"},{"font_family":"Martel Sans","font_styles":"200,300,regular,600,700,800,900","font_types":"200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal,900 bold regular:900:normal"},{"font_family":"Marvel","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Mate","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Mate SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Maven Pro","font_styles":"regular,500,700,900","font_types":"400 regular:400:normal,500 bold regular:500:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"McLaren","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Meddon","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"MedievalSharp","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Medula One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Megrim","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Meie Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Merienda","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Merienda One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Merriweather","font_styles":"300,300italic,regular,italic,700,700italic,900,900italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Merriweather Sans","font_styles":"300,300italic,regular,italic,700,700italic,800,800italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic,800 bold regular:800:normal,800 bold italic:800:italic"},{"font_family":"Metal","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Metal Mania","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Metamorphous","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Metrophobic","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Michroma","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Milonga","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Miltonian","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Miltonian Tattoo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Miniver","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Miss Fajardose","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Modak","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Modern Antiqua","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Molengo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Molle","font_styles":"italic","font_types":"400 italic:400:italic"},{"font_family":"Monda","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Monofett","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Monoton","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Monsieur La Doulaise","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Montaga","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Montez","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Montserrat","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Montserrat Alternates","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Montserrat Subrayada","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Moul","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Moulpali","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Mountains of Christmas","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Mouse Memoirs","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Mr Bedfort","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Mr Dafoe","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Mr De Haviland","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Mrs Saint Delafield","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Mrs Sheppards","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Muli","font_styles":"300,300italic,regular,italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic"},{"font_family":"Mystery Quest","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"NTR","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Neucha","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Neuton","font_styles":"200,300,regular,italic,700,800","font_types":"200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,800 bold regular:800:normal"},{"font_family":"New Rocker","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"News Cycle","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Niconne","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nixie One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nobile","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Nokora","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Norican","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nosifer","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nothing You Could Do","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Noticia Text","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Noto Sans","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Noto Serif","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Nova Cut","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nova Flat","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nova Mono","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nova Oval","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nova Round","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nova Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nova Slim","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nova Square","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Numans","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Nunito","font_styles":"300,regular,700","font_types":"300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Odor Mean Chey","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Offside","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Old Standard TT","font_styles":"regular,italic,700","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal"},{"font_family":"Oldenburg","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Oleo Script","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Oleo Script Swash Caps","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Open Sans","font_styles":"300,300italic,regular,italic,600,600italic,700,700italic,800,800italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic,800 bold regular:800:normal,800 bold italic:800:italic"},{"font_family":"Open Sans Condensed","font_styles":"300,300italic,700","font_types":"300 light regular:300:normal,300 light italic:300:italic,700 bold regular:700:normal"},{"font_family":"Oranienbaum","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Orbitron","font_styles":"regular,500,700,900","font_types":"400 regular:400:normal,500 bold regular:500:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"Oregano","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Orienta","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Original Surfer","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Oswald","font_styles":"300,regular,700","font_types":"300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Over the Rainbow","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Overlock","font_styles":"regular,italic,700,700italic,900,900italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Overlock SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ovo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Oxygen","font_styles":"300,regular,700","font_types":"300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Oxygen Mono","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"PT Mono","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"PT Sans","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"PT Sans Caption","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"PT Sans Narrow","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"PT Serif","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"PT Serif Caption","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Pacifico","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Palanquin","font_styles":"100,200,300,regular,500,600,700","font_types":"100 light regular:100:normal,200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Palanquin Dark","font_styles":"regular,500,600,700","font_types":"400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Paprika","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Parisienne","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Passero One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Passion One","font_styles":"regular,700,900","font_types":"400 regular:400:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"Pathway Gothic One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Patrick Hand","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Patrick Hand SC","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Patua One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Paytone One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Peddana","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Peralta","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Permanent Marker","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Petit Formal Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Petrona","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Philosopher","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Piedra","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Pinyon Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Pirata One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Plaster","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Play","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Playball","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Playfair Display","font_styles":"regular,italic,700,700italic,900,900italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Playfair Display SC","font_styles":"regular,italic,700,700italic,900,900italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Podkova","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Poiret One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Poller One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Poly","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Pompiere","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Pontano Sans","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Poppins","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Port Lligat Sans","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Port Lligat Slab","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Pragati Narrow","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Prata","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Preahvihear","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Press Start 2P","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Princess Sofia","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Prociono","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Prosto One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Puritan","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Purple Purse","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Quando","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Quantico","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Quattrocento","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Quattrocento Sans","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Questrial","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Quicksand","font_styles":"300,regular,700","font_types":"300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Quintessential","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Qwigley","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Racing Sans One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Radley","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Rajdhani","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Raleway","font_styles":"100,200,300,regular,500,600,700,800,900","font_types":"100 light regular:100:normal,200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal,900 bold regular:900:normal"},{"font_family":"Raleway Dots","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ramabhadra","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ramaraja","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rambla","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Rammetto One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ranchers","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rancho","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ranga","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Rationale","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ravi Prakash","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Redressed","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Reenie Beanie","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Revalia","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rhodium Libre","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ribeye","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ribeye Marrow","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Righteous","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Risque","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Roboto","font_styles":"100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,900,900italic","font_types":"100 light regular:100:normal,100 light italic:100:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Roboto Condensed","font_styles":"300,300italic,regular,italic,700,700italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Roboto Mono","font_styles":"100,100italic,300,300italic,regular,italic,500,500italic,700,700italic","font_types":"100 light regular:100:normal,100 light italic:100:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Roboto Slab","font_styles":"100,300,regular,700","font_types":"100 light regular:100:normal,300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Rochester","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rock Salt","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rokkitt","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Romanesco","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ropa Sans","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Rosario","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Rosarivo","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Rouge Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rozha One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rubik","font_styles":"300,300italic,regular,italic,500,500italic,700,700italic,900,900italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Rubik Mono One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rubik One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ruda","font_styles":"regular,700,900","font_types":"400 regular:400:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"Rufina","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Ruge Boogie","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ruluko","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rum Raisin","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ruslan Display","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Russo One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ruthie","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Rye","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sacramento","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sahitya","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Sail","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Salsa","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sanchez","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Sancreek","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sansita One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sarala","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Sarina","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sarpanch","font_styles":"regular,500,600,700,800,900","font_types":"400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal,900 bold regular:900:normal"},{"font_family":"Satisfy","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Scada","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Scheherazade","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Schoolbell","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Seaweed Script","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sevillana","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Seymour One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Shadows Into Light","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Shadows Into Light Two","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Shanti","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Share","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Share Tech","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Share Tech Mono","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Shojumaru","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Short Stack","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Siemreap","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sigmar One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Signika","font_styles":"300,regular,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Signika Negative","font_styles":"300,regular,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Simonetta","font_styles":"regular,italic,900,900italic","font_types":"400 regular:400:normal,400 italic:400:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Sintony","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Sirin Stencil","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Six Caps","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Skranji","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Slabo 13px","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Slabo 27px","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Slackey","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Smokum","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Smythe","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sniglet","font_styles":"regular,800","font_types":"400 regular:400:normal,800 bold regular:800:normal"},{"font_family":"Snippet","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Snowburst One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sofadi One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sofia","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sonsie One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sorts Mill Goudy","font_styles":"regular,italic","font_types":"400 regular:400:normal,400 italic:400:italic"},{"font_family":"Source Code Pro","font_styles":"200,300,regular,500,600,700,900","font_types":"200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"Source Sans Pro","font_styles":"200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,900,900italic","font_types":"200 light regular:200:normal,200 light italic:200:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal,900 bold italic:900:italic"},{"font_family":"Source Serif Pro","font_styles":"regular,600,700","font_types":"400 regular:400:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Special Elite","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Spicy Rice","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Spinnaker","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Spirax","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Squada One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sree Krushnadevaraya","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Stalemate","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Stalinist One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Stardos Stencil","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Stint Ultra Condensed","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Stint Ultra Expanded","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Stoke","font_styles":"300,regular","font_types":"300 light regular:300:normal,400 regular:400:normal"},{"font_family":"Strait","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sue Ellen Francisco","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sumana","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Sunshiney","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Supermercado One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Sura","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Suranna","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Suravaram","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Suwannaphum","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Swanky and Moo Moo","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Syncopate","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Tangerine","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Taprom","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Tauri","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Teko","font_styles":"300,regular,500,600,700","font_types":"300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal"},{"font_family":"Telex","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Tenali Ramakrishna","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Tenor Sans","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Text Me One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"The Girl Next Door","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Tienne","font_styles":"regular,700,900","font_types":"400 regular:400:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"Tillana","font_styles":"regular,500,600,700,800","font_types":"400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal"},{"font_family":"Timmana","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Tinos","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Titan One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Titillium Web","font_styles":"200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,900","font_types":"200 light regular:200:normal,200 light italic:200:italic,300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,600 bold regular:600:normal,600 bold italic:600:italic,700 bold regular:700:normal,700 bold italic:700:italic,900 bold regular:900:normal"},{"font_family":"Trade Winds","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Trocchi","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Trochut","font_styles":"regular,italic,700","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal"},{"font_family":"Trykker","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Tulpen One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ubuntu","font_styles":"300,300italic,regular,italic,500,500italic,700,700italic","font_types":"300 light regular:300:normal,300 light italic:300:italic,400 regular:400:normal,400 italic:400:italic,500 bold regular:500:normal,500 bold italic:500:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Ubuntu Condensed","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Ubuntu Mono","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Ultra","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Uncial Antiqua","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Underdog","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Unica One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"UnifrakturCook","font_styles":"700","font_types":"700 bold regular:700:normal"},{"font_family":"UnifrakturMaguntia","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Unkempt","font_styles":"regular,700","font_types":"400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Unlock","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Unna","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"VT323","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Vampiro One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Varela","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Varela Round","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Vast Shadow","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Vesper Libre","font_styles":"regular,500,700,900","font_types":"400 regular:400:normal,500 bold regular:500:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"Vibur","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Vidaloka","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Viga","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Voces","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Volkhov","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Vollkorn","font_styles":"regular,italic,700,700italic","font_types":"400 regular:400:normal,400 italic:400:italic,700 bold regular:700:normal,700 bold italic:700:italic"},{"font_family":"Voltaire","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Waiting for the Sunrise","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Wallpoet","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Walter Turncoat","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Warnes","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Wellfleet","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Wendy One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Wire One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Work Sans","font_styles":"100,200,300,regular,500,600,700,800,900","font_types":"100 light regular:100:normal,200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,600 bold regular:600:normal,700 bold regular:700:normal,800 bold regular:800:normal,900 bold regular:900:normal"},{"font_family":"Yanone Kaffeesatz","font_styles":"200,300,regular,700","font_types":"200 light regular:200:normal,300 light regular:300:normal,400 regular:400:normal,700 bold regular:700:normal"},{"font_family":"Yantramanav","font_styles":"100,300,regular,500,700,900","font_types":"100 light regular:100:normal,300 light regular:300:normal,400 regular:400:normal,500 bold regular:500:normal,700 bold regular:700:normal,900 bold regular:900:normal"},{"font_family":"Yellowtail","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Yeseva One","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Yesteryear","font_styles":"regular","font_types":"400 regular:400:normal"},{"font_family":"Zeyada","font_styles":"regular","font_types":"400 regular:400:normal"}]';

    return json_decode($font_list);
}
endif;