<?php
add_action( 'vc_before_init', 'pix_vc_buttons' );
if ( !function_exists('pix_vc_buttons')) :
/**
* Adds a custom icon set to VC
*/
function pix_vc_buttons() {

    $custom_css_id = uniqid('pix-custom-');

	vc_map( array(
		"name" => esc_html__( "Logan buttons", "logan" ),
		"base" => "pix_button",
		"icon" => "pix-vc-buttons",
		"class" => "",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
		'heading' => esc_html__( 'Buttons', 'logan' ),
		'admin_enqueue_js' => LOGANADDONS_URL . 'scripts/vc-js.js',
		'js_view' => 'PixVcButtonsView',
		"description" => esc_html__( "Single and dual buttons, with icons and hover effect.", 'logan' ),
		"params" => array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Button alignment', 'logan' ),
				'param_name' => 'align',
				'value' => array(
					esc_html__( 'Left', 'logan' ) => 'left',
					esc_html__( 'Right', 'logan' ) => 'right',
					esc_html__( 'Center', 'logan' ) => 'center'
				),
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Button width', 'logan' ),
				'param_name' => 'width',
				'value' => array(
					esc_html__( 'Auto', 'logan' ) => '',
					esc_html__( '100%', 'logan' ) => 'width_100'
				),
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Border radius (will be intended in pixels)', 'logan' ),
				'param_name' => 'radius',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Border width (will be intended in pixels)', 'logan' ),
				'param_name' => 'border_width',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Text size (intended in pixels)', 'logan' ),
				'param_name' => 'text_size',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Icon size (intended in pixels)', 'logan' ),
				'param_name' => 'icon_size',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Css animation', 'logan' ),
				'param_name' => 'css_animation',
				'admin_label' => 'true',
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' ),
				'group' =>  esc_html__( 'General', 'logan' )
	        ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'ID', 'logan' ),
				'param_name' => 'id',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Class', 'logan' ),
				'param_name' => 'class',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),

			array(
				'type' => 'vc_link',
				'heading' => esc_html__( 'URL (Link)', 'logan' ),
				'param_name' => 'single_link',
				'description' => esc_html__( 'Add link to button.', 'logan' ),
				'admin_label' => true,
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'logan' ),
				'param_name' => 'icon_single',
				'admin_label' => true,
				'value' => '', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 4000,
			        'type' => 'budicons',
				),
				'description' => esc_html__( 'Select icon from library.', 'logan' ),
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon position', 'logan' ),
				'param_name' => 'single_icon_position',
				'value' => array(
					esc_html__( 'Before', 'logan' ) => 'before',
					esc_html__( 'After', 'logan' ) => 'after',
				),
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Icon color', 'logan' ),
				'param_name' => 'single_icon_color',
				'value' => '',
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Icon color on hover', 'logan' ),
				'param_name' => 'single_icon_color_hover',
				'value' => '',
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Button content', 'logan' ),
				'param_name' => 'single_content',
				'value' => '',
				'admin_label' => true,
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Text color', 'logan' ),
				'param_name' => 'single_text_color',
				'value' => '',
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Text color on hover', 'logan' ),
				'param_name' => 'single_text_color_hover',
				'value' => '',
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background color', 'logan' ),
				'param_name' => 'single_bg_color',
				'value' => '',
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background color on hover', 'logan' ),
				'param_name' => 'single_bg_color_hover',
				'value' => '',
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Border color', 'logan' ),
				'param_name' => 'single_border_color',
				'value' => '',
				'group' =>  esc_html__( 'Single', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Border color on hover', 'logan' ),
				'param_name' => 'single_border_color_hover',
				'value' => '',
				'group' =>  esc_html__( 'Single', 'logan' )
			),

			array(
				'type' => 'vc_link',
				'heading' => esc_html__( 'URL (Link)', 'logan' ),
				'param_name' => 'double_link',
				'description' => esc_html__( 'Add link to button.', 'logan' ),
				'admin_label' => true,
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'logan' ),
				'param_name' => 'icon_double',
				'value' => '', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 4000,
			        'type' => 'budicons',
				),
				'admin_label' => true,
				'description' => esc_html__( 'Select icon from library.', 'logan' ),
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon position', 'logan' ),
				'param_name' => 'double_icon_position',
				'value' => array(
					esc_html__( 'Before', 'logan' ) => 'before',
					esc_html__( 'After', 'logan' ) => 'after',
				),
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Icon color', 'logan' ),
				'param_name' => 'double_icon_color',
				'value' => '',
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Icon color on hover', 'logan' ),
				'param_name' => 'double_icon_color_hover',
				'value' => '',
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Button content', 'logan' ),
				'param_name' => 'double_content',
				'value' => '',
				'admin_label' => true,
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Text color', 'logan' ),
				'param_name' => 'double_text_color',
				'value' => '',
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Text color on hover', 'logan' ),
				'param_name' => 'double_text_color_hover',
				'value' => '',
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background color', 'logan' ),
				'param_name' => 'double_bg_color',
				'value' => '',
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background color on hover', 'logan' ),
				'param_name' => 'double_bg_color_hover',
				'value' => '',
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Border color', 'logan' ),
				'param_name' => 'double_border_color',
				'value' => '',
				'group' =>  esc_html__( 'Double', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Border color on hover', 'logan' ),
				'param_name' => 'double_border_color_hover',
				'value' => '',
				'group' =>  esc_html__( 'Double', 'logan' )
			),

			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Type', 'logan' ),
				'param_name' => 'separator',
				'value' => array(
					esc_html__( 'Nothing', 'logan' ) => '',
					esc_html__( 'Text', 'logan' ) => 'text',
					esc_html__( 'Icon', 'logan' ) => 'icon',
					esc_html__( 'Border', 'logan' ) => 'border',
				),
				'group' =>  esc_html__( 'Separator', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Content', 'logan' ),
				'param_name' => 'separator_content',
				'group' =>  esc_html__( 'Separator', 'logan' ),
				'description' =>  esc_html__( 'Use a very short text like OR', 'logan' ),
				'dependency' => array(
					'element' => 'separator',
					'value' => array( 'text' )
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'logan' ),
				'param_name' => 'separator_icon',
				'admin_label' => true,
				'value' => '', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 4000,
			        'type' => 'budicons',
				),
				'description' => esc_html__( 'Select icon from library.', 'logan' ),
				'group' =>  esc_html__( 'Separator', 'logan' ),
				'dependency' => array(
					'element' => 'separator',
					'value' => array( 'icon' )
				),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Color', 'logan' ),
				'param_name' => 'separator_text_color',
				'value' => '',
				'group' =>  esc_html__( 'Separator', 'logan' ),
				'dependency' => array(
					'element' => 'separator',
					'value' => array( 'text', 'icon' )
				),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Border color', 'logan' ),
				'param_name' => 'separator_border_color',
				'value' => '',
				'group' =>  esc_html__( 'Separator', 'logan' ),
				'dependency' => array(
					'element' => 'separator',
					'value' => array( 'border' )
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Border width', 'logan' ),
				'param_name' => 'separator_border_width',
				'group' =>  esc_html__( 'Separator', 'logan' ),
				'dependency' => array(
					'element' => 'separator',
					'value' => array( 'border' )
				),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background color', 'logan' ),
				'param_name' => 'separator_bg_color',
				'value' => '',
				'group' =>  esc_html__( 'Separator', 'logan' ),
				'dependency' => array(
					'element' => 'separator',
					'value' => array( 'text', 'icon' )
				),
			),
			array(
				'type' => 'pix_hidden',
				'param_name' => 'custom_css_uniq_id',
				'value' => $custom_css_id,
				'group' =>  esc_html__( 'Separator', 'logan' ),
			),
			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
	            'group' => __( 'Design options', 'logan' ),
	        ),
		),
		'custom_markup' => '{{title}}<div class="pix-vc-shortcode-wrap pix-vc-button-wrap"></div>',
	) );
}
endif;//pix_vc_buttons

add_action( 'vc_before_init', 'pix_vc_integrate_buydicons' );
if ( !function_exists('pix_vc_integrate_buydicons')) :
/**
* Adds a custom icon set to VC
*/
function pix_vc_integrate_buydicons() {

	if ( class_exists( 'WPBakeryShortCode' ) && ! class_exists( 'WPBakeryShortCode_Logan_Icons' ) ) {
		class WPBakeryShortCode_Logan_Icons extends WPBakeryShortCode { }
	}

	vc_map( array(
		"name" => esc_html__( "Logan icon box", "logan" ),
		"base" => "logan_icons",
		"class" => "",
		"icon" => "pix-vc-iconboxes",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
		'heading' => esc_html__( 'Budicons', 'logan' ),
		'custom_markup' => '{{title}}<div class="pix-vc-shortcode-wrap"></div>',
		'js_view' => 'PixVcIconboxesView',
		'admin_enqueue_js' => LOGANADDONS_URL . 'scripts/vc-js.js',
		'admin_enqueue_css' => array(get_template_directory_uri() . '/font/budicon-font.css'),
		"description" => esc_html__( "Text boxes with icons", 'logan' ),
		"params" => array(
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Logan icon', 'logan' ),
				'param_name' => 'icon',
				'value' => 'logan-icon-web-browseinternetnetwork', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false,
					'iconsPerPage' => 4000,
			        'type' => 'budicons',
				),
				'description' => esc_html__( 'Select icon from library.', 'logan' ),
				'group' =>  esc_html__( 'Icon', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Icon color', 'logan' ),
				'param_name' => 'color',
				'value' => '',
				'group' =>  esc_html__( 'Icon', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Size (will be intended in pixels)', 'logan' ),
				'param_name' => 'size',
				'value' => '16',
				'group' =>  esc_html__( 'Icon', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon position', 'logan' ),
				'param_name' => 'position',
				'value' => array(
					esc_html__( 'top left', 'logan' ) => 'top left',
					esc_html__( 'top center', 'logan' ) => 'top center',
					esc_html__( 'top right', 'logan' ) => 'top right',
					esc_html__( 'middle left', 'logan' ) => 'middle left',
					esc_html__( 'middle right', 'logan' ) => 'middle right',
					esc_html__( 'bottom left', 'logan' ) => 'bottom left',
					esc_html__( 'bottom center', 'logan' ) => 'bottom center',
					esc_html__( 'bottom right', 'logan' ) => 'bottom right'
				),
				'description' => esc_html__( 'Vertical alignments depends on text, if you enter some.', 'logan' ),
				'group' =>  esc_html__( 'Icon', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon container', 'logan' ),
				'param_name' => 'wrap',
				'value' => array(
					esc_html__( 'square', 'logan' ) => 'square',
					esc_html__( 'circle', 'logan' ) => 'circle',
					esc_html__( 'rounded', 'logan' ) => 'rounded'
				),
				'group' =>  esc_html__( 'Icon', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Container background', 'logan' ),
				'param_name' => 'bg',
				'value' => '',
				'group' =>  esc_html__( 'Icon', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Container size (will be intended in pixels)', 'logan' ),
				'param_name' => 'wrap_size',
				'value' => '40',
				'group' =>  esc_html__( 'Icon', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Border width (will be intended in pixels)', 'logan' ),
				'param_name' => 'border_width',
				'value' => '0',
				'group' =>  esc_html__( 'Icon', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Border color', 'logan' ),
				'param_name' => 'border_color',
				'value' => '',
				'group' =>  esc_html__( 'Icon', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Border style', 'logan' ),
				'param_name' => 'border_style',
				'value' => array(
					esc_html__( 'none', 'logan' ) => 'none',
					esc_html__( 'hidden', 'logan' ) => 'hidden',
					esc_html__( 'dotted', 'logan' ) => 'dotted',
					esc_html__( 'dashed', 'logan' ) => 'dashed',
					esc_html__( 'solid', 'logan' ) => 'solid',
					esc_html__( 'double', 'logan' ) => 'double',
					esc_html__( 'groove', 'logan' ) => 'groove',
					esc_html__( 'ridge', 'logan' ) => 'ridge',
					esc_html__( 'inset', 'logan' ) => 'inset',
					esc_html__( 'outset', 'logan' ) => 'outset'
				),
				'std' => 'solid',
				'group' =>  esc_html__( 'Icon', 'logan' )
	        ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon animation', 'logan' ),
				'param_name' => 'icon_animation',
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' ),
				'group' =>  esc_html__( 'Icon', 'logan' )
	        ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Text animation', 'logan' ),
				'param_name' => 'text_animation',
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' ),
				'group' =>  esc_html__( 'Content', 'logan' )
	        ),
			array(
				'type' => 'textarea_html',
				'heading' => esc_html__( 'Text', 'logan' ),
				'param_name' => 'content',
				'value' => esc_html__( 'I am promo text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'logan' ),
				'group' =>  esc_html__( 'Content', 'logan' )
			),
			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
	            'group' => __( 'Design options', 'logan' ),
	        ),
		)
	) );
}
endif;


add_action( 'vc_before_init', 'pix_vc_integrate_gmaps' );
if ( !function_exists('pix_vc_integrate_gmaps')) :
/**
* Adds a Google Maps generator to VC
*/
function pix_vc_integrate_gmaps() {
    $arr = array(
		"name" => esc_html__( "Logan Google Maps", "logan" ),
		"base" => "logan_maps",
		"class" => "",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
		'heading' => esc_html__( 'Logan Google Maps', 'logan' ),
		"icon" => "pix-vc-maps",
		"description" => esc_html__( "Google maps allowing JSON styles.", 'logan' ),
        "params" => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Latitude', 'logan' ),
				'admin_label' => true,
				'param_name' => 'lat',
				'group' => esc_html__( 'Basic', 'logan' ),
				'value' => '41.890322',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Longitude', 'logan' ),
				'admin_label' => true,
				'param_name' => 'long',
				'group' => esc_html__( 'Basic', 'logan' ),
				'value' => '12.492312',
			),
			array(
                "type" => "pix_slider",
				'heading' => esc_html__( 'Width (percentage based on the map parent element)', 'logan' ),
                "param_name" => "width",
                "value" => "100",
                'settings' => array(
                    'step' => '1',
                    'min' => '0',
                    'max' => '100',
                    'range' => false
                ),
				'group' => esc_html__( 'Basic', 'logan' ),
			),
			array(
                "type" => "pix_slider",
				'heading' => esc_html__( 'Height (percentage based on the width here above)', 'logan' ),
				'param_name' => 'height',
				'value' => '56',
                'settings' => array(
                    'step' => '1',
                    'min' => '0',
                    'max' => '100',
                    'range' => false
                ),
				'group' => esc_html__( 'Basic', 'logan' ),
			),
			array(
                "type" => "textfield",
				'heading' => esc_html__( 'Min. height (in pixels)', 'logan' ),
				'param_name' => 'min_height',
				'value' => '0',
				'group' => esc_html__( 'Basic', 'logan' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Type', 'logan' ),
				'param_name' => 'type',
				'group' => esc_html__( 'Basic', 'logan' ),
				'value' => array(
					esc_html__( 'roadmap', 'logan' ) => 'roadmap',
					esc_html__( 'hybrid', 'logan' ) => 'hybrid',
					esc_html__( 'satellite', 'logan' ) => 'satellite',
					esc_html__( 'terrain', 'logan' ) => 'terrain',
					esc_html__( 'streetview', 'logan' ) => 'streetview'
				)
			),
			array(
                "type" => "pix_slider",
				'heading' => esc_html__( 'Zoom', 'logan' ),
				'description' => esc_html__( 'A number between 0 and 23 (some places allow only 17 at maximum zoom, it depends on the case', 'logan' ),
				'param_name' => 'zoom',
				'value' => '12',
                'settings' => array(
                    'step' => '1',
                    'min' => '0',
                    'max' => '23',
                    'range' => false
                ),
				'group' => esc_html__( 'Basic', 'logan' ),
			),
			array(
                "type" => "pix_slider",
				'heading' => esc_html__( 'Heading', 'logan' ),
				'description' => esc_html__( 'A number between -90 and 90 (degrees): it defines the rotation angle around the camera locus in degrees relative from true north (Google Streetview only).', 'logan' ),
				'param_name' => 'heading',
				'value' => '0',
                'settings' => array(
                    'step' => '1',
                    'min' => '-90',
                    'max' => '90',
                    'range' => false
                ),
				'group' => esc_html__( 'Streetview', 'logan' ),
			),
			array(
                "type" => "pix_slider",
				'heading' => esc_html__( 'Pitch', 'logan' ),
				'description' => esc_html__( 'A number between -90 and 90 (degrees): it defines the angle variance UP or DOWN from the camera\'s initial default pitch, which is often (but not always) flat horizontal (Google Streetview only).', 'logan' ),
				'param_name' => 'pitch',
				'value' => '0',
                'settings' => array(
                    'step' => '1',
                    'min' => '-90',
                    'max' => '90',
                    'range' => false
                ),
				'group' => esc_html__( 'Streetview', 'logan' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Streetview controls', 'logan' ),
				'param_name' => 'sw_ctrls',
				'group' => esc_html__( 'Streetview', 'logan' ),
				'value' => array(
					esc_html__( 'hide', 'logan' ) => 'hide',
					esc_html__( 'top_center', 'logan' ) => 'top_center',
					esc_html__( 'top_left', 'logan' ) => 'top_left',
					esc_html__( 'top_right', 'logan' ) => 'top_right',
					esc_html__( 'left_top', 'logan' ) => 'left_top',
					esc_html__( 'right_top', 'logan' ) => 'right_top',
					esc_html__( 'left_center', 'logan' ) => 'left_center',
					esc_html__( 'right_center', 'logan' ) => 'right_center',
					esc_html__( 'left_bottom', 'logan' ) => 'left_bottom',
					esc_html__( 'right_bottom', 'logan' ) => 'right_bottom',
					esc_html__( 'bottom_center', 'logan' ) => 'bottom_center',
					esc_html__( 'bottom_left', 'logan' ) => 'bottom_left',
					esc_html__( 'bottom_right', 'logan' ) => 'bottom_right',
				),
		        "std" => 'top_right'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Map type controls', 'logan' ),
				'param_name' => 'map_type_ctrls',
				'group' => esc_html__( 'Controls', 'logan' ),
				'value' => array(
					esc_html__( 'hide', 'logan' ) => 'hide',
					esc_html__( 'top_center', 'logan' ) => 'top_center',
					esc_html__( 'top_left', 'logan' ) => 'top_left',
					esc_html__( 'top_right', 'logan' ) => 'top_right',
					esc_html__( 'left_top', 'logan' ) => 'left_top',
					esc_html__( 'right_top', 'logan' ) => 'right_top',
					esc_html__( 'left_center', 'logan' ) => 'left_center',
					esc_html__( 'right_center', 'logan' ) => 'right_center',
					esc_html__( 'left_bottom', 'logan' ) => 'left_bottom',
					esc_html__( 'right_bottom', 'logan' ) => 'right_bottom',
					esc_html__( 'bottom_center', 'logan' ) => 'bottom_center',
					esc_html__( 'bottom_left', 'logan' ) => 'bottom_left',
					esc_html__( 'bottom_right', 'logan' ) => 'bottom_right',
				),
		        "std" => 'top_right'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Pan controls', 'logan' ),
				'param_name' => 'pan_ctrls',
				'group' => esc_html__( 'Controls', 'logan' ),
				'value' => array(
					esc_html__( 'hide', 'logan' ) => 'hide',
					esc_html__( 'top_center', 'logan' ) => 'top_center',
					esc_html__( 'top_left', 'logan' ) => 'top_left',
					esc_html__( 'top_right', 'logan' ) => 'top_right',
					esc_html__( 'left_top', 'logan' ) => 'left_top',
					esc_html__( 'right_top', 'logan' ) => 'right_top',
					esc_html__( 'left_center', 'logan' ) => 'left_center',
					esc_html__( 'right_center', 'logan' ) => 'right_center',
					esc_html__( 'left_bottom', 'logan' ) => 'left_bottom',
					esc_html__( 'right_bottom', 'logan' ) => 'right_bottom',
					esc_html__( 'bottom_center', 'logan' ) => 'bottom_center',
					esc_html__( 'bottom_left', 'logan' ) => 'bottom_left',
					esc_html__( 'bottom_right', 'logan' ) => 'bottom_right',
				),
		        "std" => 'top_left'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Zoom controls', 'logan' ),
				'param_name' => 'zoom_ctrls',
				'group' => esc_html__( 'Controls', 'logan' ),
				'value' => array(
					esc_html__( 'hide', 'logan' ) => 'hide',
					esc_html__( 'top_center', 'logan' ) => 'top_center',
					esc_html__( 'top_left', 'logan' ) => 'top_left',
					esc_html__( 'top_right', 'logan' ) => 'top_right',
					esc_html__( 'left_top', 'logan' ) => 'left_top',
					esc_html__( 'right_top', 'logan' ) => 'right_top',
					esc_html__( 'left_center', 'logan' ) => 'left_center',
					esc_html__( 'right_center', 'logan' ) => 'right_center',
					esc_html__( 'left_bottom', 'logan' ) => 'left_bottom',
					esc_html__( 'right_bottom', 'logan' ) => 'right_bottom',
					esc_html__( 'bottom_center', 'logan' ) => 'bottom_center',
					esc_html__( 'bottom_left', 'logan' ) => 'bottom_left',
					esc_html__( 'bottom_right', 'logan' ) => 'bottom_right',
				),
		        "std" => 'top_left'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Zoom controls size', 'logan' ),
				'param_name' => 'zoom_ctrls_size',
				'group' => esc_html__( 'Controls', 'logan' ),
				'value' => array(
					esc_html__( 'default', 'logan' ) => 'DEFAULT',
					esc_html__( 'small', 'logan' ) => 'SMALL',
					esc_html__( 'large', 'logan' ) => 'LARGE',
				),
		        "std" => 'DEFAULT'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Scale controls', 'logan' ),
				'param_name' => 'scale_ctrls',
				'group' => esc_html__( 'Controls', 'logan' ),
				'value' => array(
					esc_html__( 'show', 'logan' ) => '',
					esc_html__( 'hide', 'logan' ) => 'hide',
				),
		        "std" => ''
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display marker', 'logan' ),
				'param_name' => 'marker',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'display', 'logan' ) => '',
					esc_html__( 'hide', 'logan' ) => 'hide',
					esc_html__( 'custom', 'logan' ) => 'custom',
				),
		        "std" => ''
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Custom marker', 'logan' ),
				'param_name' => 'custom_marker',
				'description' => esc_html__( 'The marker will be display halved for Retina devices', 'logan' ),
				'group' => esc_html__( 'Design', 'logan' ),
				'dependency' => array(
					'element' => 'marker',
					'value' => array( 'custom' )
				),
			),
			array(
				'type' => 'textarea',
				'heading' => esc_html__( 'Map custom colors', 'logan' ),
				'description' => esc_html__( 'JSON code you can create with Snazzy or the tool you prefer.', 'logan' ),
				'param_name' => 'custom_style',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => '',
				'dependency' => array(
					'callback' => 'pix_vc_google_map_custom_json_sanitize',
				)
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Route starting point', 'logan' ),
				'description' => esc_html__( 'Latitude and logintude comma separated (41.890322,12.492312)', 'logan' ),
				'param_name' => 'route_start',
				'group' => esc_html__( 'Route', 'logan' ),
				'value' => '',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Route destination point', 'logan' ),
				'description' => esc_html__( 'Latitude and logintude comma separated (41.890322,12.492312)', 'logan' ),
				'param_name' => 'route_destination',
				'group' => esc_html__( 'Route', 'logan' ),
				'value' => '',
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Travel mode', 'logan' ),
				'param_name' => 'travel_mode',
				'group' => esc_html__( 'Route', 'logan' ),
				'value' => array(
					esc_html__( 'driving', 'logan' ) => 'driving',
					esc_html__( 'bicycling', 'logan' ) => 'bicycling',
					esc_html__( 'walking', 'logan' ) => 'walking',
				),
		        "std" => 'driving'
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Color of the path', 'logan' ),
				'param_name' => 'color_path',
				'group' => esc_html__( 'Route', 'logan' ),
				'value' => '#d42027',
			),
			array(
                "type" => "pix_slider",
				'heading' => esc_html__( 'Opacity of the path stroke', 'logan' ),
				'description' => esc_html__( 'A number between 0 and 1 (including decimals)', 'logan' ),
				'param_name' => 'color_path_opacity',
				'value' => '0.75',
                'settings' => array(
                    'step' => '0.01',
                    'min' => '0',
                    'max' => '1',
                    'range' => false
                ),
				'group' => esc_html__( 'Route', 'logan' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Width of the path stroke', 'logan' ),
				'param_name' => 'color_path_width',
				'group' => esc_html__( 'Route', 'logan' ),
				'value' => '3',
			),
			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
	            'group' => __( 'Design options', 'logan' ),
	        ),

        ),
    );

	vc_map( $arr );
}
endif;

/************************************************
*
*		LINK IMAGE
*
************************************************/
add_action( 'vc_before_init', 'pix_vc_imagebox' );
function pix_vc_imagebox() {
    $arr = array(
        "name" => esc_html__( "Logan image box", "logan" ),
        "base" => "logan_image_box",
        "class" => "",
        "icon" => "pix-vc-images",
		'js_view' => 'PixVcImageboxView',
		'admin_enqueue_js' => LOGANADDONS_URL . 'scripts/vc-js.js',
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
        "description" => esc_html__( "Box with image as background", "logan" ),
		'custom_markup' => '{{title}}<div class="pix-vc-shortcode-wrap"></div>',
        "params" => array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Image source', 'logan' ),
				'param_name' => 'source',
				'value' => array(
					esc_html__( 'Media library', 'logan' ) => 'media_library',
					esc_html__( 'External link', 'logan' ) => 'external_link'
				),
				'std' => 'media_library',
				'description' => esc_html__( 'Select image source.', 'logan' )
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Image', 'logan' ),
				'param_name' => 'image',
				'value' => '',
				'description' => esc_html__( 'Select image from media library.', 'logan' ),
				'dependency' => array(
					'element' => 'source',
					'value' => 'media_library'
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Image size', 'logan' ),
				'param_name' => 'size',
				'value' => 'full',
				'description' => esc_html__( 'Enter image size, example: "thumbnail", "medium", "pix-large", "full" or other sizes defined by theme. Look among documentation for more info. Alternatively enter size in pixels, example: array(200,100) (Width x Height).', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'External link', 'logan' ),
				'param_name' => 'custom_src',
				'description' => esc_html__( 'Select external link.', 'logan' ),
				'dependency' => array(
					'element' => 'source',
					'value' => 'external_link'
				),
			),
			array(
				'type' => 'vc_link',
				'heading' => esc_html__( 'URL (Link)', 'logan' ),
				'param_name' => 'link',
				'description' => esc_html__( 'Add link to button.', 'logan' )
			),
	        array(
	            "type" => "colorpicker",
	            "heading" => esc_html__( "Text color", "logan" ),
	            "param_name" => "color",
	            "value" => ''
	        ),
	        array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Text alignment', 'logan' ),
				'param_name' => 'align',
				'value' => array(
					esc_html__( 'Top left', 'logan' ) => 'top,left',
					esc_html__( 'Top center', 'logan' ) => 'top,center',
					esc_html__( 'Top right', 'logan' ) => 'top,right',
					esc_html__( 'Middle left', 'logan' ) => 'middle,left',
					esc_html__( 'Middle center', 'logan' ) => 'middle,center',
					esc_html__( 'Middle right', 'logan' ) => 'middle,right',
					esc_html__( 'Bottom left', 'logan' ) => 'bottom,left',
					esc_html__( 'Bottom center', 'logan' ) => 'bottom,center',
					esc_html__( 'Bottom right', 'logan' ) => 'bottom,right'
				),
				'std' => 'middle left',
	        ),

			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'CSS Animation', 'logan' ),
				'param_name' => 'css_animation',
				'admin_label' => true,
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' )
	        ),

			array(
				'type' => 'textarea_html',
				'heading' => esc_html__( 'Text', 'logan' ),
				'param_name' => 'content',
				'value' => esc_html__( 'I am promo text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'logan' )
			),
			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
	            'group' => __( 'Design options', 'logan' ),
	        ),
        )
    );

	vc_map( $arr );
}

add_action( 'vc_before_init', 'pix_css_animator_wrap' );
function pix_css_animator_wrap(){

	if ( class_exists( 'WPBakeryShortCodesContainer' ) && ! class_exists( 'WPBakeryShortCode_Logan_Css_Animation' ) ) {
		class WPBakeryShortCode_Logan_Css_Animation extends WPBakeryShortCodesContainer {
		}
	} else {
		global $composer_settings;

		if ( ! empty( $composer_settings ) ) {
			if ( array_key_exists( 'COMPOSER_LIB', $composer_settings ) ) {
				$lib_dir = $composer_settings['COMPOSER_LIB'];
				if ( file_exists( $lib_dir . 'shortcodes.php' ) ) {
					require_once( $lib_dir . 'shortcodes.php' );
				}
			}
		}

		if ( class_exists( 'WPBakeryShortCodesContainer' ) && ! class_exists( 'WPBakeryShortCode_Logan_Css_Animation' ) ) {
			class WPBakeryShortCode_Logan_Css_Animation extends WPBakeryShortCodesContainer {
			}
		}
	}

    $arr = array(
	    "name" => esc_html__( "CSS Styler and Animator", 'logan' ),
	    "base" => "logan_css_animation",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
	    "as_parent" => array('except' => 'css_animation'),
	    "content_element" => true,
	    "icon" => "pix-vc-css",
	    "js_view" => 'VcColumnView',
		"description" => esc_html__( "Add animations to your elements.", 'logan' ),
	    "params" => array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'CSS Animation', 'logan' ),
				'param_name' => 'fx',
                "admin_label" => true,
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers). Do not use together with parallax (field below), but use instead nested elements', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display', 'logan' ),
				'param_name' => 'display',
                "admin_label" => true,
				'value' => array(
					'block' => 'block',
					'inline-block' => 'inline-block'
				),
				'description' => esc_html__( 'If you see weird behaviour on the front end try to change this value', 'logan' )
			),
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Parallax", 'logan'),
                "admin_label" => true,
                "param_name" => "parallax",
                "std" => "",
				'description' => esc_html__( 'Do not use together with parallax (field below), but use instead nested elements', 'logan' ),
                "value" => array(
                    "No parallax" => "",
                    "-0.90" => "-0.90",
                    "-0.80" => "-0.80",
                    "-0.70" => "-0.70",
                    "-0.60" => "-0.60",
                    "-0.50" => "-0.50",
                    "-0.40" => "-0.40",
                    "-0.30" => "-0.30",
                    "-0.20" => "-0.20",
                    "-0.10" => "-0.10",
                    "-0.05" => "-0.05",
                    "0.05" => "0.05",
                    "0.10" => "0.10",
                    "0.20" => "0.20",
                    "0.30" => "0.30",
                    "0.40" => "0.40",
                    "0.50" => "0.50",
                    "0.60" => "0.60",
                    "0.70" => "0.70",
                    "0.80" => "0.80",
                    "0.90" => "0.90",
                    "1" => "1",
                    "1.05" => "1.05",
                    "1.10" => "1.10",
                    "1.20" => "1.20",
                    "1.30" => "1.30",
                    "1.40" => "1.40",
                    "1.50" => "1.50",
                    "1.60" => "1.60",
                    "1.70" => "1.70",
                    "1.80" => "1.80",
                    "1.90" => "1.90",
                ),
			),
			array(
				'type' => 'checkbox',
                "heading" => esc_html__("Fade parallax", 'logan'),
				'param_name' => 'fade_parallax',
                "heading" => esc_html__("Fade parallax", 'logan'),
				'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' ),
                "std" => "",
				'description' => esc_html__( 'If checked a fade effect will be added during the scroll if parallax value is selected', 'logan' )
			),
 			array(
               "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Box shadow", 'logan'),
                "admin_label" => true,
                "param_name" => "pix_box_shadow",
                "value" => array(
                    esc_html__( "No shadow", 'logan' )  => "",
                    esc_html__( "Mild shadow", 'logan' ) => "mild-pix-box-shadow",
                    esc_html__( "Shadow", 'logan' ) => "pix-box-shadow"
                )
			),
			array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Z-Index", 'logan'),
                "admin_label" => true,
                "param_name" => "z_index",
				'description' => esc_html__( 'Use this field to put elements over or under the other ones', 'logan' ),
                "value" => "",               
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Additional class', 'logan' ),
				'param_name' => 'class',
				'value' => ''
			),

			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
                "admin_label" => true,
	            'group' => __( 'Design options', 'logan' ),
	        ),
	    ),
	);

	vc_map( $arr );
}

add_action( 'vc_before_init', 'pix_vc_projectlist' );
function pix_vc_projectlist() {

	$uniqid = uniqid('post-list-');

    $arr = array(
        "name" => esc_html__( "Logan project list", "logan" ),
        "description" => esc_html__( "Display your portfolio items", "logan" ),
        "base" => "logan_project",
        "icon" => "pix-vc-projects",
        "class" => "",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
        "params" => array(
			array(
				'type' => 'autocomplete',
				'heading' => esc_html__( 'Include only', 'logan' ),
				'param_name' => 'include',
				'description' => esc_html__( 'Start typing posts by title. Leave blank to display everything (you cannot combine include and exclude in the same shortcode', 'logan' ),
				'group' => esc_html__( 'Content', 'logan' ),
				'settings' => array(
					'min_length' => 1,
					'multiple' => true,
					'sortable' => true,
					'groups' => true,
				)
			),
			array(
				'type' => 'autocomplete',
				'heading' => esc_html__( 'Exclude posts', 'logan' ),
				'param_name' => 'exclude',
				'description' => esc_html__( 'Start typing posts by title. Leave blank to display everything (you cannot combine include and exclude in the same shortcode', 'logan' ),
				'group' => esc_html__( 'Content', 'logan' ),
				'settings' => array(
					'min_length' => 1,
					'multiple' => true,
					'sortable' => true,
					'groups' => true,
				)
			),
			array(
				'type' => 'autocomplete',
				'heading' => esc_html__( 'Projects', 'logan' ),
				'param_name' => 'taxonomies',
				'group' => esc_html__( 'Content', 'logan' ),
				'settings' => array(
					'multiple' => true,
					// is multiple values allowed? default false
					// 'sortable' => true, // is values are sortable? default false
					'min_length' => 1,
					// min length to start search -> default 2
					// 'no_hide' => true, // In UI after select doesn't hide an select list, default false
					'groups' => true,
					// In UI show results grouped by groups, default false
					'unique_values' => true,
					// In UI show results except selected. NB! You should manually check values in backend, default false
					'display_inline' => true,
					// In UI show results inline view, default false (each value in own line)
					'delay' => 500,
					// delay for search. default 500
					'auto_focus' => true,
					// auto focus input, default true
					// 'values' => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter categories or tags. Leave blank to display everything', 'logan' ),
			),
			array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Posts already appeared on the page', 'logan' ),
				'param_name' => 'duplicate',
				'group' => esc_html__( 'Content', 'logan' ),
				'value' => array( esc_html__( 'Do not display them', 'logan' ) => 'yes' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Order', 'logan' ),
				'param_name' => 'order',
				'group' => esc_html__( 'Content', 'logan' ),
				'value' => array(
					esc_html__( 'Descending', 'logan' ) => 'DESC',
					esc_html__( 'Ascending', 'logan' ) => 'ASC'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Order by', 'logan' ),
				'param_name' => 'orderby',
				'group' => esc_html__( 'Content', 'logan' ),
				'value' => array(
					esc_html__( 'Date', 'logan' ) => 'date',
					esc_html__( 'Title', 'logan' ) => 'title',
					esc_html__( 'Slug', 'logan' ) => 'name',
					esc_html__( 'ID', 'logan' ) => 'ID',
					esc_html__( 'Author', 'logan' ) => 'author',
					esc_html__( 'Post type', 'logan' ) => 'type',
					esc_html__( 'Modified date', 'logan' ) => 'modified',
					esc_html__( 'Parent ID', 'logan' ) => 'parent',
					esc_html__( 'Random', 'logan' ) => 'rand',
					esc_html__( 'Comment count', 'logan' ) => 'comment_count',
					esc_html__( 'Menu order', 'logan' ) => 'menu_order',
					esc_html__( 'Custom (the order of the included posts)', 'logan' ) => 'post__in'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Grid', 'logan' ),
				'param_name' => 'grid',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'Rows', 'logan' ) => 'fitRows',
					esc_html__( 'Vertically cascading grid (masonry)', 'logan' ) => 'masonry',
					esc_html__( 'Horizontal carousel', 'logan' ) => 'carousel',
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Text position', 'logan' ),
				'param_name' => 'text_position',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'below the image', 'logan' ) => '',
					esc_html__( 'over the image', 'logan' ) => 'over',
					esc_html__( 'over on hover', 'logan' ) => 'hover'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Text align', 'logan' ),
				'param_name' => 'text_align',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'default', 'logan' ) => '',
					esc_html__( 'center', 'logan' ) => 'center',
					esc_html__( 'left', 'logan' ) => 'left',
					esc_html__( 'right', 'logan' ) => 'right'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'CSS Animation', 'logan' ),
				'param_name' => 'fx',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' )
	        ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (desktop view)', 'logan' ),
				'param_name' => 'columns',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => '3'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (tablet landscape view)', 'logan' ),
				'param_name' => 'columns_landscape',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'inherit', 'logan' ) => '',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => ''
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (tablet portrait view)', 'logan' ),
				'param_name' => 'columns_portrait',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'inherit', 'logan' ) => '',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => ''
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (mob. phones portrait view)', 'logan' ),
				'param_name' => 'columns_phones',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'inherit', 'logan' ) => '',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => ''
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Items per module', 'logan' ),
				'param_name' => 'ppp',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => get_option('jetpack_portfolio_posts_per_page'),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Pagination', 'logan' ),
				'param_name' => 'pagination',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'none', 'logan' ) => '',
					esc_html__( 'numbers', 'logan' ) => 'numbers',
					esc_html__( 'load more', 'logan' ) => 'infinite'
				)
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Gutter between elements', 'logan' ),
				'param_name' => 'gutter',
				'value' => '30',
				'group' => esc_html__( 'Design', 'logan' ),
				'description' => esc_html__( 'In pixels', 'logan' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Optional image width (will be intended in pixels)', 'logan' ),
				'param_name' => 'pixel',
				'value' => '',
				'description' => esc_html__( 'If left blank, the width of the image will be calculated according with content width and column amount', 'logan' ),
				'group' => esc_html__( 'Design', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Image ratio width', 'logan' ),
				'param_name' => 'width',
				'value' => '4',
				'group' => esc_html__( 'Design', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Image ratio height', 'logan' ),
				'param_name' => 'height',
				'value' => '3',
				'group' => esc_html__( 'Design', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display tag filter', 'logan' ),
				'param_name' => 'filter',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'display', 'logan' ) => '',
					esc_html__( 'hide', 'logan' ) => 'hide'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display project categories', 'logan' ),
				'param_name' => 'categories',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'display', 'logan' ) => '',
					esc_html__( 'hide', 'logan' ) => 'hide'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display title', 'logan' ),
				'param_name' => 'title',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'display', 'logan' ) => '',
					esc_html__( 'hide', 'logan' ) => 'hide'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Title heading tag', 'logan' ),
				'param_name' => 'heading',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'Heading 1', 'logan' ) => 'h1',
					esc_html__( 'Heading 2', 'logan' ) => 'h2',
					esc_html__( 'Heading 3', 'logan' ) => 'h3',
					esc_html__( 'Heading 4', 'logan' ) => 'h4',
					esc_html__( 'Heading 5', 'logan' ) => 'h5',
					esc_html__( 'Heading 6', 'logan' ) => 'h6',
				),
		        "std" => 'h5'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Link the title to', 'logan' ),
				'param_name' => 'link',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'nothing', 'logan' ) => '',
					esc_html__( 'the post', 'logan' ) => 'post'
				),
				'std' => 'post'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Open featured media in a modal box', 'logan' ),
				'param_name' => 'cbox',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'disable', 'logan' ) => '',
					esc_html__( 'enable', 'logan' ) => 'enable'
				),
				'std' => 'enable'
			),
			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
	            'group' => __( 'Design options', 'logan' ),
	        ),
			array(
				'type' => 'pix_hidden',
				'param_name' => 'uniqid',
				'value' => $uniqid,
	            'group' => __( 'Design options', 'logan' ),
			),
        ),
    );

	if ( function_exists( 'printLikes' ) ) {
		$arr['params'][] = array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Display likes counter', 'logan' ),
			'param_name' => 'likes',
			'group' => esc_html__( 'Elements', 'logan' ),
			'value' => array(
				esc_html__( 'display', 'logan' ) => '',
				esc_html__( 'hide', 'logan' ) => 'hide'
			)
		);
	}

	$arr['params'][] = array(
		'type' => 'dropdown',
		'heading' => esc_html__( 'Display read more button', 'logan' ),
		'param_name' => 'read_more',
		'group' => esc_html__( 'Elements', 'logan' ),
		'value' => array(
			esc_html__( 'display', 'logan' ) => '',
			esc_html__( 'hide', 'logan' ) => 'hide'
		)
	);

	vc_map( $arr );
}

add_action( 'vc_before_init', 'pix_vc_postlist' );
function pix_vc_postlist() {

	$uniqid = uniqid('post-list-');

    $arr = array(
        "name" => esc_html__( "Logan post list", "logan" ),
        "description" => esc_html__( "Display your blog posts", "logan" ),
        "base" => "logan_post",
        "class" => "",
        "icon" => "pix-vc-posts",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
        "params" => array(
			array(
				'type' => 'autocomplete',
				'heading' => esc_html__( 'Include only', 'logan' ),
				'param_name' => 'include',
				'description' => esc_html__( 'Start typing posts by title. Leave blank to display everything (you cannot combine include and exclude in the same shortcode', 'logan' ),
				'group' => esc_html__( 'Content', 'logan' ),
				'settings' => array(
					'min_length' => 1,
					'multiple' => true,
					'sortable' => true,
					'groups' => true,
				)
			),
			array(
				'type' => 'autocomplete',
				'heading' => esc_html__( 'Exclude posts', 'logan' ),
				'param_name' => 'exclude',
				'description' => esc_html__( 'Start typing posts by title. Leave blank to display everything (you cannot combine include and exclude in the same shortcode', 'logan' ),
				'group' => esc_html__( 'Content', 'logan' ),
				'settings' => array(
					'min_length' => 1,
					'multiple' => true,
					'sortable' => true,
					'groups' => true,
				)
			),
			array(
				'type' => 'autocomplete',
				'heading' => esc_html__( 'Categories and tags', 'logan' ),
				'param_name' => 'taxonomies',
				'group' => esc_html__( 'Content', 'logan' ),
				'settings' => array(
					'multiple' => true,
					// is multiple values allowed? default false
					// 'sortable' => true, // is values are sortable? default false
					'min_length' => 1,
					// min length to start search -> default 2
					// 'no_hide' => true, // In UI after select doesn't hide an select list, default false
					'groups' => true,
					// In UI show results grouped by groups, default false
					'unique_values' => true,
					// In UI show results except selected. NB! You should manually check values in backend, default false
					'display_inline' => true,
					// In UI show results inline view, default false (each value in own line)
					'delay' => 500,
					// delay for search. default 500
					'auto_focus' => true,
					// auto focus input, default true
					// 'values' => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter categories or tags. Leave blank to display everything', 'logan' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Sticky posts', 'logan' ),
				'param_name' => 'sticky',
				'group' => esc_html__( 'Content', 'logan' ),
				'value' => array(
					esc_html__( 'ignore', 'logan' ) => 'ignore',
					esc_html__( 'display', 'logan' ) => 'display'
				)
			),
			array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Posts already appeared on the page', 'logan' ),
				'param_name' => 'duplicate',
				'group' => esc_html__( 'Content', 'logan' ),
				'value' => array( esc_html__( 'Do not display them', 'logan' ) => 'yes' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Order', 'logan' ),
				'param_name' => 'order',
				'group' => esc_html__( 'Content', 'logan' ),
				'value' => array(
					esc_html__( 'Descending', 'logan' ) => 'DESC',
					esc_html__( 'Ascending', 'logan' ) => 'ASC'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Order by', 'logan' ),
				'param_name' => 'orderby',
				'group' => esc_html__( 'Content', 'logan' ),
				'value' => array(
					esc_html__( 'Date', 'logan' ) => 'date',
					esc_html__( 'Title', 'logan' ) => 'title',
					esc_html__( 'Slug', 'logan' ) => 'name',
					esc_html__( 'ID', 'logan' ) => 'ID',
					esc_html__( 'Author', 'logan' ) => 'author',
					esc_html__( 'Post type', 'logan' ) => 'type',
					esc_html__( 'Modified date', 'logan' ) => 'modified',
					esc_html__( 'Parent ID', 'logan' ) => 'parent',
					esc_html__( 'Random', 'logan' ) => 'rand',
					esc_html__( 'Comment count', 'logan' ) => 'comment_count',
					esc_html__( 'Menu order', 'logan' ) => 'menu_order',
					esc_html__( 'Custom (the order of the included posts)', 'logan' ) => 'post__in'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Grid', 'logan' ),
				'param_name' => 'grid',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'Rows', 'logan' ) => 'fitRows',
					esc_html__( 'Vertically cascading grid (masonry)', 'logan' ) => 'masonry',
					esc_html__( 'Horizontal carousel', 'logan' ) => 'carousel',
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Text position', 'logan' ),
				'param_name' => 'text_position',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'below the image', 'logan' ) => '',
					esc_html__( 'over the image', 'logan' ) => 'over',
					esc_html__( 'left of the image', 'logan' ) => 'left',
					esc_html__( 'right of the image', 'logan' ) => 'right'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'CSS Animation', 'logan' ),
				'param_name' => 'fx',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' )
	        ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Text align', 'logan' ),
				'param_name' => 'text_align',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'default', 'logan' ) => '',
					esc_html__( 'center', 'logan' ) => 'center',
					esc_html__( 'left', 'logan' ) => 'left',
					esc_html__( 'right', 'logan' ) => 'right'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (desktop view)', 'logan' ),
				'param_name' => 'columns',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => '3'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (tablet landscape view)', 'logan' ),
				'param_name' => 'columns_landscape',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'inherit', 'logan' ) => '',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => ''
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (tablet portrait view)', 'logan' ),
				'param_name' => 'columns_portrait',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'inherit', 'logan' ) => '',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => ''
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (mob. phones portrait view)', 'logan' ),
				'param_name' => 'columns_phones',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'inherit', 'logan' ) => '',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => ''
			),
			array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Make the first post width double', 'logan' ),
				'param_name' => 'first_double',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Items per module', 'logan' ),
				'param_name' => 'ppp',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => get_option('posts_per_page'),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Pagination', 'logan' ),
				'param_name' => 'pagination',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'none', 'logan' ) => '',
					esc_html__( 'numbers', 'logan' ) => 'numbers',
					esc_html__( 'load more', 'logan' ) => 'infinite'
				)
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Gutter between elements', 'logan' ),
				'param_name' => 'gutter',
				'value' => '30',
				'group' => esc_html__( 'Design', 'logan' ),
				'description' => esc_html__( 'In pixels', 'logan' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Optional image width (will be intended in pixels)', 'logan' ),
				'param_name' => 'pixel',
				'value' => '',
				'description' => esc_html__( 'If left blank, the width of the image will be calculated according with content width and column amount', 'logan' ),
				'group' => esc_html__( 'Design', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Image ratio width', 'logan' ),
				'param_name' => 'width',
				'value' => '4',
				'group' => esc_html__( 'Design', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Image ratio height', 'logan' ),
				'param_name' => 'height',
				'value' => '3',
				'group' => esc_html__( 'Design', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display featured image', 'logan' ),
				'param_name' => 'image',
				'group' => esc_html__( 'Elements', 'logan' ),
				'description' => esc_html__( '(or the media relative to the post format)', 'logan' ),
				'value' => array(
					esc_html__( 'display', 'logan' ) => '',
					esc_html__( 'hide', 'logan' ) => 'hide'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display categories', 'logan' ),
				'param_name' => 'categories',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'display', 'logan' ) => '',
					esc_html__( 'hide', 'logan' ) => 'hide'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display post format icon', 'logan' ),
				'param_name' => 'format',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'display', 'logan' ) => '',
					esc_html__( 'hide', 'logan' ) => 'hide'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display title', 'logan' ),
				'param_name' => 'title',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'display', 'logan' ) => '',
					esc_html__( 'hide', 'logan' ) => 'hide'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Title heading tag', 'logan' ),
				'param_name' => 'heading',
				'group' => esc_html__( 'Elements', 'logan' ),
				'value' => array(
					esc_html__( 'Heading 1', 'logan' ) => 'h1',
					esc_html__( 'Heading 2', 'logan' ) => 'h2',
					esc_html__( 'Heading 3', 'logan' ) => 'h3',
					esc_html__( 'Heading 4', 'logan' ) => 'h4',
					esc_html__( 'Heading 5', 'logan' ) => 'h5',
					esc_html__( 'Heading 6', 'logan' ) => 'h6',
				),
		        "std" => 'h3'
			),
			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
	            'group' => __( 'Design options', 'logan' ),
	        ),
 			array(
				'type' => 'pix_hidden',
				'param_name' => 'uniqid',
				'value' => $uniqid,
	            'group' => __( 'Design options', 'logan' ),
			),
       ),
    );

	if ( class_exists( 'Subtitles' ) ) {
		$arr['params'][] = array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Display subtitles', 'logan' ),
			'param_name' => 'subtitle',
			'group' => esc_html__( 'Elements', 'logan' ),
			'value' => array(
				esc_html__( 'display', 'logan' ) => '',
				esc_html__( 'hide', 'logan' ) => 'hide'
			)
		);
	}

	if ( function_exists( 'printLikes' ) ) {
		$arr['params'][] = array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Display likes counter', 'logan' ),
			'param_name' => 'likes',
			'group' => esc_html__( 'Elements', 'logan' ),
			'value' => array(
				esc_html__( 'display', 'logan' ) => '',
				esc_html__( 'hide', 'logan' ) => 'hide'
			)
		);
	}

	if ( function_exists( 'wp_review_show_total' ) ) {
		$arr['params'][] = array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Display post reviews', 'logan' ),
			'param_name' => 'reviews',
			'group' => esc_html__( 'Elements', 'logan' ),
			'value' => array(
				esc_html__( 'display', 'logan' ) => '',
				esc_html__( 'hide', 'logan' ) => 'hide'
			)
		);
	}

	$arr['params'][] = array(
		'type' => 'dropdown',
		'heading' => esc_html__( 'Display author', 'logan' ),
		'param_name' => 'author',
		'group' => esc_html__( 'Elements', 'logan' ),
		'value' => array(
			esc_html__( 'display', 'logan' ) => '',
			esc_html__( 'hide', 'logan' ) => 'hide'
		)
	); 
	$arr['params'][] = array(
		'type' => 'dropdown',
		'heading' => esc_html__( 'Display avatar', 'logan' ),
		'param_name' => 'avatar',
		'group' => esc_html__( 'Elements', 'logan' ),
		'value' => array(
			esc_html__( 'display', 'logan' ) => '',
			esc_html__( 'hide', 'logan' ) => 'hide'
		),
	);
	$arr['params'][] = array(
		'type' => 'dropdown',
		'heading' => esc_html__( 'Display date', 'logan' ),
		'param_name' => 'date',
		'group' => esc_html__( 'Elements', 'logan' ),
		'value' => array(
			esc_html__( 'from now', 'logan' ) => '',
			esc_html__( 'absolute', 'logan' ) => 'absolute',
			esc_html__( 'hide', 'logan' ) => 'hide'
		)
	);
	$arr['params'][] = array(
		'type' => 'dropdown',
		'heading' => esc_html__( 'Display reading time', 'logan' ),
		'param_name' => 'time',
		'group' => esc_html__( 'Elements', 'logan' ),
		'value' => array(
			esc_html__( 'display', 'logan' ) => '',
			esc_html__( 'hide', 'logan' ) => 'hide'
		)
	);
	$arr['params'][] = array(
		'type' => 'dropdown',
		'heading' => esc_html__( 'Display comment amount', 'logan' ),
		'param_name' => 'comments',
		'group' => esc_html__( 'Elements', 'logan' ),
		'value' => array(
			esc_html__( 'display', 'logan' ) => '',
			esc_html__( 'hide', 'logan' ) => 'hide'
		)
	);
	$arr['params'][] = array(
		'type' => 'dropdown',
		'heading' => esc_html__( 'Display content', 'logan' ),
		'param_name' => 'the_content',
		'group' => esc_html__( 'Elements', 'logan' ),
		'value' => array(
			esc_html__( 'no content', 'logan' ) => '',
			esc_html__( 'content', 'logan' ) => 'content',
			esc_html__( 'excerpt', 'logan' ) => 'excerpt'
		)
	);
	$arr['params'][] = array(
		'type' => 'dropdown',
		'heading' => esc_html__( 'Display read more button', 'logan' ),
		'param_name' => 'read_more',
		'group' => esc_html__( 'Elements', 'logan' ),
		'value' => array(
			esc_html__( 'display', 'logan' ) => '',
			esc_html__( 'hide', 'logan' ) => 'hide'
		)
	);

	vc_map( $arr );
}


add_action( 'vc_before_init', 'pix_vc_member' );
function pix_vc_member() {
    $arr = array(
        "name" => esc_html__( "Logan team members", "logan" ),
        "description" => esc_html__( "Display your team members", "logan" ),
        "base" => "logan_team_members",
        "class" => "",
        "icon" => "pix-vc-members",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
        "params" => array(
			array(
				'type' => 'autocomplete',
				'heading' => esc_html__( 'Include only', 'logan' ),
				'param_name' => 'include',
				'description' => esc_html__( 'Start typing member name. Leave blank to display everything (you cannot combine include and exclude in the same shortcode', 'logan' ),
				'group' => esc_html__( 'Content', 'logan' ),
				'settings' => array(
					'min_length' => 1,
					'multiple' => true,
					'sortable' => true,
					'groups' => true,
				)
			),
			array(
				'type' => 'autocomplete',
				'heading' => esc_html__( 'Exclude members', 'logan' ),
				'param_name' => 'exclude',
				'description' => esc_html__( 'Start typing member name. Leave blank to display everything (you cannot combine include and exclude in the same shortcode', 'logan' ),
				'group' => esc_html__( 'Content', 'logan' ),
				'settings' => array(
					'min_length' => 1,
					'multiple' => true,
					'sortable' => true,
					'groups' => true,
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Order', 'logan' ),
				'param_name' => 'order',
				'group' => esc_html__( 'Content', 'logan' ),
				'value' => array(
					esc_html__( 'Descending', 'logan' ) => 'DESC',
					esc_html__( 'Ascending', 'logan' ) => 'ASC'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Order by', 'logan' ),
				'param_name' => 'orderby',
				'group' => esc_html__( 'Content', 'logan' ),
				'value' => array(
					esc_html__( 'Date', 'logan' ) => 'date',
					esc_html__( 'Title', 'logan' ) => 'title',
					esc_html__( 'Slug', 'logan' ) => 'name',
					esc_html__( 'ID', 'logan' ) => 'ID',
					esc_html__( 'Modified date', 'logan' ) => 'modified',
					esc_html__( 'Random', 'logan' ) => 'rand',
					esc_html__( 'Menu order', 'logan' ) => 'menu_order',
					esc_html__( 'Custom (the order of the included team members)', 'logan' ) => 'post__in'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'CSS Animation', 'logan' ),
				'param_name' => 'fx',
				'admin_label' => 'true',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' )
	        ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (desktop view)', 'logan' ),
				'param_name' => 'columns',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => '3'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (tablet landscape view)', 'logan' ),
				'param_name' => 'columns_landscape',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'inherit', 'logan' ) => '',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => ''
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (tablet portrait view)', 'logan' ),
				'param_name' => 'columns_portrait',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'inherit', 'logan' ) => '',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => ''
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Elements per row (mob. phones portrait view)', 'logan' ),
				'param_name' => 'columns_phones',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => array(
					esc_html__( 'inherit', 'logan' ) => '',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6'
				),
		        "std" => ''
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Items per module', 'logan' ),
				'param_name' => 'ppp',
				'group' => esc_html__( 'Design', 'logan' ),
				'value' => get_option('posts_per_page'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Gutter between elements', 'logan' ),
				'param_name' => 'gutter',
				'value' => '30',
				'group' => esc_html__( 'Design', 'logan' ),
				'description' => esc_html__( 'In pixels', 'logan' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Custom max width', 'logan' ),
				'param_name' => 'max_width',
				'value' => '',
				'group' => esc_html__( 'Design', 'logan' ),
				'description' => esc_html__( 'In pixels. If left blank, it will be calculated on content width', 'logan' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Member name heading tag', 'logan' ),
				'param_name' => 'heading',
				'group' => esc_html__( 'Look', 'logan' ),
				'value' => array(
					esc_html__( 'Heading 1', 'logan' ) => 'h1',
					esc_html__( 'Heading 2', 'logan' ) => 'h2',
					esc_html__( 'Heading 3', 'logan' ) => 'h3',
					esc_html__( 'Heading 4', 'logan' ) => 'h4',
					esc_html__( 'Heading 5', 'logan' ) => 'h5',
					esc_html__( 'Heading 6', 'logan' ) => 'h6',
					esc_html__( 'Paragraph', 'logan' ) => 'p',
				),
		        "std" => 'h4'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Link the name to', 'logan' ),
				'param_name' => 'link',
				'group' => esc_html__( 'Look', 'logan' ),
				'value' => array(
					esc_html__( 'nothing', 'logan' ) => '',
					esc_html__( 'the post', 'logan' ) => 'post'
				),
				'std' => 'post'
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Ratio width', 'logan' ),
				'param_name' => 'width',
				'value' => '',
				'group' => esc_html__( 'Design', 'logan' ),
				'description' => esc_html__( 'An integer number, you must fill out the ratio height as well', 'logan' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Ratio height', 'logan' ),
				'param_name' => 'height',
				'value' => '',
				'group' => esc_html__( 'Design', 'logan' ),
				'description' => esc_html__( 'An integer number, you must fill out the ratio height as width', 'logan' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Text position', 'logan' ),
				'param_name' => 'position',
				'group' => esc_html__( 'Look', 'logan' ),
				'value' => array(
					esc_html__( 'Over', 'logan' ) => '',
					esc_html__( 'On hover state', 'logan' ) => 'hover',
					esc_html__( 'Below the image', 'logan' ) => 'below'
				),
		        "std" => ''
			),
			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
	            'group' => __( 'Design options', 'logan' ),
	        ),
        ),
    );

	vc_map( $arr );
}

add_action( 'vc_before_init', 'pix_vc_separators' );
if ( !function_exists('pix_vc_separators')) :
function pix_vc_separators() {

	vc_map( array(
		"name" => esc_html__( "Logan typography separators", "logan" ),
		"base" => "logan_separator",
		"class" => "",
		"icon" => "pix-vc-separators",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
		'heading' => esc_html__( 'Separators', 'logan' ),
		'custom_markup' => '{{title}}<div class="pix-vc-shortcode-wrap"></div>',
		'js_view' => 'PixVcSeparatorView',
		'admin_enqueue_js' => LOGANADDONS_URL . 'scripts/vc-js.js',
		'admin_enqueue_css' => array(get_template_directory_uri() . '/font/budicon-font.css'),
		"description" => esc_html__( "Rules or borders to visually separate text blocks", 'logan' ),
		"params" => array(
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Rule color', 'logan' ),
				'param_name' => 'color',
				'value' => '#222324',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Alignment', 'logan' ),
				'param_name' => 'alignment',
				'value' => array(
					esc_html__( 'Center', 'logan' ) => 'center',
					esc_html__( 'Left', 'logan' ) => 'left',
					esc_html__( 'Right', 'logan' ) => 'right',
				),
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Height (will be intended in pixels)', 'logan' ),
				'param_name' => 'height',
				'value' => '2',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Max-width (will be intended in pixels)', 'logan' ),
				'param_name' => 'max_width',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Width (will be intended in percentage)', 'logan' ),
				'param_name' => 'width',
				'value' => '100',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'CSS animation', 'logan' ),
				'param_name' => 'fx',
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' ),
				'group' =>  esc_html__( 'General', 'logan' )
	        ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'ID', 'logan' ),
				'param_name' => 'id',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Class', 'logan' ),
				'param_name' => 'class',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
	            'group' => __( 'Design options', 'logan' ),
	        ),
		)
	) );
}
endif;

add_action( 'vc_before_init', 'pix_vc_progress_bars' );
if ( !function_exists('pix_vc_progress_bars')) :
function pix_vc_progress_bars() {

	vc_map( array(
		"name" => esc_html__( "Logan progress bars", "logan" ),
		"base" => "logan_progress",
		"class" => "",
		"icon" => "pix-vc-progress",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
		'heading' => esc_html__( 'Progress bars', 'logan' ),
		"description" => esc_html__( "Animated linear progress bars", 'logan' ),
		"params" => array(
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background color', 'logan' ),
				'param_name' => 'bg',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Progress bar color', 'logan' ),
				'param_name' => 'color',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Label', 'logan' ),
				'param_name' => 'label',
				'admin_label' => true,
				'value' => 'Skill',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
                "type" => "pix_slider",
				'heading' => esc_html__( 'Amount', 'logan' ),
				'admin_label' => true,
				'description' => esc_html__( 'A number from 0 to 100', 'logan' ),
                "param_name" => "amount",
                "value" => "50",
                'settings' => array(
                    'step' => '1',
                    'min' => '0',
                    'max' => '100',
                    'range' => false
                ),
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Value', 'logan' ),
				'description' => esc_html__( 'An integer, positive number, it will be displayed above the progress bar', 'logan' ),
				'param_name' => 'counter',
				'value' => '',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Unit', 'logan' ),
				'description' => esc_html__( 'Type the unit of measure or anything else you want to display near the value', 'logan' ),
				'param_name' => 'unit',
				'value' => '%',
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Unit position', 'logan' ),
				'param_name' => 'unit_position',
				'value' => array(
					esc_html__( 'after the value', 'logan' ) => '',
					esc_html__( 'before the value', 'logan' ) => 'before',
				),
				'group' =>  esc_html__( 'General', 'logan' )
			),
			array(
	            'type' => 'css_editor',
	            'heading' => __( 'Css', 'logan' ),
	            'param_name' => 'css',
	            'group' => __( 'Design options', 'logan' ),
	        ),
		)
	) );
}
endif;

add_action( 'vc_before_init', 'pix_vc_media' );
if ( !function_exists('pix_vc_media')) :
function pix_vc_media() {

	vc_map( array(
		"name" => esc_html__( "Logan media player", "logan" ),
		"base" => "pix_media",
		"class" => "",
		"icon" => "pix-vc-media",
        "category" => array(esc_html__( 'Content', 'logan' ), esc_html__( 'Logan', 'logan' ) ),
		'heading' => esc_html__( 'Media player', 'logan' ),
		"description" => esc_html__( "Add video and audio to your page", 'logan' ),
		"params" => array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Media type', 'logan' ),
				'param_name' => 'type',
				'admin_label' => true,
				'value' => array(
					esc_html__( 'Self hosted media', 'logan' ) => 'media',
					esc_html__( 'YouTube video', 'logan' ) => 'youtube',
					esc_html__( 'Vimeo video', 'logan' ) => 'vimeo',
					esc_html__( 'Soundcloud', 'logan' ) => 'soundcloud',
					esc_html__( 'Spotify', 'logan' ) => 'spotify',
				)
			),
			array(
				'type' => 'pix_add_media',
				'heading' => esc_html__( 'Media ID(s)', 'logan' ),
				'param_name' => 'sc',
				'value' => '',
				'description' => esc_html__( 'Select from media library.', 'logan' ),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'media' )
				),
			),
			array(
				'type' => 'textarea',
				'heading' => esc_html__( 'YouTube', 'logan' ),
				'param_name' => 'youtube',
				'value' => '',
				'description' => esc_html__( 'Paste here the page URL or the video ID. For playlists you must paste the playlist ID, starting with &quot;PL&quot;', 'logan' ),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'youtube' )
				),
			),
			array(
				'type' => 'textarea',
				'heading' => esc_html__( 'Vimeo', 'logan' ),
				'param_name' => 'vimeo',
				'value' => '',
				'description' => esc_html__( 'Paste here the page URL or the video ID', 'logan' ),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'vimeo' )
				),
			),
			array(
				'type' => 'pix_double_input',
				'heading' => esc_html__( 'Soundcloud', 'logan' ),
				'param_name' => 'soundcloud',
				'value' => '',
				'description' => esc_html__( 'Paste here the Wordpress shortcode, ex.: [soundcloud url=&quot;&hellip;', 'logan' ),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'soundcloud' )
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Spotify', 'logan' ),
				'param_name' => 'spotify',
				'value' => '',
				'description' => esc_html__( 'Paste here Spotify URI, ex.: spotify:track:4SRk3Bn5cOakigp3KcW43Q', 'logan' ),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'spotify' )
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Show related videos', 'logan' ),
				'param_name' => 'rel',
				'value' => array(
					esc_html__( 'no', 'logan' ) => '0',
					esc_html__( 'yes', 'logan' ) => '1',
				),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'youtube' )
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Automatically hide video controls after a video begins playing', 'logan' ),
				'param_name' => 'autohide',
				'value' => array(
					esc_html__( 'yes', 'logan' ) => '2',
					esc_html__( 'no', 'logan' ) => '0',
				),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'youtube' )
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Automatically start to play the video', 'logan' ),
				'param_name' => 'autoplay',
				'value' => array(
					esc_html__( 'no', 'logan' ) => '0',
					esc_html__( 'yes', 'logan' ) => '1',
				),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'youtube', 'vimeo' )
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display video player controls', 'logan' ),
				'param_name' => 'controls',
				'value' => array(
					esc_html__( 'yes', 'logan' ) => '1',
					esc_html__( 'no', 'logan' ) => '0',
				),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'youtube' )
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Loop: play the video again and again', 'logan' ),
				'param_name' => 'loop',
				'value' => array(
					esc_html__( 'no', 'logan' ) => '0',
					esc_html__( 'yes', 'logan' ) => '1',
				),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'youtube', 'vimeo' )
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display information like the video title and uploader before the video starts playing', 'logan' ),
				'param_name' => 'showinfo',
				'value' => array(
					esc_html__( 'yes', 'logan' ) => '1',
					esc_html__( 'no', 'logan' ) => '0',
				),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'youtube' )
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Begin to play the video at... (seconds from the start)', 'logan' ),
				'param_name' => 'start',
				'value' => '',
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'youtube' )
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Ends to play the video at... (seconds from the start)', 'logan' ),
				'param_name' => 'end',
				'value' => '',
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'youtube' )
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display user avatar', 'logan' ),
				'param_name' => 'portrait',
				'value' => array(
					esc_html__( 'no', 'logan' ) => '0',
					esc_html__( 'yes', 'logan' ) => '1',
				),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'vimeo' )
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display user info', 'logan' ),
				'param_name' => 'byline',
				'value' => array(
					esc_html__( 'no', 'logan' ) => '0',
					esc_html__( 'yes', 'logan' ) => '1',
				),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'vimeo' )
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display video title', 'logan' ),
				'param_name' => 'title',
				'value' => array(
					esc_html__( 'no', 'logan' ) => '0',
					esc_html__( 'yes', 'logan' ) => '1',
				),
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'vimeo' )
				),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Player accent color', 'logan' ),
				'param_name' => 'color',
				'value' => '',
				'dependency' => array(
					'element' => 'type',
					'value' => array( 'vimeo' )
				),
			),
			array(
				'type' => 'checkbox',
                "heading" => esc_html__("Open in a lightbox", 'logan'),
				'param_name' => 'lightbox',
				'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' ),
                "std" => "",
				'group' =>  esc_html__( 'Lightbox', 'logan' )
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Placeholder image', 'logan' ),
				'param_name' => 'placeholder',
				'group' => esc_html__( 'Lightbox', 'logan' ),
				'dependency' => array(
                    'element' => 'lightbox',
                    'not_empty' => true,
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Placeholder size', 'logan' ),
				'param_name' => 'thumb_size',
				'group' => esc_html__( 'Lightbox', 'logan' ),
				'value' => 'pix-large',
				'description' => esc_html__( 'Enter image size, example: "thumbnail", "medium", "pix-large", "full" or other sizes defined by theme. Look among documentation for more info. Alternatively enter size in pixels, example: array(200,100) (Width x Height).', 'logan' ),
				'dependency' => array(
                    'element' => 'lightbox',
                    'not_empty' => true,
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Lightbox gallery group', 'logan' ),
				'param_name' => 'group',
				'group' => esc_html__( 'Lightbox', 'logan' ),
				'value' => 'my-group',
				'description' => esc_html__( 'Type here a word (no special chars., no empty spaces), it will be used to group elements that has the same word together for a gallery', 'logan' ),
				'dependency' => array(
                    'element' => 'lightbox',
                    'not_empty' => true,
				),
			),
		)
	) );
}
endif;

add_action( 'vc_before_init', 'pix_vc_woocommerce' );
if ( !function_exists('pix_vc_woocommerce')) :
/**
* Workaround to avoid notices
*/
function pix_vc_woocommerce() {

    $woo_shortcodes_arr = array(
    	'product_category',
        'featured_products',
        'recent_products',
        'products',
        'sale_products',
        'best_selling_products',
        'top_rated_products',
        'product_attribute'
    );

    foreach ($woo_shortcodes_arr as $shortcode) {
		vc_map( array(
			'name' => __( 'Name' ),
			'base' => $shortcode,
			'params' => array(),
		) );
	}
}
endif;