<?php
vc_add_shortcode_param( 'pix_hidden', 'pix_hidden_settings_field' );
if ( !function_exists('pix_hidden_settings_field')) :
function pix_hidden_settings_field( $settings, $value ) {
    $value = $value == '' ? uniqid('pix-custom-') : $value;

   return '<input name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value ' .
             esc_attr( $settings['param_name'] ) . ' ' .
             esc_attr( $settings['type'] ) . '_field" type="hidden" value="' . esc_attr( $value ) . '" />';
}
endif;

vc_add_shortcode_param( 'pix_slider', 'pix_vc_slider_field', LOGANADDONS_URL . 'scripts/vc-js.js' );
if ( !function_exists('pix_vc_slider_field')) :
function pix_vc_slider_field( $settings, $value ) {
   return '<div class="pix-vc-slider" data-min="' . esc_attr( $settings['settings']['min'] ) . '" data-max="' . esc_attr( $settings['settings']['max'] ) . '" data-range="' . esc_attr( $settings['settings']['range'] ) . '" data-step="' . esc_attr( $settings['settings']['step'] ) . '">
        <div class="pix-vc-slider-value">' . esc_attr( $value ) . '</div>
        <div class="pix-actual-slider"></div>
        <input name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value ' .
             esc_attr( $settings['param_name'] ) . ' ' .
             esc_attr( $settings['type'] ) . '_field" type="hidden" value="' . esc_attr( $value ) . '" />
    </div>';
}
endif;

vc_add_shortcode_param( 'pix_add_media', 'pix_vc_add_media', LOGANADDONS_URL . 'scripts/vc-js.js' );
if ( !function_exists('pix_vc_add_media')) :
function pix_vc_add_media( $settings, $value ) {
   return '<div class="pix-vc-media">
        <textarea class="pix_add_media_field" readonly></textarea>
        <input name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value ' .
             esc_attr( $settings['param_name'] ) . ' ' .
             esc_attr( $settings['type'] ) . '_field" type="hidden" value="' . esc_attr( $value ) . '">
        <input type="button" class="button add_media" value="' . esc_html__( 'Select media', 'logan' ) . '">
        <input type="button" class="button edit_media" value="' . esc_html__( 'Edit media', 'logan' ) . '">
        <input type="button" class="button remove_media" value="' . esc_html__( 'Remove media', 'logan' ) . '">
    </div>';
}
endif;

vc_add_shortcode_param( 'pix_double_input', 'pix_vc_double_input', LOGANADDONS_URL . 'scripts/vc-js.js' );
if ( !function_exists('pix_vc_double_input')) :
function pix_vc_double_input( $settings, $value ) {
   return '<div class="pix-vc-double_input">
        <textarea class="pix_add_shortcode"></textarea>
        <input name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value ' .
             esc_attr( $settings['param_name'] ) . ' ' .
             esc_attr( $settings['type'] ) . '_field" type="hidden" value="' . esc_attr( $value ) . '">
    </div>';
}
endif;

add_action('vc_before_init', 'logan_vc_remove_shortcodes');
if ( !function_exists('logan_vc_remove_shortcodes')) :
/**
* @since Logan 1.0
*/
function logan_vc_remove_shortcodes() {

    if ( get_theme_mod('pix_vc_enable_hidden') == true )
        return;

    vc_remove_element( 'vc_cta' );
    vc_remove_element( 'vc_video' );
    vc_remove_element( 'rev_slider' );
    vc_remove_element( 'vc_gmaps' );
    vc_remove_element( 'vc_pie' );
    vc_remove_element( 'vc_progress_bar' );
    vc_remove_element( 'vc_posts_slider' );
    vc_remove_element( 'vc_basic_grid' );
    vc_remove_element( 'vc_masonry_grid' );

    /*VC Extensions*/
    vc_remove_element( 'TS-VCSC-Image-Adipoli' );
    vc_remove_element( 'TS-VCSC-Image-Picstrips' );
    vc_remove_element( 'TS-VCSC-Image-Caman' );
    vc_remove_element( 'TS_VCSC_Image_Magnify' );
    vc_remove_element( 'TS-VCSC-Image-Switch' );
    vc_remove_element( 'TS_VCSC_Image_Scroll' );
    vc_remove_element( 'TS_VCSC_Image_SVG' );
    vc_remove_element( 'TS_VCSC_Image_Full' );
    vc_remove_element( 'TS-VCSC-Image-Overlay' );
    vc_remove_element( 'TS_VCSC_Image_Link_Grid' );
    vc_remove_element( 'TS_VCSC_Image_Placeholder' );
    vc_remove_element( 'TS-VCSC-Lightbox-Image' );
    vc_remove_element( 'TS_VCSC_Soundcloud' );
    vc_remove_element( 'TS_VCSC_Mixcloud' );
    vc_remove_element( 'TS_VCSC_HTML5_Audio' );
    vc_remove_element( 'TS_VCSC_HTML5_Video' );
    vc_remove_element( 'TS-VCSC-Motion' );
    vc_remove_element( 'TS-VCSC-Vimeo' );
    vc_remove_element( 'TS-VCSC-Youtube' );
    vc_remove_element( 'TS_VCSC_Page_Background' );
    vc_remove_element( 'TS_VCSC_Icon_Button' );
    vc_remove_element( 'TS_VCSC_Icon_Flat_Button' );
    vc_remove_element( 'TS-VCSC-Icon-Title' );
    vc_remove_element( 'TS_VCSC_Title_Ticker' );
    vc_remove_element( 'TS-VCSC-Modal-Popup' );
    vc_remove_element( 'TS_VCSC_Amaran_Popup' );
    vc_remove_element( 'TS-VCSC-Font-Icons' );
    vc_remove_element( 'TS-VCSC-Icon-Box-Tiny' );
    vc_remove_element( 'TS-VCSC-Icon-List' );
    vc_remove_element( 'TS_VCSC_Info_Notice' );
    vc_remove_element( 'TS-VCSC-Content-Flip' );
    vc_remove_element( 'TS_VCSC_Quick_Skills' );
    vc_remove_element( 'TS-VCSC-Pricing-Table' );
    vc_remove_element( 'TS-VCSC-Social-Icons' );
    vc_remove_element( 'TS-VCSC-Timeline' );
    vc_remove_element( 'TS-VCSC-Divider' );
    vc_remove_element( 'TS_VCSC_Star_Rating' );
    vc_remove_element( 'TS_VCSC_Image_Hover_Effects' );
    vc_remove_element( 'TS_VCSC_Image_IHover' );
    vc_remove_element( 'TS_VCSC_Lightbox_Gallery' );
    vc_remove_element( 'TS_VCSC_Figure_Navigation_Container' );
    vc_remove_element( 'TS_VCSC_Figure_Navigation_Item' );
    vc_remove_element( 'TS_VCSC_Posts_Grid_Standalone' );
    vc_remove_element( 'TS_VCSC_Posts_Image_Grid_Standalone' );
    vc_remove_element( 'TS_VCSC_Posts_Slider_Standalone' );
    vc_remove_element( 'TS_VCSC_Posts_Ticker_Standalone' );
    vc_remove_element( 'TS_VCSC_Teaser_Block_Standalone' );
    vc_remove_element( 'TS_VCSC_Teaser_Block_Slider_Custom' );
    vc_remove_element( 'TS_VCSC_Circle_Steps_Container' );
    vc_remove_element( 'TS_VCSC_iPresenter_Container' );
    vc_remove_element( 'TS_VCSC_Anything_Grid' );
    vc_remove_element( 'TS_VCSC_Animation_Frame' );
    vc_remove_element( 'TS_VCSC_RowCenter_Frame' );
    vc_remove_element( 'TS_VCSC_Icon_Wall_Container' );
    vc_remove_element( 'TS_VCSC_Icon_Animations' );
    vc_remove_element( 'TS_VCSC_Icon_Dual_Button' );
    /*Timeline*/
    vc_remove_param( "TS_VCSC_Posts_Timeline_Standalone", "button_style" );
    vc_remove_param( "TS_VCSC_Posts_Timeline_Standalone", "button_hover" );
    vc_remove_param( "TS_VCSC_Posts_Timeline_Standalone", "show_share" );
    vc_remove_param( "TS_VCSC_Posts_Timeline_Standalone", "show_categories" );
    vc_remove_param( "TS_VCSC_Posts_Timeline_Standalone", "show_tags" );
    vc_remove_param( "TS_VCSC_Posts_Timeline_Standalone", "show_metadata" );
    vc_remove_param( "TS_VCSC_Posts_Timeline_Standalone", "show_avatar" );
    vc_remove_param( "TS_VCSC_Posts_Timeline_Standalone", "show_editlinks" );
}
endif; //logan_vc_remove_shortcodes


add_action('vc_before_init', 'logan_vc_extra_param');
if ( !function_exists('logan_vc_extra_param')) :
/**
* @since Logan 1.0
*/
function logan_vc_extra_param() {

    if (function_exists('vc_add_param')) {

		$group_name = 'Logan extra';

        //if (shortcode_exists('vc_row')) {

            vc_add_param("vc_row", array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Overlay color', 'logan' ),
                'param_name' => 'overlay',
                "admin_label" => true,
                'value' => '',
                "group" => $group_name
            ));
            vc_add_param("vc_row", array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Inner shadow", 'logan'),
                "admin_label" => true,
                "param_name" => "inner_shadow",
                "value" => array(
                    "No shadow" => "",
                    "From bottom" => "bottom",
                    "From top" => "top"
                ),
                "group" => $group_name
            ));
            /*vc_add_param("vc_row", array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Vertical alignment", 'logan'),
                "admin_label" => true,
                "param_name" => "vertical_align",
                "value" => array(
                    "Default position" => "",
                    "Top" => "top",
                    "Middle" => "middle",
                    "Bottom" => "bottom"
                ),
                "group" => $group_name
            ));*/

            vc_add_param("vc_row", array(
                "type" => "textfield",
                'heading' => esc_html__( 'Max height', 'logan' ),
                'param_name' => 'max_height',
                'value' => '',
                "group" => $group_name
            ));
            
            vc_add_param("vc_row", array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Background parallax", 'logan'),
                "admin_label" => true,
                "param_name" => "parallax_value",
                "std" => "",
                "value" => array(
                    esc_html__("Default", 'logan') => "",
                    "0.05" => "0.05",
                    "0.10" => "0.10",
                    "0.20" => "0.20",
                    "0.30" => "0.30",
                    "0.40" => "0.40",
                    "0.50" => "0.50",
                    "0.60" => "0.60",
                    "0.70" => "0.70",
                    "0.80" => "0.80",
                    "0.90" => "0.90",
                    "1" => "1",
                ),
                "group" => $group_name
            ));
            vc_add_param("vc_row", array(
                "type" => "pix_slider",
                "class" => "",
                "heading" => esc_html__("Parallax position (intended in percentage)", 'logan'),
                "admin_label" => true,
                "param_name" => "parallax_position",
                "value" => "0,100",
                'settings' => array(
                    'step' => '1',
                    'min' => '0',
                    'max' => '100',
                    'range' => true
                ),
                "group" => $group_name
            ));

            vc_add_param("vc_row", array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Overflow', 'logan' ),
                'param_name' => 'overflow',
                "value" => array(
                    esc_html__( "Auto", 'logan' ) => "",
                    esc_html__( "Hidden", 'logan' ) => "overflow_hidden",
                    esc_html__( "Visible", 'logan' ) => "overflow_visible"
                ),
                "group" => $group_name
            ));
        //}

        //if (shortcode_exists('vc_row_inner')) {

            vc_add_param("vc_row_inner", array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Inner shadow", 'logan'),
                "admin_label" => true,
                "param_name" => "inner_shadow",
                "value" => array(
                    "No shadow" => "",
                    "From bottom" => "bottom",
                    "From top" => "top",
                    "From left" => "left",
                    "From right" => "right",
                    "All around" => "all"
                ),
                "group" => $group_name
            ));

            /*vc_add_param("vc_row_inner", array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Vertical alignment", 'logan'),
                "admin_label" => true,
                "param_name" => "vertical_align",
                "value" => array(
                    esc_html__( "Default position", 'logan' ) => "",
                    esc_html__( "Top", 'logan' ) => "top",
                    esc_html__( "Middle", 'logan' ) => "middle",
                    esc_html__( "Bottom", 'logan' ) => "bottom"
                ),
                "group" => $group_name
            ));*/

            vc_add_param("vc_row_inner", array(
                "type" => "textfield",
                'heading' => esc_html__( 'Max height', 'logan' ),
                'param_name' => 'max_height',
                'value' => '',
                "group" => $group_name
            ));
            
            vc_add_param("vc_row_inner", array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Overflow', 'logan' ),
                'param_name' => 'overflow',
                "value" => array(
                    esc_html__( "Auto", 'logan' ) => "",
                    esc_html__( "Hidden", 'logan' ) => "overflow_hidden",
                    esc_html__( "Visible", 'logan' ) => "overflow_visible"
                ),
                "group" => $group_name
            ));
        //}

        //if (shortcode_exists('vc_column')) {
            vc_add_param("vc_row", array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Overlay color', 'logan' ),
                'param_name' => 'overlay',
                'value' => '',
                "group" => $group_name
            ));

            vc_add_param("vc_column", array(
                'type' => 'checkbox',
                'heading' => esc_html__( 'Sticky column?', 'logan' ),
                'param_name' => 'sticky_col',
                'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' ),
                "group" => $group_name
            ));

            vc_add_param("vc_column_inner", array(
                'type' => 'checkbox',
                'heading' => esc_html__( 'Sticky column?', 'logan' ),
                'param_name' => 'sticky_col',
                'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' ),
                "group" => $group_name
            ));

            vc_add_param("vc_column", array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Overflow', 'logan' ),
                "admin_label" => true,
                'param_name' => 'overflow',
                "value" => array(
                    esc_html__( "Auto", 'logan' ) => "",
                    esc_html__( "Hidden", 'logan' ) => "overflow_hidden",
                    esc_html__( "Visible", 'logan' ) => "overflow_visible"
                ),
                "group" => $group_name
            ));

            vc_add_param("vc_column_inner", array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Overflow', 'logan' ),
                "admin_label" => true,
                'param_name' => 'overflow',
                "value" => array(
                    esc_html__( "Auto", 'logan' ) => "",
                    esc_html__( "Hidden", 'logan' ) => "overflow_hidden",
                    esc_html__( "Visible", 'logan' ) => "overflow_visible"
                ),
                "group" => $group_name
            ));

       //}

        //if (shortcode_exists('vc_column_text')) {
            vc_add_param("vc_column_text", array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Box shadow", 'logan'),
                "admin_label" => true,
                "param_name" => "pix_box_shadow",
                "value" => array(
                    esc_html__( "No shadow", 'logan' )  => "",
                    esc_html__( "Mild shadow", 'logan' ) => "mild-pix-box-shadow",
                    esc_html__( "Shadow", 'logan' ) => "pix-box-shadow"
                ),
                "group" => $group_name
            ));
        //}

        //if (shortcode_exists('rev_slider_vc')) {
            vc_add_param("rev_slider_vc", array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Parallax", 'logan'),
                "admin_label" => true,
                "param_name" => "rev_slider_parax",
                "std" => "",
                "value" => array(
                    "No parallax" => "",
                    "0.10" => "0.10",
                    "0.20" => "0.20",
                    "0.30" => "0.30",
                    "0.40" => "0.40",
                    "0.50" => "0.50",
                    "0.60" => "0.60",
                    "0.70" => "0.70",
                    "0.80" => "0.80",
                    "0.90" => "0.90",
                    "1" => "1",
                ),
                "group" => $group_name
            ));
        //}

        vc_add_param("vc_tta_tabs", array(
            'type' => 'checkbox',
            'heading' => esc_html__( 'Expand the tabs for the entire column width', 'logan' ),
            'param_name' => 'fullwidth_tabs',
            "group" => $group_name,
            'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' )
        ));

        vc_add_param("vc_tta_tabs", array(
            'type' => 'checkbox',
            'heading' => esc_html__( 'Remove any tab style (borders and backgrounds)', 'logan' ),
            'param_name' => 'no_style',
            "group" => $group_name,
            'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' )
        ));

        vc_add_param("vc_tta_tabs", array(
            'type' => 'checkbox',
            'heading' => esc_html__( 'Button look for tab labels', 'logan' ),
            'param_name' => 'button_look',
            "group" => $group_name,
            'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' )
        ));

        if ( get_theme_mod('pix_vc_enable_hidden') != true ) {

            vc_remove_param( "vc_tta_tabs", "style" );
            vc_remove_param( "vc_tta_tabs", "color" );
            vc_remove_param( "vc_tta_tabs", "no_fill_content_area" );
            vc_remove_param( "vc_tta_tabs", "pagination_style" );
            vc_remove_param( "vc_tta_tabs", "pagination_color" );

        }

        //if (shortcode_exists('vc_message')) {
            vc_add_param("vc_message", array(
                'type' => 'checkbox',
                'heading' => esc_html__( 'Use custom icons', 'logan' ),
                'param_name' => 'logan_icon_enable',
                'admin_label' => true,
                "group" => $group_name,
                'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' )
            ));

            vc_add_param("vc_message", array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Logan icon', 'logan' ),
                'param_name' => 'logan_icon',
                'admin_label' => true,
                'value' => '', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    'iconsPerPage' => 4000,
                    'type' => 'budicons',
                ),
                'description' => esc_html__( 'Select icon from library.', 'logan' ),
                "group" => $group_name,
                'dependency' => array(
                    'element' => 'logan_icon_enable',
                    'not_empty' => true,
                ),
            ));

        //if (shortcode_exists('vc_tta_section')) {

            vc_add_param("vc_tta_section", array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'CSS Animation', 'logan' ),
				'param_name' => 'fx',
                "group" => $group_name,
				'value' => array(
					esc_html__( 'No', 'logan' ) => '',
					esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
					esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
					esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
					esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
					esc_html__( 'Appear from center', 'logan' ) => 'appear'
				),
				'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' )
            ));

            vc_add_param("vc_tta_section", array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Use custom icons', 'logan' ),
				'param_name' => 'logan_icon_enable',
                "group" => $group_name,
				'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' )
            ));

            vc_add_param("vc_tta_section", array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Logan icon', 'logan' ),
				'param_name' => 'logan_icon',
				'admin_label' => true,
				'value' => '', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false,
					'iconsPerPage' => 4000,
			        'type' => 'budicons',
				),
				'description' => esc_html__( 'Select icon from library.', 'logan' ),
                "group" => $group_name,
				'dependency' => array(
					'element' => 'logan_icon_enable',
					'not_empty' => true,
				),
            ));

            vc_add_param("vc_tta_section", array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Icon position", 'logan'),
                "admin_label" => true,
                "param_name" => "icon_position",
                "std" => "before",
                "value" => array(
					esc_html__( 'Before title', 'logan' ) => 'before',
					esc_html__( 'After title', 'logan' ) => 'after',
					esc_html__( 'Above title', 'logan' ) => 'above',
                ),
                "group" => $group_name,
				'dependency' => array(
					'element' => 'logan_icon_enable',
					'not_empty' => true,
				),
            ));

            vc_add_param("vc_tta_section", array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Icon size (in EMs)', 'logan' ),
				'param_name' => 'icon_size',
				'value' => '1',
                "group" => $group_name,
				'dependency' => array(
					'element' => 'logan_icon_enable',
					'not_empty' => true,
				),
            ));

        //}

        //if (shortcode_exists('vc_icon')) {

            vc_add_param("vc_icon", array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Use custom icons', 'logan' ),
				'param_name' => 'logan_icon_enable',
                "group" => $group_name,
				'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' )
            ));

            vc_add_param("vc_icon", array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Logan icon', 'logan' ),
				'param_name' => 'logan_icon',
				'admin_label' => true,
				'value' => '', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false,
					'iconsPerPage' => 4000,
			        'type' => 'budicons',
				),
				'description' => esc_html__( 'Select icon from library.', 'logan' ),
                "group" => $group_name,
				'dependency' => array(
					'element' => 'logan_icon_enable',
					'not_empty' => true,
				),
            ));

        //}

        vc_add_param("vc_text_separator", array(
            'type' => 'checkbox',
            'heading' => esc_html__( 'Use custom icons', 'logan' ),
            'param_name' => 'logan_icon_enable',
            "group" => $group_name,
            'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' )
        ));

        vc_add_param("vc_text_separator", array(
            'type' => 'iconpicker',
            'heading' => esc_html__( 'Logan icon', 'logan' ),
            'param_name' => 'logan_icon',
            'value' => '', // default value to backend editor admin_label
            'settings' => array(
                'emptyIcon' => false,
                'iconsPerPage' => 4000,
                'type' => 'budicons',
            ),
            'description' => esc_html__( 'Select icon from library.', 'logan' ),
            "group" => $group_name,
            'dependency' => array(
                'element' => 'logan_icon_enable',
                'not_empty' => true,
            ),
        ));

        if ( get_theme_mod('pix_vc_enable_hidden') != true ) {

            vc_remove_param( "vc_media_grid", "btn_style" );
            vc_remove_param( "vc_media_grid", "btn_shape" );
            vc_remove_param( "vc_media_grid", "btn_color" );
            vc_remove_param( "vc_media_grid", "btn_size" );
            vc_remove_param( "vc_media_grid", "btn_custom_background" );
            vc_remove_param( "vc_media_grid", "btn_custom_text" );
            vc_remove_param( "vc_media_grid", "btn_outline_custom_color" );
            vc_remove_param( "vc_media_grid", "btn_outline_custom_hover_background" );
            vc_remove_param( "vc_media_grid", "btn_outline_custom_hover_text" );

        }

        $woo_shortcodes_arr = array(
            'product_category',
            'featured_products',
            'recent_products',
            'products',
            'sale_products',
            'best_selling_products',
            'top_rated_products',
            'product_attribute'
        );

        foreach ($woo_shortcodes_arr as $shortcode) {

            //if (shortcode_exists($shortcode)) {

                $custom_css_id = uniqid('pix-custom-');

                vc_add_param($shortcode, array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'CSS Animation', 'logan' ),
                    'param_name' => 'fx',
                    "group" => $group_name,
                    'value' => array(
                        esc_html__( 'None', 'logan' ) => '',
                        esc_html__( 'Top to bottom', 'logan' ) => 'top-to-bottom',
                        esc_html__( 'Bottom to top', 'logan' ) => 'bottom-to-top',
                        esc_html__( 'Left to right', 'logan' ) => 'left-to-right',
                        esc_html__( 'Right to left', 'logan' ) => 'right-to-left',
                        esc_html__( 'Appear from center', 'logan' ) => 'appear'
                    ),
                    'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'logan' )
                ));

                vc_add_param($shortcode, array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Gutter', 'logan' ),
                    'param_name' => 'gutter',
                    "group" => $group_name,
                    'value' => ''
                ));

                vc_add_param($shortcode, array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Image ratio (width)', 'logan' ),
                    'param_name' => 'width',
                    "group" => $group_name,
                    'value' => ''
                ));

                vc_add_param($shortcode, array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Image ratio (height)', 'logan' ),
                    'param_name' => 'height',
                    "group" => $group_name,
                    'value' => ''
                ));

                vc_add_param($shortcode, array(
                    'type' => 'pix_hidden',
                    'param_name' => 'custom_css_uniq_id',
                    'value' => $custom_css_id,
                    "group" => $group_name,
                ));
            //}

        }

    }
}
endif;

/************************************************
*
*		TABS
*
************************************************/
add_action( 'vc_before_init', 'pix_logan_vc_tab_style' );
function pix_logan_vc_tab_style() {

    if ( get_theme_mod('pix_vc_enable_hidden') == true )
        return;

    vc_remove_param( "vc_tta_accordion", "shape" );
    vc_remove_param( "vc_tta_accordion", "no_fill" );
    vc_remove_param( "vc_tta_accordion", "style" );
    vc_remove_param( "vc_tta_accordion", "color" );
    vc_remove_param( "vc_tta_accordion", "no_fill_content_area" );
    vc_remove_param( "vc_tta_accordion", "pagination_style" );
    vc_remove_param( "vc_tta_accordion", "pagination_color" );

    vc_remove_param( "vc_tta_tour", "style" );
    vc_remove_param( "vc_tta_tour", "color" );
    vc_remove_param( "vc_tta_tour", "no_fill_content_area" );
    vc_remove_param( "vc_tta_tour", "pagination_style" );
    vc_remove_param( "vc_tta_tour", "pagination_color" );
}

/************************************************
*
*       TOGGLES
*
************************************************/
add_action( 'vc_before_init', 'pix_logan_vc_toggle_style' );
function pix_logan_vc_toggle_style() {
    $tabs_style = array(
        'type' => 'dropdown',
        'param_name' => 'style',
        'value' => array(
            esc_html__( 'Logan', 'logan' ) => 'logan',
            esc_html__( 'Default', 'logan' ) => 'default',
            esc_html__( 'Simple', 'logan' ) => 'simple',
            esc_html__( 'Round Outline', 'logan' ) => 'round_outline',
            esc_html__( 'Rounded', 'logan' ) => 'rounded',
            esc_html__( 'Rounded Outline', 'logan' ) => 'rounded_outline',
            esc_html__( 'Square', 'logan' ) => 'square',
            esc_html__( 'Square Outline', 'logan' ) => 'square_outline',
            esc_html__( 'Arrow', 'logan' ) => 'arrow',
            esc_html__( 'Text Only', 'logan' ) => 'text_only',
        ),
        'heading' => esc_html__( 'Style', 'logan' ),
        'description' => esc_html__( 'Select toggle design style', 'logan' ),
    );
    vc_add_param( 'vc_toggle', $tabs_style );
}

/************************************************
*
*       IMAGE GALLERY
*
************************************************/
add_action( 'vc_before_init', 'pix_logan_vc_gallery_grid' );
function pix_logan_vc_gallery_grid() {
    $gallery_grid = array(
        'type' => 'dropdown',
        'param_name' => 'type',
        'value' => array(
            esc_html__( 'Slick slider', 'logan' ) => 'slick',
            esc_html__( 'Image grid', 'logan' ) => 'image_grid',
        ),
        'heading' => esc_html__( 'Gallery type', 'logan' ),
        'description' => esc_html__( 'Select gallery type.', 'logan' ),
    );
    vc_add_param( 'vc_gallery', $gallery_grid );

    $gallery_autoplay = array(
        'type' => 'checkbox',
        'param_name' => 'slick_autoplay',
        'heading' => esc_html__( 'Autoplay', 'logan' ),
        'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' ),
        'dependency' => array(
            'element' => 'type',
            'value' => array( 'slick' )
        ),
    );
    vc_add_param( 'vc_gallery', $gallery_autoplay );

    $gallery_speed = array(
        'type' => 'textfield',
        'heading' => esc_html__( 'Autoplay speed', 'logan' ),
        'description' => esc_html__( 'Type an integer (milliseconds).', 'logan' ),
        'param_name' => 'slick_speed',
        'value' => '',
        'dependency' => array(
            'element' => 'type',
            'value' => array( 'slick' )
        ),
    );
    vc_add_param( 'vc_gallery', $gallery_speed );

    $gallery_dots = array(
        'type' => 'checkbox',
        'param_name' => 'slick_dots',
        'heading' => esc_html__( 'Display dots', 'logan' ),
        'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' ),
        'dependency' => array(
            'element' => 'type',
            'value' => array( 'slick' )
        ),
    );
    vc_add_param( 'vc_gallery', $gallery_dots );

    $gallery_arrows = array(
        'type' => 'checkbox',
        'param_name' => 'slick_arrows',
        'heading' => esc_html__( 'Display arrows', 'logan' ),
        'value' => array( esc_html__( 'Yes', 'logan' ) => 'yes' ),
        'dependency' => array(
            'element' => 'type',
            'value' => array( 'slick' )
        ),
    );
    vc_add_param( 'vc_gallery', $gallery_arrows );

    $image_action = array(
        'type' => 'dropdown',
        'param_name' => 'onclick',
        'value' => array(
            esc_html__( 'None', 'logan' ) => '',
            esc_html__( 'Link to large image', 'logan' ) => 'img_link_large',
            esc_html__( 'Open ColorBox', 'logan' ) => 'link_image',
            esc_html__( 'Open custom link', 'logan' ) => 'custom_link',
            esc_html__( 'Zoom', 'logan' ) => 'zoom',
        ),
        'heading' => esc_html__( 'On click action', 'logan' ),
        'description' => esc_html__( 'Select action for click event.', 'logan' ),
    );
    vc_add_param( 'vc_single_image', $image_action );

    $gallery_action = array(
        'type' => 'dropdown',
        'param_name' => 'onclick',
        'value' => array(
            esc_html__( 'None', 'logan' ) => '',
            esc_html__( 'Link to large image', 'logan' ) => 'img_link_large',
            esc_html__( 'Open ColorBox', 'logan' ) => 'link_image',
            esc_html__( 'Open custom link', 'logan' ) => 'custom_link',
        ),
        'heading' => esc_html__( 'On click action', 'logan' ),
        'description' => esc_html__( 'Select action for click action.', 'logan' ),
    );
    vc_add_param( 'vc_gallery', $gallery_action );

    $carousel_action = array(
        'type' => 'dropdown',
        'param_name' => 'onclick',
        'value' => array(
            esc_html__( 'Open ColorBox', 'logan' ) => 'link_image',
            esc_html__( 'None', 'logan' ) => 'link_no',
            esc_html__( 'Open custom link', 'logan' ) => 'custom_link',
        ),
        'heading' => esc_html__( 'On click action', 'logan' ),
        'description' => esc_html__( 'Select action for click event.', 'logan' ),
    );
    vc_add_param( 'vc_images_carousel', $carousel_action );

}