<?php
/************************************************
*
*		LOGAN MEDIA PLAYERS
*
************************************************/
add_shortcode( 'pix_soundcloud', 'pix_soundcloud' );
if ( !function_exists( 'pix_soundcloud' )) :
function pix_soundcloud( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'url' => '',
		'params' => '',
		'width' => '100%',
		'height' => '166',
		'iframe' => 'true',
	), $atts ) );

	$url = esc_url($url);
	$params = '?' . esc_attr($params);
	$enc = $url.$params;
	return '<iframe width="' . esc_attr($width) . '" height="' . esc_attr($height) . '" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=' . $enc . '"></iframe>';

}
endif;//pix_soundcloud

add_shortcode( 'pix_media', 'logan_vc_media' );
if ( !function_exists('logan_vc_media')) :
/**
* @since Logan 1.0
*/
function logan_vc_media( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'type' => 'media',
		'sc' => '',
		'youtube' => '',
		'rel' => '0',
		'autohide' => '2',
		'autoplay' => '0',
		'controls' => '1',
		'start' => '0',
		'end' => '0',
		'loop' => '0',
		'showinfo' => '1',
		'vimeo' => '',
		'portrait' => '0',
		'byline' => '0',
		'title' => '0',
		'color' => '',
		'soundcloud' => '',
		'spotify' => '',
		'lightbox' => '',
		'placeholder' => '',
		'thumb_size' => 'pix-medium',
		'group' => 'my-group',
	), $atts ) );

	$out = '';

	$start = intval($start);
	$end = intval($end);

	$color = preg_replace("/\#/", '', pix_sanitize_hex_color($color));

	$src = '';

	if ( $sc != '' && $type == 'media' ) {
		$sc = preg_replace("/pix_quot;/", "\"", $sc);
		$sc = preg_replace("/pix_op_brack;/", "[", $sc);
		$sc = preg_replace("/pix_cl_brack;/", "]", $sc);
		$out = $inline = do_shortcode($sc);
	} elseif ( $youtube != '' && $type == 'youtube' ) {
		if ( preg_match( '/youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)/i', esc_url($youtube) ) ) {
			preg_match( '/youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)/i', esc_url($youtube), $id );
			$youtube = $id[1];
		} elseif ( preg_match( '/youtu.be\/([a-zA-Z0-9\-_]+)/i', esc_url($youtube) ) ) {
			preg_match( '/youtu.be\/([a-zA-Z0-9\-_]+)/i', esc_url($youtube), $id );
			$youtube = $id[1];
		}

		$youtube = esc_attr($youtube);

		$playlist = $loop == '1' ? $youtube : '';

		$src = "//www.youtube.com/embed/{$youtube}?rel={$rel}&amp;autohide={$autohide}&amp;autoplay={$autoplay}&amp;controls={$controls}&amp;start={$start}&amp;end={$end}&amp;loop={$loop}&amp;showinfo={$showinfo}&amp;playlist={$playlist}";
		$iframe = "<iframe class=\"pix_youtube_player\" src=\"{$src}\" width=\"1280\" height=\"720\" frameborder=\"0\" allowfullscreen></iframe>";
		$out = $iframe;
	} elseif ( $vimeo != '' && $type == 'vimeo' ) {
		if ( preg_match( '/vimeo.com\/([0-9\-_]+)/i', esc_url($vimeo) ) ) {
			preg_match( '/vimeo.com\/([0-9\-_]+)/i', esc_url($vimeo), $id );
			$vimeo = $id[1];
		}

		$vimeo = esc_attr($vimeo);

		$src = "//player.vimeo.com/video/{$vimeo}?autoplay={$autoplay}&amp;loop={$loop}&amp;color={$color}&amp;title={$title}&amp;byline={$byline}&amp;portrait={$portrait}";
		$iframe = "<iframe class=\"pix_vimeo_player\" src=\"{$src}\" width=\"1280\" height=\"720\" frameborder=\"0\" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
		$out = $iframe;
	} elseif ( $soundcloud != '' && $type == 'soundcloud' ) {
		$soundcloud = preg_replace("/pix_quot;/", "\"", $soundcloud);
		$soundcloud = preg_replace("/pix_op_brack;/", "[", $soundcloud);
		$soundcloud = preg_replace("/pix_cl_brack;/", "]", $soundcloud);
		$soundcloud = preg_replace("/\[soundcloud/", "[pix_soundcloud", $soundcloud);
		$src_sc = true;
		$out = do_shortcode($soundcloud);
	} elseif ( $spotify != '' && $type == 'spotify' ) {
		$spotify = esc_attr($spotify);
		$spotify = preg_replace('/:/', '%3A', $spotify);
		$src_spot = 'https://embed.spotify.com/?uri='.$spotify;
		$iframe = $inline = '<iframe src="'.$src_spot.'" width="100%" height="380" frameborder="0" allowtransparency="true"></iframe>';
		$out = $iframe;
	} else {
		return;
	}

	if ( $lightbox == 'yes' && $placeholder != '' ) {

		$thumb_size = esc_attr( $thumb_size );
		if ( preg_match( '/array\(([0-9]*),([0-9]*)\)/', $thumb_size) ) {
			preg_match( '/array\(([0-9]*),([0-9]*)\)/', $thumb_size, $thumb_sizes);
			$thumb_size = array( $thumb_sizes[1], $thumb_sizes[2] );
		}
		$image = wp_get_attachment_image( esc_attr($placeholder), $thumb_size );

		if ( $src != '' ) {
			$out = '<a class="cboxiframe pix-media-icon" data-size="[1280,720]" href="' . $src . '" data-rel="' . esc_attr($group) . '">' . $image . '</a>';
		} elseif ( isset($src_spot) && $src_spot != '' ) {
			$out = '<a class="cboxiframe pix-media-icon" data-size="[300,380]" href="' . $src_spot . '" data-rel="' . esc_attr($group) . '">' . $image . '</a>';
		} elseif ( isset($src_sc) && $src_sc ) {
			preg_match('/<iframe([^>]*)src="(.*?)"([^>]*)>/', $out, $src);
			preg_match('/<iframe([^>]*)width="(.*?)"([^>]*)>/', $out, $width);
			preg_match('/<iframe([^>]*)height="(.*?)"([^>]*)>/', $out, $height);

			$width = $width[2] == '100%' ? '1024' : $width[2];

			$out = '<a class="cboxiframe pix-media-icon" data-size="['.$width.','.$height[2].']" href="' . $src[2] . '" data-rel="' . esc_attr($group) . '">' . $image . '</a>';
		} elseif ( isset($inline) && $inline != '' ) {
			$id_unique = uniqid('pix-video-inline-');
			$out = '<a href="#' . $id_unique . '" class="cboxinline pix-media-icon"  data-rel="' . esc_attr($group) . '">' . $image . '</a>';
			$out .= '<div class="fake_hidden"><div id="' . $id_unique . '" class="pix-video-inline">' . $inline . '</div></div>';
		}
	}

	return $out;
}
endif; //logan_vc_buttons

/************************************************
*
*		LOGAN BUTTONS
*
************************************************/

add_shortcode( 'pix_button', 'logan_vc_buttons' );
if ( !function_exists('logan_vc_buttons')) :
/**
* @since Logan 1.0
*/
function logan_vc_buttons( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'align' => '',
		'width' => '',
		'radius' => '',
		'border_width' => '',
		'text_size' => '',
		'icon_size' => '',
		'css_animation' => '',
		'id' => '',
		'class' => '',
		'single_link' => '',
		'icon_single' => '',
		'single_icon_position' => '',
		'single_icon_color' => '',
		'single_icon_color_hover' => '',
		'single_content' => '',
		'single_text_color' => '',
		'single_text_color_hover' => '',
		'single_bg_color' => '',
		'single_bg_color_hover' => '',
		'single_border_color' => '',
		'single_border_color_hover' => '',
		'double_link' => '',
		'icon_double' => '',
		'double_icon_position' => '',
		'double_icon_color' => '',
		'double_icon_color_hover' => '',
		'double_content' => '',
		'double_text_color' => '',
		'double_text_color_hover' => '',
		'double_bg_color' => '',
		'double_bg_color_hover' => '',
		'double_border_color' => '',
		'double_border_color_hover' => '',
		'separator' => '',
		'separator_content' => '',
		'separator_icon' => '',
		'separator_text_color' => '',
		'separator_border_color' => '',
		'custom_css_uniq_id' => '',
		'css' => ''
	), $atts ) );

	$uniq_id = uniqid('pix-custom-css-');

	$wrap_style = $align != '' ? ' style="text-align:' . esc_attr($align) .';"' : '';

	$button_class = 'logan-button ' . $width;
	if ( isset($css_animation) && $css_animation!='' )
		$class .= ' wpb_animate_when_almost_visible wpb_' . esc_attr($css_animation);

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'pix_button', $atts );

	$class .= ' ' . $css_class;

	$id = $id != '' ? ' id="' . esc_attr($id) . '"' : '';

	$allowed_html = array(
	    'em' => array(),
	    'strong' => array()
	);

	$single_icon_color = $single_icon_color != '' ? ' custom-color' : '';
	$double_icon_color = $double_icon_color != '' ? ' custom-color' : '';

	$icon_single = $icon_single != '' ? '<i class="' . esc_attr( $icon_single . $single_icon_color ) . '"></i>' : '';
	$icon_double = $icon_double != '' ? '<i class="' . esc_attr( $icon_double . $double_icon_color ) . '"></i>' : '';

	$double_class = $double_link != '' ? 'button-doubled ' : '';

	$out = '<div class="logan-button-wrap ' . $double_class . esc_attr($class) . ' ' . esc_attr($custom_css_uniq_id) . '"' . $id . $wrap_style . '>';

		/* SINGLE LINK */
		if ( $single_link != '' ) {
			$vc_single_link = vc_build_link( $single_link );

			$url = $vc_single_link['url'];

			$title = esc_attr( $vc_single_link['title'] );
			$title = $title != '' ? " title='$title'" : "";

			$target = esc_attr( $vc_single_link['target'] );
			$target = $target != '' ? " target='$target'" : "";

			$out .= '<a href="' . esc_url($url) . '" ' . $title . $target . ' class="' . $button_class . '">';
		}

		if ( $single_icon_position != 'after' && $icon_single != '' )
			$out .= $icon_single . '&nbsp;&nbsp;&nbsp;';

		$out .= wp_kses( $single_content, $allowed_html );

		if ( $single_icon_position == 'after' && $icon_single != '' )
			$out .= '&nbsp;&nbsp;&nbsp;' . $icon_single;

		if ( $single_link != '' ) {
			$out .= "</a>";
		}

		/* SEPARATOR */
		if ( $separator == 'text' && $separator_content!='' ) {
			$out .= '<span class="logan-button-separator">' . wp_kses( $separator_content, $allowed_html ) . '</span>';
		} elseif ( $separator == 'icon' && $separator_icon!='' ) {
			$out .= '<span class="logan-button-separator"><i class="' . esc_attr( $separator_icon ) . '"></i></span>';
		}
		if ( $separator != '' ) {
			$out .= '<span class="logan-button-separator-border"></span>';
		} 

		/* DOUBLE LINK */
		if ( $double_link != '' ) {
			$vc_double_link = vc_build_link( $double_link );

			$url = $vc_double_link['url'];

			if ( $url != '' ) {

				$title = esc_attr( $vc_double_link['title'] );
				$title = $title != '' ? " title='$title'" : "";

				$target = esc_attr( $vc_double_link['target'] );
				$target = $target != '' ? " target='$target'" : "";

				$out .= '<a href="' . esc_url($url) . '" ' . $title . $target . ' class="' . $button_class . '">';

			}
		}

		if ( $double_icon_position != 'after' && $icon_double != '' )
			$out .= $icon_double . '&nbsp;&nbsp;&nbsp;';

		$out .= wp_kses( $double_content, $allowed_html );

		if ( $double_icon_position == 'after' && $icon_double != '' )
			$out .= '&nbsp;&nbsp;&nbsp;' . $icon_double;

		if ( $double_link != '' ) {
			$out .= "</a>";
		}

	$out .= '</div>';

	return $out;
}
endif; //logan_vc_buttons

/************************************************
*
*		LOGAN ICONS
*
************************************************/

add_shortcode( 'logan_icons', 'logan_vc_icons' );
if ( !function_exists('logan_vc_icons')) :
/**
* @since Logan 1.0
*/
function logan_vc_icons( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'icon' => 'logan-icon-web-browseinternetnetwork',
		'color' => '',
		'position' => 'top left',
		'size' => '16',
		'wrap' => 'square',
		'bg' => '',
		'wrap_size' => '40',
		'border_width' => '0',
		'border_style' => 'solid',
		'border_color' => '',
		'icon_animation' => '',
		'text_animation' => '',
		'css' => ''
	), $atts ) );

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'logan_icons', $atts );

	$out = '';

	$fontsize = $size;

	$size = $wrap_size + 20;

	$position_arr = explode(' ', $position);

	$position_arr_rev['right'] = 'left';
	$position_arr_rev['left'] = 'right';

	$style_icon = '';
	$style_icon .= 'width:'. $wrap_size . 'px;';
	$style_icon .= $border_width != 0 || ( $bg != '' && $bg != 'transparent' ) ? 'text-align:center;' : 'text-align:' . $position_arr[1] . ';';
	$style_icon .= $bg != '' ? 'background-color:' . $bg . ';' : '';
	$style_icon .= $border_width != 0 ? 'border:' . $border_width . 'px ' . $border_style . ' ' . $border_color . ';' : '';
	$style_icon .= $position_arr[0] == 'top' && $position_arr[1] == 'center'  ? 'margin:0 auto 20px;' : '';
	$style_icon .= $position_arr[0] == 'bottom' && $position_arr[1] == 'center'  ? 'margin:20px auto 0;' : '';
	$style_icon .= $position_arr[1] != 'center' && ( $border_width != 0 || ( $bg != '' && $bg != 'transparent' ) ) ? 'margin-' . $position_arr_rev[$position_arr[1]] . ':20px;' : '';
	$style_icon .= $color != '' ? 'color:' . $color .';' : '';
	$style_icon .= $border_width != 0 || ( $bg != '' && $bg != 'transparent' ) ? 'line-height:'. $wrap_size . 'px;' : '';
	$style_icon .= 'font-size:' . $fontsize .'px;';

	$wrap_icon_w = $position_arr[1] != 'center' && ( $border_width != 0 || ( $bg != '' && $bg != 'transparent' ) ) ? ($wrap_size + 20) . 'px' : 'auto';
	$style_icon_wrap = 'width:'. $wrap_icon_w;

	$style_div = $position_arr[1] != 'center' ? ' style="width:calc( 100% - ' . $size .'px );"' : '';

	$out .= '<div class="pix-icon-wrap pix-icon-wrap-' . $wrap . ' '. esc_attr($css_class) . '" data-position="' . $position . '">';

	$text_animation = $text_animation != '' ? 'wpb_animate_when_almost_visible wpb_' . $text_animation : '';

	$div_text = '<div class="pix-icon-text-box ' . $text_animation . '"' . $style_div . '>' . wpautop(do_shortcode($content)) . '</div>';

	if ( $content!=null && ( $position=='top right' || $position=='middle right' || $position=='bottom center' || $position=='bottom right' ) ) {
		$out .= $div_text;
	}

	$icon_animation = $icon_animation != '' ? 'wpb_animate_when_almost_visible wpb_' . $icon_animation : '';
	$out .= '<div class="pix-icon-box ' . $icon_animation . '" style="' . $style_icon_wrap . '"><i class="' . $icon . '" style="' . $style_icon . '"></i></div>';

	if ( $content!=null && ( $position=='top left' || $position=='top center' || $position=='middle left' || $position=='bottom left' ) ) {
		$out .= $div_text;
	}

	$out .= '</div><!-- .pix-icon-wrap -->';

	return $out;
}
endif;

/************************************************
*
*		LOGAN PROGRESS BARS
*
************************************************/

add_shortcode( 'logan_progress', 'logan_vc_progress' );
if ( !function_exists('logan_vc_progress')) :
/**
* @since Logan 1.0
*/
function logan_vc_progress( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'bg' => '',
		'color' => '',
		'label' => esc_html__( 'Skill', 'logan' ),
		'amount' => '50',
		'counter' => '',
		'unit' => '%',
		'unit_position' => '',
		'css' => ''
	), $atts ) );

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'logan_progress', $atts );

	$track_bg = $bg != '' ? ' style="background-color:' . esc_html($bg) . '"' : '';
	$progress_bg = $color != '' ? ' style="background-color:' . esc_html($color) . '"' : '';

	$out = '';

	$out .= '<div class="pix_progress_bar ' . esc_html($css_class) . '">
		<small class="pix_label uppercase alternative_font"><strong>' . esc_html( $label ) . '</strong></small>
		<div class="pix_single_bar" ' . $track_bg . '>
			<span class="pix_bar_track " style="width:' . intval( $amount ) . '%">
				<span class="pix_bar pix_animate_bar_almost_visible" ' . $progress_bg . '>
					<small class="pix_label alternative_font amount-progress" data-amount="' . intval( $counter ) . '" data-sign="' . esc_html( $unit ) . '" data-position="' . esc_html( $unit_position ) . '"></small>
				</span>
			</span>
		</div>
	</div>';

	return $out;
}
endif;

/************************************************
*
*		GOOGLE MAPS
*
************************************************/

add_shortcode( 'logan_maps', 'logan_vc_maps' );
if ( !function_exists('logan_vc_maps')) :
/**
* @since Logan 1.0
*/
function logan_vc_maps( $atts ) {
	extract( shortcode_atts( array(
		'lat' => '41.890322',
		'long' => '12.492312',
		'width' => '100',
		'height' => '56',
		'min_height' => '0',
		'type' => 'roadmap',
		'zoom' => '12',
		'heading' => '0',
		'pitch' => '0',
		'sw_ctrls' => 'top_right',
		'map_type_ctrls' => 'top_right',
		'pan_ctrls' => 'top_left',
		'zoom_ctrls' => 'top_left',
		'zoom_ctrls_size' => 'DEFAULT',
		'scale_ctrls' => '',
		'marker' => '',
		'custom_marker' => '',
		'custom_style' => '',
		'route_start' => '',
		'route_destination' => '',
		'travel_mode' => 'driving',
		'color_path' => '#d42027',
		'color_path_opacity' => '0.75',
		'color_path_width' => '3',
		'css' => ''
	), $atts ) );

	if ( $custom_style!='' ) {
		$custom_style = preg_replace("/\s+/", " ", $custom_style);
		$custom_style = preg_replace("/_QUOTES_/", "\"", $custom_style);
		$custom_style = preg_replace("/_QUOTE_/", "'", $custom_style);
		$custom_style = preg_replace("/BRACK_/", "[", $custom_style);
		$custom_style = preg_replace("/_KCARB/", "]", $custom_style);
			
		$styles = $custom_style;
	} else {
		$styles = '';
	}

	$min_height = intval($min_height);

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'logan_maps', $atts );

	$custom_marker = $custom_marker!='' ? wp_get_attachment_image_src($custom_marker, 'full') : '';
	$custom_marker = $custom_marker!='' ? $custom_marker[0] : '';

	$out = "<div style=\"min-height:{$min_height}px\" class='pix_map '" . esc_attr($css_class) . " data-opts='{\"lat\":\"" . floatval($lat) . "\",\"long\":\"" . floatval($long) . "\",\"width\":\"" . floatval($width) . "\",\"height\":\"" . floatval($height) . "\",\"type\":\"" . esc_attr($type) . "\",\"zoom\":\"" . floatval($zoom) . "\",\"heading\":\"" . floatval($heading) . "\",\"pitch\":\"" . floatval($pitch) . "\",\"swcontrol\":\"" . esc_attr($sw_ctrls) . "\",\"maptypectrl\":\"" . esc_attr($map_type_ctrls) . "\",\"pancontrol\":\"" . esc_attr($pan_ctrls) . "\",\"zoomcontrol\":\"" . esc_attr($zoom_ctrls) . "\",\"zoomcontrolsize\":\"" . esc_attr($zoom_ctrls_size) . "\",\"scalecontrol\":\"" . esc_attr($scale_ctrls) . "\",\"route_start\":\"" . esc_attr($route_start) . "\",\"route_destination\":\"" . esc_attr($route_destination) . "\",\"travel_mode\":\"" . esc_attr($travel_mode) . "\",\"color_path\":\"". pix_sanitize_hex_color($color_path) . "\",\"color_path_opacity\":\"" . floatval($color_path_opacity) . "\",\"color_path_width\":\"" . floatval($color_path_width) . "\",\"marker\":\"" . esc_attr($marker) . "\",\"custom_marker\":\"" . esc_url($custom_marker) . "\"}' data-styles='$styles'>";

	$out .= "</div><!-- .pix_maps -->";

	return $out;
}
endif; //logan_vc_maps

/************************************************
*
*		MEMBER LISTS
*
************************************************/
add_shortcode( 'logan_team_members', 'logan_vc_members_loop' );
function logan_vc_members_loop( $atts ) {
	extract( shortcode_atts( array(
		'include' => '',
		'exclude' => '',
		'order' => 'DESC',
		'orderby' => 'date',
		'fx' => '',
		'columns' => '3',
		'columns_landscape' => '',
		'columns_portrait' => '',
		'columns_phones' => '',
		'ppp' => get_option('posts_per_page'),
		'gutter' => '30',
		'max_width' => '',
		'width' => '',
		'height' => '',
		'position' => '',
		'heading' => 'h4',
		'link' => 'post',
		'css' => ''
	), $atts ) );

	$args = array(
	  'post_type' => 'member',
	  'posts_per_page' => -1,
	  'order' => $order,
	  'orderby' => $orderby
	);

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'logan_team_members', $atts );

	global $logan_max_width;

	if ( $include != '' ) {
		$include  = explode(",", $include);
		$args['post__in'] = $include;
	}

	if ( $exclude != '' && $include == '' ) {
		$exclude  = explode(",", $exclude);
		$args['post__not_in'] = $exclude;
	}

	$fx = $fx != '' ? 'wpb_animate_when_almost_visible wpb_' . $fx : '';
	$fx2 = $fx != '' && $position == '' ? 'wpb_animate_when_almost_visible wpb_top-to-bottom' : $position;

	$query = new WP_Query($args);

	$out = '';

	if ( $max_width=='' )
		$w = $logan_max_width / $columns;
	else
		$w = $max_width;

	$height = $height == '' || $width == '' ? null : $w * ( intval($height) / intval($width) );
	$crop = $height == null ? false : true;

	if ( $query->have_posts() ) :

		$count = 0;

		$uniq_id = uniqid('member-list-');

	    $out .= '<ul id="' . $uniq_id . '" class="member-list ' . $css_class . '" data-grid="fitRows" data-columns="' . esc_attr( $columns ) . '" data-landscape="' . esc_attr( $columns_landscape ) . '" data-portrait="' . esc_attr( $columns_portrait ) . '" data-phone="' . esc_attr( $columns_phones ) . '" style="margin-left:-' . $gutter . 'px;">';

		while ( $query->have_posts() && $count < $ppp ) : $query->the_post();

			if ( has_post_thumbnail() ) {

			    ob_start();

	            $src = wp_get_attachment_url( get_post_thumbnail_id() );
	            $src_res = function_exists('logan_resizer') ? logan_resizer( $src, $w, $height, $crop ) : $src; ?>

				<li id="post-<?php the_ID(); ?>" <?php post_class( array( $fx ) ); ?> style="padding-left: <?php echo $gutter; ?>px; margin-bottom: <?php echo $gutter; ?>px;">

					<span class="member-wrap">

						<img src="<?php echo esc_url($src_res); ?>" class="attachment-custom_size wp-post-image" width="<?php echo esc_attr($w); ?>" height="<?php echo esc_attr($h); ?>" alt="<?php the_title_attribute(); ?>">

						<span class="member-info <?php echo esc_attr($fx2); ?>"><span><span>

							<<?php echo esc_attr( $heading ); ?> class="member-name">
								<?php if ( $link != '' ) { ?>
						            <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
								<?php } ?>
								<?php the_title(); ?>
							</<?php echo esc_attr( $heading ); ?>>
								<?php if ( $link != '' ) { ?>
						            </a>
								<?php } ?>

							<?php if ( get_post_meta( get_the_id(), 'pix_role' ) != '' ) { ?>

								<small class="member-role alternative_font alternative_color uppercase"><?php echo esc_html( get_post_meta( get_the_id(), 'pix_role', true ) ); ?></small>

							<?php } ?>

							<?php $social = pixedelic_member_socials();
								if ( get_post_meta( get_the_id(), 'pix_role' ) != '' ) { ?>

								<span class="member-socials"><?php echo $social; ?></span>

							<?php } ?>

						</span></span></span>

					</span><!-- .member-wrap -->

				</li><!-- #post-<?php the_ID(); ?> -->
				<?php $count++;

				$out .= ob_get_clean();
	        } 

		endwhile;

	    $out .= '</ul><!-- #' . $uniq_id . ' -->';
	endif;

	wp_reset_postdata();

	return $out;

}
  

/************************************************
*
*		PROJECT LISTS
*
************************************************/
add_shortcode( 'logan_project', 'logan_vc_portfolio_loop' );
function logan_vc_portfolio_loop( $atts ) {
	extract( shortcode_atts( array(
		'include' => '',
		'exclude' => '',
		'taxonomies' => '',
		'duplicate' => '',
		'grid' => 'fitRows',
		'order' => 'DESC',
		'orderby' => 'date',
		'text_position' => '',
		'text_align' => '',
		'fx' => '',
		'columns' => '3',
		'columns_landscape' => '',
		'columns_portrait' => '',
		'columns_phones' => '',
		'ppp' => get_option('jetpack_portfolio_posts_per_page'),
		'pagination' => '',
		'gutter' => '30',
		'pixel' => '',
		'width' => '4',
		'height' => '3',
		'filter' => '',
		'categories' => '',
		'title' => '',
		'heading' => 'h5',
		'title_link' => 'post',
		'likes' => '',
		'read_more' => '',
		'cbox' => 'enable',
		'css' => '',
		'uniqid' => ''
	), $atts ) );

	$ppp = $ppp == 0 || $ppp == '' ? get_option('posts_per_page') : $ppp;

	$args = array(
	  'post_type' => 'jetpack-portfolio',
	  'posts_per_page' => -1,
	  'order' => $order,
	  'orderby' => $orderby
	);

	if ( is_front_page() ) {
		$paged = (get_query_var('page')) ? get_query_var('page') : 1;	
	} else {
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;	
	}
	$args['paged'] = $paged;

	if ( $paged == 1 ) {
		unset( $_SESSION['pix_random_fix'] );
		$_SESSION['pix_random_fix'] = rand();
	}

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'logan_project', $atts );

	if ( $include != '' ) {
		$include  = explode(",", $include);
		$args['post__in'] = $include;
	}

	if ( $exclude != '' && $include == '' ) {
		$exclude  = explode(",", $exclude);
		$args['post__not_in'] = $exclude;
	}

    $tax_query = array();

    if ( $taxonomies != '' ) {
		$taxonomies  = explode(",", $taxonomies);

		if ( !empty($taxonomies) ) {

		    if( count( $taxonomies > 1 ) )
		        $tax_query['relation'] = 'OR' ;

		    foreach( $taxonomies as $custom_term ) {

		        $tax_query[] = array(
		            'taxonomy' => 'jetpack-portfolio-type',
		            'field' => 'id',
		            'terms' => $custom_term
		        );

		    }
		}

		if ( !empty($tax_query) )
			$args['tax_query'] = $tax_query;
	}

	global $wp_query,
		$actual_query,
		$logan_max_width,
		$pix_module_posts_already_displayed,
		$shortcode_loop;

	$or_query = $wp_query;

	$likes = $likes=='' ? true : false;

	$h_start = intval( $height );
	$w_start = intval( $width );

	$w = intval( ( $logan_max_width - ( $gutter * ($columns-1) ) ) / $columns );
	$w_landscape = $columns_landscape != '' ? intval( ( 1024 - ( $gutter * ($columns_landscape-1) ) ) / $columns_landscape ) : 0;
	$w_portrait = $columns_portrait != '' ? intval( ( 800 - ( $gutter * ($columns_portrait-1) ) ) / $columns_portrait ) : 0;
	$w_phones = $columns_phones != '' ? intval( ( 400 - ( $gutter * ($columns_phones-1) ) ) / $columns_phones ) : 0;

	$w = $pixel != '' ? $pixel : max( $w, $w_landscape, $w_portrait, $w_phones);

	if ( $h_start != 0 && $w_start != 0 ) {
	    $h = intval($w / ( $w_start / $h_start ));
	    $crop = true;
	} else {
	    $h = 0;
	    $crop = null;
	}

	$shortcode_loop = true;

	$out = '';

	$actual_query = $args;

	$query = new WP_Query($args);

	if ( $query->have_posts() ) :

		$number_of_posts = sizeof($query->posts);
		$wp_query->max_num_pages = ceil($number_of_posts/$ppp);

	    $out .= '<div id="wrap-' . $uniqid . '" class="post-list-wrap ' . esc_attr($css_class) . '" style="margin-left:-' . $gutter . 'px;">';

		if ( $filter != 'hide' && $grid != 'carousel' && function_exists('pixedelic_portfolio_filter') )
			$out .= pixedelic_portfolio_filter();

		$infinite = $pagination == 'infinite' ? ' post-list-infinite' : '';

	    $out .= '<div id="' . $uniqid . '" class="post-list project-list' . $infinite . '" data-grid="' . esc_attr( $grid ) . '" data-columns="' . esc_attr( $columns ) . '" data-landscape="' . esc_attr( $columns_landscape ) . '" data-portrait="' . esc_attr( $columns_portrait ) . '" data-phone="' . esc_attr( $columns_phones ) . '">';

		if ( $grid == 'carousel' ) {
			$out .= '<div class="post-list-overlay-gutter" style="width: ' . $gutter . 'px"></div>';
			$out .= '<button type="button" data-role="none" class="post-list-prev slick-prev" aria-label="' . esc_html__("Previous", "logan") . '" role="button" style="margin-left: ' . $gutter . 'px;"></button>';
			$out .= '<button type="button" data-role="none" class="post-list-next slick-next" aria-label="' . esc_html__("Next", "logan") . '" role="button"></button>';			
		}

		$count = 1;

		$ppp = $ppp == 0 || $ppp == '' ? 1000000000000 : intval($ppp);

		$fx = $fx != '' ? 'wpb_animate_when_almost_visible wpb_' . $fx : '';

		while ( $query->have_posts() ) : $query->the_post();

			if ( $count > $ppp*$paged ) 
				break;

			$galleries = array();

			if ( get_post_format()=='gallery' ) {

				$more = 1;
				$the_content = get_the_content();
				preg_match('/\[gallery(.+?)\]/', $the_content, $gallery_match);
				preg_match('/ids=[\'|\"](.+?)[\'|\"]/', $gallery_match[0], $gallery_ids);
				$galleries = explode(",", $gallery_ids[1]);

		    }

			$text_pos = ( !has_post_thumbnail() && !pixedelic_featured_media() && !( get_post_format()=='gallery' && !empty($galleries) ) ) ? 'below' : $text_position;
			$text_pos = $text_pos == '' ? 'below' : $text_pos;

			$gutter_pos = $text_pos == 'below' ? 0 : $gutter;
			$gutter_room = $text_pos == 'below' && $columns > 1 ? ( 20 - $gutter ) : 0;

			$style_align = $text_align != '' ? ' text-align:' . $text_align . ';' : '';
			$style_align = $text_pos=='below' ? $style_align . ' width: calc(100% - ' . ($gutter_room*2) . 'px); padding-bottom: ' . $gutter_room . 'px;' : $style_align;
			$style_align = ' style="' . $style_align . '"';

			$term_list = wp_get_post_terms( get_the_ID(), 'jetpack-portfolio-tag' );
			$tag_classes = 'tag-all';
			foreach ( $term_list as $term ) {
				$tag_classes .= ' tag-' . esc_attr( $term->slug );
			}
		
			$first_time = false;

		    ob_start();

			if ( !in_array( get_the_ID(), $pix_module_posts_already_displayed ) ) {
				$first_time = true;
				$pix_module_posts_already_displayed[] = get_the_ID();
			}

			if ( $first_time || $duplicate == '' ) {

				$count++;

			?>

			<article id="post-<?php the_ID(); ?>" <?php post_class( array( 'text-position-' . $text_pos, 'alignleft', $tag_classes, 'transparent_opacity' ) ); ?> style="padding-left: <?php echo $gutter; ?>px; margin-bottom: <?php echo $gutter; ?>px;">

				<div class="for-reveal <?php echo $fx; ?>">

				<?php

				$w = intval( ( $logan_max_width - ( $gutter * ($columns-1) ) ) / $columns );
				$w_landscape = $columns_landscape != '' ? intval( ( 1024 - ( $gutter * ($columns_landscape-1) ) ) / $columns_landscape ) : 0;
				$w_portrait = $columns_portrait != '' ? intval( ( 800 - ( $gutter * ($columns_portrait-1) ) ) / $columns_portrait ) : 0;
				$w_phones = $columns_phones != '' ? intval( ( 400 - ( $gutter * ($columns_phones-1) ) ) / $columns_phones ) : 0;

				$w = $pixel != '' ? $pixel : max( $w, $w_landscape, $w_portrait, $w_phones);

				if ( $h_start != 0 && $w_start != 0 ) {
				    $h = intval($w / ( $w_start / $h_start ));
				    $crop = true;
				} else {
				    $h = 0;
				    $crop = null;
				}

				?>

				<?php
				$entry_text = '<div class="entry-text"' . $style_align . '><div class="entry-table"><div class="entry-cell">';
				$entry_text_content = '';

						/* COLORBOX */

							if ( $cbox == 'enable' && $text_pos != 'below' ) {
								if ( has_post_thumbnail() && ( !pixedelic_featured_media() || ( pixedelic_featured_media() && get_post_format()=='gallery' ) ) ) {

						            $src = wp_get_attachment_url( get_post_thumbnail_id() );
									$entry_text_content .= '<a href="' . esc_url($src) . '" class="enlarge-featured-media" data-rel="projects"></a>';

						        } elseif ( !has_post_thumbnail() && pixedelic_featured_media() && get_post_format()=='gallery' ) {

							    	$img_id = $galleries[0];
									$src = wp_get_attachment_url( $img_id );
									$entry_text_content .= '<a href="' . esc_url($src) . '" class="enlarge-featured-media" data-rel="projects"></a>';

						        } elseif ( get_post_format()=='video' && pixedelic_featured_media() ) {

						        	if ( strpos(pixedelic_featured_media(), '<iframe') !== false ) {
										$entry_text_content .= '<a href="' . pixedelic_get_featured_video() . '" data-size="[' . esc_attr( implode(",", pixedelic_get_featured_video('size') ) ) . ']" class="enlarge-featured-media cboxiframe" data-rel="projects"></a>';
									} else {
										$id_unique = uniqid('pix-video-inline-');
										$entry_text_content .= '<a href="#' . $id_unique . '" class="enlarge-featured-media cboxinline" data-rel="projects"></a>';
										$entry_text_content .= '<div class="fake_hidden"><div id="' . $id_unique . '">' . pixedelic_get_featured_video() . '</div></div>';
									}

						        }
							}

						/* TITLE */
							if ( $title != 'hide' ) {
								if ( $title_link != '' )
									$entry_text_content .= sprintf( "<{$heading} class='entry-title'><a href='%s' rel='bookmark'>%s</a></{$heading}>", esc_url(get_permalink()), get_the_title() );
								else
									$entry_text_content .= sprintf( "<{$heading} class='entry-title'>%s</{$heading}>", get_the_title() );
							}

						/* CATEGORIES and FORMAT */
							if ( $categories != 'hide' ) {

								$entry_text_content .= '<span class="cat-links">';

									if ( $categories != 'hide' ) {
										$i = 0;
										$term_list = wp_get_post_terms(get_the_ID(), 'jetpack-portfolio-type');
										foreach ( $term_list as $term ) {
											$i++;
											$entry_text_content .= '<a href="' . esc_url( get_term_link( $term ) ) . '">' . esc_html( $term->name ) . '</a>';
											if ( $i < count($term_list) ) $entry_text_content .= ', ';
										}
									}
								$entry_text_content .= '</span>';

							}

						/* POST META */
							ob_start();

							logan_posted_on(false, false, false, false, $likes, false);

							$entry_text_content .= ob_get_clean();

							if ( $read_more=='' ) {
								$entry_text_content .= '<a class="more-link logan-button" href="' . esc_url( get_permalink() ) . '">' . esc_html__( 'See more', 'logan' ) . '</a>';
							}

					if ( $entry_text_content != '' ) {
						$entry_text = $entry_text . $entry_text_content . '</div></div></div><!-- .entry-text -->'; 
					} else {
						$entry_text = ''; 
					}
					

					/* FEATURED IMAGE */

							if ( has_post_thumbnail() && ( !pixedelic_featured_media() || get_post_format()=='gallery' ) ) {

					            $src = wp_get_attachment_url( get_post_thumbnail_id() );
					            $src_res = logan_resizer( $src, $w, $h, $crop );

					            ?>

					            <div class="featured-media-wrap">

									<?php
									$box = false; 
									if ( $cbox == 'enable' && $text_pos == 'below' ) {
										if ( has_post_thumbnail() && ( !pixedelic_featured_media() || ( pixedelic_featured_media() && get_post_format()=='gallery' ) ) ) {

											echo '<a href="' . esc_url($src) . '" class="enlarge-featured-media" data-rel="projects">';

											$box = true; 

								        }
									}
									?>
						            <img src="<?php echo esc_url($src_res); ?>" class="attachment-custom_size wp-post-image" width="<?php echo esc_attr($w); ?>" height="<?php echo esc_attr($h); ?>" alt="<?php the_title_attribute(); ?>">
						            <?php
						            if ( $box == true )
						            	echo '</a>';
						            ?>

									<?php if ( get_post_format()=='gallery' ) {

								    	foreach ($galleries as $img_id) {
											$src_2 = wp_get_attachment_url( $img_id );
											if ( $src_2 != $src )
									    		break;
								    	}

								    	if ( $text_pos != 'below' ) echo $entry_text;

							            if ( isset($src_2) ) {
								            $src_2_res = logan_resizer( $src_2, $w, $h, $crop );
							            	echo '<div style="background-image:url(' . esc_url($src_2_res) . ')" class="attachment-custom_size wp-post-image gallery-second-img"></div>';

											if ( $box == true ) {
												echo '<a href="' . esc_url($src_2) . '" class="enlarge-featured-media hidden_important" data-rel="projects"></a>';
											}
							            }

								    } else {

								    	if ( $text_pos != 'below' ) echo $entry_text;

								    } ?>

					            </div>

					        <?php } elseif ( !has_post_thumbnail() && get_post_format()=='gallery' ) { ?>

					            <div class="featured-media-wrap">

									<?php

								    	$img_id = $galleries[0];

										$src = wp_get_attachment_url( $img_id );
							            $src_res = logan_resizer( $src, $w, $h, $crop );

										$box = false; 
										if ( $cbox == 'enable' && $text_pos == 'below' ) {

											echo '<a href="' . esc_url($src) . '" class="enlarge-featured-media" data-rel="projects">';

											$box = true; 

										}

						            	echo '<img src="' . esc_url($src_res) . '" class="attachment-custom_size wp-post-image" width="' . esc_attr($w) . '" height="' . esc_attr($h) . '" alt="' . the_title_attribute( 'echo=0' ) . '">';

							            if ( $box == true )
							            	echo '</a>';

							            /**** 2 ****/

								    	$img_id_2 = $galleries[1];

										$src_2 = wp_get_attachment_url( $img_id_2 );
							            $src_2_res = logan_resizer( $src_2, $w, $h, $crop );

						            	echo '<div style="background-image:url(' . esc_url($src_2_res) . ')" class="attachment-custom_size wp-post-image gallery-second-img"></div>';

						            	if ( $text_pos != 'below' ) echo $entry_text;

										if ( $cbox == 'enable' ) {
											echo '<a href="' . esc_url($src_2) . '" class="enlarge-featured-media hidden_important" data-rel="projects"></a>';
										}
								    ?>

					            </div>

					        <?php } elseif ( pixedelic_featured_media() && get_post_format()!='gallery' ) { ?>

								<div class="featured-video featured-media" data-width="<?php echo $w; ?>" data-height="<?php echo $h; ?>">

									<?php
										if ( $cbox == 'enable' && $text_pos == 'below' ) {

								        	if ( strpos(pixedelic_featured_media(), '<iframe') !== false ) {
												echo '<a href="' . pixedelic_get_featured_video() . '" data-size="[' . esc_attr( implode(",", pixedelic_get_featured_video('size') ) ) . ']" class="enlarge-featured-media cboxiframe" data-rel="projects"></a>';
											} else {
												$id_unique = uniqid('pix-video-inline-');
												echo '<a href="#' . $id_unique . '" class="enlarge-featured-media cboxinline" data-rel="projects"></a>';
												echo '<div class="fake_hidden"><div id="' . $id_unique . '">' . pixedelic_get_featured_video() . '</div></div>';
											}

								        }
								    ?>

									<?php 
										echo pixedelic_featured_media();
									?>

								    <?php 
								    	if ( $text_pos != 'below' ) {

								    		echo $entry_text;

								    	} else {

								    		if ( $cbox == 'enable' )
								    			echo '</a>';

								    	}
								    ?>

								</div>

					        <?php }

					    	if ( $text_pos == 'below' ) echo $entry_text;
					?>

				</div><!-- .for-reveal -->

			</article><!-- #post-<?php the_ID(); ?> -->

			<?php }

			if ( $count-1 > $ppp * ( $paged - 1 ) )
				$out .= ob_get_contents();
				
			ob_get_clean();

		endwhile;

		$out .= '</div><!-- #' . $uniqid . '.post-list -->';

		ob_start();

		if ( $pagination!='' && $grid != 'carousel' ) {
			$pagination = $pagination == 'infinite' ? true : false;
			logan_pagenavi($gutter, $pagination, 50-$gutter);
		}

		$out .= ob_get_clean();

		$out .= '</div><!-- #wrap-' . $uniqid . ' -->';

	endif;

	$actual_query = '';
	$wp_query = $or_query;
	wp_reset_postdata();

	$shortcode_loop = false;
	return $out;
}

add_shortcode( 'logan_css_animation', 'logan_vc_css_animation' );
function logan_vc_css_animation( $atts, $content ) {
	extract( shortcode_atts( array(
		'fx' => '',
		'display' => 'block',
		'parallax' => '',
		'fade_parallax' => '',
		'z_index' => '',
		'class' => '',
		'pix_box_shadow' => '',
		'css' => ''
	), $atts ) );

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'logan_css_animation', $atts );

	$class = $class != '' ? ' ' . esc_attr($class) : '';
	$class .= esc_attr($css_class);

	$fx = $fx != '' ? 'wpb_animate_when_almost_visible wpb_' . esc_attr($fx)  : '';
	$z_index = $z_index != '' ? intval($z_index) : 0;

	if ( $pix_box_shadow!='' )
		$class .= $class . ' ' . $pix_box_shadow;

	$fade_parallax = $fade_parallax == 'yes' ? ' data-parallax-o-fade="on"' : '';
	$parallax = $parallax != '' ? $fade_parallax . ' data-parallax="on" data-offset-param="' . esc_attr($parallax) . '"' : '';

	return '<div class="' . $fx . '" style="display:' . esc_attr($display) . '; position:relative; z-index:' . $z_index . ';"><div class="' . $class . '" style="display:' . esc_attr($display) . '; position:relative; z-index:' . $z_index . ';"' . $parallax . '>' . do_shortcode($content) . '</div></div>';
}

add_shortcode( 'logan_separator', 'logan_vc_separator' );
function logan_vc_separator( $atts, $content ) {
	extract( shortcode_atts( array(
		'color' => '#222324',
		'alignment' => 'center',
		'height' => '2',
		'max_width' => '',
		'width' => '100',
		'fx' => '',
		'id' => '',
		'class' => '',
		'css' => ''
	), $atts ) );

	$max_width = $max_width == '' ? 'none' : intval($max_width);

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'logan_css_animation', $atts );

	$class = $class != '' ? ' ' . esc_attr($class) : '';
	$css_class = esc_attr($css_class);

	$float = $alignment == 'center' ? 'none' : $alignment;

	$fx = $fx != '' ? 'wpb_animate_when_almost_visible wpb_' . esc_attr($fx) . $class : $class;

	return '<div class="logan-typo-separator ' . $css_class . '"><div class="' . $fx . 
		'" style="background-color:' . esc_attr($color) . 
		'; margin:auto; float: ' . esc_attr($float) . 
		'; height: ' . intval($height) . 
		'px; max-width: ' . esc_attr($max_width) . 
		'px; width: ' . intval($width) . 
		'%;"></div></div>';
}

add_shortcode( 'logan_image_box', 'logan_vc_image_box' );
function logan_vc_image_box( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'source' => '',
		'image' => '',
		'size' => 'full',
		'custom_src' => '',
		'link' => '',
		'color' => '',
		'align' => '',
		'css_animation' => '',
		'css' => ''
	), $atts ) );

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'logan_image_box', $atts );

	if ( isset($css_animation) && $css_animation!='' )
		$anim = ' wpb_animate_when_almost_visible wpb_' . esc_attr($css_animation) ;
	else
		$anim = '';

	$out = '<div class="pix-image-box'. $anim . ' ' . $css_class . '">';

	if ( isset( $link ) ) {
		$vc_link = vc_build_link( $link );

		$url = $vc_link['url'];

		$title = esc_attr( $vc_link['title'] );
		$title = $title != '' ? " title='$title'" : "";

		$target = esc_attr( $vc_link['target'] );
		$target = $target != '' ? " target='$target'" : "";

		$out .= "<a href='" . esc_url($url) . "' $title $target>";
	}

	if ( isset( $image ) ) {
		if ( preg_match( '/array\(([0-9]+),([0-9]+)\)/', $size) ) {
			preg_match( '/array\(([0-9]+),([0-9]+)\)/', $size, $sizes);
			$size = array( $sizes[1], $sizes[2] );
		} elseif ( preg_match( '/([0-9]+)x([0-9]+)/', $size) ) {
			preg_match( '/([0-9]+)x([0-9]+)/', $size, $sizes);
			$size = array( $sizes[1], $sizes[2] );
		}
		$out .= wp_get_attachment_image( esc_attr($image), $size );

	}

	if ( $content != null ) {

		$content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content

		$align = explode(",", $align);

		$out .= '<div class="image-box-entry-box" style="color:' . pix_sanitize_hex_color($color) . '"><div><div style="vertical-align:' . $align[0] . '; text-align:' . $align[1] . '">';

		$out .= $content;

		$out .= '</div></div></div>';

	}

	if ( isset( $link ) ) {
		$out .= '</a>';
	}

	$out .= '</div><!-- .pix-image-box -->';

	return $out;

}


/************************************************
*
*		POST LISTS
*
************************************************/
add_shortcode( 'logan_post', 'logan_vc_post_loop' );
function logan_vc_post_loop( $atts ) {
	extract( shortcode_atts( array(
		'include' => '',
		'exclude' => '',
		'taxonomies' => '',
		'sticky' => 'ignore',
		'duplicate' => '',
		'grid' => 'fitRows',
		'order' => 'DESC',
		'orderby' => 'date',
		'text_position' => 'below',
		'fx' => '',
		'text_align' => '',
		'columns' => '3',
		'columns_landscape' => '',
		'columns_portrait' => '',
		'columns_phones' => '',
		'first_double' => '',
		'ppp' => get_option('posts_per_page'),
		'pagination' => '',
		'gutter' => '30',
		'pixel' => '',
		'width' => '4',
		'height' => '3',
		'categories' => '',
		'format' => '',
		'image' => '',
		'title' => '',
		'heading' => 'h3',
		'subtitle' => '',
		'likes' => '',
		'reviews' => '',
		'author' => '',
		'avatar' => '',
		'date' => '',
		'time' => '',
		'comments' => '',
		'the_content' => '',
		'read_more' => '',
		'css' => '',
		'uniqid' => ''
	), $atts ) );

	$uniqid = $uniqid == '' ? uniqid('post-list-') : $uniqid;

	//$include = is_array( $include ) ? $include : array($include);
	$sticky = $sticky == 'display' ? false : true;
  
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'logan_post', $atts );

	$args = array(
	  'post_type' => 'post',
	  'posts_per_page' => -1,
	  'ignore_sticky_posts' => esc_attr( $sticky ),
	  'order' => $order,
	  'orderby' => $orderby
	);

	if ( is_front_page() ) {
		$paged = (get_query_var('page')) ? get_query_var('page') : 1;	
	} else {
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;	
	}
	$args['paged'] = $paged;

	if ( $paged == 1 ) {
		unset( $_SESSION['pix_random_fix'] );
		$_SESSION['pix_random_fix'] = rand();
	}

	if ( $include != '' ) {
		$include  = explode(",", $include);
		$args['post__in'] = $include;
	}

	if ( $exclude != '' && $include == '' ) {
		$exclude  = explode(",", $exclude);
		$args['post__not_in'] = $exclude;
	}

	if ( $taxonomies != '' ) {
		$tags = array();
		$cats = array();
		
		$taxonomies  = explode(",", $taxonomies);
		foreach ($taxonomies as $tax) {
			if ( term_exists( intval($tax), 'post_tag' ) ) {
				$tags[] = intval($tax);
			}
			if ( term_exists( intval($tax), 'category' ) ) {
				$cats[] = intval($tax);
			}
		}

		if ( !empty($tags) ) {
			$args['tag__in'] = $tags;
		}

		if ( !empty($cats) ) {
			$args['category__in'] = $cats;
		}
	}

	global $wp_query,
		$actual_query,
		$logan_subtitle, 
		$logan_max_width,
		$pix_module_posts_already_displayed,
		$pix_read_more,
		$shortcode_loop;

	$or_query = $wp_query;

	$author = $author=='' ? true : false;
	$avatar = $avatar=='' ? true : false;
	$date = $date=='' ? 'relative' : $date;
	$date = $date=='hide' ? false : $date;
	$time = $time=='' ? true : false;
	$likes = $likes=='' ? true : false;
	$comments = $comments=='' ? true : false;

	$or_readmore = $pix_read_more;
	$pix_read_more = $read_more=='' ? $pix_read_more : 'hide';

	$def_subtitle = $logan_subtitle;
	$logan_subtitle = $subtitle == 'hide' ? 'off' : esc_attr( $subtitle );

	$h_start = intval( $height );
	$w_start = intval( $width );

	$first_article = $first_double ? 2 : 1;

    if ( $first_article==2 ) {
	    $w = intval( ( $logan_max_width - ( $gutter * ($columns-1) ) ) / $columns ) * 2 + $gutter;
		$w_landscape = $columns_landscape != '' ? intval( ( 1024 - ( $gutter * ($columns_landscape-1) ) ) / $columns_landscape ) * 2 + $gutter : 0;
		$w_portrait = $columns_portrait != '' ? intval( ( 800 - ( $gutter * ($columns_portrait-1) ) ) / $columns_portrait ) * 2 + $gutter : 0;
		$w_phones = $columns_phones != '' ? intval( ( 400 - ( $gutter * ($columns_phones-1) ) ) / $columns_phones ) * 2 + $gutter : 0;

		$w = $pixel != '' ? $pixel*2 : max( $w, $w_landscape, $w_portrait, $w_phones);

	    if ( $h_start != 0 && $w_start != 0 ) {
	        $h = intval($w / ( $w_start / $h_start ));
	        $crop = true;
	    } else {
	        $h = 0;
	        $crop = null;
	    }
    } else {
	$w = intval( ( $logan_max_width - ( $gutter * ($columns-1) ) ) / $columns );
    	$w = intval( ( $logan_max_width - ( $gutter * ($columns-1) ) ) / $columns );
		$w_landscape = $columns_landscape != '' ? intval( ( 1024 - ( $gutter * ($columns_landscape-1) ) ) / $columns_landscape ) : 0;
		$w_portrait = $columns_portrait != '' ? intval( ( 800 - ( $gutter * ($columns_portrait-1) ) ) / $columns_portrait ) : 0;
		$w_phones = $columns_phones != '' ? intval( ( 400 - ( $gutter * ($columns_phones-1) ) ) / $columns_phones ) : 0;

		$w = $pixel != '' ? $pixel : max( $w, $w_landscape, $w_portrait, $w_phones);

		if ( $h_start != 0 && $w_start != 0 ) {
		    $h = intval($w / ( $w_start / $h_start ));
		    $crop = true;
		} else {
		    $h = 0;
		    $crop = null;
		}
	}

	$shortcode_loop = true;

	$out = '';

	$actual_query = $args;

	$query = new WP_Query($args);

	if ( $query->have_posts() ) :

		$number_of_posts = sizeof($query->posts);
		$wp_query->max_num_pages = ceil($number_of_posts/$ppp);

		$infinite = $pagination == 'infinite' ? ' post-list-infinite' : '';

		$out .= '<div id="' . $uniqid . '" class="post-list ' . esc_attr($css_class) . $infinite . '" data-grid="' . esc_attr( $grid ) . '" data-columns="' . esc_attr( $columns ) . '" data-landscape="' . esc_attr( $columns_landscape ) . '" data-portrait="' . esc_attr( $columns_portrait ) . '" data-phone="' . esc_attr( $columns_phones ) . '" data-double="' . esc_attr( $first_double ) . '" style="margin-left:-' . $gutter . 'px;">';

		if ( $grid == 'carousel' ) {
			$out .= '<div class="post-list-overlay-gutter" style="width: ' . $gutter . 'px"></div>';
			$out .= '<button type="button" data-role="none" class="post-list-prev slick-prev" aria-label="' . esc_html__("Previous", "logan") . '" role="button" style="margin-left: ' . $gutter . 'px;"></button>';
			$out .= '<button type="button" data-role="none" class="post-list-next slick-next" aria-label="' . esc_html__("Next", "logan") . '" role="button"></button>';			
		}

		$count = 1;

		$ppp = $ppp == 0 || $ppp == '' ? 1000000000000 : intval($ppp);

		$text_align = $text_align != '' ? 'text-align-' . $text_align : $text_align;

		$fx = $fx != '' ? 'wpb_animate_when_almost_visible wpb_' . $fx : '';

		while ( $query->have_posts() ) : $query->the_post();

			if ( $count > $ppp*$paged ) 
				break;

			$post_id = get_the_ID();

			$post = get_post($post_id);

			$galleries = array();
			$gallery_arr = get_post_gallery( get_the_ID(), false );

			if ( !empty($gallery_arr) )
				$galleries = explode(",", $gallery_arr['ids']);

			$text_pos = ( !has_post_thumbnail() && !pixedelic_featured_media() && !( get_post_format()=='gallery' && !empty($galleries) ) ) || ( ( get_post_format()=='quote' || get_post_format()=='status' || get_post_format()=='link' ) && $the_content != 'content' ) ? 'below' : $text_position;
			$text_pos = $text_pos == '' ? 'below' : $text_pos;
		
			$gutter_pos = $text_pos == 'over' && $columns > 1 ? $gutter : 0;
			$gutter_pos = $text_pos == 'below' && $columns > 1 ? 40 - $gutter*2 : $gutter_pos;
			$gutter_pos = $gutter_pos < 0 ? 0 : $gutter_pos;

			$first_time = false;

		    ob_start();

			if ( !in_array( get_the_ID(), $pix_module_posts_already_displayed ) ) {
				$first_time = true;
				$pix_module_posts_already_displayed[] = get_the_ID();
			}

			if ( $first_time || $duplicate == '' ) {

				$count++;

			?>

			<article <?php post_class( array( 'text-position-' . $text_pos, 'alignleft', 'avatar-' . $avatar, 'read-more-' . $read_more, 'transparent_opacity', $text_align ) ); ?> style="padding-left: <?php echo $gutter; ?>px; margin-bottom: <?php echo $gutter; ?>px; font-size: <?php echo $first_article == 2 ? '1.325' : '1'; ?>em">

				<div class="for-reveal <?php echo $fx; ?>">

			<?php //post format
				if ( ( get_post_format()=='quote' || get_post_format()=='status' || get_post_format()=='link' ) && $the_content != 'content' ) { ?>

				<?php if ( has_post_thumbnail() && $image != 'hide' ) {

			            $src = wp_get_attachment_url( get_post_thumbnail_id() );
			            $src_res = logan_resizer( $src, $w, $h, $crop );
						$bg_quote = ' has-bg-img" style="background-image:url(' . $src_res . ')';

					} else {
						$bg_quote = '"';
					} ?>

					<div class="entry-text entry-excerpt<?php echo $bg_quote; ?>">

			            <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">&nbsp;</a>

						<?php if ( function_exists('wp_review_show_total') && $reviews=='' ) {
							echo '<span class="review-box">';
								wp_review_show_total(); 
							echo '</span><!-- .review-box -->';
						} ?>

						<span class="entry-quote">
							<?php 
							/* CATEGORIES and FORMAT */
								if ( $categories != 'hide' || $format != 'hide' ) {
								?>
									<span class="cat-links">

										<?php if ( $format != 'hide' ) { ?>
											<span class="entry-format"></span>
										<?php } ?>

										<?php if ( $categories != 'hide' ) {

											$categories_list = get_the_category_list( esc_html__( ', ', 'logan' ) );

											if ( $categories_list ) echo $categories_list;
										} ?>
									</span>

								<?php }
							?>

							<?php 
							/* TITLE */
								if ( $title != 'hide' && ( get_post_format()=='status' || get_post_format()=='link' ) ) {
									the_title( sprintf( "<{$heading} class='entry-title'><a href='%s' rel='bookmark'>", esc_url( get_permalink() ) ), "</a></{$heading}>" );
								}
							?>

							<?php 
								if ( $title != 'hide' ) {
									the_content();
								}
							?>
						</span>

					</div><!-- .entry-text -->
				<?php } else {

				/* FEATURED IMAGE */
					if ( $image != 'hide' ) {

						if ( ( has_post_thumbnail() && ( !pixedelic_featured_media() || get_post_format()=='gallery' ) ) || ( !has_post_thumbnail() && get_post_format()=='image' && pixedelic_catch_that_image()!='' ) ) {

				            if ( !has_post_thumbnail() && get_post_format()=='image' && pixedelic_catch_that_image()!='' ) {
					            $frst_src = pixedelic_catch_that_image();
					            $frst_meta = pix_attachment_meta_by_url($frst_src);
					            if ( $frst_meta['id']=='' ) {
						            $src_res = $frst_src;
					            } else {
						            $src = wp_get_attachment_url( $frst_meta['id'] );
						            $src_res = logan_resizer( $src, $w, $h, $crop );
						        }
				            } else {
					            $src = wp_get_attachment_url( get_post_thumbnail_id() );
					            $src_res = logan_resizer( $src, $w, $h, $crop );
					        }

					        ?>

				            <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="featured-media-wrap">

								<?php if ( function_exists('wp_review_show_total') && $reviews=='' ) {
									echo '<span class="review-box">';
										wp_review_show_total(); 
									echo '</span><!-- .review-box -->';
								} ?>

				            	<img src="<?php echo esc_url($src_res); ?>" class="attachment-custom_size wp-post-image" width="<?php echo esc_attr($w); ?>" height="<?php echo esc_attr($h); ?>" alt="<?php the_title_attribute(); ?>">

								<?php if ( get_post_format()=='gallery' ) {

							    	foreach ($galleries as $img_id) {
										$src_2 = wp_get_attachment_url( $img_id );
										if ( $src_2 != $src )
								    		break;
							    	}

						            $src_2_res = logan_resizer( $src_2, $w, $h, $crop );

					            	echo '<div style="background-image:url(' . esc_url($src_2_res) . ')" class="attachment-custom_size wp-post-image gallery-second-img"></div>';

							    } ?>

				            </a>

				        <?php } elseif ( !has_post_thumbnail() && get_post_format()=='gallery' ) { ?>

				            <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="featured-media-wrap">

								<?php if ( function_exists('wp_review_show_total') && $reviews=='' ) {
									echo '<span class="review-box">';
										wp_review_show_total(); 
									echo '</span><!-- .review-box -->';
								} ?>
								
								<?php if ( get_post_format()=='gallery' ) {

									$src = wp_get_attachment_url( $galleries[0] );
						            $src_res = logan_resizer( $src, $w, $h, $crop );

					            	echo '<img src="' . esc_url($src_res) . '" class="attachment-custom_size wp-post-image" width="' . esc_attr($w) . '" height="' . esc_attr($h) . '" alt="' . the_title_attribute( 'echo=0' ) . '">';

									$src_2 = wp_get_attachment_url( $galleries[1] );
						            $src_2_res = logan_resizer( $src_2, $w, $h, $crop );

					            	echo '<div style="background-image:url(' . esc_url($src_2_res) . ')" class="attachment-custom_size wp-post-image gallery-second-img"></div>';

							    } ?>
				            </a>

				        <?php } elseif ( pixedelic_featured_media() && get_post_format()!='gallery' ) { ?>

							<div class="featured-video featured-media" data-width="<?php echo $w; ?>" data-height="<?php echo $h; ?>">

								<?php if ( function_exists('wp_review_show_total') && $reviews=='' ) {
									echo '<span class="review-box">';
										wp_review_show_total(); 
									echo '</span><!-- .review-box -->';
								} ?>

								<?php echo pixedelic_featured_media(); ?>

							</div>

				        <?php }

				    }

				?>

				<div class="entry-text" style="width: calc(100% - <?php echo $gutter_pos; ?>px); padding-bottom: <?php echo $gutter_pos; ?>px">

					<?php 
					/* CATEGORIES and FORMAT */
						if ( $categories != 'hide' || $format != 'hide' ) {
						?>
							<span class="cat-links">

								<?php if ( $format != 'hide' ) { ?>
									<span class="entry-format"></span>
								<?php } ?>

								<?php if ( $categories != 'hide' ) {

									$categories_list = get_the_category_list( esc_html__( ', ', 'logan' ) );

									if ( $categories_list ) echo $categories_list;
								} ?>
							</span>

						<?php }
					?>

					<?php 
					/* TITLE */
						if ( $title != 'hide' ) {
							the_title( sprintf( "<$heading class='entry-title'><a href='%s' rel='bookmark'>", esc_url( get_permalink() ) ), "</a></$heading>" );
						}
					?>

					<?php 
					/* POST META */
						logan_posted_on($author, $avatar, $date, $time, $likes, $comments);
					?>

					<?php 
					/* CONTENT */
						if ( $the_content == 'content' ) {
							echo '<div class="entry-content">';
								echo get_the_content();
							echo '</div><!-- .entry-content -->';
						} elseif ( $the_content == 'excerpt' ) {
							echo '<div class="entry-content entry-excerpt">';
								if ( $read_more=='' )
									$pix_read_more = 'show';
								the_excerpt();
							echo '</div><!-- .entry-content.entry-excerpt -->';
						} else {
							if ( $read_more=='' ) {
								echo '<br><a class="more-link logan-button" href="' . esc_url( get_permalink() ) . '">' . esc_html__( 'Read more', 'logan' ) . '</a>';
							}
						}
					?>

				</div><!-- .entry-text -->

			<?php //post format
				}
			?>
				</div><!-- .for-reveal -->

			</article><!-- #post-<?php the_ID(); ?> -->

			<?php }

			$first_article = 1;

			if ( $count-1 > $ppp * ( $paged - 1 ) )
				$out .= ob_get_contents();
				
			ob_get_clean();

		endwhile;

		$out .= '</div><!-- #' . $uniqid . '.post-list -->';

		ob_start();

		if ( $pagination!='' && $grid != 'carousel' ) {
			$pagination = $pagination == 'infinite' ? true : false;
			logan_pagenavi($gutter, $pagination, 50-$gutter);
		}

		$out .= ob_get_clean();

	endif;

	$logan_subtitle = $def_subtitle;
	$pix_read_more = $or_readmore;
	$actual_query = '';
	$wp_query = $or_query;

	wp_reset_postdata();

	$shortcode_loop = false;

	return $out;
}