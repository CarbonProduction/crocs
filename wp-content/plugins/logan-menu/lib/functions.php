<?php

class PixMenu{

	/**
	 * @since   0.0.2
	 *
	 * @var     string
	 */
	protected $version = '0.0.2';

	/**
	 * @since    0.0.1
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'logan-menu';

	/**
	 * @since    0.0.1
	 *
	 * @var      string
	 */
	protected $plugin_name = 'Logan Menu';

	/**
	 * @since    0.0.1
	 *
	 * @var      object
	 */
	protected static $instance = null;

	public function __construct() {
		add_action( 'init', array( &$this, 'load_plugin_textdomain' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_scripts' ) );

		add_action( 'admin_head', array( &$this, 'js_vars' ) );
		add_action( 'admin_footer', array( &$this, 'font_box' ) );
		add_action( 'wp_ajax_pixmenu_get_thumb', array( &$this, 'ajax_get_thumb' ) );

		add_filter( 'body_class', array( &$this, 'body_class' ) );

		add_filter( 'pre_set_site_transient_update_plugins', array( &$this, 'check_for_plugin_update' ));
		add_filter( 'plugins_api', array( &$this, 'plugin_api_call' ), 10, 3);

		add_action( 'wp_update_nav_menu_item', array( &$this, 'pix_megamenu_update' ),10, 3);
		add_filter( 'wp_setup_nav_menu_item', array( &$this, 'pix_megamenu_items' ) );
		add_filter( 'wp_edit_nav_menu_walker', array( &$this, 'pix_nav_menu_class' ) );

		add_filter( 'wp_nav_menu_items', array( &$this, 'pix_replace_lang_switcher' ),12, 2);
		add_action( 'init', array( &$this, 'pix_replace_default_lang_switcher' ) );
    }

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public static function activate( $network_wide ) {
	}

	/**
	 * Fired when the plugin is uninstall.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Deactivate" action, false if WPMU is disabled or plugin is deactivated on an individual blog.
	 */
	public static function uninstall( $network_wide ) {
	}

	/**
	 * Load text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, WP_LANG_DIR . '/' . $domain . '/' . $locale . '.mo' );
		load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
	}


	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since    1.0.0
	 */
	public function admin_styles() {
		global $pagenow;
		if ( 'nav-menus.php' == $pagenow ) {
			if ( file_exists(get_template_directory().'/font/font-awesome.css') )
				wp_enqueue_style( 'fontawesome-font', get_template_directory_uri() . '/font/font-awesome.css', array(), '4.3.0' );

			if ( file_exists(get_template_directory().'/font/budicon-font.css') )
			    wp_enqueue_style( 'budicon-font', get_template_directory_uri() . '/font/budicon-font.css', array(), '1.2' );

			if ( file_exists(get_template_directory().'/css/jquery-ui.css') )
			    wp_enqueue_style( 'jquery-style', get_template_directory_uri().'/css/jquery-ui.css', array() );

			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style("menu-back", PIXMENU_URL."css/menu-back.css", false, false, "all");
		}
	}

	/**
	 * Register and enqueue admin-specific scripts.
	 *
	 * @since    1.0.0
	 */
	public function admin_scripts() {
		global $pagenow;
		if ( 'nav-menus.php' == $pagenow ) {
			if( function_exists( 'wp_enqueue_media' ) ) {
			    wp_enqueue_media();
			}
		    wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script("pix-menu-back", PIXMENU_URL."scripts/menu-back.js", array('jquery','jquery-ui-dialog'));
		}
	}

	/**
	 * Add the class "pixmenu" to the front-end body
	 *
	 * @since    1.0.0
	 */
	public function body_class($classes) {
		$classes[] = 'logan-menu';
		return $classes;
	}


	/**
	 * Set the content width as JS var.
	 *
	 * @since    0.0.1
	 */
	public static function js_vars() {
		global $pagenow;

		if ( 'nav-menus.php' == $pagenow ) {
			$ajax_nonce = wp_create_nonce('pixmenu_nonce');
		?>

		<script type="text/javascript">
		//<![CDATA[
			var pixmenu_row = "<?php _e('Row', 'logan-menu'); ?>", pixmenu_column = "<?php _e('Column', 'logan-menu'); ?>", pixmenu_nonce = "<?php echo $ajax_nonce; ?>";
		//]]>
		</script>

		<?php
		}
	}


	/**
	 * Print font icon generator on footer for dialog box.
	 *
	 * @since    0.0.1
	 */
	public static function font_box() {
		global $pagenow;

		if ( 'nav-menus.php' == $pagenow && file_exists(get_template_directory().'/inc/icon-list.php') ) { ?>

			<div id="list-icon" class="hidden" style="background-image:url(<?php echo esc_url( admin_url() . '/images/spinner-2x.gif' ); ?>);">
		        <?php require_once get_template_directory() . '/inc/icon-list.php'; ?>
		    </div><!-- #list-icon -->

		<?php }
	}


/*=========================================================================================*/

	/**
	 * Check for update.
	 *
	 * @since    0.0.2
	 */
	public function check_for_plugin_update($checked_data) {
		global $wp_version;

		$api_url = 'http://www.pixedelic.com/api/api.php';

		if (empty($checked_data->checked) || !isset($checked_data->checked[ PIXMENU_NAME ]))
			return $checked_data;
		
		$args = array(
			'dir' => 'logan-menu',
			'slug' => 'logan-menu',
			'version' => $checked_data->checked[ PIXMENU_NAME ],
			'id' => apply_filters('pixedelic_itemID',''),
			'user' => apply_filters('pixedelic_username',''),
			'license' => apply_filters('pixedelic_licensekey','')
		);

		$request_string = array(
				'body' => array(
					'action' => 'basic_check', 
					'request' => serialize($args),
					'api-key' => md5(get_bloginfo('url'))
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
			);
		
		$raw_response = wp_remote_post($api_url, $request_string);

		if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
			$response = unserialize($raw_response['body']);

		if (is_object($response) && !empty($response)) // Feed the update data into WP updater
			$checked_data->response[ PIXMENU_NAME ] = $response;
		
		return $checked_data;
	}

	/**
	 * Call for update.
	 *
	 * @since    0.0.1
	 */
	public function plugin_api_call($def, $action, $args) {
		global $wp_version;
		
		$plugin_slug = 'logan-menu';
		$api_url = 'http://www.pixedelic.com/api/api.php';
		
		if (!isset($args->slug) || ($args->slug != $plugin_slug))
			return false;
			
		$plugin_info = get_site_transient('update_plugins');
		$current_version = $plugin_info->checked[ PIXMENU_NAME ];
		$args->version = $current_version;
		
		$request_string = array(
				'body' => array(
					'action' => $action, 
					'request' => serialize($args),
					'api-key' => md5(get_bloginfo('url'))
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
			);
		
		$request = wp_remote_post($api_url, $request_string);

		if (is_wp_error($request)) {
			$res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
		} else {
			$res = unserialize($request['body']);

			if ($res === false)
				$res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
		}
		
		return $res;
	}


	/**
	 * Return a thumbnail
	 *
	 * @since    1.0.0
	 */

	public static function ajax_get_thumb() {

		check_ajax_referer('pixmenu_nonce', 'security');

		//echo $_POST['content'];

        $attachment_id = $_POST['content'];
        $attachment_bg = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
        echo $attachment_bg[0];

		die();
	}

/*=========================================================================================*/
	public function pix_megamenu_update($menu_id, $menu_item_db_id, $args ) {
	    if(isset($_POST['pix-megamenu-item'])){
			if ( is_array($_POST['pix-megamenu-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-megamenu-item'])) {
					$custom_value = $_POST['pix-megamenu-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_megamenu_item', $custom_value );
				}
			}
		}
	    /*if(isset($_POST['pix-calltoaction-item'])){
			if ( is_array($_POST['pix-calltoaction-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-calltoaction-item'])) {
					$custom_value = $_POST['pix-calltoaction-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_calltoaction_item', $custom_value );
				}
			}
		}*/
	    if(isset($_POST['pix-labeltext-item'])){
			if ( is_array($_POST['pix-labeltext-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-labeltext-item'])) {
					$custom_value = $_POST['pix-labeltext-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_labeltext_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-labellink-item'])){
			if ( is_array($_POST['pix-labellink-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-labellink-item'])) {
					$custom_value = $_POST['pix-labellink-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_labellink_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-taglabel-item'])){
			if ( is_array($_POST['pix-taglabel-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-taglabel-item'])) {
					$custom_value = $_POST['pix-taglabel-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_taglabel_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-tagcolor-item'])){
			if ( is_array($_POST['pix-tagcolor-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-tagcolor-item'])) {
					$custom_value = $_POST['pix-tagcolor-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_tagcolor_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-tagbg-item'])){
			if ( is_array($_POST['pix-tagbg-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-tagbg-item'])) {
					$custom_value = $_POST['pix-tagbg-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_tagbg_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-column-item'])){
			if ( is_array($_POST['pix-column-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-column-item'])) {
					$custom_value = $_POST['pix-column-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_column_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-bg-item'])){
			if ( is_array($_POST['pix-bg-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-bg-item'])) {
					$custom_value = $_POST['pix-bg-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_bg_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-bg-id'])){
			if ( is_array($_POST['pix-bg-id']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-bg-id'])) {
					$custom_value = $_POST['pix-bg-id'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_bg_id', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-bg-size'])){
			if ( is_array($_POST['pix-bg-size']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-bg-size'])) {
					$custom_value = $_POST['pix-bg-size'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_bg_size', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-bgcss-item'])){
			if ( is_array($_POST['pix-bgcss-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-bgcss-item'])) {
					$custom_value = $_POST['pix-bgcss-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_bgcss_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-icon-item'])){
			if ( is_array($_POST['pix-icon-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-icon-item'])) {
					$custom_value = $_POST['pix-icon-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_icon_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-width100-item'])){
			if ( is_array($_POST['pix-width100-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-width100-item'])) {
					$custom_value = $_POST['pix-width100-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_width100_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-image-item'])){
			if ( is_array($_POST['pix-image-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-image-item'])) {
					$custom_value = $_POST['pix-image-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_image_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-image-id'])){
			if ( is_array($_POST['pix-image-id']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-image-id'])) {
					$custom_value = $_POST['pix-image-id'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_image_id', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-image-size'])){
			if ( is_array($_POST['pix-image-size']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-image-size'])) {
					$custom_value = $_POST['pix-image-size'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_image_size', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-sidebar-item'])){
			if ( is_array($_POST['pix-sidebar-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-sidebar-item'])) {
					$custom_value = $_POST['pix-sidebar-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_sidebar_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-width-item'])){
			if ( is_array($_POST['pix-width-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-width-item'])) {
					$custom_value = $_POST['pix-width-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_width_item', $custom_value );
				}
			}
		}
	    if(isset($_POST['pix-title-item'])){
			if ( is_array($_POST['pix-title-item']) ) {
				if(array_key_exists($menu_item_db_id, $_POST['pix-title-item'])) {
					$custom_value = $_POST['pix-title-item'][$menu_item_db_id];
					update_post_meta( $menu_item_db_id, '_pix_title_item', $custom_value );
				}
			}
		}
	}

	public function pix_megamenu_items($menu_item) {
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_megamenu_item', true );
	    //$menu_item->custom = get_post_meta( $menu_item->ID, '_pix_calltoaction_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_labeltext_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_labellink_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_taglabel_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_tagcolor_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_tagbg_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_column_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_bg_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_bg_id', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_bg_size', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_bgcss_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_icon_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_width100_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_image_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_image_id', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_image_size', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_sidebar_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_width_item', true );
	    $menu_item->custom = get_post_meta( $menu_item->ID, '_pix_title_item', true );
	    return $menu_item;
	}

	public function pix_nav_menu_class(){
		return 'Pix_Walker_Nav_Menu_Edit';
	}

	public function pix_replace_lang_switcher($items, $args){
        global $sitepress_settings, $sitepress, $icl_language_switcher, $wpml_en;

        if ( is_plugin_active('sitepress-multilingual-cms/sitepress.php') && !empty($sitepress_settings['display_ls_in_menu'])) {

	        $argsArr = get_object_vars($args);

	        $walker = '';
	        if ( is_object($argsArr['walker']) ) {
	        	$walker = get_class($argsArr['walker']);
	        }

	        // menu can be passed as integger or object
	        if(isset($args->menu->term_id)) $args->menu = $args->menu->term_id;

	        $abs_menu_id = icl_object_id($args->menu, 'nav_menu', false, $sitepress->get_default_language());

	        if($abs_menu_id == $sitepress_settings['menu_for_ls']){

	            $languages = $sitepress->get_ls_languages();

	            $classes = apply_filters('pixmenu_sitepress_classes', '');

	            //if ($walker=='Geode_Walker') {
		            $items .= '<div role="listitem" class="menu-item menu-item-language menu-item-language-current'.$classes.'">';
		            if(isset($args->before)){
		                $items .= $args->before;
		            }
		            $items .= '<span></span><a href="#" onclick="return false"><span>';
		            if(isset($args->link_before)){
		                $items .= $args->link_before;
		            }
		            $items .= apply_filters('pixmenu_sitepress_icon', '');
		            $items .= $languages[$sitepress->get_current_language()]['translated_name'];
		            if(isset($args->link_after)){
		                $items .= $args->link_after;
		            }
		            $items .= '</span></a>';
		            if(isset($args->after)){
		                $items .= $args->after;
		            }

		            unset($languages[$sitepress->get_current_language()]);

		            if(!empty($languages)){
		                $items .= '<div role="list" class="sub-menu submenu-languages children">';
		                foreach($languages as $code => $lang){
		                    $items .= '<div role="listitem" class="menu-item menu-item-language">';
		                    $items .= '<div></div>';
		                    $items .= '<span></span><a href="'.$lang['url'].'">';
		                    if( $sitepress_settings['icl_lso_flags'] ){
		                        $items .= '<img class="iclflag" src="'.$lang['country_flag_url'].'" width="18" height="12" alt="'.$lang['translated_name'].'" />';
		                    }
		                    if($sitepress_settings['icl_lso_native_lang']){
		                        $items .= $lang['native_name'];
		                    }
		                    if($sitepress_settings['icl_lso_display_lang'] && $sitepress_settings['icl_lso_native_lang']){
		                        $items .= ' (';
		                    }
		                    if($sitepress_settings['icl_lso_display_lang']){
		                        $items .= $lang['translated_name'];
		                    }
		                    if($sitepress_settings['icl_lso_display_lang'] && $sitepress_settings['icl_lso_native_lang']){
		                        $items .= ')';
		                    }
		                    $items .= '</a>';
		                    $items .= '</div>';

		                }
		                $items .= '</div>';
		            }
		            $items .= '</div>';

		        /*} elseif ($walker=='Geode_Walker_Mobile') {
		            $items .= '<option class="menu-item menu-item-language menu-item-language-current">';
		            if(isset($args->before)){
		                $items .= $args->before;
		            }
		            if(isset($args->link_before)){
		                $items .= $args->link_before;
		            }
		            $items .= $languages[$sitepress->get_current_language()]['translated_name'];
		            if(isset($args->link_after)){
		                $items .= $args->link_after;
		            }
		            if(isset($args->after)){
		                $items .= $args->after;
		            }

		            unset($languages[$sitepress->get_current_language()]);

		            if(!empty($languages)){
		                foreach($languages as $code => $lang){
		                    $items .= '<option class="menu-item menu-item-language menu-item-language-current" data-href="'.$lang['url'].'">';
		                    if($sitepress_settings['icl_lso_native_lang']){
		                        $items .= '- ' . $lang['native_name'];
		                    }
		                    if($sitepress_settings['icl_lso_display_lang'] && $sitepress_settings['icl_lso_native_lang']){
		                        $items .= ' (';
		                    }
		                    if($sitepress_settings['icl_lso_display_lang']){
		                        $items .= $lang['translated_name'];
		                    }
		                    if($sitepress_settings['icl_lso_display_lang'] && $sitepress_settings['icl_lso_native_lang']){
		                        $items .= ')';
		                    }
		                    $items .= '</option>';

		                }
		            }
		            $items .= '</option>';

		        }*/
		    }
		}

	    return $items;

	}

	public function pix_replace_default_lang_switcher() {
        global $icl_language_switcher;
        remove_filter( 'wp_nav_menu_items', array($icl_language_switcher, 'wp_nav_menu_items_filter') );
	}

}

require_once( ABSPATH . 'wp-admin/includes/nav-menu.php');
	class Pix_Walker_Nav_Menu_Edit extends Walker_Nav_Menu_Edit  {
		/**
		 * @see Walker_Nav_Menu::start_lvl()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference.
		 */
		function start_lvl(&$output, $depth = 0, $args = array()) {}

		/**
		 * @see Walker_Nav_Menu::end_lvl()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference.
		 */
		function end_lvl(&$output, $depth = 0, $args = array()) {
		}

		/**
		 * @see Walker::start_el()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param object $item Menu item data object.
		 * @param int $depth Depth of menu item. Used for padding.
		 * @param object $args
		 */
		function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
			global $_wp_nav_menu_max_depth, $megamenu;
			$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			ob_start();
			$item_id = esc_attr( $item->ID );
			$removed_args = array(
				'action',
				'customlink-tab',
				'edit-menu-item',
				'menu-item',
				'page-tab',
				'_wpnonce',
			);

			$original_title = '';
			if ( 'taxonomy' == $item->type ) {
				$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
				if ( is_wp_error( $original_title ) )
					$original_title = false;
			} elseif ( 'post_type' == $item->type ) {
				$original_object = get_post( $item->object_id );
				$original_title = $original_object->post_title;
			}

			$classes = array(
				'menu-item menu-item-depth-' . $depth,
				'menu-item-' . esc_attr( $item->object ),
				'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
			);

			$title = $item->title;

			if ( ! empty( $item->_invalid ) ) {
				$classes[] = 'menu-item-invalid';
				/* translators: %s: title of menu item which is invalid */
				$title = sprintf( __( '%s (Invalid)', 'logan-menu' ), $item->title );
			} elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
				$classes[] = 'pending';
				/* translators: %s: title of menu item in draft status */
				$title = sprintf( __('%s (Pending)', 'logan-menu'), $item->title );
			}

			$title = empty( $item->label ) ? $title : $item->label;

			$submenu_text = '';
			if ( 0 == $depth )
				$submenu_text = 'style="display: none;"';

			?>
			<li id="menu-item-<?php echo $item_id; ?>" class="<?php echo implode(' ', $classes ); ?>">
				<div class="menu-item-bar">
					<div class="menu-item-handle">
						<span class="item-title"><span class="menu-item-title"><?php echo esc_html( $title ); ?></span> <span class="is-submenu" <?php echo $submenu_text; ?>><?php _e( 'sub item' ); ?></span></span>
						<span class="item-controls">
							<span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
							<a class="item-edit" id="edit-<?php echo $item_id; ?>" title="<?php _e('Edit Menu Item'); ?>" href="<?php
								echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) );
							?>"><?php _e( 'Edit Menu Item' ); ?></a>
						</span>
					</div>
				</div>

				<div class="menu-item-settings" id="menu-item-settings-<?php echo $item_id; ?>">
                    <!---custom items-->
                	<div class="pix_custom-items">
                        <p class="field-checkbox description pix_megamenu-item">
                            <label for="pix-megamenu-item-<?php echo $item_id; ?>">
                                <input type="hidden" name="pix-megamenu-item[<?php echo $item_id; ?>]" value='0' />
                                <input type="checkbox" id="pix-megamenu-item-<?php echo $item_id; ?>" class="pix-megamenu-item" name="pix-megamenu-item[<?php echo $item_id; ?>]"<?php if (get_post_meta( $item_id, '_pix_megamenu_item', true)=='1') { echo 'checked="checked"'; } ?> value='1' />
                                <?php _e( 'Use it for your megamenu' ); ?>
                            </label>
                        </p>

                        <?php /*<p class="field-checkbox description">
                            <label for="pix-calltoaction-item-<?php echo $item_id; ?>">
                                <input type="hidden" name="pix-calltoaction-item[<?php echo $item_id; ?>]" value='0' />
                                <input type="checkbox" id="pix-calltoaction-item-<?php echo $item_id; ?>" class="pix-calltoaction-item" name="pix-calltoaction-item[<?php echo $item_id; ?>]"<?php if (get_post_meta( $item_id, '_pix_calltoaction_item', true)=='1') { echo 'checked="checked"'; } ?> value='1' />
                                <?php _e( 'Call to action button' ); ?>
                            </label>
                        </p>*/ ?>

                        <p class="field-checkbox description pix_bg-item pix_megamenu-check">
                            <label for="pix-bg-item-<?php echo $item_id; ?>">
                                <?php _e( 'Mega menu background image', 'logan-menu' ); ?><br />
						        <span class="pix_upload upload_image">
                                    <input type="text" id="pix-bg-item-<?php echo $item_id; ?>" class="pix-image-item" name="pix-bg-item[<?php echo $item_id; ?>]" value="<?php echo esc_html(get_post_meta( $item_id, '_pix_bg_item', true)); ?>">
                                    <input type="hidden" id="pix-bg-id-<?php echo $item_id; ?>" class="pix-image-id" name="pix-bg-id[<?php echo $item_id; ?>]" value="<?php echo esc_html(get_post_meta( $item_id, '_pix_bg_id', true)); ?>">
                                    <input type="hidden" id="pix-bg-size-<?php echo $item_id; ?>" class="pix-image-size" name="pix-bg-size[<?php echo $item_id; ?>]" value="<?php echo esc_html(get_post_meta( $item_id, '_pix_bg_size', true)); ?>">
						            <span class="img_preview slide_bg add-descriptive-thumb"></span>
						            <a class="pix_button add-descriptive-thumb" href="#"><?php _e('Insert','geode'); ?></a>
						            <a class="remove-descriptive-thumb" href="#"><i class="scicon-awesome-cancel"></i></a>
						        </span>

                            </label>
                        </p>

                        <p class="description pix_width100-item pix_megamenu-check">
                            <label for="pix-width100-item-<?php echo $item_id; ?>">
                                <input type="hidden" name="pix-width100-item[<?php echo $item_id; ?>]" value='0' />
                                <input type="checkbox" id="pix-width100-item-<?php echo $item_id; ?>" class="pix-width100-item" name="pix-width100-item[<?php echo $item_id; ?>]"<?php if (get_post_meta( $item_id, '_pix_width100_item', true)=='100%' && $megamenu == 0) { echo 'checked="checked"'; } ?> value='100%' />
                                <?php _e( 'Make it 100% wide', 'logan-menu' ); ?>
                            </label>
                        </p>


                        <p class="description pix_bgcss-item pix_megamenu-check">
                            <label for="pix-bgcss-item-<?php echo $item_id; ?>">
                                <?php _e( 'Mega menu custom CSS', 'logan-menu' ); ?><br />
								<textarea id="pix-bgcss-item-<?php echo $item_id; ?>" class="widefat edit-pix-bgcss-item-<?php echo $item_id; ?>" rows="3" cols="20" name="pix-bgcss-item[<?php echo $item_id; ?>]"><?php echo esc_html(get_post_meta( $item_id, '_pix_bgcss_item', true)); ?></textarea>
                                <small class="description"><em><?php _e( 'For instance: "background-size: cover; padding-right: 100px;"', 'logan-menu' ); ?></em></small>
                            </label>
                        </p>

                        <p class="field-checkbox description pix_icon-item">
                            <label for="pix-icon-item-<?php echo $item_id; ?>">
                                <?php _e( 'Descriptive icon', 'logan-menu' ); ?><br />
                                <input type="hidden" id="pix-icon-item-<?php echo $item_id; ?>" class="pix-icon-item" name="pix-icon-item[<?php echo $item_id; ?>]" value="<?php echo esc_html(get_post_meta( $item_id, '_pix_icon_item', true)); ?>">
			                    <span class="icon-preview alignleft"><i class="<?php echo esc_html(get_post_meta( $item_id, '_pix_icon_item', true)); ?>"></i></span><!-- .icon-preview -->
			                    <a class="pix_button icon-preview-edit alignleft" href="#"><i class="logan-icon-editorial-pencil"></i></a>
			                    <a class="pix_button icon-remove alignleft" href="#"></a>
                            </label>
                        </p>

                        <?php if( 'custom' == $item->type ) : ?>
                         <p class="field-checkbox description pix_column-item">
                            <label for="pix-column-item-<?php echo $item_id; ?>">
                                <?php _e( 'Use it to start a new column or a new row', 'logan-menu' ); ?><br />
                                <select id="pix-column-item-<?php echo $item_id; ?>" class="widefat pix-column-item" name="pix-column-item[<?php echo $item_id; ?>]">
                                    <option value="no" <?php if (get_post_meta( $item_id, '_pix_column_item', true)=='no') { echo 'selected="selected"'; } ?>><?php _e('No', 'logan-menu'); ?></option>
                                    <option value="column" <?php if (get_post_meta( $item_id, '_pix_column_item', true)=='column') { echo 'selected="selected"'; } ?>><?php _e('Start a new column', 'logan-menu'); ?></option>
                                    <option value="row" <?php if (get_post_meta( $item_id, '_pix_column_item', true)=='row') { echo 'selected="selected"'; } ?>><?php _e('Start a new row', 'logan-menu'); ?></option>
                                </select>
                            </label>
                        </p>

                         <p class="field-checkbox description pix_width-item">
                            <label for="pix-width-item-<?php echo $item_id; ?>">
                                <?php _e( 'Make it &hellip; wide', 'logan-menu' ); ?><br />
                                <select id="pix-width-item-<?php echo $item_id; ?>" class="widefat pix-width-item" name="pix-width-item[<?php echo $item_id; ?>]">
                                    <option value="1" <?php if (get_post_meta( $item_id, '_pix_width_item', true)=='1') { echo 'selected="selected"'; } ?>><?php _e('1 column', 'logan-menu'); ?></option>
                                    <option value="2" <?php if (get_post_meta( $item_id, '_pix_width_item', true)=='2') { echo 'selected="selected"'; } ?>><?php _e('2 columns', 'logan-menu'); ?></option>
                                    <option value="3" <?php if (get_post_meta( $item_id, '_pix_width_item', true)=='3') { echo 'selected="selected"'; } ?>><?php _e('3 columns', 'logan-menu'); ?></option>
                                    <option value="4" <?php if (get_post_meta( $item_id, '_pix_width_item', true)=='4') { echo 'selected="selected"'; } ?>><?php _e('4 columns', 'logan-menu'); ?></option>
                                    <option value="5" <?php if (get_post_meta( $item_id, '_pix_width_item', true)=='5') { echo 'selected="selected"'; } ?>><?php _e('5 columns', 'logan-menu'); ?></option>
                                    <option value="6" <?php if (get_post_meta( $item_id, '_pix_width_item', true)=='6') { echo 'selected="selected"'; } ?>><?php _e('6 columns', 'logan-menu'); ?></option>
                                </select>
                            </label>
                        </p>
                        <?php endif; ?>

                         <p class="field-checkbox description pix_title-item">
                            <label for="pix-title-item-<?php echo $item_id; ?>">
                                <input type="hidden" name="pix-title-item[<?php echo $item_id; ?>]" value='0' />
                                <input type="checkbox" id="pix-title-item-<?php echo $item_id; ?>" class="pix-title-item" name="pix-title-item[<?php echo $item_id; ?>]"<?php if (get_post_meta( $item_id, '_pix_title_item', true)=='pix_mega_title' && $megamenu == 0) { echo 'checked="checked"'; } ?> value='pix_mega_title' />
                                <?php _e( 'Use it as title', 'logan-menu' ); ?>
                            </label>
                        </p>

                         <p class="field-checkbox description pix_image-item">
                            <label for="pix-image-item-<?php echo $item_id; ?>">
                                <?php _e( 'Featured thumb', 'logan-menu' ); ?><br />
						        <span class="pix_upload upload_image">
                                    <input type="text" id="pix-image-item-<?php echo $item_id; ?>" class="pix-image-item" name="pix-image-item[<?php echo $item_id; ?>]" value="<?php echo esc_html(get_post_meta( $item_id, '_pix_image_item', true)); ?>">
                                    <input type="hidden" id="pix-image-id-<?php echo $item_id; ?>" class="pix-image-id" name="pix-image-id[<?php echo $item_id; ?>]" value="<?php echo esc_html(get_post_meta( $item_id, '_pix_image_id', true)); ?>">
                                    <input type="hidden" id="pix-image-size-<?php echo $item_id; ?>" class="pix-image-size" name="pix-image-size[<?php echo $item_id; ?>]" value="<?php echo esc_html(get_post_meta( $item_id, '_pix_image_size', true)); ?>">
						            <span class="img_preview slide_bg add-descriptive-thumb"></span>
						            <a class="pix_button add-descriptive-thumb" href="#"><?php _e('Insert','geode'); ?></a>
						            <a class="remove-descriptive-thumb" href="#"><i class="scicon-awesome-cancel"></i></a>
						        </span>

                            </label>
                        </p>

                         <p class="field-checkbox description pix_sidebar-item">
                            <label for="pix-sidebar-item-<?php echo $item_id; ?>">
								<?php _e( 'Select a sidebar', 'logan-menu' ); ?><br />
								<select id="pix-sidebar-item-<?php echo $item_id; ?>" class="pix-sidebar-item-<?php echo $item_id; ?>" name="pix-sidebar-item[<?php echo $item_id; ?>]">
								     <option value="" <?php selected(esc_html(get_post_meta( $item_id, '_pix_sidebar_item', true)), ''); ?>><?php _e( 'None', 'logan-menu' ); ?></option>
								<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) { ?>
								     <option value="<?php echo ucwords( $sidebar['id'] ); ?>" <?php selected(esc_html(get_post_meta( $item_id, '_pix_sidebar_item', true)), ucwords( $sidebar['id'] )); ?>><?php echo ucwords( $sidebar['name'] ); ?></option>
								<?php } ?>
								</select>
                            </label>
                        </p>

                    </div>
                    <div class="clear"></div>
                    <!--end custom items-->

					<?php if( 'custom' == $item->type ) : ?>
						<p class="field-url description description-wide pix_url-item">
							<label for="edit-menu-item-url-<?php echo $item_id; ?>">
								<?php _e( 'URL', 'logan-menu' ); ?><br />
								<input type="text" id="edit-menu-item-url-<?php echo $item_id; ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
							</label>
						</p>
					<?php endif; ?>
					<p class="description description-thin">
						<label for="edit-menu-item-title-<?php echo $item_id; ?>">
							<?php _e( 'Navigation Label', 'logan-menu' ); ?><br />
							<input type="text" id="edit-menu-item-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
						</label>
					</p>

					<p class="description description-thin">
						<label for="edit-menu-item-attr-title-<?php echo $item_id; ?>">
							<?php _e( 'Title Attribute', 'logan-menu' ); ?><br />
							<input type="text" id="edit-menu-item-attr-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
						</label>
					</p>

                    <p class="field-checkbox description-thin">
                        <label for="pix-labeltext-item-<?php echo $item_id; ?>">
                            <input type="hidden" name="pix-labeltext-item[<?php echo $item_id; ?>]" value='0' />
                            <input type="checkbox" id="pix-labeltext-item-<?php echo $item_id; ?>" class="pix-labeltext-item" name="pix-labeltext-item[<?php echo $item_id; ?>]"<?php if (get_post_meta( $item_id, '_pix_labeltext_item', true)=='1') { echo 'checked="checked"'; } ?> value='1' />
                            <?php _e( 'Hide label text' ); ?>
                        </label>
                    </p>

                    <p class="field-checkbox description-thin">
                        <label for="pix-labellink-item-<?php echo $item_id; ?>">
                            <input type="hidden" name="pix-labellink-item[<?php echo $item_id; ?>]" value='0' />
                            <input type="checkbox" id="pix-labellink-item-<?php echo $item_id; ?>" class="pix-labellink-item" name="pix-labellink-item[<?php echo $item_id; ?>]"<?php if (get_post_meta( $item_id, '_pix_labellink_item', true)=='1') { echo 'checked="checked"'; } ?> value='1' />
                            <?php _e( 'Not a link' ); ?>
                        </label>
                    </p>

					<?php if( 'custom' == $item->type ) : ?>
					<p class="field-link-target description description-thin">
						<label for="edit-menu-item-target-<?php echo $item_id; ?>">
							<?php _e( 'Link Target', 'logan-menu' ); ?><br />
							<select id="edit-menu-item-target-<?php echo $item_id; ?>" class="widefat edit-menu-item-target" name="menu-item-target[<?php echo $item_id; ?>]">
								<option value="" <?php selected( $item->target, ''); ?>><?php _e('Same window or tab', 'logan-menu'); ?></option>
								<option value="_blank" <?php selected( $item->target, '_blank'); ?>><?php _e('New window or tab', 'logan-menu'); ?></option>
							</select>
						</label>
					</p>
					<?php endif; ?>

					<div class="clear"></div>

					<p class="field-tag-label description description-thin">
						<label for="pix-taglabel-item-<?php echo $item_id; ?>">
							<?php _e( 'Tag label (optional)', 'logan-menu' ); ?><br />
							<input type="text" id="pix-taglabel-item-<?php echo $item_id; ?>" class="widefat edit-menu-item-tag" name="pix-taglabel-item[<?php echo $item_id; ?>]" value="<?php echo esc_attr( get_post_meta( $item_id, '_pix_taglabel_item', true) ); ?>" />
						</label>
					</p>

					<div class="clear"></div>

					<p class="field-tag-label description description-thin">
						<label for="pix-tagcolor-item-<?php echo $item_id; ?>">
							<?php _e( 'Tag label text color', 'logan-menu' ); ?><br />
							<input type="text" id="pix-tagcolor-item-<?php echo $item_id; ?>" class="widefat edit-menu-item-tag color-picker" name="pix-tagcolor-item[<?php echo $item_id; ?>]" data-default-color="false" value="<?php echo esc_attr( get_post_meta( $item_id, '_pix_tagcolor_item', true) ); ?>" />
							<small class="description"><em><?php _e( 'Leave blank to use default theme colors', 'logan-menu' ); ?></em></small>
						</label>
					</p>

					<div class="clear"></div>

					<p class="field-tag-label description description-thin">
						<label for="pix-tagbg-item-<?php echo $item_id; ?>">
							<?php _e( 'Tag label bg color', 'logan-menu' ); ?><br />
							<input type="text" id="pix-tagbg-item-<?php echo $item_id; ?>" class="widefat edit-menu-item-tag color-picker" name="pix-tagbg-item[<?php echo $item_id; ?>]" data-default-color="false" value="<?php echo esc_attr( get_post_meta( $item_id, '_pix_tagbg_item', true) ); ?>" />
							<small class="description"><em><?php _e( 'Leave blank to use default theme colors', 'logan-menu' ); ?></em></small>
						</label>
					</p>

					<div class="clear"></div>

					<p class="field-css-classes description description-thin">
						<label for="edit-menu-item-classes-<?php echo $item_id; ?>">
							<?php _e( 'CSS Classes (optional)', 'logan-menu' ); ?><br />
							<input type="text" id="edit-menu-item-classes-<?php echo $item_id; ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo $item_id; ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
						</label>
					</p>
					<p class="field-xfn description description-thin">
						<label for="edit-menu-item-xfn-<?php echo $item_id; ?>">
							<?php _e( 'Link Relationship (XFN)', 'logan-menu' ); ?><br />
							<input type="text" id="edit-menu-item-xfn-<?php echo $item_id; ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
						</label>
					</p>
					<p class="field-description description description-wide">
						<label for="edit-menu-item-description-<?php echo $item_id; ?>">
							<?php _e( 'Description', 'logan-menu' ); ?><br />
							<textarea id="edit-menu-item-description-<?php echo $item_id; ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo $item_id; ?>]"><?php echo esc_html( $item->description ); // textarea_escaped ?></textarea>
							<span class="description"><?php _e('The description will be displayed in the menu if the current theme supports it.', 'logan-menu'); ?></span>
						</label>
					</p>

					<p class="field-move hide-if-no-js description description-wide">
						<label>
							<span><?php _e( 'Move', 'logan' ); ?></span>
							<a href="#" class="menus-move menus-move-up" data-dir="up"><?php _e( 'Up one', 'logan' ); ?></a>
							<a href="#" class="menus-move menus-move-down" data-dir="down"><?php _e( 'Down one', 'logan' ); ?></a>
							<a href="#" class="menus-move menus-move-left" data-dir="left"></a>
							<a href="#" class="menus-move menus-move-right" data-dir="right"></a>
							<a href="#" class="menus-move menus-move-top" data-dir="top"><?php _e( 'To the top', 'logan' ); ?></a>
						</label>
					</p>

					<div class="menu-item-actions description-wide submitbox">
						<?php if( 'custom' != $item->type && $original_title !== false ) : ?>
							<p class="link-to-original">
								<?php printf( __('Original: %s', 'logan-menu'), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
							</p>
						<?php endif; ?>
						<a class="item-delete submitdelete deletion" id="delete-<?php echo $item_id; ?>" href="<?php
						echo wp_nonce_url(
							add_query_arg(
								array(
									'action' => 'delete-menu-item',
									'menu-item' => $item_id,
								),
								remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
							),
							'delete-menu_item_' . $item_id
						); ?>"><?php _e('Remove', 'logan-menu'); ?></a> <span class="meta-sep"> | </span> <a class="item-cancel submitcancel" id="cancel-<?php echo $item_id; ?>" href="<?php	echo esc_url( add_query_arg( array('edit-menu-item' => $item_id, 'cancel' => time()), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) ) ) );
							?>#menu-item-settings-<?php echo $item_id; ?>"><?php _e('Cancel', 'logan-menu'); ?></a>
					</div>

					<input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo $item_id; ?>]" value="<?php echo $item_id; ?>" />
					<input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
					<input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
					<input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
					<input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
					<input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
				</div><!-- .menu-item-settings-->
				<ul class="menu-item-transport"></ul>
			<?php
			$output .= ob_get_clean();
		}
	}


if ( !class_exists('PixMenu_Walker') ) :
class PixMenu_Walker extends Walker_Nav_Menu {
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query, $megamenu, $column_width;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$item_id = esc_attr( $item->ID );
		$a_class = get_post_meta( $item_id, '_pix_title_item', true);
		if ( get_post_meta( $item_id, '_pix_icon_item', true )!='' )
			$a_class .= ' with-icon';

		if ($depth==0) {

			if ( get_post_meta( $item_id, '_pix_megamenu_item', true ) == '1' ) {

				$classes[] = 'menu-item-' . $item->ID;

				$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
				$class_names .= ' pix_megamenu';

				if ( get_post_meta( $item_id, '_pix_width100_item', true ) == '100%' )
					$class_names .= ' wide_width';

				if ( get_post_meta( $item_id, '_pix_labeltext_item', true ) == true ) {
					$item->title = '&nbsp;'; $class_names .= ' no-text';
				}
				if ( get_post_meta( $item_id, '_pix_icon_item', true )!='' )
					$class_names .= ' with-icon';
				$class_names = ' class="' . esc_attr( $class_names ) . '"';

				$output .= $indent . '<div role="listitem"' . $id . $value . $class_names .'>';

				$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
				$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
				$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

				$item_url = esc_attr($item->url);
				$site_url = site_url();
				$home_url = home_url( '/' );
				$network_url = network_site_url();
				$item_url = strpos($item_url,'[site_url]') !== false ? preg_replace( '/(.*?)\[site_url\](.*?)/', $site_url . '$2', $item_url ) : $item_url;
				$item_url = strpos($item_url,'[home_url]') !== false ? preg_replace( '/(.*?)\[home_url\](.*?)/', $home_url . '$2', $item_url ) : $item_url;
				$item_url = strpos($item_url,'[network_url]') !== false ? preg_replace( '/(.*?)\[network_url\](.*?)/', $network_url . '$2', $item_url ) : $item_url;
				$item_url = preg_replace( '/([^:])(\/+)/', '$1/', $item_url );
				$attributes .= ! empty( $item->url )        ? ' href="'   . esc_url( $item_url ) .'"' : '';

				$item_output = $args->before;
				if ( get_post_meta( $item_id, '_pix_labellink_item', true ) != true )
					$item_output .= '<span></span><a'. $attributes .'><span>';
				else
					$item_output .= '<span class="pix-menu-no-link">';
				$item_icon = get_post_meta( $item_id, '_pix_icon_item', true )!='' ? ' class="'.get_post_meta( $item_id, '_pix_icon_item', true ).' pix_icon_menu"' : '';
				$item_output .= $item_icon!='' ? '<i'.$item_icon.'></i>' : '';
				$item_output .= $args->link_before . strip_tags(apply_filters( 'the_title', $item->title, $item->ID )) . $args->link_after;
				/*if ( $item->description != '' ) {
					$item_output .= '<span class="pix_menu_desc">' . $item->description . '</span>';
				}*/
				//$item_output .= '<span>' . get_post_meta( $item_id, '_pix_column_item', true) . '</span>';
				if ( get_post_meta( $item_id, '_pix_taglabel_item', true ) != '' ) {
					$tag_style_color = pixmenu_sanitize_hex_color( get_post_meta( $item_id, '_pix_tagcolor_item', true ) );
					$tag_style_color = $tag_style_color != '' ? 'color:'.$tag_style_color.';' : '';
					$tag_style_bg = pixmenu_sanitize_hex_color( get_post_meta( $item_id, '_pix_tagbg_item', true ) );
					$tag_style_bg = $tag_style_bg != '' ? 'background-color:'.$tag_style_bg.';' : '';
					$tag_style = $tag_style_color != '' || $tag_style_bg != '' ? 'style="' . $tag_style_color . $tag_style_bg .'";' : '';

					$item_output .= '<span class="pix_menu_tag" ' . $tag_style .'>' . esc_attr( get_post_meta( $item_id, '_pix_taglabel_item', true ) ) . '</span>';
				}
				if ( get_post_meta( $item_id, '_pix_labellink_item', true ) != true )
					$item_output .= '</span></a>';
				else
					$item_output .= '</span>';
				$item_output .= $args->after;

				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

				if ( get_post_meta( $item_id, '_pix_bgcss_item', true ) != '' ) {
					$wrap_bg_css = esc_attr( get_post_meta( $item_id, '_pix_bgcss_item', true ) );
				} else {
					$wrap_bg_css = '';
				}

				if ( get_post_meta( $item_id, '_pix_bg_item', true ) != '' ) {
					$wrap_bg = ' style="background-image: url(' . esc_url( get_post_meta( $item_id, '_pix_bg_item', true ) ) . '); ' . $wrap_bg_css . '"';
				} else {
					$wrap_bg = '';
				}

				$output .= $indent . '<div class="pixmenu-wrap-level"' . $wrap_bg . '><div class="pixmenu-wrap-row">';
				$megamenu = 1;

			} else {

				$classes[] = 'menu-item-' . $item->ID;

				$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
				/*if ( get_post_meta( $item_id, '_pix_calltoaction_item', true ) == 1 )
					$class_names .= ' pix-call-to-action';*/
				if ( get_post_meta( $item_id, '_pix_labeltext_item', true ) == true ) {
					$item->title = ''; $class_names .= ' no-text';
				}
				if ( get_post_meta( $item_id, '_pix_icon_item', true )!='' )
					$class_names .= ' with-icon';
				$class_names = ' class="' . esc_attr( $class_names ) . '"';


				$output .= $indent . '<div role="listitem"' . $id . $value . $class_names .'>';

				$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
				$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
				$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

				$item_url = esc_attr($item->url);
				$site_url = site_url();
				$home_url = home_url( '/' );
				$network_url = network_site_url();
				$item_url = strpos($item_url,'[site_url]') !== false ? preg_replace( '/(.*?)\[site_url\](.*?)/', $site_url . '$2', $item_url ) : $item_url;
				$item_url = strpos($item_url,'[home_url]') !== false ? preg_replace( '/(.*?)\[home_url\](.*?)/', $home_url . '$2', $item_url ) : $item_url;
				$item_url = strpos($item_url,'[network_url]') !== false ? preg_replace( '/(.*?)\[network_url\](.*?)/', $network_url . '$2', $item_url ) : $item_url;
				$item_url = preg_replace( '/([^:])(\/+)/', '$1/', $item_url );
				$attributes .= ! empty( $item->url )        ? ' href="'   . esc_url( $item_url ) .'"' : '';

				$attributes .= ! empty( $a_class )     	   ? ' class="'   . $a_class .'"' : '';

				$item_output = isset($args->before) ? $args->before : '';
				if ( get_post_meta( $item_id, '_pix_labellink_item', true ) != true )
					$item_output .= '<span></span><a'. $attributes .'><span>';
				else
					$item_output .= '<span class="pix-menu-no-link">';
				$item_icon = get_post_meta( $item_id, '_pix_icon_item', true )!='' ? ' class="'.get_post_meta( $item_id, '_pix_icon_item', true ).' pix_icon_menu"' : '';
				$item_output .= $item_icon!='' ? '<i'.$item_icon.'></i>' : '';
				$item_output .= isset($args->link_before) ? $args->link_before : '';
				$item_output .= strip_tags(apply_filters( 'the_title', $item->title, $item->ID ));
				$item_output .= isset($args->link_after) ? $args->link_after : '';
				/*if ( $item->description != '' ) {
					$item_output .= '<span class="pix_menu_desc">' . $item->description . '</span>';
				}*/
				//$item_output .= '<span>' . get_post_meta( $item_id, '_pix_column_item', true) . '</span>';
				if ( get_post_meta( $item_id, '_pix_taglabel_item', true ) != '' ) {
					$tag_style_color = pixmenu_sanitize_hex_color( get_post_meta( $item_id, '_pix_tagcolor_item', true ) );
					$tag_style_color = $tag_style_color != '' ? 'color:'.$tag_style_color.';' : '';
					$tag_style_bg = pixmenu_sanitize_hex_color( get_post_meta( $item_id, '_pix_tagbg_item', true ) );
					$tag_style_bg = $tag_style_bg != '' ? 'background-color:'.$tag_style_bg.';' : '';
					$tag_style = $tag_style_color != '' || $tag_style_bg != '' ? 'style="' . $tag_style_color . $tag_style_bg .'";' : '';

					$item_output .= '<span class="pix_menu_tag" ' . $tag_style .'>' . esc_attr( get_post_meta( $item_id, '_pix_taglabel_item', true ) ) . '</span>';
				}
				if ( get_post_meta( $item_id, '_pix_labellink_item', true ) != true )
					$item_output .= '</span></a>';
				else
					$item_output .= '</span>';
				$item_output .= isset($args->after) ? $args->after : '';

				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
				$megamenu = 0;

			}

		} elseif (get_post_meta( $item_id, '_pix_column_item', true) == 'column' && $depth==1) {

			$column_width = get_post_meta( $item_id, '_pix_width_item', true );

			$output .= $indent . '<div role="list" class="alignleft" data-col="'. $column_width .'">';

		} elseif (get_post_meta( $item_id, '_pix_column_item', true) == 'row' && $depth==1) {

			$column_width = '';

			$output .= $indent . '</div><div class="mega_clear"><div></div></div><div class="pixmenu-wrap-row">';

		} else {

			if( $depth>1 && $megamenu == 1 ) {

				$output .= $indent . '<div role="listitem"' . $id . $value . ' class="pix_megamenu_'. $column_width .'_col" data-col="'. $column_width .'">';

			} else {

				$output .= $indent . '<div role="listitem"' . $id . $value . $class_names .'>';

			}

			$item_output = $args->before;
			$item_output .= $args->after;

			if ( get_post_meta( $item_id, '_pix_labeltext_item', true ) == true ) { $item->title = ''; $a_class .= ' no-text'; }
			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

			$item_url = esc_attr($item->url);
			$site_url = site_url();
			$home_url = home_url( '/' );
			$network_url = network_site_url();
			$item_url = strpos($item_url,'[site_url]') !== false ? preg_replace( '/(.*?)\[site_url\](.*?)/', $site_url . '$2', $item_url ) : $item_url;
			$item_url = strpos($item_url,'[home_url]') !== false ? preg_replace( '/(.*?)\[home_url\](.*?)/', $home_url . '$2', $item_url ) : $item_url;
			$item_url = strpos($item_url,'[network_url]') !== false ? preg_replace( '/(.*?)\[network_url\](.*?)/', $network_url . '$2', $item_url ) : $item_url;
			$item_url = preg_replace( '/([^:])(\/+)/', '$1/', $item_url );
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_url( $item_url ) .'"' : '';
			$attributes .= ! empty( $a_class )     	   ? ' class="'   . esc_attr( $a_class ) .'"' : '';

			if(get_post_meta( $item_id, '_pix_sidebar_item', true) != ''){

				ob_start();
				global $cell_weight, $content_width, $column_width;
				$fact = $content_width / 6;
				$cell_weight = $column_width * $fact;
				dynamic_sidebar(strtolower(get_post_meta( $item_id, '_pix_sidebar_item', true)));
				$sidebar = ob_get_contents();  // get the contents of the buffer and turn it off.
				ob_get_clean();
				$sidebar = preg_replace('/<aside (.*?) wpb_animate_when_almost_visible wpb_bottom-to-top/', '<aside $1', $sidebar);
				if ($sidebar) {
					$item_output = '<div class="menu-sidebar">';
					if ( get_post_meta( $item_id, '_pix_labellink_item', true ) != true )
						$item_output .= '<span class="pix_mega_title">' . strip_tags(apply_filters( 'the_title', $item->title, $item->ID )) . '</span>';
				    $item_output .= $sidebar;
					$item_output .= '</div>';
				}

			} else {

				$item_output = $args->before;
				if ( get_post_meta( $item_id, '_pix_labellink_item', true ) != true )
					$item_output .= '<span></span><a'. $attributes .'>';
				else
					$item_output .= '<span class="pix-menu-no-link '.$a_class.'">';

				if(get_post_meta( $item_id, '_pix_image_item', true) != ''){
					if ( function_exists('pixedelic_attachment_meta_by_url') ) {
						$img_meta = pixedelic_attachment_meta_by_url( esc_url(get_post_meta( $item_id, '_pix_image_item', true) ) );
						$img_attrs = ' width="' . intval($img_meta['width']) . '" height="' . intval($img_meta['height']) . '" ';
					} else {
						$img_attrs = '';
					}
					$item_output .= '<span class="pix_desc_image cf"><img src="'.esc_url(get_post_meta( $item_id, '_pix_image_item', true)).'" alt="" ' . $img_attrs . '></span>';
				}

				$item_icon = get_post_meta( $item_id, '_pix_icon_item', true )!='' ? ' class="'.get_post_meta( $item_id, '_pix_icon_item', true ).' pix_icon_menu"' : '';
				$item_output .= $item_icon!='' ? '<i'.$item_icon.'></i>' : '';
				$item_output .= $args->link_before . strip_tags(apply_filters( 'the_title', $item->title, $item->ID )) . $args->link_after;

				if ( $item->description != '' ) {
					if ( get_post_meta( $item_id, '_pix_labeltext_item', true ) != true)
						$item_output .= '<br>';
					$item_output .= '<small>' . $item->description . '</small>';
				}

				if ( get_post_meta( $item_id, '_pix_taglabel_item', true ) != '' ) {
					$tag_style_color = pixmenu_sanitize_hex_color( get_post_meta( $item_id, '_pix_tagcolor_item', true ) );
					$tag_style_color = $tag_style_color != '' ? 'color:'.$tag_style_color.';' : '';
					$tag_style_bg = pixmenu_sanitize_hex_color( get_post_meta( $item_id, '_pix_tagbg_item', true ) );
					$tag_style_bg = $tag_style_bg != '' ? 'background-color:'.$tag_style_bg.';' : '';
					$tag_style = $tag_style_color != '' || $tag_style_bg != '' ? 'style="' . $tag_style_color . $tag_style_bg .'";' : '';

					$item_output .= '<span class="pix_menu_tag" ' . $tag_style .'>' . esc_attr( get_post_meta( $item_id, '_pix_taglabel_item', true ) ) . '</span>';
				}

				if ( get_post_meta( $item_id, '_pix_labellink_item', true ) != true )
					$item_output .= '</a>';
				else
					$item_output .= '</span>';
				$item_output .= $args->after;

			}

			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}


	}


	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		global $wp_query, $megamenu;
		$item_id = esc_attr( $item->ID );

		if (get_post_meta( $item_id, '_pix_megamenu_item', true) == '1' && $depth==0) {

			$output .= "</div></div></div>";
			$megamenu = 0;

		} elseif (get_post_meta( $item_id, '_pix_column_item', true) == 'column' && $depth==1) {

			$output .= "</div>";

		} elseif (get_post_meta( $item_id, '_pix_column_item', true) == 'row' && $depth==1) {

			$output .= "";

		}  else {

			$output .= "</div>";

		}
	}


	function start_lvl( &$output, $depth = 0, $args = array() ) {
		global $megamenu;
		if(($megamenu==1 && $depth==0) || ($megamenu==1 && $depth==1)){
			$output .= '';
		} else {
			$output .= "<div role='list' class='children depth-$depth'>";
		}
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		global $megamenu;
		if(($megamenu==1 && $depth==0) || ($megamenu==1 && $depth==1)){
			$output .= '';
		} else {
			$output .= "</div>";
		}
	}
}
endif;

if ( !function_exists( 'pixmenu_sanitize_hex_color' ) ) {
	function pixmenu_sanitize_hex_color( $color ) {
		if ( '' === $color )
			return '';

		// 3 or 6 hex digits, or the empty string.
		if ( preg_match('|^#([A-Fa-f0-9]{3}){1,2}$|', $color ) )
			return $color;

		return null;
	}
}