<?php 
/*
Plugin Name: Logan Menu
Plugin URI: http://www.pixedelic.com/themes/geode
Description: An advanced menu builder
Version: 0.0.2
Author: Manuel Masia | Pixedelic.com
Author URI: http://www.pixedelic.com
License: GPL2
*/

define( 'PIXMENU_PATH', plugin_dir_path( __FILE__ ) );
define( 'PIXMENU_URL', plugin_dir_url( __FILE__ ) );
define( 'PIXMENU_NAME', plugin_basename( __FILE__ ) );

require_once( PIXMENU_PATH . 'lib/functions.php' );

register_activation_hook( __FILE__, array( 'PixMenu', 'activate' ) );
register_uninstall_hook( __FILE__, array( 'PixMenu', 'uninstall' ) );

PixMenu::get_instance();