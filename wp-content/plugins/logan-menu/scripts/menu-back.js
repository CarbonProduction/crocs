/******************************************************
*
*	MegaMenu
*
******************************************************/
function update_megamenu(){
	jQuery('.menu-item-depth-0 .pix-megamenu-item').each(function(){
		var c = jQuery(this),
			li = c.closest('li'),
			p = jQuery('> div .item-type',li),
			megaEq = li.index('.menu-item-depth-0'),
			megaNext = jQuery('.menu-item-depth-0').eq(megaEq+1),
			startEq = li.index('.menu-item'),
			endEq = megaNext.length ? megaNext.index('.menu-item') : jQuery('.menu-item').length;
		if(!jQuery('> div .custom-type',li).length){
			p.after('<span class="custom-type" />');
		}
		var te = jQuery('> div .custom-type',li),
			$relSet = jQuery('.pix_megamenu-check',li);

		if(c.is(':checked')){
			te.html('<span class="pixmegamenu-label">PixMegaMenu</span>').show();
			$relSet.show();
			//p.hide();
			li.addClass('pix-megamenu-parent');
			li.nextUntil('.menu-item-depth-0').addClass('in-a-megamenu');
		} else {
			//te.hide();
			//p.show();
			$relSet.hide();
			li.removeClass('pix-megamenu-parent');
			li.nextUntil('.menu-item-depth-0').removeClass('in-a-megamenu');
		}
		var update_children = function() {
			jQuery('.in-a-megamenu').each(function(){
				var subs = jQuery(this).attr('class').indexOf('menu-item-depth-'),
					depth = parseFloat(jQuery(this).attr('class').substring(subs+16,subs+17));
				if ( depth > 2 ) {
					jQuery(this).addClass('menu-item-alert');
					if ( !jQuery('#pix_builder_cant').length ) {
						jQuery('body').append('<div id="pix_builder_cant" /></div>');
					}
					jQuery('#pix_builder_cant').html('<p>PixMegaMenu <strong>can\'t</strong> have more than two levels, please check and fix</p>')
						.dialog({
							height: 'auto',
							width: 300,
							modal: true,
							dialogClass: 'wp-dialog pix-modal',
							zIndex: 50,
							title: 'Attention',
							close: function(){
								jQuery('#pix_builder_cant').remove();
							}
						});
				} else {
					jQuery(this).removeClass('menu-item-alert');
				}
			});
		};
		c.bind('click',function(){
			if(c.is(':checked')){
				te.html('<span class="pixmegamenu-label">PixMegaMenu</span>').show();
				$relSet.show();
				//p.hide();
				li.addClass('pix-megamenu-parent');
				li.nextUntil('.menu-item-depth-0').addClass('in-a-megamenu');
			} else {
				//te.hide();
				//p.show();
				$relSet.hide();
				li.removeClass('pix-megamenu-parent');
				li.nextUntil('.menu-item-depth-0').removeClass('in-a-megamenu');
			}
			update_children();
		});
		update_children();
	});
	jQuery('.menu-item-depth-1 .pix-column-item').each(function(){
		var c = jQuery(this),
			v = jQuery('option:selected',c).val(),
			li = c.closest('li'),
			p = jQuery('> div .item-type',li);
		if(!jQuery('> div .custom-type',li).length){
			p.after('<span class="custom-type" />');
		}
		var te = jQuery('> div .custom-type',li);
		if(v == 'column'){
			c.parents('li').eq(0).addClass('menu-item-column');
			te.html('<span class="pixmegamenu-column">'+pixmenu_column+'</span>');
			//p.hide();
			jQuery('.pix_url-item, .field-link-target, field-css-classes, .field-xfn, .field-description, .pix_image-item, .pix_sidebar-item, .pix_icon-item',li).hide();
			jQuery('.pix_width-item',li).show();
		} else if(v == 'row'){
			c.parents('li').eq(0).addClass('menu-item-row');
			te.html('<span class="pixmegamenu-row">'+pixmenu_row+'</span>');
			//p.hide();
			jQuery('.pix_url-item, .field-link-target, field-css-classes, .field-xfn, .field-description, .pix_image-item, .pix_sidebar-item, .pix_icon-item',li).hide();
			jQuery('.pix_width-item',li).hide();
		} else {
			//te.hide();
			//p.show();
			jQuery('.pix_url-item, .field-link-target, field-css-classes, .field-xfn, .field-description, .pix_image-item, .pix_sidebar-item, .pix_icon-item',li).show();
			jQuery('.pix_width-item',li).hide();
		}
		c.change(function(){
			v = jQuery('option:selected',c).val();
			if(v == 'column'){
				c.parents('li').eq(0).removeClass('menu-item-row').addClass('menu-item-column');
				te.html('<span class="pixmegamenu-column">'+pixmenu_column+'</span>');
				//p.hide();
				jQuery('.pix_url-item, .field-link-target, field-css-classes, .field-xfn, .field-description, .pix_image-item, .pix_sidebar-item, .pix_icon-item',li).slideUp();
				jQuery('.pix_width-item',li).slideDown();
			} else if(v == 'row'){
				c.parents('li').eq(0).removeClass('menu-item-column').addClass('menu-item-row');
				te.html('<span class="pixmegamenu-row">'+pixmenu_row+'</span>');
				//p.hide();
				jQuery('.pix_url-item, .field-link-target, field-css-classes, .field-xfn, .field-description, .pix_image-item, .pix_sidebar-item, .pix_icon-item',li).slideUp();
				jQuery('.pix_width-item',li).slideUp();
			} else {
				c.parents('li').eq(0).removeClass('menu-item-row').removeClass('menu-item-column');
				//te.hide();
				//p.show();
				jQuery('.pix_url-item, .field-link-target, field-css-classes, .field-xfn, .field-description, .pix_image-item, .pix_sidebar-item, .pix_icon-item',li).slideDown();
				jQuery('.pix_width-item',li).slideUp();
			}
		});
	});
}

/********************************
*
*   Get thumbnail from the server
*
********************************/
function getThumbnailUrl($id, $field, $altbg){
	var data = {
		action: 'pixmenu_get_thumb',
		security: pixmenu_nonce,
		content: $id
	};
	jQuery.post(ajaxurl, data)
		.success(function(html){
			$field.css({backgroundImage: $altbg + 'url('+html+')'}).addClass('filled');
		});
}

/********************************
*
*   Upload media
*
********************************/
function uploadSlidesForSlideshow(){

	jQuery('.pix_upload').each(function(){
		var t = jQuery(this),
			pix_media_frame,
			formlabel = 0,
			id = jQuery('input.pix-image-id',this).val(),
			text = jQuery('input.pix-image-item',this).val();
		if ( id!=='' && typeof id != 'undefined' && text !== '' ) {
			getThumbnailUrl(id, jQuery('.img_preview',this), '');
		}

		t.off('click','.add-descriptive-thumb');
		t.on('click','.add-descriptive-thumb', function( e ){
			e.preventDefault();
			var button = jQuery(this),
				wrap = button.parents('.pix_upload').eq(0);

			if ( pix_media_frame ) {
				pix_media_frame.open();
				return;
			}

			pix_media_frame = wp.media.frames.pix_media_frame = wp.media({

				className: 'media-frame pix-media-frame',
				frame: 'post',
				multiple: false,
				library: {
					type: 'image'
				}
			});

			pix_media_frame.on('insert', function(){
				var media_attachments = pix_media_frame.state().get('selection').toJSON(),
					thumbSize = jQuery('.attachment-display-settings select.size option:selected').val(),
					thumbUrl;

				jQuery.each(media_attachments, function(index, el) {
					//console.log(JSON.stringify(media_attachments));
					var url = this.url,
						id = this.id,
						size = typeof thumbSize!=='undefined' ? thumbSize : '';

					if ( size !== '' ) {
						size = this.sizes[size];
						url = size.url;
					}

					if ( typeof this.sizes != 'undefined' && typeof this.sizes.thumbnail != 'undefined' ) {
						previewUrl = this.sizes.thumbnail.url;
					} else {
						previewUrl = url;
					}
					wrap
						.find('input.pix-image-item').val(url).end()
						.find('input.pix-image-id').val(id).end()
						.find('input.pix-image-size').val(size);
					getThumbnailUrl(id, wrap.find('.img_preview'), '');
				});

			});

			pix_media_frame.open();
			jQuery('.media-menu a').eq(1).hide();
			jQuery('.media-toolbar-secondary').hide();
		});

		t.off('click','.remove-descriptive-thumb');
		t.on('click','.remove-descriptive-thumb', function( e ){
			e.preventDefault();
			var button = jQuery(this),
				wrap = button.parents('.pix_upload').eq(0);
			wrap
				.find('input.pix-image-item').val('').end()
				.find('input.pix-image-id').val('').end()
				.find('input.pix-image-size').val('').end()
				.find('.img_preview').css({background:'none'}).removeClass('filled');
		});
	});

}

var filterIcon = function($){
	$ = jQuery;
	$(document).off('keyup', '#pix-icon-finder input[type="text"]');
	$(document).on('keyup', '#pix-icon-finder input[type="text"]', function(){
		var $input = $(this),
			$iconList = $('#pix-icon-render'),
			val,
			setInput;

		var showFilteredIcons = function(){
			val = $input.val();

			if ( val === '' ) {
				$('> div', $iconList).show();
			} else {
				$('> div', $iconList).not('[data-keys*="'+val+'"]').hide();
				$('> div[data-keys*="'+val+'"]', $iconList).show();
			}
		};
		clearTimeout(setInput);
		setInput = setTimeout(showFilteredIcons, 50);
	});
};

var selectIcon = function($){
	$ = jQuery;
	$('#list-icon .the-list > div').off('click')
		.on('click', function(){
		var icon;
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
		} else {
			icon = $(this).attr('class');
			$('#list-icon .the-list > div.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	});
};

var loadIconList = function(){
	$ = jQuery;
	$(document).off('change', '#pix-icon-finder select');
	$(document).on('change', '#pix-icon-finder select', function(){
		//ajaxLoaders(true);
		var t = $(this),
			url = $('option:selected', t).data('url');

		var ajaxReqIcon = function(){
			$.ajax({
				url: url,
				type: "POST",
				cache: false,
				success: function(loadeddata) {
					var html = $("<div/>").append(loadeddata.replace(/<script(.|\s)*?\/script>/g, "")).html();
					$('#pix-icon-render')
						.html(html)
						.animate({opacity:1},250);
					selectIcon();
					filterIcon();
				}
			});
		};

		$('#pix-icon-render').animate({opacity:0},250, function(){
			ajaxReqIcon();
		});
	});
};


/********************************
*
*   Font icons
*
********************************/
function menuFontIcons(){
	jQuery(document).off('click','.icon-preview, .icon-preview-edit');
	jQuery(document).on('click','.icon-preview, .icon-preview-edit',function(e){
		e.preventDefault();

		$item = jQuery(this).parents('li.menu-item').eq(0);

		var div = jQuery('#list-icon'),
			triggerChange;

		var selectIcon = function(){
			jQuery('#list-icon .the-list > div').off('click')
				.on('click', function(){
				var icon;
				if ( jQuery(this).hasClass('selected') ) {
					jQuery(this).removeClass('selected');
				} else {
					icon = jQuery(this).attr('class');
					jQuery('#list-icon .the-list > div.selected').removeClass('selected');
					jQuery(this).addClass('selected');
				}
			});
		};

		div.dialog({
			resizable: false,
			draggable: false,
			modal: true,
			dialogClass: 'pix-modal font-modal wp-dialog',
			width: 'auto',
			position: { my: "center", at: "center", of: window },
			title: "Select an icon",
			buttons: {
				"Select": function() {
					var icon = jQuery('#list-icon .the-list > div.selected').data('iconname');
					if ( typeof icon != 'undefined' && icon !== '' ) {
						jQuery('input.pix-icon-item', $item).val(icon);
						jQuery('.icon-preview i', $item).attr('class', icon);
					}
					jQuery('#list-icon .the-list > div.selected').removeClass('selected');
					jQuery( this ).dialog( "close" );
				}
			},
			open: function( event, ui ) {
				loadIconList();
				if ( $('#pix-icon-render').html() === '' )
					$('#pix-icon-finder select').trigger('change');
			}
		});

	});

	jQuery(document).off('click','.icon-remove');
	jQuery(document).on('click','.icon-remove',function(e){
		e.preventDefault();
		var dial = jQuery(this).parents('li.menu-item').eq(0);
		dial.find('.icon-preview i').attr('class','');
		dial.find('.pix-icon-item').val('');
	});
}

function pixColorPicker(){
	jQuery('.color-picker').wpColorPicker();
}


jQuery(function(){
	if(pagenow=='nav-menus') {

		update_megamenu();
		uploadSlidesForSlideshow();
		menuFontIcons();
		pixColorPicker();

		jQuery('.button-controls input[type="submit"]').click(function(){
			var set = setTimeout('pixColorPicker()',1000);
		});

		jQuery('body').ajaxSuccess(function() {
			var set = setTimeout(function(){
				update_megamenu();
				pixColorPicker();
			},1);
		});

		jQuery('#menu-to-edit').on( "sortstop", function(event, ui) {
			var set = setTimeout(update_megamenu,10);
		});

		jQuery('.menus-move').on( "click", function(event, ui) {
			var set = setTimeout(update_megamenu,10);
		});

	}
});
