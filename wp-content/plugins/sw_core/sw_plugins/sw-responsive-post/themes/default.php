<?php 
	/**
		** Theme: Responsive Slider
		** Author: Smartaddons
		** Version: 1.0
	**/
	//var_dump($category);
	$default = array(
			'category' => $category, 
			'orderby' => $orderby,
			'order' => $order, 
			'numberposts' => $numberposts,
	);
	$list = get_posts($default);
	do_action( 'before' ); 
	$id = 'sw_reponsive_post_slider_'.rand().time();
	if ( count($list) > 0 ){
?>
<div class="clear"></div>
<div id="<?php echo esc_attr( $id ) ?>" class="responsive-post-slider responsive-slider clearfix loading" data-lg="<?php echo esc_attr( $columns ); ?>" data-md="<?php echo esc_attr( $columns1 ); ?>" data-sm="<?php echo esc_attr( $columns2 ); ?>" data-xs="<?php echo esc_attr( $columns3 ); ?>" data-mobile="<?php echo esc_attr( $columns4 ); ?>" data-speed="<?php echo esc_attr( $speed ); ?>" data-scroll="<?php echo esc_attr( $scroll ); ?>" data-interval="<?php echo esc_attr( $interval ); ?>" data-autoplay="<?php echo esc_attr( $autoplay ); ?>">
	<div class="resp-slider-container">
		<?php 
			if( $title2 != '' ){
					$titles = strpos($title2, ' ');
		?>
		<div class="block-title">
			<?php 
				if( $titles ) : 
					echo '<h2><span>' . substr( $title2, 0, $titles ) . ' </span>' . substr( $title2, $titles + 1 ) .'</h2>'; 
				else: 
					echo '<h2><span>' . substr( $title2, 0, 1 ) . '</span>' . substr( $title2, 1 ) .'</h2>'; 
				endif;
			?>		
		</div>
		<?php } ?>    
		<div class="slider responsive">
			<?php foreach ($list as $post){ ?>
				<?php if($post->post_content != Null) { ?>
				<div class="item widget-pformat-detail">
					<div class="item-inner">								
						<div class="item-detail">
							<div class="img_over">
								<a href="<?php echo get_permalink($post->ID)?>" >
									<?php 
								if ( has_post_thumbnail( $post->ID ) ){
									
										echo get_the_post_thumbnail( $post->ID, 'styleshop_blog-responsive1' ) ? get_the_post_thumbnail( $post->ID, 'styleshop_blog-responsive1' ): '<img src="'.get_template_directory_uri().'/assets/img/placeholder/'.'large'.'.png" alt="No thumb">';		
								}else{
									echo '<img src="'.get_template_directory_uri().'/assets/img/placeholder/'.'large'.'.png" alt="No thumb">';
								}
							?></a>
							</div>
							<div class="entry-content">
								<div class="item-title">
									<h4><a href="<?php echo get_permalink($post->ID)?>"><?php echo $post->post_title;?></a></h4>
								</div>
								<div class="entry-meta clearfix">
									<div class="meta-entry entry-date pull-left">
										<span class="month-time"><?php echo get_the_time( 'M', $post->ID );?></span> <span class="day-time"><?php echo get_the_time('d', $post->ID );?></span>, <span class="year-time"><?php echo get_the_time('Y', $post->ID );?></span>
									</div>
									<div class="meta-entry entry-comment pull-left">
										<a href="<?php comments_link(); ?>"><?php echo $post-> comment_count .  ( ($post-> comment_count) > 1 ? esc_html__('  Comments', 'styleshop') : esc_html__('  Comment', 'styleshop')); ?></a>
									</div>
								</div>
								<div class="item-content">
									<p>
										<?php 
											if ( preg_match('/<!--more(.*?)?-->/', $post->post_content, $matches) ) {
											$content = explode($matches[0], $post->post_content, 2);
											$content =  wp_trim_words($content[0], $length, ' ');
											} else {
												$content = wp_trim_words($post->post_content, $length, ' ');
											}
											echo esc_html( $content ); 
										?>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			<?php }?>
		</div>
	</div>
</div>
<?php } ?>