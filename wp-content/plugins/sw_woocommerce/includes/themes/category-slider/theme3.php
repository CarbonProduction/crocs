<?php 
	
	$widget_id = isset( $widget_id ) ? $widget_id : 'category_slide_'.$this->generateID();
	$viewall = get_permalink( wc_get_page_id( 'shop' ) );
	if( $category == '' ){
		return '<div class="alert alert-warning alert-dismissible" role="alert">
			<a class="close" data-dismiss="alert">&times;</a>
			<p>'. esc_html__( 'Please select a category for SW Woocommerce Category Slider. Layout ', 'sw_woocommerce' ) . $layout .'</p>
		</div>';
	}
?>
<div id="<?php echo 'listing_' . $widget_id; ?>" class="responsive-listing-banner2">
	<div class="banner-listing-container clearfix">
		<?php
			if( !is_array( $category ) ){
				$category = explode( ',', $category );
			}
			$i = 0;
			foreach( $category as $cat ){
				$term = get_term_by('slug', $cat, 'product_cat');	
				if( $term ) :
				$viewall = get_term_link( $term->term_id, 'product_cat' );
				$thumbnail_id 	= get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
				$thumb = wp_get_attachment_image( $thumbnail_id,'full' );
				$class = '';				
				$class .= ( $i % 2 == 0 ) ? 'item-left' : 'item-center';
				
				if( $i % 3 == 2){
					$class .= ' item-right';
				}
		?>
			<div class="item-product-cat pull-left <?php echo esc_attr( $class ); ?>">
				<?php if( $i % 3 == 1 ) : ?>
				<div class="item-wrap">
					<?php 
						if( $title1 != '' ){
								$titles = strpos($title1, ' ');
					?>
					<div class="item-content-center">
						<a href="<?php echo get_term_link( $term->term_id, 'product_cat' ); ?>" title="<?php echo esc_attr( $term->name ); ?>"><?php echo $thumb; ?></a>
						<div class="item-title">
							<?php 
								if( $titles ) : 
									echo '<h2><span>' . substr( $title1, 0, $titles ) . ' </span>' . substr( $title1, $titles + 1 ) .'</h2>'; 
								else: 
									echo '<h2><span>' . substr( $title1, 0, 1 ) . '</span>' . substr( $title1, 1 ) .'</h2>'; 
								endif;
							?>
							<h5><a href="<?php echo get_term_link( $term->term_id, 'product_cat' ); ?>"><?php sw_trim_words( $term->name, $title_length ); ?></a></h5>
							<div class="des-cat"><?php echo  $term->description; ?></div>
						</div>						
					</div>
					<?php } ?>
				</div>
				<?php else: ?>
				<div class="item-wrap">
					<div class="item-image">
						<a href="<?php echo get_term_link( $term->term_id, 'product_cat' ); ?>" title="<?php echo esc_attr( $term->name ); ?>"><?php echo $thumb; ?></a>
					</div>
					<div class="item-content">
						<div class="des-cat"><?php echo  $term->description; ?></div>	
						<h4><a href="<?php echo get_term_link( $term->term_id, 'product_cat' ); ?>"><?php sw_trim_words( $term->name, $title_length ); ?></a></h4>
						<?php echo '<div class="view-all"><a href="' . esc_url( $viewall ) . '">' .esc_html__( 'Shop it now!', 'sw_woocommerce' ) . '</a></div>'; ?>					
					</div>
				</div>
				<?php endif; ?>
			</div>
			<?php $i ++; ?>
			<?php endif; ?>
		<?php } ?>
	</div>
</div>		