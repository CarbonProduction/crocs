<?php 

/**
	* Layout Default
	* @version     1.0.0
**/


$widget_id = isset( $widget_id ) ? $widget_id : $this->generateID();
$viewall = get_permalink( wc_get_page_id( 'shop' ) );
$term_name = esc_html__( 'All Categories', 'sw_woocommerce' );
$default = array(
	'post_type' => 'product',		
	'orderby' => $orderby,
	'order' => $order,
	'post_status' => 'publish',
	'showposts' => $numberposts
);
if( $category != '' ){
	$term = get_term_by( 'slug', $category, 'product_cat' );	
	if( $term ) :
		$term_name = $term->name;
		$viewall = get_term_link( $term->term_id, 'product_cat' );
	endif;
	
	$default['tax_query'] = array(
		array(
			'taxonomy'  => 'product_cat',
			'field'     => 'slug',
			'terms'     => $category )
	);	
}
$list = new WP_Query( $default );

if ( $list -> have_posts() ){ ?>
	<div id="<?php echo 'slider_' . $widget_id; ?>" class="sw-woocommerce-collection">		       
		<div class="sw-collection-container clearfix">
			<div class="box-collection-left flex-item">	
				<?php 
					if( $title1 != '' || $description != '' || $banner != '' ){						
							
				?>
					<div class="box-slider-image">
						<?php 
							$thumb = wp_get_attachment_image( $banner, 'full' );
							if( $thumb ) : 
						?>
							<div class="box-banner">
								<?php echo $thumb; ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="box-slider-title">
					<?php 
						if( $title1 != '' ) {
							$titles = strpos($title1, ' ');
							if( $titles ) : 
								echo '<h2><span>' . substr( $title1, 0, $titles ) . ' </span>' . substr( $title1, $titles + 1 ) .'</h2>'; 
							else: 
								echo '<h2><span>' . substr( $title1, 0, 1 ) . '</span>' . substr( $title1, 1 ) .'</h2>'; 
							endif;
						}
					?>	
						<?php echo ( $description != '' ) ? '<div class="box-description">' . $description . '</div>' : ''; ?>
						<?php echo '<div class="view-all"><a href="' . esc_url( $viewall ) . '">' .esc_html__( 'view all products', 'sw_woocommerce' ) . '</a></div>'; ?>	
					</div>
				<?php } ?>
			</div>
			<div class="flex-item box-collection-center row">
			<?php 
				$class = 'col-lg-'. 12/$columns . ' col-md-'. 12/$columns1 . ' col-sm-'. 12/$columns2 . ' col-xs-'. 12/$columns3;
				$count_items 	= 0;
				$numb 			= ( $list->found_posts > 0 ) ? $list->found_posts : count( $list->posts );
				$count_items 	= ( $numberposts >= $numb ) ? $numb : $numberposts;
				$center = intval( ceil( $count_items / 2 ) );
				$i 				= 0;
				while($list->have_posts()): $list->the_post();global $product, $post;				
				if( $i % $item_row == 0 ){
			?>
				<div class="item product <?php echo esc_attr( $class ); ?>">
			<?php } ?>
					<?php include( WCTHEME . '/default-item.php' ); ?>			
				<?php if( ( $i+1 ) % $item_row == 0 || ( $i+1 ) == $count_items ){?> </div><?php } ?>
			<?php if( ( $i + 1 ) == $center ){ echo '</div><div class="flex-item box-collection-right row">'; } ?>
			<?php $i++; endwhile; wp_reset_postdata();?>
			</div>
		</div>            
	</div>
	<?php
	}else{
		echo '<div class="alert alert-warning alert-dismissible" role="alert">
		<a class="close" data-dismiss="alert">&times;</a>
		<p>'. esc_html__( 'Has no product in this category', 'sw_woocommerce' ) .'</p>
	</div>';
	}
?>
