<?php 
	if( !is_array( $select_order ) ){
		$select_order = explode( ',', $select_order );
	}
	$widget_id = isset( $widget_id ) ? $widget_id : $this->generateID();
?>
<div class="sw-wootab-slider sw-woo-tab-<?php echo esc_attr( $layout );?>" id="<?php echo esc_attr( 'woo_tab_' . $widget_id ); ?>" >
	<div class="resp-tab" style="position:relative;">				
		<div class="category-slider-content <?php echo esc_attr( $style );?> clearfix">
			<!-- Get child category -->
		<?php 
		$term_str = '';
		if( $category != '' ){
			$term = get_term_by( 'slug', $category, 'product_cat' );
			if( $term ) :
				$term_name = $term->name;
		?>
			<div class="box-title">
				<h3><?php echo ( $title1 != '' ) ? $title1 : $term_name ; ?></h3>
			</div>
		<?php 							
			$thumbnail_id 	= get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
			$thumb = wp_get_attachment_image( $thumbnail_id,'large' );
		?>			
			<?php endif; ?>
			
		<?php }else{ ?>
			<div class="order-title">
				<h2><?php echo ( $title1 != '' ) ? $title1 : esc_html__( 'All Categories', 'sw_woocommerce' ) ; ?></h2>
			</div>
		<?php } ?>
			<button class="button-collapse collapsed pull-right" type="button" data-toggle="collapse" data-target="#<?php echo 'nav_'.$widget_id; ?>"  aria-expanded="false">				
			</button>
			<div class="nav-tabs-select">
				<ul class="nav nav-tabs" id="<?php echo 'nav_'.$widget_id; ?>">
					<?php 
							$tab_title = '';
							foreach( $select_order as $i  => $so ){						
								switch ($so) {
								case 'latest':
									$tab_title = __( 'Latest Products', 'sw_woocommerce' );
								break;
								case 'rating':
									$tab_title = __( 'Top Rating Products', 'sw_woocommerce' );
								break;
								case 'bestsales':
									$tab_title = __( 'Best Selling Products', 'sw_woocommerce' );
								break;						
								default:
									$tab_title = __( 'Featured Products', 'sw_woocommerce' );
								}
						?>
						<li <?php echo ( $i == ( $tab_active -1 ) )? 'class="active"' : ''; ?>>
							<a href="#<?php echo esc_attr( $so. '_' .$widget_id ) ?>" data-type="so_ajax" data-layout="<?php echo esc_attr( $layout );?>" data-row="<?php echo esc_attr( $item_row ) ?>" data-length="<?php echo esc_attr( $title_length ) ?>" data-ajaxurl="<?php echo esc_url( sw_ajax_url() ) ?>" data-category="<?php echo esc_attr( $category ) ?>" data-toggle="tab" data-sorder="<?php echo esc_attr( $so ); ?>" data-catload="ajax" data-number="<?php echo esc_attr( $numberposts ); ?>" data-lg="<?php echo esc_attr( $columns ); ?>" data-md="<?php echo esc_attr( $columns1 ); ?>" data-sm="<?php echo esc_attr( $columns2 ); ?>" data-xs="<?php echo esc_attr( $columns3 ); ?>" data-mobile="<?php echo esc_attr( $columns4 ); ?>" data-speed="<?php echo esc_attr( $speed ); ?>" data-scroll="<?php echo esc_attr( $scroll ); ?>" data-interval="<?php echo esc_attr( $interval ); ?>"  data-autoplay="<?php echo esc_attr( $autoplay ); ?>">
								<?php echo esc_html( $tab_title ); ?>
							</a>
						</li>			
					<?php } ?>
				</ul>
			</div>
		<!-- End get child category -->		
			<div class="tab-content clearfix">		
				<?php if( $term ) : ?>
				<div class="categories-image clearfix">
					<div class="item-image">			 
						<a href="<?php echo get_term_link( $term->term_id, 'product_cat' ); ?>" title="<?php echo esc_attr( $term->name ); ?>"><?php echo wp_get_attachment_image( $images, 'large' ); ?></a>
					</div>
				</div>
				<?php endif; ?>
				
			<!-- Product tab slider -->
			<?php 
				foreach( $select_order as $i  => $so ){ 
	
			?>
				<div class="tab-pane <?php echo ( $i == ( $tab_active -1 ) ) ? 'active in' : ''; ?>" id="<?php echo esc_attr( $so. '_' .$widget_id ) ?>"></div>
			<?php } ?>
			<!-- End product tab slider -->
			</div>
		</div>
	</div>
</div>