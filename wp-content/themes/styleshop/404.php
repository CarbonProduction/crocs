<?php get_template_part('header'); ?>
<div class="wrapper_404">
	<div class="container">
		<div class="row">
			<div class="content_404">
				<div class="item-left">
					<div class="erro-image">
							<span class="erro-key">
								<img class="img_logo" alt="404" src="<?php echo get_template_directory_uri(); ?>/assets/img/img-404.png">
							</span>
							<div class="erro-content">
								<h2><?php esc_html_e( 'Page note found', 'styleshop') ?></h2>
								<p><?php esc_html_e( "It looks like you may have a wrong turn. Don't worry... it happens to the best of us.", 'styleshop') ?><p>
							</div>
					</div>
				</div>
				<div class="item-right">
					<div class="block-top">
						<h3><?php esc_html_e( 'If you want go back to my store. Please in put the BOX BELOW', 'styleshop') ?></h3>
					</div>
					<div class="block-middle">
						<div class="styleshop_search_404">
							<?php get_template_part( 'widgets/sw_top/search' ); ?>
						</div>
					</div>
					<div class="block-bottom">
						<a href="<?php echo esc_url( home_url('/') ); ?>" class="btn-404 back2home" title="<?php esc_attr_e( 'Go Home', 'styleshop' ) ?>"><?php esc_html_e( "BACK TO HOME", 'styleshop' )?><i class="fa fa-arrow-circle-right"></i></a>				
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_template_part('footer'); ?>