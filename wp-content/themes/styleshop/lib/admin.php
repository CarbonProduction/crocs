<?php

/**
 * Add Theme Options page.
 */
function styleshop_theme_admin_page(){
	add_theme_page(
		esc_html__('Theme Options', 'styleshop'),
		esc_html__('Theme Options', 'styleshop'),
		'manage_options',
		'styleshop_theme_options',
		'styleshop_theme_admin_page_content'
	);
}
add_action('admin_menu', 'styleshop_theme_admin_page', 49);

function styleshop_theme_admin_page_content(){ ?>
	<div class="wrap">
		<h2><?php esc_html_e( 'Styleshop Advanced Options Page', 'styleshop' ); ?></h2>
		<?php do_action( 'styleshop_theme_admin_content' ); ?>
	</div>
<?php
}