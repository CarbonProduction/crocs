jQuery(document).ready(function(){
	
	
	if(jQuery('#last_tab').val() == ''){

		jQuery('.styleshop-opts-group-tab:first').slideDown('fast');
		jQuery('#styleshop-opts-group-menu li:first').addClass('active');
	
	}else{
		
		tabid = jQuery('#last_tab').val();
		jQuery('#'+tabid+'_section_group').slideDown('fast');
		jQuery('#'+tabid+'_section_group_li').addClass('active');
		
	}
	
	
	jQuery('input[name="'+styleshop_opts.opt_name+'[defaults]"]').click(function(){
		if(!confirm(styleshop_opts.reset_confirm)){
			return false;
		}
	});
	
	jQuery('.styleshop-opts-group-tab-link-a').click(function(){
		relid = jQuery(this).attr('data-rel');
		
		jQuery('#last_tab').val(relid);
		
		jQuery('.styleshop-opts-group-tab').each(function(){
			if(jQuery(this).attr('id') == relid+'_section_group'){
				jQuery(this).show();
			}else{
				jQuery(this).hide();
			}
			
		});
		
		jQuery('.styleshop-opts-group-tab-link-li').each(function(){
				if(jQuery(this).attr('id') != relid+'_section_group_li' && jQuery(this).hasClass('active')){
					jQuery(this).removeClass('active');
				}
				if(jQuery(this).attr('id') == relid+'_section_group_li'){
					jQuery(this).addClass('active');
				}
		});
	});
	
	
	
	
	if(jQuery('#styleshop-opts-save').is(':visible')){
		jQuery('#styleshop-opts-save').delay(4000).slideUp('slow');
	}
	
	if(jQuery('#styleshop-opts-imported').is(':visible')){
		jQuery('#styleshop-opts-imported').delay(4000).slideUp('slow');
	}	
	
	jQuery('input, textarea, select').change(function(){
		jQuery('#styleshop-opts-save-warn').slideDown('slow');
	});
	
	
	jQuery('#styleshop-opts-import-code-button').click(function(){
		if(jQuery('#styleshop-opts-import-link-wrapper').is(':visible')){
			jQuery('#styleshop-opts-import-link-wrapper').fadeOut('fast');
			jQuery('#import-link-value').val('');
		}
		jQuery('#styleshop-opts-import-code-wrapper').fadeIn('slow');
	});
	
	jQuery('#styleshop-opts-import-link-button').click(function(){
		if(jQuery('#styleshop-opts-import-code-wrapper').is(':visible')){
			jQuery('#styleshop-opts-import-code-wrapper').fadeOut('fast');
			jQuery('#import-code-value').val('');
		}
		jQuery('#styleshop-opts-import-link-wrapper').fadeIn('slow');
	});
	
	
	
	
	jQuery('#styleshop-opts-export-code-copy').click(function(){
		if(jQuery('#styleshop-opts-export-link-value').is(':visible')){jQuery('#styleshop-opts-export-link-value').fadeOut('slow');}
		jQuery('#styleshop-opts-export-code').toggle('fade');
	});
	
	jQuery('#styleshop-opts-export-link').click(function(){
		if(jQuery('#styleshop-opts-export-code').is(':visible')){jQuery('#styleshop-opts-export-code').fadeOut('slow');}
		jQuery('#styleshop-opts-export-link-value').toggle('fade');
	});
	
	

	
	
	
});