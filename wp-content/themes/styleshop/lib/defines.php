<?php
$lib_dir = trailingslashit( str_replace( '\\', '/', get_template_directory() . '/lib/' ) );

if( !defined('STYLE_DIR') ){
	define( 'STYLE_DIR', $lib_dir );
}

if( !defined('STYLE_URL') ){
	define( 'STYLE_URL', trailingslashit( get_template_directory_uri() ) . 'lib' );
}

if( !defined('STYLE_OPTIONS_URL') ){
	define( 'STYLE_OPTIONS_URL', trailingslashit( get_template_directory_uri() ) . 'lib/options/' ); 
}

defined('STYLE_THEME') or die;

if (!isset($content_width)) { $content_width = 940; }

define("STYLE_PRODUCT_TYPE","product");
define("STYLE_PRODUCT_DETAIL_TYPE","product_detail");

require_once( get_template_directory().'/lib/options.php' );
function styleshop_Options_Setup(){
	global $styleshop_options, $options, $options_args;

	$options = array();
	$options[] = array(
			'title' => esc_html__('General', 'styleshop'),
			'desc' => wp_kses( __('<p class="description">The theme allows to build your own styles right out of the backend without any coding knowledge. Upload new logo and favicon or get their URL.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it styleshop for default.
			'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_019_cogwheel.png',
			//Lets leave this as a styleshop section, no options just some intro text set above.
			'fields' => array(	
					
					array(
						'id' => 'sitelogo',
						'type' => 'upload',
						'title' => esc_html__('Logo Image', 'styleshop'),
						'sub_desc' => esc_html__( 'Use the Upload button to upload the new logo and get URL of the logo', 'styleshop' ),
						'std' => get_template_directory_uri().'/assets/img/logo-default.png'
					),
					
					array(
						'id' => 'favico',
						'type' => 'upload',
						'title' => esc_html__('Favicon', 'styleshop'),
						'sub_desc' => esc_html__( 'Use the Upload button to upload the custom favicon', 'styleshop' ),
						'std' => ''
					),
					
					array(
						'id' => 'tax_select',
						'type' => 'multi_select_taxonomy',
						'title' => esc_html__('Select Taxonomy', 'styleshop'),
						'sub_desc' => esc_html__( 'Select taxonomy to show custom term metabox', 'styleshop' ),
					),
					
					array(
						'id' => 'title_length',
						'type' => 'text',
						'title' => esc_html__('Title Length Of Item Listing Page', 'styleshop'),
						'sub_desc' => esc_html__( 'Choose title length if you want to trim word, leave 0 to not trim word', 'styleshop' ),
						'std' => 0
					)					
			)		
		);
	
	$options[] = array(
			'title' => esc_html__('Schemes', 'styleshop'),
			'desc' => wp_kses( __('<p class="description">Custom color scheme for theme. Unlimited color that you can choose.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it styleshop for default.
			'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_163_iphone.png',
			//Lets leave this as a styleshop section, no options just some intro text set above.
			'fields' => array(		
				
					
				array(
					'id' => 'developer_mode',
					'title' => esc_html__( 'Developer Mode', 'styleshop' ),
					'type' => 'checkbox',
					'sub_desc' => esc_html__( 'Turn on/off compile less to css and custom color', 'styleshop' ),
					'desc' => '',
					'std' => '0'
				),
				
				array(
					'id' => 'scheme_color',
					'type' => 'color',
					'title' => esc_html__('Color', 'styleshop'),
					'sub_desc' => esc_html__('Select main custom color.', 'styleshop'),
					'std' => ''
				)
							
			)
	);
	
	$options[] = array(
			'title' => esc_html__('Layout', 'styleshop'),
			'desc' => wp_kses( __('<p class="description">SmartAddons Framework comes with a layout setting that allows you to build any number of stunning layouts and apply theme to your entries.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it styleshop for default.
			'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_319_sort.png',
			//Lets leave this as a styleshop section, no options just some intro text set above.
			'fields' => array(
					array(
						'id' => 'layout',
						'type' => 'select',
						'title' => esc_html__('Box Layout', 'styleshop'),
						'sub_desc' => esc_html__( 'Select Layout Box or Wide', 'styleshop' ),
						'options' => array(
							'full' => esc_html__( 'Wide', 'styleshop' ),
							'boxed' => esc_html__( 'Boxed', 'styleshop' )
						),
						'std' => 'wide'
					),
				
					array(
						'id' => 'bg_box_img',
						'type' => 'upload',
						'title' => esc_html__('Background Box Image', 'styleshop'),
						'sub_desc' => '',
						'std' => ''
					),
					array(
							'id' => 'sidebar_left_expand',
							'type' => 'select',
							'title' => esc_html__('Left Sidebar Expand', 'styleshop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12', 
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '3',
							'sub_desc' => esc_html__( 'Select width of left sidebar.', 'styleshop' ),
						),
					
					array(
							'id' => 'sidebar_right_expand',
							'type' => 'select',
							'title' => esc_html__('Right Sidebar Expand', 'styleshop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '3',
							'sub_desc' => esc_html__( 'Select width of right sidebar medium desktop.', 'styleshop' ),
						),
						array(
							'id' => 'sidebar_left_expand_md',
							'type' => 'select',
							'title' => esc_html__('Left Sidebar Medium Desktop Expand', 'styleshop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '4',
							'sub_desc' => esc_html__( 'Select width of left sidebar medium desktop.', 'styleshop' ),
						),
					array(
							'id' => 'sidebar_right_expand_md',
							'type' => 'select',
							'title' => esc_html__('Right Sidebar Medium Desktop Expand', 'styleshop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '4',
							'sub_desc' => esc_html__( 'Select width of right sidebar.', 'styleshop' ),
						),
						array(
							'id' => 'sidebar_left_expand_sm',
							'type' => 'select',
							'title' => esc_html__('Left Sidebar Tablet Expand', 'styleshop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '4',
							'sub_desc' => esc_html__( 'Select width of left sidebar tablet.', 'styleshop' ),
						),
					array(
							'id' => 'sidebar_right_expand_sm',
							'type' => 'select',
							'title' => esc_html__('Right Sidebar Tablet Expand', 'styleshop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '4',
							'sub_desc' => esc_html__( 'Select width of right sidebar tablet.', 'styleshop' ),
						),				
				)
		);
	 	$options[] = array(
			'title' => esc_html__('Mobile Layout', 'styleshop'),
			'desc' => wp_kses( __('<p class="description">SmartAddons Framework comes with a mobile setting home page layout.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it styleshop for default.
			'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_163_iphone.png',
			//Lets leave this as a styleshop section, no options just some intro text set above.
			'fields' => array(				
				array(
					'id' => 'mobile_enable',
					'type' => 'checkbox',
					'title' => esc_html__('Enable Mobile Layout', 'styleshop'),
					'sub_desc' => '',
					'desc' => '',
					'std' => '1'// 1 = on | 0 = off
				),
				array(
					'id' => 'sticky_mobile',
					'type' => 'checkbox',
					'title' => esc_html__('Sticky Mobile', 'styleshop'),
					'sub_desc' => '',
					'desc' => '',
					'std' => '0'// 1 = on | 0 = off
				),
				array(
					'id' => 'mobile_content',
					'type' => 'pages_select',
					'title' => esc_html__('Mobile Layout Content', 'styleshop'),
					'sub_desc' => esc_html__('Select content index for this mobile layout', 'styleshop'),
					'std' => ''
				),
				array(
					'id' => 'mobile_header_style',
					'type' => 'select',
					'title' => esc_html__('Header Mobile Style', 'styleshop'),
					'sub_desc' => esc_html__('Select header mobile style', 'styleshop'),
					'options' => array(
							'mstyle1'  => esc_html__( 'Style 1', 'styleshop' ),
							'mstyle2'  => esc_html__( 'Style 2', 'styleshop' ),
							'mstyle3'  => esc_html__( 'Style 3', 'styleshop' ),
							'mstyle4'  => esc_html__( 'Style 4', 'styleshop' ),
					),
					'std' => 'style1'
				),
				array(
					'id' => 'mobile_footer_style',
					'type' => 'select',
					'title' => esc_html__('Footer Mobile Style', 'styleshop'),
					'sub_desc' => esc_html__('Select footer mobile style', 'styleshop'),
					'options' => array(
							'mstyle1'  => esc_html__( 'Style 1', 'styleshop' ),
							'mstyle2'  => esc_html__( 'Style 2', 'styleshop' ),
							'mstyle3'  => esc_html__( 'Style 3', 'styleshop' ),
					),
					'std' => 'style1'
				)				
			)
	);
			
	$options[] = array(
		'title' => esc_html__('Header & Footer', 'styleshop'),
			'desc' => wp_kses( __('<p class="description">SmartAddons Framework comes with a header and footer setting that allows you to build style header.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it styleshop for default.
			'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_336_read_it_later.png',
			//Lets leave this as a styleshop section, no options just some intro text set above.
			'fields' => array(
				 array(
					'id' => 'header_style',
					'type' => 'select',
					'title' => esc_html__('Header Style', 'styleshop'),
					'sub_desc' => esc_html__('Select Header style', 'styleshop'),
					'options' => array(
							'style1'  => esc_html__( 'Style 1', 'styleshop' ),
							'style3'  => esc_html__( 'Style 3', 'styleshop' ),
							'style4'  => esc_html__( 'Style 4', 'styleshop' ),
							),
					'std' => 'style1'
				),
				
				array(
					'id' => 'disable_search',
					'title' => esc_html__( 'Disable Search', 'styleshop' ),
					'type' => 'checkbox',
					'sub_desc' => esc_html__( 'Check this to disable search on header', 'styleshop' ),
					'desc' => '',
					'std' => '0'
				),
				
				array(
					'id' => 'disable_cart',
					'title' => esc_html__( 'Disable Cart', 'styleshop' ),
					'type' => 'checkbox',
					'sub_desc' => esc_html__( 'Check this to disable cart on header', 'styleshop' ),
					'desc' => '',
					'std' => '0'
				),				
				
				array(
				   'id' => 'footer_style',
				   'type' => 'pages_select',
				   'title' => esc_html__('Footer Style', 'styleshop'),
				   'sub_desc' => esc_html__('Select Footer style', 'styleshop'),
				   'std' => ''
				),
				
				array(
					'id' => 'footer_copyright',
					'type' => 'editor',
					'sub_desc' => '',
					'title' => esc_html__( 'Copyright text', 'styleshop' )
				),	
				
			)
	);
	$options[] = array(
			'title' => esc_html__('Navbar Options', 'styleshop'),
			'desc' => wp_kses( __('<p class="description">If you got a big site with a lot of sub menus we recommend using a mega menu. Just select the dropbox to display a menu as mega menu or dropdown menu.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it styleshop for default.
			'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_157_show_lines.png',
			//Lets leave this as a styleshop section, no options just some intro text set above.
			'fields' => array(
				array(
						'id' => 'menu_type',
						'type' => 'select',
						'title' => esc_html__('Menu Type', 'styleshop'),
						'options' => array( 'dropdown' => 'Dropdown Menu', 'mega' => 'Mega Menu' ),
						'std' => 'mega'
					),
				array(
						'id' => 'menu_location',
						'type' => 'menu_location_multi_select',
						'title' => esc_html__('Theme Location', 'styleshop'),
						'sub_desc' => esc_html__( 'Select theme location to active mega menu and menu responsive.', 'styleshop' ),
						'std' => 'primary_menu'
					),			
				array(
						'id' => 'sticky_menu',
						'type' => 'checkbox',
						'title' => esc_html__('Active sticky menu', 'styleshop'),
						'sub_desc' => '',
						'desc' => '',
						'std' => '0'// 1 = on | 0 = off
					),	
			)
		);
	$options[] = array(
		'title' => esc_html__('Blog Options', 'styleshop'),
		'desc' => wp_kses( __('<p class="description">Select layout in blog listing page.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it styleshop for default.
		'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_071_book.png',
		//Lets leave this as a styleshop section, no options just some intro text set above.
		'fields' => array(
				array(
						'id' => 'sidebar_blog',
						'type' => 'select',
						'title' => esc_html__('Sidebar Blog Layout', 'styleshop'),
						'options' => array(
								'full' => esc_html__( 'Full Layout', 'styleshop' ),		
								'left'	=>  esc_html__( 'Left Sidebar', 'styleshop' ),
								'right' => esc_html__( 'Right Sidebar', 'styleshop' ),
						),
						'std' => 'left',
						'sub_desc' => esc_html__( 'Select style sidebar blog', 'styleshop' ),
					),
					array(
						'id' => 'blog_layout',
						'type' => 'select',
						'title' => esc_html__('Layout blog', 'styleshop'),
						'options' => array(
								'list'	=>  esc_html__( 'List Layout', 'styleshop' ),
								'grid' =>  esc_html__( 'Grid Layout', 'styleshop' )								
						),
						'std' => 'list',
						'sub_desc' => esc_html__( 'Select style layout blog', 'styleshop' ),
					),
					array(
						'id' => 'blog_column',
						'type' => 'select',
						'title' => esc_html__('Blog column', 'styleshop'),
						'options' => array(								
								'2' => '2 columns',
								'3' => '3 columns',
								'4' => '4 columns'								
							),
						'std' => '2',
						'sub_desc' => esc_html__( 'Select style number column blog', 'styleshop' ),
					),
			)
	);	
	$options[] = array(
		'title' => esc_html__('Product Options', 'styleshop'),
		'desc' => wp_kses( __('<p class="description">Select layout in product listing page.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it styleshop for default.
		'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_202_shopping_cart.png',
		//Lets leave this as a styleshop section, no options just some intro text set above.
		'fields' => array(
			array(
				'id' => 'product_col_large',
				'type' => 'select',
				'title' => esc_html__('Product Listing column Desktop', 'styleshop'),
				'options' => array(
						'2' => '2',
						'3' => '3',
						'4' => '4',							
					),
				'std' => '3',
				'sub_desc' => esc_html__( 'Select number of column on Desktop Screen', 'styleshop' ),
			),
			array(
				'id' => 'product_col_medium',
				'type' => 'select',
				'title' => esc_html__('Product Listing column Medium Desktop', 'styleshop'),
				'options' => array(
						'2' => '2',
						'3' => '3',
						'4' => '4',							
					),
				'std' => '2',
				'sub_desc' => esc_html__( 'Select number of column on Medium Desktop Screen', 'styleshop' ),
			),
			array(
				'id' => 'product_col_sm',
				'type' => 'select',
				'title' => esc_html__('Product Listing column Tablet', 'styleshop'),
				'options' => array(
						'2' => '2',
						'3' => '3',
						'4' => '4',							
					),
				'std' => '2',
				'sub_desc' => esc_html__( 'Select number of column on Tablet Screen', 'styleshop' ),
			),
			array(
					'id' => 'sidebar_product',
					'type' => 'select',
					'title' => esc_html__('Sidebar Product Layout', 'styleshop'),
					'options' => array(
							'left'	=> esc_html__( 'Left Sidebar', 'styleshop' ),
							'full' => esc_html__( 'Full Layout', 'styleshop' ),		
							'right' => esc_html__( 'Right Sidebar', 'styleshop' )
					),
					'std' => 'left',
					'sub_desc' => esc_html__( 'Select style sidebar product', 'styleshop' ),
				),
			array(
				'id' => 'product_quickview',
				'title' => esc_html__( 'Quickview', 'styleshop' ),
				'type' => 'checkbox',
				'sub_desc' => '',
				'desc' => esc_html__( 'Turn On/Off Product Quickview', 'styleshop' ),
				'std' => '1'
			),
			array(
				'id' => 'product_zoom',
				'title' => esc_html__( 'Product Zoom', 'styleshop' ),
				'type' => 'checkbox',
				'sub_desc' => '',
				'desc' => esc_html__( 'Turn On/Off image zoom when hover on single product', 'styleshop' ),
				'std' => '1'
			),
			array(
				'id' => 'product_number',
				'type' => 'text',
				'title' => esc_html__('Product Listing Number', 'styleshop'),
				'sub_desc' => esc_html__( 'Show number of product in listing product page.', 'styleshop' ),
				'std' => 12
			),			
		)
);		
	$options[] = array(
			'title' => esc_html__('Typography', 'styleshop'),
			'desc' => wp_kses( __('<p class="description">Change the font style of your blog, custom with Google Font.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it styleshop for default.
			'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_151_edit.png',
			//Lets leave this as a styleshop section, no options just some intro text set above.
			'fields' => array(
					array(
							'id' => 'google_webfonts',
							'type' => 'text',
							'title' => esc_html__('Use Google Webfont', 'styleshop'),
							'sub_desc' => esc_html__( 'Insert font style that you actually need on your webpage.', 'styleshop' ), 
							'std' => ''
						),
					array(
							'id' => 'webfonts_weight',
							'type' => 'multi_select',
							'sub_desc' => esc_html__( 'For weight, see Google Fonts to custom for each font style.', 'styleshop' ),
							'title' => esc_html__('Webfont Weight', 'styleshop'),
							'options' => array(
									'100' => '100',
									'200' => '200',
									'300' => '300',
									'400' => '400',
									'600' => '600',
									'700' => '700',
									'800' => '800',
									'900' => '900'
								),
							'std' => ''
						),
					array(
							'id' => 'webfonts_assign',
							'type' => 'select',
							'title' => esc_html__( 'Webfont Assign to', 'styleshop' ),
							'sub_desc' => esc_html__( 'Select the place will apply the font style headers, every where or custom.', 'styleshop' ),
							'options' => array(
									'headers' => esc_html__( 'Headers',    'styleshop' ),
									'all'     => esc_html__( 'Everywhere', 'styleshop' ),
									'custom'  => esc_html__( 'Custom',     'styleshop' )
								)
						),
					 array(
							'id' => 'webfonts_custom',
							'type' => 'text',
							'sub_desc' => esc_html__( 'Insert the places will be custom here, after selected custom Webfont assign.', 'styleshop' ),
							'title' => esc_html__( 'Webfont Custom Selector', 'styleshop' )
						),
				)
		);
	
	$options[] = array(
		'title' => __('Social', 'styleshop'),
		'desc' => wp_kses( __('<p class="description">This feature allow to you link to your social.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it blank for default.
		'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_222_share.png',
		//Lets leave this as a blank section, no options just some intro text set above.
		'fields' => array(
				array(
						'id' => 'social-share-fb',
						'title' => esc_html__( 'Facebook', 'styleshop' ),
						'type' => 'text',
						'sub_desc' => '',
						'desc' => '',
						'std' => '',
					),
				array(
						'id' => 'social-share-tw',
						'title' => esc_html__( 'Twitter', 'styleshop' ),
						'type' => 'text',
						'sub_desc' => '',
						'desc' => '',
						'std' => '',
					),
				array(
						'id' => 'social-share-tumblr',
						'title' => esc_html__( 'Tumblr', 'styleshop' ),
						'type' => 'text',
						'sub_desc' => '',
						'desc' => '',
						'std' => '',
					),
				array(
						'id' => 'social-share-in',
						'title' => esc_html__( 'Linkedin', 'styleshop' ),
						'type' => 'text',
						'sub_desc' => '',
						'desc' => '',
						'std' => '',
					),
				array(
						'id' => 'social-share-go',
						'title' => esc_html__( 'Google+', 'styleshop' ),
						'type' => 'text',
						'sub_desc' => '',
						'desc' => '',
						'std' => '',
					),
				array(
					'id' => 'social-share-pi',
					'title' => esc_html__( 'Pinterest', 'styleshop' ),
					'type' => 'text',
					'sub_desc' => '',
					'desc' => '',
					'std' => '',
				)
					
			)
	);
	
	$options[] = array(
			'title' => esc_html__('Maintaincece Mode', 'styleshop'),
			'desc' => wp_kses( __('<p class="description">Enable and config for Maintaincece mode.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it styleshop for default.
			'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_083_random.png',
			//Lets leave this as a styleshop section, no options just some intro text set above.
			'fields' => array(
					array(
						'id' => 'maintaince_enable',
						'title' => esc_html__( 'Enable Maintaincece Mode', 'styleshop' ),
						'type' => 'checkbox',
						'sub_desc' => esc_html__( 'Turn on/off Maintaince mode on this website', 'styleshop' ),
						'desc' => '',
						'std' => '0'
					),
					
					array(
						'id' => 'maintaince_background',
						'title' => esc_html__( 'Maintaince Background', 'styleshop' ),
						'type' => 'upload',
						'sub_desc' => esc_html__( 'Choose maintance background image', 'styleshop' ),
						'desc' => '',
						'std' => get_template_directory_uri().'/assets/img/maintaince/bg-main.jpg'
					),
					
					array(
						'id' => 'maintaince_content',
						'title' => esc_html__( 'Maintaince Content', 'styleshop' ),
						'type' => 'editor',
						'sub_desc' => esc_html__( 'Change text of maintaince mode', 'styleshop' ),
						'desc' => '',
						'std' => ''
					),
					
					array(
						'id' => 'maintaince_date',
						'title' => esc_html__( 'Maintaince Date', 'styleshop' ),
						'type' => 'date',
						'sub_desc' => esc_html__( 'Put date to this field to show countdown date on maintaince mode.', 'styleshop' ),
						'desc' => '',
						'placeholder' => 'mm/dd/yy',
						'std' => ''
					),
					
					array(
						'id' => 'maintaince_form',
						'title' => esc_html__( 'Maintaince Form', 'styleshop' ),
						'type' => 'text',
						'sub_desc' => esc_html__( 'Put shortcode form to this field and it will be shown on maintaince mode frontend.', 'styleshop' ),
						'desc' => '',
						'std' => ''
					),
					
				)
		);
	$options[] = array(
		'title' => esc_html__('Popup Config', 'styleshop'),
		'desc' => wp_kses( __('<p class="description">Enable popup and more config for Popup.</p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it styleshop for default.
		'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_083_random.png',
		//Lets leave this as a styleshop section, no options just some intro text set above.
		'fields' => array(
				array(
					'id' => 'popup_active',
					'type' => 'checkbox',
					'title' => esc_html__( 'Active Popup Subscribe', 'styleshop' ),
					'sub_desc' => esc_html__( 'Check to active popup subscribe', 'styleshop' ),
					'desc' => '',
					'std' => '0'// 1 = on | 0 = off
				),	
				
				array(
					'id' => 'popup_background',
					'title' => esc_html__( 'Popup Background', 'styleshop' ),
					'type' => 'upload',
					'sub_desc' => esc_html__( 'Choose popup background image', 'styleshop' ),
					'desc' => '',
					'std' => get_template_directory_uri().'/assets/img/popup/bg-main.jpg'
				),
				
				array(
					'id' => 'popup_content',
					'title' => esc_html__( 'Popup Content', 'styleshop' ),
					'type' => 'editor',
					'sub_desc' => esc_html__( 'Change text of popup mode', 'styleshop' ),
					'desc' => '',
					'std' => ''
				),	
				
				array(
					'id' => 'popup_form',
					'title' => esc_html__( 'Popup Form', 'styleshop' ),
					'type' => 'text',
					'sub_desc' => esc_html__( 'Put shortcode form to this field and it will be shown on popup mode frontend.', 'styleshop' ),
					'desc' => '',
					'std' => ''
				),
				array(
					'id' => 'popup_link',
					'title' => esc_html__( 'Popup Link', 'styleshop' ),
					'type' => 'text',
					'sub_desc' => esc_html__( 'Change link of popup mode', 'styleshop' ),
					'desc' => '',
					'std' => ''
				),
				
			)
	);
	$options[] = array(
			'title' => esc_html__('Advanced', 'styleshop'),
			'desc' => wp_kses( __('<p class="description">Custom advanced with Cpanel, Widget advanced, Developer mode </p>', 'styleshop'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it styleshop for default.
			'icon' => STYLE_URL.'/options/img/glyphicons/glyphicons_083_random.png',
			//Lets leave this as a styleshop section, no options just some intro text set above.
			'fields' => array(
					array(
						'id' => 'show_cpanel',
						'title' => esc_html__( 'Show cPanel', 'styleshop' ),
						'type' => 'checkbox',
						'sub_desc' => esc_html__( 'Turn on/off Cpanel', 'styleshop' ),
						'desc' => '',
						'std' => ''
					),
					
					array(
						'id' => 'widget-advanced',
						'title' => esc_html__('Widget Advanced', 'styleshop'),
						'type' => 'checkbox',
						'sub_desc' => esc_html__( 'Turn on/off Widget Advanced', 'styleshop' ),
						'desc' => '',
						'std' => '1'
					),					
					
					array(
						'id' => 'social_share',
						'title' => esc_html__( 'Social Share', 'styleshop' ),
						'type' => 'checkbox',
						'sub_desc' => esc_html__( 'Turn on/off social share', 'styleshop' ),
						'desc' => '',
						'std' => '1'
					),
					
					array(
						'id' => 'breadcrumb_active',
						'title' => esc_html__( 'Turn Off Breadcrumb', 'styleshop' ),
						'type' => 'checkbox',
						'sub_desc' => esc_html__( 'Turn off breadcumb on all page', 'styleshop' ),
						'desc' => '',
						'std' => '0'
					),
					
					array(
						'id' => 'back_active',
						'type' => 'checkbox',
						'title' => esc_html__('Back to top', 'styleshop'),
						'sub_desc' => '',
						'desc' => '',
						'std' => '1'// 1 = on | 0 = off
					),	
					
					array(
						'id' => 'direction',
						'type' => 'select',
						'title' => esc_html__('Direction', 'styleshop'),
						'options' => array( 'ltr' => 'Left to Right', 'rtl' => 'Right to Left' ),
						'std' => 'ltr'
					),
					
					array(
						'id' => 'advanced_css',
						'type' => 'textarea',
						'sub_desc' => esc_html__( 'Insert your own CSS into this block. This overrides all default styles located throughout the theme', 'styleshop' ),
						'title' => esc_html__( 'Custom CSS', 'styleshop' )
					),
					
					array(
						'id' => 'advanced_js',
						'type' => 'textarea',
						'placeholder' => esc_html__( 'Example: $("p").hide()', 'styleshop' ),
						'sub_desc' => esc_html__( 'Insert your own JS into this block. This customizes js throughout the theme', 'styleshop' ),
						'title' => esc_html__( 'Custom JS', 'styleshop' )
					)
				)
		);

	$options_args = array();

	//Setup custom links in the footer for share icons
	$options_args['share_icons']['facebook'] = array(
			'link' => 'http://www.facebook.com/SmartAddons.page',
			'title' => 'Facebook',
			'img' => STYLE_URL.'/options/img/glyphicons/glyphicons_320_facebook.png'
	);
	$options_args['share_icons']['twitter'] = array(
			'link' => 'https://twitter.com/smartaddons',
			'title' => 'Folow me on Twitter',
			'img' => STYLE_URL.'/options/img/glyphicons/glyphicons_322_twitter.png'
	);
	$options_args['share_icons']['linked_in'] = array(
			'link' => 'http://www.linkedin.com/in/smartaddons',
			'title' => 'Find me on LinkedIn',
			'img' => STYLE_URL.'/options/img/glyphicons/glyphicons_337_linked_in.png'
	);


	//Choose a custom option name for your theme options, the default is the theme name in lowercase with spaces replaced by underscores
	$options_args['opt_name'] = STYLE_THEME;

	$options_args['google_api_key'] = '';//must be defined for use with google webfonts field type

	//Custom menu title for options page - default is "Options"
	$options_args['menu_title'] = esc_html__('Theme Options', 'styleshop');

	//Custom Page Title for options page - default is "Options"
	$options_args['page_title'] = esc_html__('Styleshop Options ', 'styleshop') . wp_get_theme()->get('Name');

	//Custom page slug for options page (wp-admin/themes.php?page=***) - default is "styleshop_theme_options"
	$options_args['page_slug'] = 'styleshop_theme_options';

	//page type - "menu" (adds a top menu section) or "submenu" (adds a submenu) - default is set to "menu"
	$options_args['page_type'] = 'submenu';

	//custom page location - default 100 - must be unique or will override other items
	$options_args['page_position'] = 27;
	$styleshop_options = new Styleshop_Options($options, $options_args);
}
add_action( 'admin_init', 'styleshop_Options_Setup', 0 );
styleshop_Options_Setup();

function styleshop_widget_setup_args(){
	$styleshop_widget_areas = array(
		
		array(
				'name' => esc_html__('Sidebar Left Blog', 'styleshop'),
				'id'   => 'left-blog',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget' => '</div></div>',
				'before_title' => '<div class="block-title-widget"><h2><span>',
				'after_title' => '</span></h2></div>'
		),

		array(
				'name' => esc_html__('Sidebar Right Blog', 'styleshop'),
				'id'   => 'right-blog',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget' => '</div></div>',
				'before_title' => '<div class="block-title-widget"><h2><span>',
				'after_title' => '</span></h2></div>'
		),

		array(
				'name' => esc_html__('My Account', 'styleshop'),
				'id'   => 'my-account',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),

		array(
				'name' => esc_html__('Top Header 1', 'styleshop'),
				'id'   => 'top1',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),

		array(
				'name' => esc_html__('Top Header 2', 'styleshop'),
				'id'   => 'top2',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),
		
		array(
				'name' => esc_html__('Top Header 3', 'styleshop'),
				'id'   => 'top3',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),

		array(
				'name' => esc_html__('Text Top', 'styleshop'),
				'id'   => 'text-top',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),

		array(
				'name' => esc_html__('Bottom Header', 'styleshop'),
				'id'   => 'bottom-header',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),

		array(
				'name' => esc_html__('Bottom Header 3', 'styleshop'),
				'id'   => 'bottom-header3',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),

		array(
				'name' => esc_html__('Sidebar Left Product', 'styleshop'),
				'id'   => 'left-product',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget' => '</div></div>',
				'before_title' => '<div class="block-title-widget"><h2><span>',
				'after_title' => '</span></h2></div>'
		),
		
		array(
				'name' => esc_html__('Sidebar Right Product', 'styleshop'),
				'id'   => 'right-product',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget' => '</div></div>',
				'before_title' => '<div class="block-title-widget"><h2><span>',
				'after_title' => '</span></h2></div>'
		),
		array(
				'name' => esc_html__('Sidebar Left Detail Product', 'styleshop'),
				'id'   => 'left-product-detail',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget' => '</div></div>',
				'before_title' => '<div class="block-title-widget"><h2><span>',
				'after_title' => '</span></h2></div>'
		),
		
		array(
				'name' => esc_html__('Sidebar Right Detail Product', 'styleshop'),
				'id'   => 'right-product-detail',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget' => '</div></div>',
				'before_title' => '<div class="block-title-widget"><h2><span>',
				'after_title' => '</span></h2></div>'
		),
		array(
				'name' => esc_html__('Sidebar Bottom Detail Product', 'styleshop'),
				'id'   => 'bottom-detail-product',
				'before_widget' => '<div class="widget %1$s %2$s" data-scroll-reveal="enter bottom move 20px wait 0.2s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),
		array(
				'name' => esc_html__('Filter Mobile', 'styleshop'),
				'id'   => 'filter-mobile',
				'before_widget' => '<div class="widget %1$s %2$s" data-scroll-reveal="enter bottom move 20px wait 0.2s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),
		array(
				'name' => esc_html__('Bottom Detail Product Mobile', 'styleshop'),
				'id'   => 'bottom-detail-product-mobile',
				'before_widget' => '<div class="widget %1$s %2$s" data-scroll-reveal="enter bottom move 20px wait 0.2s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),
		array(
				'name' => esc_html__('Footer Copyright', 'styleshop'),
				'id'   => 'footer-copyright',
				'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
		),
	);
	return $styleshop_widget_areas;
}