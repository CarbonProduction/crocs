<?php
/**
 * styleshop initial setup and constants
 */
function styleshop_setup() {
	// Make theme available for translation
	load_theme_textdomain( 'styleshop', get_template_directory() . '/lang' );

	// Register wp_nav_menu() menus (http://codex.wordpress.org/Function_Reference/register_nav_menus)
	register_nav_menus(array(
		'primary_menu' => esc_html__('Primary Menu', 'styleshop'),
		'vertical_menu' => esc_html__( 'Vertical Menu', 'styleshop' ),
		'mobile_menu' => esc_html__( 'Mobile Menu', 'styleshop' ),
	));
	
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	if( styleshop_options()->getCpanelValue( 'product_zoom' ) ) :
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-slider' );
	endif;
	
	add_image_size( 'styleshop_blog-responsive1', 570, 310, true );
	add_image_size( 'styleshop_blog-responsive2', 370, 246, true );
	add_image_size( 'styleshop_shop-image', 600, 800, true );
	add_image_size( 'styleshop_detail_thumb', 870, 450, true );

	add_theme_support( "title-tag" );
	
	add_theme_support('bootstrap-gallery');     // Enable Bootstrap's thumbnails component on [gallery]
	
	// Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
	add_theme_support('post-thumbnails');

	// Add post formats (http://codex.wordpress.org/Post_Formats)
	add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
	
	// Custom image header
	$styleshop_header_arr = array(
		'default-image' => get_template_directory_uri().'/assets/img/logo-default.png',
		'uploads'       => true
	);
	add_theme_support( 'custom-header', $styleshop_header_arr );
	
	// Custom Background 
	$styleshop_bgarr = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);
	add_theme_support( 'custom-background', $styleshop_bgarr );
	
	// Tell the TinyMCE editor to use a custom stylesheet
	add_editor_style( 'css/editor-style.css' );
	
	new Styleshop_Menu();
}
add_action('after_setup_theme', 'styleshop_setup');

