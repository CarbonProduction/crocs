/*
 *
 * Styleshop_Options_radio_img function
 * Changes the radio select option, and changes class on images
 *
 */
function styleshop_radio_img_select(relid, labelclass){
	jQuery(this).prev('input[type="radio"]').prop('checked');

	jQuery('.styleshop-radio-img-'+labelclass).removeClass('styleshop-radio-img-selected');	
	
	jQuery('label[for="'+relid+'"]').addClass('styleshop-radio-img-selected');
}//function