<?php
/***** Active Plugin ********/
require_once( get_template_directory().'/lib/class-tgm-plugin-activation.php' );

add_action( 'tgmpa_register', 'styleshop_register_required_plugins' );
function styleshop_register_required_plugins() {
    $plugins = array(
		array(
            'name'               => esc_html__( 'WooCommerce', 'styleshop' ), 
            'slug'               => 'woocommerce', 
            'required'           => true, 
						'version'			       => '3.0.8'
        ),

         array(
            'name'               => esc_html__( 'Revslider', 'styleshop' ), 
            'slug'               => 'revslider', 
            'source'             => esc_url( 'http://demo.wpthemego.com/themes/sw_styleshop/plugins/revslider.zip' ), 
            'required'           => true, 
            'version'                  => '5.4.3.1'
        ),
		
				array(
            'name'     			 => esc_html__( 'SW Core', 'styleshop' ),
            'slug'      		 => 'sw_core',
						'source'         => esc_url( 'http://demo.wpthemego.com/themes/sw_styleshop/plugins/sw_core.zip' ), 
            'required'  		 => true,   
						'version'			       => '1.2.0'
			),
		
			array(
            'name'     			 => esc_html__( 'SW WooCommerce', 'styleshop' ),
            'slug'      		 => 'sw_woocommerce',
						'source'         => esc_url( 'http://demo.wpthemego.com/themes/sw_styleshop/plugins/sw_woocommerce.zip' ), 
            'required'  		 => true,
						'version'			       => '1.3.1'
        ),
				
		array(
            'name'               => esc_html__( 'One Click Install', 'styleshop' ), 
            'slug'               => 'one-click-install', 
            'source'             => esc_url( 'http://demo.wpthemego.com/themes/sw_styleshop/plugins/one-click-install.zip' ), 
            'required'           => true, 
        ),
		array(
            'name'               => esc_html__( 'Visual Composer', 'styleshop' ), 
            'slug'               => 'js_composer', 
            'source'             => esc_url( 'http://demo.wpthemego.com/themes/sw_styleshop/plugins/js_composer.zip' ), 
            'required'           => true, 
            'version'                  => '5.1.1'
        ), 		
		array(
            'name'     			 => esc_html__( 'WordPress Importer', 'styleshop' ),
            'slug'      		 => 'wordpress-importer',
            'required' 			 => true,
        ), 
		array(
            'name'      		 => esc_html__( 'MailChimp for WordPress Lite', 'styleshop' ),
            'slug'     			 => 'mailchimp-for-wp',
            'required' 			 => false,
        ),
		array(
            'name'      		 => esc_html__( 'Contact Form 7', 'styleshop' ),
            'slug'     			 => 'contact-form-7',
            'required' 			 => false,
        ),
		array(
            'name'      		 => esc_html__( 'Image Widget', 'styleshop' ),
            'slug'     			 => 'image-widget',
            'required' 			 => false,
        ),
		array(
            'name'      		 => esc_html__( 'YITH Woocommerce Compare', 'styleshop' ),
            'slug'      		 => 'yith-woocommerce-compare',
            'required'			 => false
        ),
		 array(
            'name'     			 => esc_html__( 'YITH Woocommerce Wishlist', 'styleshop' ),
            'slug'      		 => 'yith-woocommerce-wishlist',
            'required' 			 => false
        ), 
		array(
            'name'     			 => esc_html__( 'WordPress Seo', 'styleshop' ),
            'slug'      		 => 'wordpress-seo',
            'required'  		 => false,
        ),

    );
		if( styleshop_options()->getCpanelValue('developer_mode') ): 
			$plugins[] = array(
				'name'               => esc_html__( 'Less Compile', 'styleshop' ), 
				'slug'               => 'lessphp', 
				'source'             => esc_url( 'http://demo.wpthemego.com/themes/sw_styleshop/plugins/lessphp.zip' ), 
				'required'           => true, 
			);
		endif;
    $config = array();

    tgmpa( $plugins, $config );

}
add_action( 'vc_before_init', 'styleshop_vcSetAsTheme' );
function styleshop_vcSetAsTheme() {
    vc_set_as_theme();
}