<?php get_header(); ?>
<?php 
	$styleshop_sidebar_template	= get_post_meta( get_the_ID(), 'page_sidebar_layout', true );
	$styleshop_sidebar 					= get_post_meta( get_the_ID(), 'page_sidebar_template', true );
?>

	<div class="styleshop_breadcrumbs">
		<div class="container">
			<div class="listing-title">			
				<h1><span><?php styleshop_title(); ?></span></h1>				
			</div>
			<?php
				if (!is_front_page() ) {
					if (function_exists('styleshop_breadcrumb')){
						styleshop_breadcrumb('<div class="breadcrumbs custom-font theme-clearfix">', '</div>');
					} 
				} 
			?>
		</div>
	</div>	
	<div class="container">
		<div class="row">
		<?php 
			if ( is_active_sidebar( $styleshop_sidebar ) && $styleshop_sidebar_template != 'right' && $styleshop_sidebar_template !='full' ):
			$styleshop_left_span_class = 'col-lg-'.styleshop_options()->getCpanelValue('sidebar_left_expand');
			$styleshop_left_span_class .= ' col-md-'.styleshop_options()->getCpanelValue('sidebar_left_expand_md');
			$styleshop_left_span_class .= ' col-sm-'.styleshop_options()->getCpanelValue('sidebar_left_expand_sm');
		?>
			<aside id="left" class="sidebar <?php echo esc_attr( $styleshop_left_span_class ); ?>">
				<?php dynamic_sidebar( $styleshop_sidebar ); ?>
			</aside>
		<?php endif; ?>
		
			<div id="contents" role="main" class="main-page <?php styleshop_content_page(); ?>">
				<?php
				get_template_part('templates/content', 'page')
				?>
			</div>
			<?php 
			if ( is_active_sidebar( $styleshop_sidebar ) && $styleshop_sidebar_template != 'left' && $styleshop_sidebar_template !='full' ):
				$styleshop_left_span_class = 'col-lg-'.styleshop_options()->getCpanelValue('sidebar_left_expand');
				$styleshop_left_span_class .= ' col-md-'.styleshop_options()->getCpanelValue('sidebar_left_expand_md');
				$styleshop_left_span_class .= ' col-sm-'.styleshop_options()->getCpanelValue('sidebar_left_expand_sm');
			?>
				<aside id="right" class="sidebar <?php echo esc_attr($styleshop_left_span_class); ?>">
					<?php dynamic_sidebar( $styleshop_sidebar ); ?>
				</aside>
			<?php endif; ?>
		</div>		
	</div>
<?php get_footer(); ?>

