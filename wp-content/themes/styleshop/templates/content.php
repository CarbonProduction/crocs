<?php get_template_part('header'); ?>
<?php 
	$styleshop_sidebar_template = styleshop_options()->getCpanelValue('sidebar_blog') ;
	$styleshop_blog_styles = styleshop_options()->getCpanelValue('blog_layout');
?>

<div class="styleshop_breadcrumbs">
	<div class="container">
		<?php
			if (!is_front_page() ) {
				if (function_exists('styleshop_breadcrumb')){
					styleshop_breadcrumb('<div class="breadcrumbs custom-font theme-clearfix">', '</div>');
				} 
			} 
		?>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="category-contents <?php styleshop_content_blog(); ?>">
			<div class="listing-title">			
				<h1><span><?php styleshop_title(); ?></span></h1>				
			</div>
			<!-- No Result -->
			<?php if (!have_posts()) : ?>
			<?php get_template_part('templates/no-results'); ?>
			<?php endif; ?>			
			
			<?php 
				$styleshop_blogclass = 'blog-content blog-content-'. $styleshop_blog_styles;
				if( $styleshop_blog_styles == 'grid' ){
					$styleshop_blogclass .= ' row';
				}
			?>
			<div class="<?php echo esc_attr( $styleshop_blogclass ); ?>">
			<?php 			
				while( have_posts() ) : the_post();
					get_template_part( 'templates/content', $styleshop_blog_styles );
				endwhile;
			?>
			<?php get_template_part('templates/pagination'); ?>
			</div>
			<div class="clearfix"></div>
		</div>
		
		<?php if ( is_active_sidebar('left-blog') && $styleshop_sidebar_template == 'left' ):
			$styleshop_left_span_class = 'col-lg-'.styleshop_options()->getCpanelValue('sidebar_left_expand');
			$styleshop_left_span_class .= ' col-md-'.styleshop_options()->getCpanelValue('sidebar_left_expand_md');
			$styleshop_left_span_class .= ' col-sm-'.styleshop_options()->getCpanelValue('sidebar_left_expand_sm');
		?>
		<aside id="left" class="sidebar <?php echo esc_attr($styleshop_left_span_class); ?>">
			<?php dynamic_sidebar('left-blog'); ?>
		</aside>

		<?php endif; ?>
		<?php if ( is_active_sidebar('right-blog') && $styleshop_sidebar_template =='right' ):
			$styleshop_right_span_class = 'col-lg-'.styleshop_options()->getCpanelValue('sidebar_right_expand');
			$styleshop_right_span_class .= ' col-md-'.styleshop_options()->getCpanelValue('sidebar_right_expand_md');
			$styleshop_right_span_class .= ' col-sm-'.styleshop_options()->getCpanelValue('sidebar_right_expand_sm');
		?>
		<aside id="right" class="sidebar <?php echo esc_attr($styleshop_right_span_class); ?>">
			<?php dynamic_sidebar('right-blog'); ?>
		</aside>
		<?php endif; ?>
	</div>
</div>
<?php get_template_part('footer'); ?>
