<?php 	
	$styleshop_page_footer   	 = ( get_post_meta( get_the_ID(), 'page_footer_style', true ) != '' ) ? get_post_meta( get_the_ID(), 'page_footer_style', true ) : styleshop_options()->getCpanelValue( 'footer_style' );
	$styleshop_copyright_text 	 = styleshop_options()->getCpanelValue( 'footer_copyright' ); 
?>

<footer id="footer" class="footer default theme-clearfix">
	<!-- Content footer -->
	<div class="container">
		<?php 
			if( $styleshop_page_footer != '' ) :
				echo get_the_content_by_id( $styleshop_page_footer ); 
			endif;
		?>
	</div>
	<div class="footer-copyright">
		<div class="container">
			<!-- Copyright text -->
			<div class="copyright-text pull-left">
				<?php if( $styleshop_copyright_text == '' ) : ?>
					<p>&copy;<?php echo date('Y') .' '. esc_html__('WordPress Theme SW Styleshop. All Rights Reserved. Designed by ','styleshop'); ?><a class="mysite" href="http://wpthemego.com/"><?php esc_html_e('WpThemeGo.com','styleshop');?></a>.</p>
				<?php else : ?>
					<?php echo wp_kses( $styleshop_copyright_text, array( 'a' => array( 'href' => array(), 'title' => array(), 'class' => array() ), 'p' => array()  ) ) ; ?>
				<?php endif; ?>
			</div>
			<?php if (is_active_sidebar('footer-copyright')){ ?>
			<div class="sidebar-copyright pull-right">
				<?php dynamic_sidebar('footer-copyright'); ?>
			</div>
		<?php } ?>
		</div>
	</div>
	<?php if(styleshop_options()->getCpanelValue('back_active') == '1') { ?>
		<div class="btt">
			<div id="styleshop-totop">
				<a class="styleshop-totop" href="#" ></a>
			</div>
		</div>
	<?php }?>
</footer>