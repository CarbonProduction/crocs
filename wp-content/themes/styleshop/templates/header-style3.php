<?php 
/* 
** Content Header
*/
$styleshop_page_header = get_post_meta( get_the_ID(), 'page_header_style', true );
$styleshop_colorset = styleshop_options()->getCpanelValue('scheme');
$styleshop_logo = styleshop_options()->getCpanelValue('sitelogo');
$styleshop_page_header  = ( get_post_meta( get_the_ID(), 'page_header_style', true ) != '' ) ? get_post_meta( get_the_ID(), 'page_header_style', true ) : styleshop_options()->getCpanelValue('header_style');
?>
<header id="header" class="header header-<?php echo esc_attr( $styleshop_page_header ); ?>">
	<div class="header-top clearfix">
		<div class="container">
			<div class="rows">
				<div class="wrap-myacc pull-left">
					<div class="sidebar-account pull-left">
					    <?php if (is_active_sidebar('my-account')) {?>
					    	<div class="account-title">My account</div>
							<div id="my-account" class="my-account">
								<?php dynamic_sidebar('my-account'); ?>
							</div>
						<?php }?>
					</div>
					<?php if (is_active_sidebar('top1')) {?>
						<div class="pull-left top1">
								<?php dynamic_sidebar('top1'); ?>
						</div>
					<?php }?>
				</div>
				<!-- Sidebar Top Menu -->
				<?php if (is_active_sidebar('top3')) {?>
					<div class="pull-right top2">
							<?php dynamic_sidebar('top3'); ?>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div class="rows">
				<!-- Logo -->
				<div class="top-header pull-left">
					<div class="styleshop-logo">
						<?php styleshop_logo(); ?>
					</div>
				</div>
				<!-- Primary navbar -->
				<?php if ( has_nav_menu('primary_menu') ) { ?>
				<div id="main-menu" class="main-menu pull-left">
					<nav id="primary-menu" class="primary-menu">
						<div class="mid-header clearfix">
							<div class="navbar-inner navbar-inverse">
								<?php
								$styleshop_menu_class = 'nav nav-pills';
								if ( 'mega' == styleshop_options()->getCpanelValue('menu_type') ){
									$styleshop_menu_class .= ' nav-mega';
								} else $styleshop_menu_class .= ' nav-css';
								?>
								<?php wp_nav_menu(array('theme_location' => 'primary_menu', 'menu_class' => $styleshop_menu_class)); ?>
							</div>
						</div>
					</nav>
				</div>			
				<?php } ?>
				<!-- /Primary navbar -->
				<?php if (is_active_sidebar('bottom-header3')) {?>
					<div class="pull-right bottom-header3">
							<?php dynamic_sidebar('bottom-header3'); ?>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
</header>