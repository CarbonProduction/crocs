<?php if ( is_active_sidebar('left') ):
	$styleshop_left_span_class = 'col-lg-'.styleshop_options()->getCpanelValue('sidebar_left_expand');
	$styleshop_left_span_class .= ' col-md-'.styleshop_options()->getCpanelValue('sidebar_left_expand_md');
	$styleshop_left_span_class .= ' col-sm-'.styleshop_options()->getCpanelValue('sidebar_left_expand_sm');
?>
<aside id="left" class="sidebar <?php echo esc_attr($styleshop_left_span_class); ?>">
	<?php dynamic_sidebar('left'); ?>
</aside>
<?php endif; ?>