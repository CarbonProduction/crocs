<?php if ( is_active_sidebar('right') ):
	$styleshop_right_span_class = 'col-lg-'.styleshop_options()->getCpanelValue('sidebar_right_expand');
	$styleshop_right_span_class .= ' col-md-'.styleshop_options()->getCpanelValue('sidebar_right_expand_md');
	$styleshop_right_span_class .= ' col-sm-'.styleshop_options()->getCpanelValue('sidebar_right_expand_sm');
?>
<aside id="right" class="sidebar <?php echo esc_attr($styleshop_right_span_class); ?>">
	<?php dynamic_sidebar('right'); ?>
</aside>
<?php endif; ?>