<?php 
if (!isset($instance['category'])){
	$instance['category'] = 0;
}
extract($instance);

$default = array(
	'category' => $category,
	'orderby' => $orderby,
	'order' => $order,
	'include' => $include,
	'exclude' => $exclude,
	'post_status' => 'publish',
	'numberposts' => $numberposts
);

$list = get_posts($default);
if (count($list)>0){
?>
<div class="widget-latest-blog">
	<div class="blog-content">
		<?php foreach ($list as $post){?>
		<div class="blog-item">
			<div class="latest-blog-inner clearfix">
				<?php if (get_the_post_thumbnail( $post->ID )) {?>
				<div class="widget-thumb pull-left">
					<a href="<?php echo get_permalink($post->ID)?>"><?php echo get_the_post_thumbnail($post->ID, 'thumbnail');?></a>
				</div>
				<?php } ?>
				<div class="widget-content-wrap">
		 			<div class="widget-content">
		 				<div class="item-title">
		 					<h4><a href="<?php echo get_permalink($post->ID)?>"><?php echo $post->post_title;?></a></h4>
		 				</div>
		 				<div class="entry-meta clearfix">
							<div class="meta-entry entry-date pull-left">
								<a href="<?php echo get_permalink($post->ID)?>"><?php echo get_the_date( '', $post->ID );?></a>
							</div>
							<span class="meta-entry entry-comment pull-left">
								<a href="<?php comments_link(); ?>"><?php echo $post-> comment_count .  ( ($post-> comment_count) > 1 ? esc_html__('  Comments', 'styleshop') : esc_html__('  Comment', 'styleshop')); ?></a>
							</span>
						</div>
		 			</div>
				</div>
			</div>
		</div>
		<?php }?>
	</div>
</div>
<?php }?>