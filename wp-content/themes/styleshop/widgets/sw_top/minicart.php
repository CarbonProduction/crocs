<?php 
	do_action( 'before' ); 
?>
<?php if ( class_exists( 'WooCommerce' ) && !styleshop_options()->getCpanelValue( 'disable_cart' ) ) { ?>
<?php
	$styleshop_page_header = ( get_post_meta( get_the_ID(), 'page_header_style', true ) != '' ) ? get_post_meta( get_the_ID(), 'page_header_style', true ) : styleshop_options()->getCpanelValue('header_style');
	if($styleshop_page_header == 'style3' || $styleshop_page_header == 'style4'){
		get_template_part( 'woocommerce/minicart-ajax-style2' ); 
	}elseif($styleshop_page_header == 'style6'){
		get_template_part( 'woocommerce/minicart-ajax-style2' ); 
	}else{
		get_template_part( 'woocommerce/minicart-ajax' ); 
	}
	
?>
<?php } ?>