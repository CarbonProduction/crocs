<?php if( !styleshop_options()->getCpanelValue( 'disable_search' ) ) : ?>
<h3><span>search</span></h3>	
<div class="top-form top-search">
	<div class="topsearch-entry">
		<?php get_template_part('templates/searchform'); ?>
	</div>
</div>
<?php endif; ?>