<?php 
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
 
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<?php get_template_part('header'); ?>


<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
	<div class="styleshop_breadcrumbs">
		<div class="container">
			<div class="listing-title">			
				<h1><span><?php styleshop_title(); ?></span></h1>				
			</div>
			<?php
				if (!is_front_page() ) {
					if (function_exists('styleshop_breadcrumb')){
						styleshop_breadcrumb('<div class="breadcrumbs custom-font theme-clearfix">', '</div>');
					} 
				} 
			?>
		</div>
	</div>
<?php endif; ?>
<div class="container">
	<div class="row">
	<div id="contents" <?php styleshop_content_product(); ?> role="main">
		<?php
			/**
			 * woocommerce_before_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 */
			 global $post;
			do_action('woocommerce_before_main_content');
		?>
		<?php if (is_active_sidebar('banner-cat') && styleshop_sidebar_product() != 'full') { ?>
			<div class="banner-category theme-clearfix">
					<?php dynamic_sidebar('banner-cat'); ?>
			</div>	
		<?php }elseif(is_active_sidebar('banner-cat-full') && styleshop_sidebar_product() == 'full'){ ?>
			<div class="banner-category theme-clearfix">
					<?php dynamic_sidebar('banner-cat-full'); ?>
			</div>	
		<?php } ?>
		<!--  Shop Title -->
		<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
		
		<!-- Description --> 
		<?php do_action( 'woocommerce_archive_description' ); ?>
		<div class="products-wrapper">	
					
			<?php if ( have_posts() ) : ?>
					<?php do_action('woocommerce_message'); ?>
				<?php
					/**
					 * woocommerce_before_shop_loop hook
					 *
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );
				?>
				<div class="clear"></div>
				<?php woocommerce_product_loop_start(); ?>

					<?php woocommerce_product_subcategories(); ?>
					<?php while ( have_posts() ) : the_post(); ?>
		
					<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

				<?php woocommerce_product_loop_end(); ?>
				<div class="clear"></div>			
				<?php
					/**
					 * woocommerce_after_shop_loop hook
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				?>
			<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

				<?php wc_get_template( 'loop/no-products-found.php' ); ?>

			<?php endif; ?>
		</div>
		<?php
			/**
			 * woocommerce_after_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action('woocommerce_after_main_content');
		?>
	</div>
<?php 
	
	if ( is_active_sidebar('left-product') && styleshop_sidebar_product() != 'right' && styleshop_sidebar_product() != 'full' ):
		$styleshop_left_span_class = 'col-lg-'.styleshop_options()->getCpanelValue('sidebar_left_expand');
		$styleshop_left_span_class .= ' col-md-'.styleshop_options()->getCpanelValue('sidebar_left_expand_md');
		$styleshop_left_span_class .= ' col-sm-'.styleshop_options()->getCpanelValue('sidebar_left_expand_sm');
	?>
	<aside id="left" class="sidebar <?php echo esc_attr($styleshop_left_span_class); ?>">
		<?php dynamic_sidebar('left-product'); ?>
	</aside>
	
	<?php endif; ?>
	<?php if ( is_active_sidebar('right-product') && styleshop_sidebar_product() != 'left' && styleshop_sidebar_product() != 'full' ):
		$styleshop_right_span_class = 'col-lg-'.styleshop_options()->getCpanelValue('sidebar_right_expand');
		$styleshop_right_span_class .= ' col-md-'.styleshop_options()->getCpanelValue('sidebar_right_expand_md');
		$styleshop_right_span_class .= ' col-sm-'.styleshop_options()->getCpanelValue('sidebar_right_expand_sm');
	?>
	<aside id="right" class="sidebar <?php echo esc_attr($styleshop_right_span_class); ?>">
		<?php dynamic_sidebar('right-product'); ?>
	</aside>
	<?php endif; ?>

	</div>
</div>
<?php get_template_part('footer'); ?>
